<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\BookClass;
use App\Penalty;

class NotifyAbsentCutOff extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'queue:cutoff';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sweeps the database and marks all booked class yesterday as penalty 3, checks every 8:00 am every day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment("Sweep Start..");
        $comment = $this->notifyAbsentCutOff();
        $this->comment($comment);
    }

    public function notifyAbsentCutOff() {
        $now = new \DateTime("now", new \DateTimeZone("Asia/Manila"));
        $yesterday = $now->sub(new \DateInterval("P1D"));
        $yes_start = $yesterday->format("Y-m-d 00:00:00");
        $yes_end = $yesterday->format("Y-m-d 23:59:00");
        $classes = BookClass::where("start_at",">=", $yes_start)->where("end_at","<=", $yes_end)->where("status","booked")->get();
        foreach($classes as $class){
            $class->status = "penalty 3";
            $class->actor = "system";
            $class->class_fee = null;
            $penalty = Penalty::where("name", $class->status)
                        ->where("actor",$class->actor)
                        ->first();
            $class->penalty_fee = floatval($penalty->value_in_php);
            $class->save();
        }

        return "Done sweeping database..";
    }
}
