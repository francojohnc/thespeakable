<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
//use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;


class BackupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup:now';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup your database using sql query instead of mysql dump';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dateToday = new \DateTime("now", new \DateTimeZone("UTC"));
        $date = $dateToday->format("Y-m-d-H-i-s");
        $tables = [
            "applications",
            "categories",
            "data_rows",
            "data_types",
            "employees",
            "menus",
            "menu_items",
            "migrations",
            "pages",
            "password_resets",
            "permissions",
            "permission_role",
            "posts",
            "roles",
            "settings",
            "translations",
            "user_roles",
            "student_account_types",
            "teacher_account_types",
            "schools",
            "users",
            "availabilities",
            "book_classes",
            "jobs",
            "orders",
            "packages",
            "payouts",
            "payout_batches",
            "penalties",
            "referrals",
            "school_packages",
            "enrollment_incentives",
        ];


        /*

        // WORKING COMPRESS uses Zip Archive
        // uncomment this line if tar and gzip is not working...
        $zip_name = $date.".zip";
        
        Storage::makeDirectory("backup");

        $zip = new \ZipArchive();
        $res = $zip->open("storage/app/backup/".$zip_name, \ZipArchive::CREATE);

        foreach($models as $model) {
            $data = DB::table($model)->selectRaw("*")->get();
            $gzdata = gzencode($data, 9);
            $zip->addFromString($model.".gz", $gzdata);
            //$zip->setCompressionName($model.".gz", \ZipArchive::CM_DEFLATE);
            //Storage::put("backup/".$gzdata".json", $gzdata);
        }
        $zip->close();
        $this->info("done");

        */

        /*
            Create files first to prevent memory leaks...
        */
        $path = "storage/app/backup";
        $zip_name = $date.".tar";
        $zip = new \PharData($path."/".$zip_name);

        foreach($tables as $table) {
            $data = DB::table($table)->selectRaw("*")->get();
            $zip->addFromString($table.".json", $data);
        }
        $zip->compress(\Phar::GZ);
        $res2 = Storage::delete($path."/".$zip_name);
        $res = File::Delete($path."/".$zip_name);
        $this->info("done ".$res. " ".$res2);
    }
}
