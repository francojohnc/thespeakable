<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Http\Request;

use App\SchoolPackage;
use App\Availability;
use App\BookClass;
use App\Package;
use App\Order;
use App\User;
use App\Referral;
use App\Penalty;
use App\TeacherAccountType;
use App\StudentAccountType;
use App\EnrollmentIncentive;
use App\Country;
use App\CityMunicipality;
use App\Voucher;
use App\Http\Controllers\DashboardController;




class ChartController extends Controller
{
    public function __construct()
    {
        if(!auth()->user()->hasRole('admin')){
            return "opps";
        }
    }
    public static function chartBooked()
    {
        $start =DashboardController::cutOffStart();
        $end = DashboardController::cutOffEnd();
        $data = BookClass::selectRaw("date(start_at) as date,
                time(start_at) as time,
                teacher.nick as teacher,
                student.nick as student")
            ->join("users as teacher","teacher.id","teacher_id")
            ->join("users as student","student.id","student_id")
            ->whereBetween('start_at', [$start, $end])
            ->where('status', 'booked')
            ->orderBy('date','ASC')
            ->orderBy('time', 'ASC')
            ->get();

        return $data;
    }

    public static function chartBookedSeries($data)
    {

        $processDates = $data->groupBy('date');
        $level = [];

        foreach($processDates as $currentDate => $currentDateValue)
        {
            $level1["name"] = $currentDate;
            $level1["colorByPoint"] = true;
            $level1["y"] = $currentDateValue->count();
            $level1["drilldown"] = $currentDate;
            $level[] = $level1;
        }
        return json_encode($level);
    }

    public static function chartBookedDrillTime($data)
    {
        $t = [];
        $kiss = $data->groupBy("date")->map(function($item, $key) use (&$t) {
            $m["name"] = "Booked classes for date: ".$key;
            $m["id"] = $key;
            $m["colorByPoint"] = true;

            $m["data"] = $item->groupBy("time")->map(function($timeItem, $timeKey) use ($key, &$t) {
                $n["name"] = $timeKey;
                $n["y"] = $timeItem->count();
                $n["drilldown"] = $key."-".$timeKey;
                $da["id"] = $key."-".$timeKey;
                $da["colorByPoint"] = true;
                foreach($timeItem as $key2 => $person)
                {
                    $da["data"][] = ["name"=>"Teacher ".$person->teacher.": Student ".$person->student,"y"=>1];
                }
                $t[] = $da;
                return $n;
            })->values();
            return $m;
        })->values();

        foreach($t as $val)
        {
            $kiss->push($val);
        }
        return $kiss;
    }


    public static function chartAvailable()
    {
        $start =DashboardController::cutOffStart();
        $end = DashboardController::cutOffEnd();
        $data = Availability::selectRaw("date(start_at) as date,
                time(start_at) as time,
                teacher.nick as teacher")
            ->join("users as teacher","teacher.id","teacher_id")
            ->whereBetween('start_at', [$start, $end])
            ->where('status', 'open')
            ->orderBy('date','ASC')
            ->orderBy('time', 'ASC')
            ->get();

        return $data;
    }


    public static function chartAvailableSeries($data)
    {

        $processDates = $data->groupBy('date');
        $level = [];

        foreach($processDates as $currentDate => $currentDateValue)
        {
            $level1["name"] = $currentDate;
            $level1["colorByPoint"] = true;
            $level1["y"] = $currentDateValue->count();
            $level1["drilldown"] = $currentDate;
            $level[] = $level1;
        }
        return json_encode($level);
    }

    public static function chartAvailableDrillTime($data)
    {
        $t = [];
        $kiss = $data->groupBy("date")->map(function($item, $key) use (&$t) {
            $m["name"] = "Open slots for date: ".$key;
            $m["id"] = $key;
            $m["colorByPoint"] = true;

            $m["data"] = $item->groupBy("time")->map(function($timeItem, $timeKey) use ($key, &$t) {
                $n["name"] = $timeKey;
                $n["y"] = $timeItem->count();
                $n["drilldown"] = $key."-".$timeKey;
                $da["id"] = $key."-".$timeKey;
                foreach($timeItem as $key2 => $person)
                {
                    $da["data"][] = ["name"=>$person->teacher,"y"=>1];
                }
                $t[] = $da;
                return $n;
            })->values();
            return $m;
        })->values();

        foreach($t as $val)
        {
            $kiss->push($val);
        }
        return $kiss;
    }

}
