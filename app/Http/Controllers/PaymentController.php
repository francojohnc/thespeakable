<?php
namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\QueryException;
use App\Order as Order;


/** All Paypal Details class **/
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

use Redirect;
use Session;
use URL;

class PaymentController extends Controller
{
    private $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);

    }
    
    public function index()
    {
        return view('paywithpaypal');
    }

    
    public function payWithpaypal(Request $request)
    {
        $data = json_decode(json_encode($request->post()));
        $payables = json_decode($data->payables);
        
        $payer  = new Payer();
        $payer->setPaymentMethod("paypal");
        $items = new ItemList();
        $order_numbers = [];
        $conversion_rate = 0.0000;
        $payItems = [];
        $currency = "USD";
        $total_in_usd = 0.00; 
        foreach($payables->items as $item) {
            $unit_price = $item->price;
            $payItem = new Item();
            $payItem->setName($item->id)
              ->setCurrency("USD")
              ->setQuantity(1)
              ->setPrice($unit_price);
            $payItems[] = $payItem;
            $total_in_usd += $unit_price;
            $order_numbers[] = $item->id;
        }

        Session::put('order_numbers', $order_numbers);
        
        $items->setItems($payItems);

        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal($total_in_usd);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($items)
            ->setDescription('Speakable Payment: '.$currency." ".$payables->total);


        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::to('status')) // Specify return URL
            ->setCancelUrl(URL::to('status'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));


        //return $payment;
        //dd($payment->create($this->_api_context));

        //return $payment;
        //dd($payment->create($this->_api_context));
        //exit;
        
        try {

            $payment->create($this->_api_context);

        } catch (\PayPal\Exception\PPConnectionException $ex) {

            if (\Config::get('app.debug')) {

                \Session::put('error', 'Connection timeout');
                return Redirect::to('/orders');

            } else {

                \Session::put('error', 'Some error occur, sorry for inconvenient');
                return Redirect::to('/orders');

            }

        }


        //return $payment;
        foreach ($payment->getLinks() as $link) {

            if ($link->getRel() == 'approval_url') {

                $redirect_url = $link->getHref();
                break;

            }

        }

        // add payment ID to session
        Session::put('paypal_payment_id', $payment->getId());

        if (isset($redirect_url)) {

            // redirect to paypal
            return Redirect::away($redirect_url);

        }

        \Session::put('error', 'Unknown error occurred');
        return Redirect::to('/orders');
    
    }
    

    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {

            \Session::put('error', 'Payment failed');
            return Redirect::to('/orders');

        }

        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));

        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        if ($result->getState() == 'approved') {

            \Session::put('success', 'Payment success');
            $order_numbers = Session::get('order_numbers');

            foreach($order_numbers as $id) {
                $data = Order::where('order_number',$id)->first();
                $data->transaction_id = $payment_id;
                $data->status = "paid";
                if(Session::get('conversion_rate') != null){
                    $data->conversion_rate = Session::get('conversion_rate');
                    $data->converted_from = "USD";
                }
                $data->save();
            }
            Session::forget('conversion_rate');
            Session::forget('order_numbers');

            return Redirect::to('/orders');

        }

        \Session::put('error', 'Payment failed');
        return Redirect::to('/orders');

    }

}
