<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Http\Request;
use App\SchoolPackage;
use App\Availability;
use App\BookClass;
use App\Package;
use App\Order;
use App\User;
use App\Referral;
use App\Penalty;
use App\TeacherAccountType;
use App\StudentAccountType;
use App\EnrollmentIncentive;
use App\Country;
use App\CityMunicipality;
use App\Voucher;


use App\Mail\BookClassStudentZhCn;
use App\Mail\TeacherCancelledClassZhCn;
use App\Mail\BookClassStudent;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;




use App\Jobs\SendEmailJob;
use App\Job;

use Illuminate\Contracts\Bus\Dispatcher;



class ApiController extends Controller
{
    public function routeUserMiddleware(Request $request){
         return $request->user();
    }

    public function readPackage(Request $request){
    	$list = Package::where("id", $request->id)->firstOrFail();
    	return $list;
    }

    public function readSchoolPackage(Request $request){
    	$list = SchoolPackage::where("id", $request->id)->firstOrFail();
    	return $list;
    }

    public function searchAvailableTeacher(Request $request) {
        // always add 30 minutes to date and time
        $newDateTime = new \DateTime($request->dateandtime, new \DateTimeZone(auth()->user()->timezone));

        $today = new \DateTime("now", new \DateTimeZone( auth()->user()->timezone ) );
        $today->add(new \DateInterval("PT30M"));

        $datetime = explode(" ",$newDateTime->format("Y-m-d 00:00:00"));
        

        // we always receive date and no time so we need to fill time to current time if it is today
        if( $today->format("d") == $newDateTime->format("d") ) {
            $datetime = explode(" ",$today->format("Y-m-d H:i:s"));
        }

        //return implode($datetime," ");

    	$list = DB::table('users')
            ->join('availabilities', 'users.id', '=', 'availabilities.teacher_id')
            ->groupBy('users.id','nick','avatar','users.timezone')
            ->select('users.id','nick', 'avatar','users.timezone')
            ->where("start_at",">=", implode($datetime," ") )
            ->where("start_at","<=", $datetime[0]." 23:59")
            ->where("status","open")
            ->get();

    	return $list;
    }

    public function teacherCancelPlot(Request $request){
        $teacher = User::find($request->id);
        $start = DashboardController::cutOffStart();
        $end = DashboardController::cutOffEnd();

        $availabilities = Availability::where("teacher_id",$teacher->id)
            ->where("status","open")
            ->where("start_at",">=", $start)
            ->where("start_at","<=", $end)
            ->get();
        $result = "Unsuccessful";
        if(count($availabilities) == 0) {
            $result = "No Opened Schedule";
        }
        
        foreach( $availabilities as $availability )
        {
            $availability->delete();
            $result = "sucessfully removed";
        }

        return $result;

    }

    public function teacherCancelAccess(Request $request){
        $teacher = User::find($request->id);
        $teacher->disabled = 1;
        $teacher->save();
        return "true";
    }

    public function teacherEnableAccess(Request $request){
        $teacher = User::find($request->id);
        $teacher->disabled = 0;
        $teacher->save();
        return "true";
    }


    public function availabilitiesDelete(Request $request){

        $data = json_decode(json_encode($request->post()));

        $availability = Availability::find($data->id);

        $result = "Unsuccessful";

        if($availability->status == "open") {

            $start = explode(" ", $availability->start_at);
            $date = explode("-",$start[0]);
            $time = explode(":",$start[1]);
            $peak1 = auth()->user()->peak1to15;
            $peak2 = auth()->user()->peak16to31;
            // determine if this is a peak hour 
            if( ($time[0] >= "18") && ($time[0] <= "21") ) {

                if($date[2] >= 1 && $date[2]<= 15) {
                    if($peak1 > 40 || auth()->user()->special_plotting_indefinite == true ) {
                        // lets delete it else do not
                        $availability->delete();
                        $peak1--;
                        $result = "Slot successfully removed";
                    }
                    else {
                        $result = "Cannot Delete, Peak hour slot should not be less than 40";
                    }
                }
                // all other dates goes here
                else{             
                    if($peak2 > 40 || auth()->user()->special_plotting_indefinite == true ) {
                        // lets delete it else do not
                        $availability->delete();
                        $peak2--;
                        $result = "Slot successfully removed";
                        return $result;
                    }
                    else{
                        $result = "Cannot Delete, Peak hour slot should not be less than 40";
                    }
                }
            }
            else{
                $availability->delete();
                $result = "Slot Successfully Removed";
                // if time is not peak time.. we can just delete everything
            }

            $user = User::find(auth()->user()->id);
            $user->peak1to15 = $peak1;
            $user->peak16to31 = $peak2;
            $user->save();

        }
        else {
            $result = "Cannot delete availability that is not open..";
        }

        return $result;

    }

    public function browseAvailabilities(Request $request){
    	return "Louie";
    }

    public function bookClass(Request $request) {

        if($request->ajax()){
            $data = json_decode(json_encode($request->post()));
            $total_booked = count($data->classes);
            //return json_encode($data);
            $order = "";
            $remaining = 0;
            if($data->order_id == "0"){
                $order = User::find($data->student_id);
                $remaining = $order->immortal;
            }
            elseif($data->order_id == "free"){
                $order = User::find($data->student_id);
                $remaining = $order->free_trial_consumable;
            }
            elseif($data->order_id[0]=="r"){
                $ref = explode("-",$data->order_id);
                $order = Referral::find($ref[1]);
                $remaining = $order->referral_consumable;
            }
            else{
                $order = Order::find($data->order_id);                
                $remaining = $order->remaining_reservation;
            }


            // if the remaining class on order is greater means, it can still fit all of the booked classes so we use the total booked class
            //else we use the remaining classes only so that it will discard the other classes
            $schedule = [];

            $teacher_data = User::where('id',$data->teacher_id)->first(['teacher_account_type_id','nick','email',"lang"]);
            $student_data = User::where('id',$data->student_id)->first(["nick","email","lang"]);
            $account_type = TeacherAccountType::where("id", $teacher_data->teacher_account_type_id)->first();

            foreach($data->classes as $class) {
                if($remaining > 0) {

                    try{
    
                        // check if this teacher has had a booked, penalty 1, penalty 2, penalty 3
                        $result = BookClass::where("status", "<>", "cancelled")->where("start_at", $class->start)->where("teacher_id", $data->teacher_id)->count();

                        if($result == 0) {

                            $plot = new BookClass();
                            $plot->start_at = $class->start;
                            $plot->end_at = $class->end;
                            $plot->status = $class->status;
                            $plot->availability_id = $class->id;
                            $plot->teacher_id = $data->teacher_id;
                            $plot->teacher_timezone = $data->teacher_timezone;
                            $plot->class_fee = $account_type->rate;
                            if($data->order_id =="free") {
                                $plot->session = "free-trial";
                                $plot->class_fee = null;
                            }
                            $plot->order_id = $data->order_id;
                            $schedule [] = $class->start;
                            $plot->student_id = $data->student_id;
                            $plot->student_timezone = $data->student_timezone;

                            $success = $plot->save();

                            if($success == true) {

                                // also update teacher availability
                                $availability = Availability::find($class->id);
                                $availability->status = "booked";
                                if($data->order_id =="free") {
                                    $availability->session = "free-trial";
                                }
                                $availability->save();
                                // subtract from App\Orders remaining Classes
                                $remaining--;

                                // Notify both Teacher and student on the 2 hrs before the indicated class start
                            
                                // send an email before the class starts to students and teachers first
                                $teacher = new \stdClass();
                                $teacher->nick = $teacher_data->nick;
                                
                                $student = new \stdClass();
                                $student->nick = $student_data->nick;


                                $class_start = new \DateTime($class->start);
                                $class_end = new \DateTime($class->end);
                                $class_id = $class_start->format("d-M-y-H:i")."-". $class_end->format("H:i") ."-".$class->id;

                                $body = new \stdClass();
                                $body->teacher_data = $teacher;
                                $body->student_data = $student;
                                $body->schedule = $class->start;
                                $body->class_id = $class_id;

                                // send email to teacher
                                $now =  new \DateTime($class->start, new \DateTimeZone(auth()->user()->timezone));
                                $now->sub(new \DateInterval("PT2H"));
                                $now->setTimeZone( new \DateTimeZone("UTC") );

                                // $emailJob = (new SendEmailJob($teacher_data->email, "book_class_teacher", $body))->delay($now);

                                // $id  = app(Dispatcher::class)->dispatch($emailJob);

                                // if($student_data->lang == "zh-cn"){
                                //     // send email to students
                                //     $emailJob = (new SendEmailJob($student_data->email, "book_class_student_zh_cn", $body))->delay($now);
                                // }else{
                                //     // send email to students
                                //     $emailJob = (new SendEmailJob($student_data->email, "book_class_student", $body))->delay($now);
                                // }
                                

                                // $id2  = app(Dispatcher::class)->dispatch($emailJob);

                                // $job_ids = new \stdClass();
                                // $job_ids->id = $id;
                                // $job_ids->id2 = $id2;

                                // $job_id = json_encode($job_ids);
                                // $plot->job_ids = $job_id;
                                $plot->save();
                            }

                        } // send email

                    }catch(QueryException $qe) {
                        //$errors $qe;
                        // do not echo anything and just continue
                        continue;
                    }

                }// if remaining is greater than 0
                

            }// end for


            // save the data before returning
            if($data->order_id == "0") {
                $order->immortal = $remaining;
            }
            elseif($data->order_id == "free") {
                $order->free_trial_consumable = $remaining;
            }
            elseif($data->order_id[0] == "r") {
                $order->referral_consumable = $remaining;
            }
            else{
                $order->remaining_reservation = $remaining;
            }

            $success = $order->save();
            $result = "Some Error Occurred";
            if($success == true ){
                $result = "true";
            }

            // to save bandwidth; we only send 1 immediate email when you book a class to the teacher and the student

            // send to teacher the date and time of all the class booked by the student
            //return new BookClassMail($teacher_data,$student_data,$schedule);
            $now =  new \DateTime(null, new \DateTimeZone("UTC"));
            //$now->add(new \DateInterval("PT1H"));
            $body = new \stdClass();
            $body->teacher_data = $teacher_data;
            $body->student_data = $student_data;
            $body->schedule = $schedule;
            // $emailJob = (new SendEmailJob($teacher_data->email, "book_class", $body))->delay($now);
            // dispatch($emailJob);


            return $result;
        }
    }

    public function availabilitiesBrowse(Request $request){
        // TODO:
        // get all booked classes from user
        // show only 
        $today = new \DateTime("now", new \DateTimeZone( auth()->user()->timezone ));
        $today->add(new \DateInterval("PT30M"));

        $user = BookClass::select('start_at')
                ->where("start_at", ">=", $today->format("Y-m-d 00:00:00"))
                ->where('student_id', auth()->user()->id)
                ->where("status","booked")
                ->get();

        $list = Availability::whereNotIn('start_at', $user)
            ->where("start_at",">=", $today->format("Y-m-d H:i:s"))
            ->where('status','open')
            ->where('teacher_id', $request->id)
            ->get();
        
        $json = [];

        foreach($list as $item){
            $data = [];
            $data["title"] = $item->status;
            $data["status"] = $item->status;
            $data["start"] = date("c",strtotime($item->start_at));
            $data["end"] = $item->end_at;
            $data["id"] = $item->id;
            $now = new \DateTime($data["start"]);
            $end = $now->format("Y-m-d 23:59:00");
            $start = $now->format("Y-m-d 00:00:00");
            // check if this user has 2 booked class that has a free-trial session
            $count = Availability::where("session","free-trial")
                ->where("start_at",">", $start)
                ->where("end_at","<", $end)
                ->count();
            $data["free_trial_count"] = $count;
            $json[] = $data;
        }

        return json_encode($json);
    }


    public function bookClassBrowseStudent(Request $request){
        // TODO:
        // get data only from this year -> consult to Boss Christian Tantengco
        $list = BookClass::where("student_id", $request->id)->whereIn("status",["booked","completed","penalty 3","lesson memo delay"])->where("teacher_absent","no")->get();


        $json = [];
        foreach($list as $item) {
            if($item->teacher_reason_for_absence == null) {        
                $teacher = User::where("id", $item->teacher_id)->first(["email","nick"]);
                $data = [];
                $data["title"] = "Teacher ".$teacher->nick;
                $data["teacher_id"] = $item->teacher_id;
                $data["status"] = $item->status;
                $data["start"] = date("c",strtotime($item->start_at));
                $data["end"] = $item->end_at;
                $data["id"] = $item->id;
                if($data["status"] == "open"){
                    $data["color"] = "#019875";
                    $data["textColor"] = "white";
                }
                else if($data["status"] == "booked") {
                    $data["color"] = "blue";
                }
                else if($data["status"] == "cancelled") {
                    $data["color"] = "red";
                }
                else if($data["status"] == "penalty 1") {
                    $data["color"] = "#01730d";
                }
                else if($data["status"] == "penalty 2") {
                    $data["color"] = "#01730d";
                }
                else if($data["status"] == "penalty 3") {
                    $data["color"] = "#01730d";
                }
                else if($data["status"] == "completed") {
                    $data["color"] = "#01730d";
                }
                else if($data["status"] == "teacher is absent") {
                    $data["color"] = "red"; //violet
                }
                else if($data["status"] == "student is absent") {
                    $data["color"] = "red"; //violet
                }
                $json[] = $data;
            }
        }
        //2018-04-26T04:21:13.942Z
        //Y-M-dTH:i:s.
        return json_encode($json);
    }


    public function bookClassBrowseTeacher(Request $request){
        // TODO:
        // get data only from this year -> consult to Boss Christian Tantengco
        $list = BookClass::where("teacher_id", $request->id)->where("status","<>","cancelled")->get();
        $json = [];
        //return $list;
        foreach($list as $item) {
            
            $student = User::where("id", $item->student_id)->first(["nick"]);
            //return $student;
            $data = [];
            $data["title"] = $student->nick." - ".$item->status;
            $data["student_id"] = $item->student_id;
            $data["status"] = $item->status;
            $data["start"] = date("c",strtotime($item->start_at));
            $data["end"] = $item->end_at;
            $data["id"] = $item->id;


            if($data["status"] == "open"){
                $data["color"] = "#019875";
                $data["textColor"] = "white";
            }
            else if($data["status"] == "booked") {
                $data["color"] = "blue";
            }
            else if($data["status"] == "cancelled") {
                $data["color"] = "red";
            }
            else if($data["status"] == "penalty 1") {
                $data["color"] = "red";
            }
            else if($data["status"] == "penalty 2") {
                $data["color"] = "red";
            }
            else if($data["status"] == "penalty 3") {
                $data["color"] = "red";
            }
            else if($data["status"] == "completed") {
                $data["color"] = "#01730d";
            }
            else if($data["status"] == "student is absent") {
                $data["color"] = "#8f3fff"; //violet
            }

            $json[] = $data;
        }
        //2018-04-26T04:21:13.942Z
        //Y-M-dTH:i:s.
        return json_encode($json);
    }

    public function bookClassShowStudent(Request $request){
        // TODO:
        // get data only from this year -> consult to Boss Christian Tantengco
        $list = BookClass::find($request->id);
        $json = [];
        foreach($list as $item) {
            $teacher = User::where("id", $item->teacher_id)->first(["nick"]);
            $data = [];
            $data["title"] = $teacher->nick." - ".$item->status;
            $data["teacher_id"] = $item->teacher_id;
            $data["status"] = $item->status;
            $data["start"] = date("c",strtotime($item->start_at));
            $data["end"] = $item->end_at;
            $data["id"] = $item->id;
            if($data["status"] == "open"){
                $data["color"] = "#019875";
                $data["textColor"] = "white";
            }
            else if($data["status"] == "booked") {
                $data["color"] = "blue";
            }
            else if($data["status"] == "cancelled") {
                $data["color"] = "red";
            }
            $json[] = $data;
        }
        
        //2018-04-26T04:21:13.942Z
        //Y-M-dTH:i:s.
        return json_encode($list);
    }

    public function bookClassShow(Request $request){
        //return BookClass::where("id",$request->id)->get();
        $book = "";
        if( auth()->user()->hasRole("student") ) {
            $book = BookClass::where("id",$request->id)->first(["start_at", "end_at", "status", "book_url", "message_to_teacher", "grammar", "pronunciation", "areas_for_improvement", "tips_and_suggestion_for_student", "class_remarks", "book_name", "lesson_unit", "page_number", "teacher_absent", "teacher_reason_for_absence", "teacher_reason_screenshot", "teacher_id", "student_id", "teacher_opened_memo","id"]);


        }else{
            $book = BookClass::where("id",$request->id)->first(["start_at", "end_at", "status", "message_to_teacher", "book_url", "book_name", "lesson_unit", "page_number",  "speed_test_1", "speed_test_2", "grammar", "pronunciation", "areas_for_improvement", "tips_and_suggestion_for_student", "class_remarks", "screenshot", "student_absent", "student_reason_for_absence", "student_reason_screenshot", "teacher_id", "student_id", "teacher_opened_memo", "id"]);
            $last30 = BookClass::whereIn("status",["completed","lesson memo delay"])->where("start_at", "<", $book->start_at)->orderBy("created_at","desc")->limit(30)->get();
            $book->last30 = $last30;


            

        }


        $previous_book = BookClass::whereIn("status",["completed","lesson memo delay"])->whereRaw("book_url is not null")->latest()->first(["book_url"]);
        if($previous_book != null){
            $previous = json_decode($previous_book->book_url);

            $book->previous_book = "<a target='_new' href='/storage/".$previous[0]->download_link."'>".$previous[0]->original_name."</a>";
        }else{
            $book->previous_book = "None";
        }


        $teacher = User::where("id", $book->teacher_id)->first(["qq","wechat","nick","created_at","id"]);
        $book->teacher_nick = $teacher->nick;        
        $book->teacher_qq = $teacher->qq;
        $book->teacher_wechat = $teacher->wechat;
        $teacher_id = new \DateTime($teacher->created_at, new \DateTimeZone("UTC"));
        $book->teacher_id = $teacher_id->format("Ymd-").$teacher->id;

        $student = User::where("id", $book->student_id)->first(["qq","wechat","nick","created_at","id"]);
        $book->student_nick = $student->nick;
        $book->student_qq = $student->qq;
        $book->student_wechat = $student->wechat;
        $student_id = new \DateTime($teacher->created_at, new \DateTimeZone("UTC"));
        $book->student_id = $student_id->format("Ymd-").$student->id;

        return json_encode($book);
    }

    public function cancelBooking(Request $request) {
        $class = BookClass::where("id", $request->id)->first();
        $timezone = auth()->user()->timezone;
        $now = ApiController::currentDateTimeIn($timezone);
        $start = new \DateTime($class->start_at, new \DateTimeZone($timezone));
        $info = "Not Allowed to Cancel";
        if(auth()->user()->hasRole("student")) {
            $class->actor = "student";

            $start->sub(new \DateInterval("PT1H"));
            if( ($now->getTimestamp() < $start->getTimestamp()) && $class->status =="booked") {
                // delete the 
                $class->status = "cancelled";

                // return to teacher the availability
                $availability = Availability::find($class->availability_id);
                $availability->status = "open";
                // return the credit to student respective packages

                if($class->order_id == "0"){
                    $order = User::find($class->student_id);
                    $order->immortal = (intval($order->immortal) + 1);

                }
                elseif($class->order_id == "free") {
                    $order = User::find($class->student_id);
                    $order->free_trial_consumable = (intval($order->free_trial_consumable ) + 1);
                }
                elseif($class->order_id[0]=="r"){
                    $ref = explode("-",$class->order_id);
                    $order = Referral::find($ref[1]);                    
                    $order->referral_consumable = (intval($order->referral_consumable) + 1);
                }
                else{
                    $order = Order::find($class->order_id);         
                    $order->remaining_reservation = (intval($order->remaining_reservation) + 1);
                }

                $result1 = $availability->save(); // return availability
                $result2 = $order->save(); // return credit to student
                $result3 = $class->save();
                if( $result1 == true && $result2 == true && $result3 == true){
                    $info = "Class Successfully Cancelled";
                }
                // also remove the notification from the queue
                if($class->job_ids != null){
                    $job_ids = json_decode($class->job_ids);
                    $job = Job::find($job_ids->id);
                    if($job != null){ $job->delete(); }

                    $job = Job::find($job_ids->id2);
                    if($job != null){ $job->delete(); }
                }

                $teacher = User::where('id',$class->teacher_id)->first(['nick','email']);
                $student = User::where('id',$class->student_id)->first(['nick','email']);


                $class_start = new \DateTime($class->start_at);
                $class_end = new \DateTime($class->end_at);
                $class_id = $class_start->format("d-M-y-H:i")."-". $class_end->format("H:i") ."-".$class->id;

                // now lets send booking cancellation to teacher
                $now =  new \DateTime(null, new \DateTimeZone("UTC"));
                $now->add(new \DateInterval("PT1M"));
                $body = new \stdClass();
                $body->teacher_data = $teacher;
                $body->student_data = $student;
                $body->schedule = $class->start_at;
                $body->class_id = $class_id;
                $emailJob = (new SendEmailJob($teacher->email, "student_cancelled_class", $body))->delay($now);
                $id  = app(Dispatcher::class)->dispatch($emailJob);
                //$class->delete();

            }
        }
        
        else if(auth()->user()->hasRole("teacher")) {

            $class->actor = "teacher";
            $status = "penalty 3";
            $penalty = Penalty::where("name", $status)->where("actor",$class->actor)->first();
            $penalty_rate = $penalty->value_in_php;

            if($now->getTimestamp() <= $start->sub(new \DateInterval("PT2H"))->getTimestamp() ) {
                $status = "penalty 2";
                $penalty = Penalty::where("name", $status)->where("actor",$class->actor)->first();
                $penalty_rate = $penalty->value_in_php;
            }

            // now lets add 2 hrs again
            $start->add(new \DateInterval("PT2H"));

            if($now->getTimestamp() <= $start->sub(new \DateInterval("PT6H"))->getTimestamp() ){
                $status = "penalty 1";
                $penalty = Penalty::where("name", $status)->where("actor", $class->actor)->first();
                $penalty_rate = $penalty->value_in_php;
            }

            if( $status == "penalty 1" || $status == "penalty 2" || $status == "penalty 3") {
                $class->class_fee = null;

                //asdasd
            }

            //return $status;

            if($class->status =="booked") {
                // delete the 
                $class->status = $status;
                $class->penalty_fee = $penalty_rate;

                $availability = Availability::where("teacher_id", $class->teacher_id)->where('start_at', $class->start_at)->first();
                $availability->status = $status; // lets delete the availability to prevent from showing up on search results
                $result = $availability->save();
                $class->teacher_absent = "yes";
                $class->teacher_reason_for_absence = "notify absence";

                $order = "";
                // return the credit to student respective packages
                if($class->order_id == "free") {
                    $order = User::find($class->student_id);
                    $order->free_trial_consumable = (intval($order->free_trial_consumable ) + 1);
                }
                elseif($class->order_id[0]=="r"){
                    $ref = explode("-",$class->order_id);
                    $order = Referral::find($ref[1]);                    
                    $order->referral_consumable = (intval($order->referral_consumable) + 1);
                }
                else{
                    $order = User::find($class->student_id);
                    $order->immortal = (intval($order->immortal) + 1);
                }
                $result2 = $order->save();
                $result3 = $class->save();

                if($result2 == true && $result3 == true){
                    // also remove the notification from the queue
                    if($class->job_ids != null) {
                        $job_ids = json_decode($class->job_ids);
                        $job = Job::find($job_ids->id);
                        if($job != null){ $job->delete(); }

                        $job = Job::find($job_ids->id2);
                        if($job != null){ $job->delete(); }
                    }

                    // notify student and admin
                    $teacher = User::where('id', $class->teacher_id)->first(['nick','email']);
                    $student = User::where('id', $class->student_id)->first(['nick','email','lang']);

                    $class_start = new \DateTime($class->start_at);
                    $class_end = new \DateTime($class->end_at);
                    $class_id = $class_start->format("d-M-y-H:i")."-". $class_end->format("H:i") ."-".$class->id;

                    // now lets send booking cancellation to teacher
                    $now =  new \DateTime(null, new \DateTimeZone("UTC"));
                    $now->add(new \DateInterval("PT1M"));
                    $body = new \stdClass();
                    $body->teacher_data = $teacher;
                    $body->student_data = $student;
                    $body->schedule = $class->start_at;
                    $body->class_id = $class_id;
                    if($student->lang == "zh-cn") {
                        $emailJob = (new SendEmailJob($student->email, "teacher_cancelled_class_zh_cn", $body))->delay($now);
                    }else{
                        $emailJob = (new SendEmailJob($student->email, "teacher_cancelled_class", $body))->delay($now);
                    }
                    
                    $id  = app(Dispatcher::class)->dispatch($emailJob);

                    $info = "Successfully marked this class as penalty";
                }
                
            }
            // if its booked dont cancel it...
            // "Cancelling a booking will become penalty status";
        }

        return $info;
    }

    public function teacherCancelBooking(Request $request) {

        $teacher = User::find($request->id);
        $start = DashboardController::cutOffStart();
        $end = DashboardController::cutOffEnd();

        $classes = BookClass::where("teacher_id",$teacher->id)
            ->where("status","booked")
            ->where("book_classes.start_at",">=", $start)
            ->where("book_classes.start_at","<=", $end)
            ->get();
       
        $timezone = $teacher->timezone;
        $now = ApiController::currentDateTimeIn($timezone);
        $info = "Not Allowed to Cancel";

        foreach( $classes as $class ) {
            if(auth()->user()->hasRole("superadmin") || auth()->user()->hasRole('admin')) {
                
                $class->status = "cancelled";
                $class->penalty_fee = null;
                $class->class_fee = null;

                $class->teacher_absent = "yes";
                $class->teacher_reason_for_absence = "banned from the system";

                $order = "";
                // return the credit to student respective packages
                if($class->order_id == "free") {
                    $order = User::find($class->student_id);
                    $order->free_trial_consumable = (intval($order->free_trial_consumable ) + 1);
                }
                elseif($class->order_id[0]=="r") {
                    $ref = explode("-",$class->order_id);
                    $order = Referral::find($ref[1]);                    
                    $order->referral_consumable = (intval($order->referral_consumable) + 1);
                }
                else {
                    $order = User::find($class->student_id);
                    $order->immortal = (intval($order->immortal) + 1);
                }
                $result2 = $order->save();
                $result3 = $class->save();

                if($result2 == true && $result3 == true){
                    // also remove the notification from the queue
                    if($class->job_ids != null) {
                        $job_ids = json_decode($class->job_ids);
                        $job = Job::find($job_ids->id);
                        if($job != null){ $job->delete(); }

                        $job = Job::find($job_ids->id2);
                        if($job != null){ $job->delete(); }
                    }

                    // notify student and admin
                    $teacher = User::where('id',$class->teacher_id)->first(['nick','email']);
                    $student = User::where('id',$class->student_id)->first(['nick','email','lang']);

                    $class_start = new \DateTime($class->start_at);
                    $class_end = new \DateTime($class->end_at);
                    $class_id = $class_start->format("d-M-y-H:i")."-". $class_end->format("H:i") ."-".$class->id;

                    // now lets send booking cancellation to teacher
                    $now =  new \DateTime(null, new \DateTimeZone("UTC"));
                    $now->add(new \DateInterval("PT1M"));
                    $body = new \stdClass();
                    $body->teacher_data = $teacher;
                    $body->student_data = $student;
                    $body->schedule = $class->start_at;
                    $body->class_id = $class_id;
                    if($student->lang == "zh-cn") {
                        $emailJob = (new SendEmailJob($student->email, "teacher_cancelled_class_zh_cn", $body))->delay($now);
                    }else{
                        $emailJob = (new SendEmailJob($student->email, "teacher_cancelled_class", $body))->delay($now);
                    }
                    
                    $id  = app(Dispatcher::class)->dispatch($emailJob);

                    $info = "Successfully Cancelled Booked Classes";
                }
                    

            }
        } //end foreach

        return $info;
    }


    public function teacherOpenedMemo($id) {
        $booking = BookClass::where('id',$id)->first();
        $booking->teacher_opened_memo = "true";
        $result = $booking->save();
        if($result == "true"){
            return "success";
        }
        else {
            return "fail";
        }
    }


    public function currentDateTimeIn($timezone) {
        return new \DateTime(null, new \DateTimeZone($timezone));
    }

    public function getCurrentDate($timezone) {
        $area = explode("-",$timezone);
        $now =  new \DateTime(null, new \DateTimeZone($area[0]."/".$area[1]));
        return $now->format("Y-M-dTH:i:s");
    }

    public static function currentDateTimeInLocal($timezone) {
        return new \DateTime(null, new \DateTimeZone($timezone));
    }

    public static function currentPhpToCny($php) {
        // get conversion
        // return converted data
        return "";
    }

    public function approveOrder(Request $request) {


        $user_id = auth()->user()->id;
        $timezone = auth()->user()->timezone;
        $role_name = auth()->user()->role->name;
        $now = ApiController::currentDateTimeIn($timezone);
        $data = json_decode(json_encode($request->post()));
        $order = Order::find($data->id);
        $order->status = "active";
        $days = "P".$order->duration."D";
        $order->approved_at = $now;
        $effective = ApiController::currentDateTimeIn($timezone);
        $order->effective_until = $effective->add(new \DateInterval($days))->format("Y-m-d 23:59:00");
        $result = $order->save();


        // check incentive
        $user = User::find($order->student_id);

        if($user->student_account_type_id == 1) {
            $user->student_account_type_id = 2;
            $user->save();
            // check the incentives
            $count = EnrollmentIncentive::where('student_id', $order->student_id)->where('status','pending')->count();
            if($count > 0) {
                $incentive = EnrollmentIncentive::where('student_id', $order->student_id)->where('status','pending')->orderBy("created_at")->first();
                $incentive->status = "confirmed";
                $incentive->save();

                $class = BookClass::where("id", $incentive->class_id)->first();
                $class->total_fee_status = "completed";
                $class->incentive_total = (doubleval($class->incentive_total) + 100.00);
                $class->incentive_comment = $class->incentive_comment . " " . "Free trial +100";
                $class->save();
            }
        }


        if($result == true) {
            return "Order Approved Successfully!";
        }
        return "Cannot Approve Order, Please Contact Administrator!";

    }


    public function sendEmail() {


        $teacher_data = User::where('id',"7")->first(['teacher_account_type_id','nick','email',"lang"]);
        $student_data = User::where('id',"26")->first(['nick','email',"lang"]);


        $teacher = new \stdClass();
        $teacher->nick = $teacher_data->nick;

        $student = new \stdClass();
        $student->nick = $student_data->nick;


        $class_start = new \DateTime("now", new \DateTimeZone("Asia/Manila"));
        $class_start->add(new \DateInterval("PT1H"));
        $class_end = new \DateTime("now", new \DateTimeZone("Asia/Manila"));
        $class_end->add(new \DateInterval("PT1H"));
        $class_end->add(new \DateInterval("PT25M"));
        $class_id = $class_start->format("d-M-y-H:i")."-". $class_end->format("H:i") ."-"."10";

        $body = new \stdClass();
        $body->teacher_data = $teacher;
        $body->student_data = $student;
        $body->schedule = $class_start->format("Y-m-d H:i:s");
        $body->class_id = $class_id;

        // send email to teacher
        $now =  new \DateTime("now", new \DateTimeZone("UTC"));

        //$emailJob = (new SendEmailJob("forondalouie@gmail.com", "book_class_student_zh_cn", $body))->delay($now);

        Mail::to("forondalouie@gmail.com")->send(new TeacherCancelledClassZhCn($body));
        return "Email Sent?";
    }


    public function availabilityList(Request $request){
        // TODO:
        // get data only from this year -> consult to Boss Christian Tantengco
        $list = Availability::where("teacher_id", $request->id)->where("status","open")->get();
        $json = [];
        foreach($list as $item){
            $data = [];
            $data["title"] = $item->status;
            $data["status"] = $item->status;
            $data["start"] = date("c",strtotime($item->start_at));
            $data["end"] = $item->end_at;
            $data["id"] = $item->id;
            if($data["status"] == "open"){
                $data["color"] = "gray";
                $data["textColor"] = "white";
            }

            $json[] = $data;
        }
        //2018-04-26T04:21:13.942Z
        //Y-M-dTH:i:s.
        return json_encode($json);
    }




    public function tester(){
    }

    public function redeemReferral($referred_id){
        //check if its logged in
        if( isset( auth()->user()->id )  ){
            $referral = User::find($referred_id);
            $user = User::find($referral->referred_by_id);
            $user->immortal = ($user->immortal + 5);
            $user->save();
            $referral->referral_taken = "true";
            $referral->referral_approved_by_id = auth()->user()->id;
            $referral->save();
            return "User has been given additional 5 immortal";
        }
        else {
            return redirect("/");
            //return "Not Logged In";
        }
    }


    public static function classFeeTally() {
        $book = BookClass::where("teacher_id", auth()->user()->id)
                    ->where("total_fee_status","completed")
                    ->where("status","<>","cancelled")
                    ->where("status","<>","booked")
                    ->get(["id","class_fee","status"]);
        $total = 0;
        foreach($book as $fee) {
            $total += $fee->class_fee;
        }
        return number_format($total,2);
    }


    public static function classFeeTallyPossibleBooked() {
        $class = BookClass::where("teacher_id",auth()->user()->id)
                    ->where("status","booked")
                    ->get(["id","class_fee","status"]);
        $total = 0;
        foreach($class as $fee) {
            $total += $fee->class_fee;
        }
        return number_format($total,2);
    }

    public static function penaltyFeeTally(){
        $book = BookClass::where("teacher_id",auth()->user()->id)
                    ->where("total_fee_status","completed")
                    ->where("status","<>","cancelled")
                    ->where("status","<>","booked")
                    ->get(["id","penalty_fee","status"]);
        $total = 0;
        foreach($book as $fee) {
            $total += $fee->penalty_fee;
        }
        return number_format($total,2);
    }



    public static function completedClass(){
        return BookClass::where("teacher_id",auth()->user()->id)
                ->where("status","<>","cancelled")
                ->where("status","<>","booked")->count();
    }





    public static function penaltyClass(){
        return BookClass::where("penalty_fee","<>",0)->count();
    }




    public static function cutOffStart(){
        $now = new \DateTime("now", new \DateTimeZone("UTC"));
        $cutoff_start = "";
        if($now->format("d") <= 15) {
            $cutoff_start = $now->format("Y-m-1 00:00:00");
        }else{
            $cutoff_start = $now->format("Y-m-16 00:00:00");
        }
        return $cutoff_start;
    }
    public static function cutOffEnd(){
        $now = new \DateTime("now", new \DateTimeZone("UTC"));
        $cutoff_end = "";
        if($now->format("d") <= 15) {
            $cutoff_end = $now->format("Y-m-15 23:59:00");
        }else{
            $cutoff_end = $now->format("Y-m-t 23:59:00");
        }
        return $cutoff_end;
    }


    public function backup() {
        if( null !== auth()->user() ) {
            if( auth()->user()->hasRole('superadmin') ){
                $exitCode = Artisan::call('backup:now');
                if($exitCode == 0) {
                    return "Successfully backed-up";
                }else{
                    return "Can't Backup..";
                }
            }else{          
                    return redirect( url("/") );
            }
        }
        else {
            return "Not Logged In";
        }
    }


    public function cities($id) {
        return CityMunicipality::where("country_id",$id)->orderBy('name','asc')->get();
    }


    public function vouchersRead($id){
        $voucher = Voucher::find($id);
        return $voucher;
    }

    public function voucherCode($code){
        $today = new \DateTime("now", new \DateTimeZone(auth()->user()->timezone));
        $voucher = Voucher::where("code",$code)
                    ->where("valid_from","<=", $today->format("Y-m-d H:i:s") )
                    ->where("valid_until",">=", $today->format("Y-m-d H:i:s"))
                    ->where("status","active")
                    ->where("quantity",">","0")
                    ->first();
        return $voucher;
    }

    public function updateBookClass(Request $request){

        //return $request;
        if(auth()->user()->hasRole('teacher')) {
            $data = BookClass::find($request->id);
            if ( $data->status == "completed" || $data->status == "student is absent" || $data->status == "booked" ) {
                $data->status = $request->status;
                $data->pronunciation = $request->pronunciation;
                $data->grammar = $request->grammar;
                $data->areas_for_improvement = $request->areas_for_improvement;
                $data->tips_and_suggestion_for_student = $request->tips_and_suggestion_for_student;
                $data->class_remarks = $request->class_remarks;
                $data->speed_test_1 = $request->speed_test_1;
                $data->speed_test_2 = $request->speed_test_2;
                $data->book_name = $request->book_name;
                $data->page_number = $request->page_number;
                $data->student_absent = $request->student_absent;
                if( $request->student_absent == "yes" ) {
                    $data->student_reason_for_absence = $request->student_reason_for_absence;
                    $data->student_reason_screenshot = $request->student_reason_screenshot;
                }

                if( isset($request->status) ) {
                    // lets check todays date to the class start
                    $data->actor = "teacher";

                    if($data->session == "free-trial") {

                        if($request->status == "completed" || $request->status == "lesson memo delay") {

                            $now = new \DateTime("now", new \DateTimeZone(auth()->user()->timezone));
                            $dea = new \DateTime($data->end_at, new \DateTimeZone(auth()->user()->timezone));
                            $dea->add(new \DateInterval("PT2H")); 

                            if($data->total_fee_status == "completed") {
                                $data->total_fee_status = "waived";
                            }


                            if( $now->getTimestamp() > $dea->getTimestamp()) {
                                $data->status = "lesson memo delay";
                                $penalty = Penalty::where("name", $data->status)
                                            ->where("actor", $data->actor)->first();
                                $data->penalty_fee = floatval($penalty->value_in_php);
                                $data->class_fee = null;
                            }

                            // we save this data to enrollment_incentives
                            $incentive = new EnrollmentIncentive();
                            $incentive->class_id = $data->id;
                            $incentive->status = "pending";
                            $incentive->student_id = $data->student_id;
                            $incentive->teacher_id = $data->teacher_id;
                            $incentive->save();
                        }

                    }else{

                        if($request->status == "completed") {
                            $now = new \DateTime("now", new \DateTimeZone(auth()->user()->timezone));
                            $dea = new \DateTime($data->end_at, new \DateTimeZone(auth()->user()->timezone));
                            $dea->add(new \DateInterval("PT2H")); 

                            if( $now->getTimestamp() > $dea->getTimestamp() ) {
                                //dd($now->format("Y-m-d H:i:s") ." ". $dea->format("Y-m-d H:i:s"));
                                $data->status = "lesson memo delay";
                                $penalty = Penalty::where("name", $data->status)
                                            ->where("actor",$data->actor)->first();
                                $data->penalty_fee = floatval($penalty->value_in_php);
                            }

                        }
                        else if($request->status == "student is absent") {
                            $data->class_fee = (floatval($data->class_fee)/2);
                        }
                    }
                }
                $data->save();


                $teacher = User::where('id', $data->teacher_id)->first(['nick','email']);
                $student = User::where('id', $data->student_id)->first(['nick','email']);
                
                // now lets send booking cancellation to teacher
                $now =  new \DateTime(null, new \DateTimeZone("UTC"));
                $now->add(new \DateInterval("PT10M"));

                $class_start = new \DateTime($data->start_at);
                $class_end = new \DateTime($data->end_at);
                $class_id = $class_start->format("d-M-y-H:i")."-". $class_end->format("H:i") ."-".$data->id;

                $book_class = new \stdClass();
                $book_class->class_id = $class_id;
                $book_class->pronunciation = $data->pronunciation;
                $book_class->grammar = $data->grammar;
                $book_class->areas_for_improvement = $data->areas_for_improvement;
                $book_class->tips_and_suggestion_for_student = $data->tips_and_suggestion_for_student;
                $book_class->class_remarks = $data->class_remarks;
                $book_class->start_at = $data->start_at;

                $body = new \stdClass();
                $body->teacher_data = $teacher;
                $body->student_data = $student;
                $body->book_class = $book_class;

                $emailJob = (new SendEmailJob($student->email, "lesson_memo", $body))->delay($now);
                $id  = app(Dispatcher::class)->dispatch($emailJob);
                return "Class Information Successfully Updated!";

            }


        }// end if role is teacher

        return "Cannot save class..";
    }// end updateBookClass function

}
