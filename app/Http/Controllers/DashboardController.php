<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Availability;
use App\User;
use App\TeacherAccountType;
use App\BookClass;
use App\Order;
use App\Referral;
use Illuminate\Database\QueryException;

class DashboardController extends Controller
{

    // TEACHER FUNCTIONS

    public static function totalOpenedSlots() {
        $start = DashboardController::cutOffStart();
        $end = DashboardController::cutOffEnd();
        $availability = Availability::where("teacher_id", auth()->user()->id )
                ->where("start_at",">=", $start)
                ->where("start_at","<=", $end)
            ->count();

        return $availability;
    }

    public static function classFeeTallyPossible() {

        $level = TeacherAccountType::where("id", auth()->user()->teacher_account_type_id)->first(["id","rate"]);
        $availability = DashboardController::totalOpenedSlots();

        $rate = ($level->rate*$availability);

        $total = 0;

        return number_format($rate,2);
    }

    public static function bookedClass() {
        $start = DashboardController::cutOffStart();
        $end = DashboardController::cutOffEnd();
        return BookClass::where("status","<>","cancelled")
                ->where("start_at",">=", $start)
                ->where("start_at","<=", $end)
                ->where("teacher_id", auth()->user()->id)
                ->count();
    }

    public static function bookedClassRate() {
        $book = DashboardController::bookedClass();
        $open = DashboardController::totalOpenedSlots();
        if($open == 0) {
            return "0";
        }
        return number_format( ($book/$open)*100, 2 );
    }

    public static function teacherLevel(){
        $account = TeacherAccountType::where("id",auth()->user()->teacher_account_type_id)->first(["name"]);
        return $account->name;
    }


    public static function totalFeeTally() {
        $start = DashboardController::cutOffStart();

        $end = DashboardController::cutOffEnd();

        $class = BookClass::where("start_at",">=",$start)
                    ->where("start_at","<=", $end)
                    ->whereIn("status",["completed","student is absent","lesson memo delay","penalty 1","penalty 2","penalty 3"])
                    ->where("teacher_id", auth()->user()->id )
                    ->whereIn("total_fee_status",["deposited","completed"])//not waived
                    ->selectRaw('teacher_id, sum(class_fee) as total_class_fee, sum(penalty_fee) as total_penalty_fee, sum(incentive_total) as total_incentive_fee')
                    ->groupBy("teacher_id")
                    ->first();

        $com = isset($class->total_class_fee) ? $class->total_class_fee : 0;

        $pen = isset($class->total_penalty_fee) ? $class->total_penalty_fee : 0;

        $inc = isset($class->total_incentive_fee)? $class->total_incentive_fee:0;

        return number_format( (doubleval($com)-doubleval($pen))+doubleval($inc),2);
    }

    public static function cutOffShort() {
        $cutoff = json_decode(DashboardController::dateCutOff("current"));
        $start = new \DateTime($cutoff->start,new \DateTimeZone("UTC"));
        $end = new \DateTime($cutoff->end, new \DateTimeZone("UTC"));

        if(auth()->user()->lang == "zh-cn"){
            $formatter = new \IntlDateFormatter('zh_cn', \IntlDateFormatter::MEDIUM, \IntlDateFormatter::MEDIUM);
            $formatter->setPattern('MMM dd');
            $s = $formatter->format($start);
            $formatter->setPattern('dd YYYY');
            $e = $formatter->format($end);
            return $s."-".$e;
        }else{
            $short = $start->format("M d")."-".$end->format("d Y");                
            return $short;
        }

    }
    public static function cutOffShortPrev() {
        $cutoff = json_decode(DashboardController::dateCutOff("previous"));
        $start = new \DateTime($cutoff->start,new \DateTimeZone("UTC"));
        $end = new \DateTime($cutoff->end, new \DateTimeZone("UTC"));

        if(auth()->user()->lang == "zh-cn"){
            $formatter = new \IntlDateFormatter('zh_cn', \IntlDateFormatter::MEDIUM, \IntlDateFormatter::MEDIUM);
            $formatter->setPattern('MMM dd');
            $s = $formatter->format($start);
            $formatter->setPattern('dd YYYY');
            $e = $formatter->format($end);
            return $s."-".$e;
        }else{
            $short = $start->format("M d")."-".$end->format("d Y");                
            return $short;
        }

    }

    public static function dateCutOff($when="current")
    {
        $cutoff = new \stdClass();
        $cutoff->start = ($when=="current") ? DashboardController::cutOffStart() : 
            DashboardController::cutOffStartPrev();
        $cutoff->end = ($when=="current") ? DashboardController::cutOffEnd() : 
            DashboardController::cutOffEndPrev();
        return json_encode($cutoff);
    }

    // STUDENT DASHBOARD
    public static function regularBalance() {
        $balance = Order::where("status","active")
                ->where("student_id", auth()->user()->id)
                ->selectRaw("sum(remaining_reservation) as remaining")
                ->first();

        return isset($balance->remaining) ? $balance->remaining:0;
    }

    public static function regularBalanceLatestDate() {
        $balance = Order::where("status","active")
                ->where("student_id", auth()->user()->id)
                ->orderBy("effective_until","desc")
                ->first();
        //return $balance;
        if( isset($balance) ){
            $human = new \DateTime($balance->effective_until, new \DateTimeZone(auth()->user()->timezone));
            if( auth()->user()->lang == "zh-cn" ) {
            $formatter = new \IntlDateFormatter('zh_cn', \IntlDateFormatter::MEDIUM, \IntlDateFormatter::MEDIUM);
            $formatter->setPattern('MMM dd, YYYY');
            return $formatter->format($human);
            }else{
                return $human->format("M d, Y");
            }
        }
        else {
            return "No Package yet";
        }
    }

    public static function referralBalance() {
        $balance = Referral::where("referrer_id", auth()->user()->id)
                ->selectRaw("sum(referral_consumable) as remaining")
                ->first();
        return ($balance->remaining) ? $balance->remaining: "0";
    }

    public static function referralBalanceLatestDate() {
        $balance = Referral::where("referrer_id", auth()->user()->id)
                ->orderBy("referral_valid_until","asc")
                ->first();
        if(isset($balance->referral_valid_until)){
            $human = new \DateTime($balance->referral_valid_until, new \DateTimeZone(auth()->user()->timezone));
            return $human->format("M d, Y");
        }else{
            return "";
        }
    }

    public static function immortalBalance() {
        $balance = User::where("id", auth()->user()->id)
                ->first();
        return $balance->immortal;
    }

    public static function totalBooked() {
        $start = DashboardController::cutOffStart();
        $end = DashboardController::cutOffEnd();
        return BookClass::where("status","<>","cancelled")
                ->where("start_at",">=", $start)
                ->where("start_at","<=", $end)
                ->where("student_id", auth()->user()->id)
                ->count();
    }


    // TEACHER FUNCTIONS

    public function runningTotal($id,$when = "current") {
        $start = ($when=="current") ? DashboardController::cutOffStart() : 
            DashboardController::cutOffStartPrev();
        $end = ($when=="current") ? DashboardController::cutOffEnd() : 
            DashboardController::cutOffEndPrev();
        // get all book_ids of completed lesson memo delay and studentis absent
        $classes = BookClass::where("book_classes.start_at",">=",$start)
                    ->where("book_classes.start_at","<=", $end)
                    ->where("teacher_id", $id)
                    ->whereIn("book_classes.status",["completed","student is absent","lesson memo delay","penalty 1","penalty 2","penalty 3"])
                    ->where("book_classes.total_fee_status","completed")//not waived
                    ->groupBy("teacher_id")
                    ->selectRaw('teacher_id, count(book_classes.class_fee) as class_count, sum(book_classes.class_fee) as total_class_fee, count(book_classes.penalty_fee) as penalty_count, sum(book_classes.penalty_fee) as total_penalty_fee, count(book_classes.incentive_total) as incentive_count, sum(book_classes.incentive_total) as total_incentive_fee')
                    ->first();

        return $classes;
    }



    public function runningTotalCompleted($id, $when="current") {
        $start = ($when=="current") ? DashboardController::cutOffStart() : 
            DashboardController::cutOffStartPrev();
        $end = ($when=="current") ? DashboardController::cutOffEnd() : 
            DashboardController::cutOffEndPrev();

        $classes = BookClass::where("book_classes.start_at",">=",$start)
                    ->where("book_classes.start_at","<=", $end)
                    ->where("teacher_id", $id)
                    ->whereIn("book_classes.status",["lesson memo delay","completed","student is absent"])
                    ->where("book_classes.total_fee_status","completed")//not waived
                    ->whereRaw("class_fee is not null")
                    ->join("users","users.id","student_id")
                    ->orderBy("start_at","asc")
                    ->get(["start_at","end_at","class_fee","status","nick"]);

        return $classes;
    }

    public function runningTotalPenalty($id,$when="current") {
        $start = ($when=="current") ? DashboardController::cutOffStart() : 
            DashboardController::cutOffStartPrev();
        $end = ($when=="current") ? DashboardController::cutOffEnd() : 
            DashboardController::cutOffEndPrev();
        // get all book_ids of completed lesson memo delay and studentis absent
        $classes = BookClass::where("book_classes.start_at",">=",$start)
                    ->where("book_classes.start_at","<=", $end)
                    ->where("teacher_id", $id)
                    ->whereIn("book_classes.status",["lesson memo delay","penalty 1","penalty 2","penalty 3"])
                    ->where("book_classes.total_fee_status","completed")//not waived
                    ->whereRaw("penalty_fee is not null")
                    ->join("users","users.id","student_id")
                    ->orderBy("start_at","asc")
                    ->get(["start_at","end_at","penalty_fee","status","nick"]);

        return $classes;
    }

    public function runningTotalIncentive($id, $when) {
        $start = ($when=="current") ? DashboardController::cutOffStart() : 
            DashboardController::cutOffStartPrev();
        $end = ($when=="current") ? DashboardController::cutOffEnd() : 
            DashboardController::cutOffEndPrev();
        // get all book_ids of completed lesson memo delay and studentis absent
        $classes = BookClass::where("book_classes.start_at",">=",$start)
                    ->where("book_classes.start_at","<=", $end)
                    ->where("teacher_id", $id)
                    ->whereIn("book_classes.status",["lesson memo delay","completed","student is absent"])
                    ->where("book_classes.total_fee_status","completed")//not waived
                    ->whereRaw("incentive_total is not null")
                    ->join("users","users.id","student_id")
                    ->orderBy("start_at","asc")
                    ->get(["start_at","end_at","incentive_total","status","incentive_comment","nick"]);

        return $classes;
    }




    // CLIENT FUNCTION

    public static function soldPackages() {
        // get all student ids from user from your school
        $today = new \DateTime("now", new \DateTimeZone("UTC"));

        $lastdate = $today->format("Y-m-t 23:59:59");

        $students = User::where("school_id", auth()->user()->school_id)
            ->where("role_id", 2)
            ->where("orders.status","active")
            ->where("orders.approved_at",">=", $today->format("Y-m-01 00:00:00") )
            ->where("orders.approved_at", "<=", $lastdate)
            ->join("orders","orders.student_id","users.id")
            ->count();

        return $students;
    }

    // CLIENT FUNCTION

    public static function activeStudents() {
        // get all student ids from user from your school
        $today = new \DateTime("now", new \DateTimeZone("UTC"));

        $lastdate = $today->format("Y-m-t 23:59:59");

        $students = User::where("school_id", auth()->user()->school_id)
            ->where("role_id", 2)
            ->where("orders.status","active")
            ->where("orders.effective_until",">=", $today->format("Y-m-d H:i:s") )
            ->join("orders","orders.student_id","users.id")
            ->count();

        return $students;
    }

    public static function inactiveStudents() {
        // get all student ids from user from your school
        $today = new \DateTime("now", new \DateTimeZone("UTC"));

        $lastdate = $today->format("Y-m-t 23:59:59");

        // get all students that has no package
        $students = DashboardController::noOrderStudents();
        // get all students that has no currently active package
        $noactive = DashboardController::noActiveStudents();

        $total = intval($students)+intval($noactive);

        return $total;
    }

    public static function noOrderStudents(){
        // get all students that has no package
        $students = User::where("school_id", auth()->user()->school_id)
            ->where("role_id", 2)
            ->whereRaw("orders.status is null")
            ->leftJoin("orders","orders.student_id","users.id")
            ->count();
        return $students;
    }

    public static function noActiveStudents() {
        // get all students that has no package
        $today = new \DateTime("now", new \DateTimeZone("UTC"));

        $students = User::where("school_id", auth()->user()->school_id )
            ->where("role_id", 2)
            ->where("orders.effective_until","<", $today->format("Y-m-d H:i:s") )
            ->join("orders","orders.student_id","users.id")
            ->count();

        return $students;
    } 


    public static function awaitingPayment(){
        $students = User::where("school_id", auth()->user()->school_id )
            ->where("role_id", 2)
            ->where("orders.status","pending")
            ->join("orders","orders.student_id","users.id")
            ->count();
        return $students;
    }

    public static function forApproval(){
        $students = User::where("school_id", auth()->user()->school_id )
            ->where("role_id", 2)
            ->where("orders.status","paid")
            ->join("orders","orders.student_id","users.id")
            ->count();
        return $students;
    }

    public static function totalStudentCompletedClass(){
        return BookClass::whereIn("status",["lesson memo delay","completed"])
                ->where("student_id", auth()->user()->id)
                ->count();
    }

    public static function clientEarningTotal(){
        // get all student ids from user from your school
        $today = new \DateTime("now", new \DateTimeZone("UTC"));

        $lastdate = $today->format("Y-m-t 23:59:59");

        $packages = json_decode(DashboardController::clientEarnings());

        $total = 0;
        foreach($packages as $package) {
            $total += $package->total_earning;
        }

        return $total;
    }

    public function clientTotalEarning(){
        $today = new \DateTime("now", new \DateTimeZone("UTC"));

        $lastdate = $today->format("Y-m-t 23:59:59");

        $packages = User::where("school_id", auth()->user()->school_id)
            ->where("role_id", 2)
            ->where("orders.status","active")
            ->where("orders.approved_at",">=", $today->format("Y-m-01 00:00:00") )
            ->where("orders.approved_at", "<=", $lastdate)
            ->selectRaw("sum(additional_price) as total_earning, count(student_id) as sold, name")
            ->groupBy("name")
            ->join("orders","orders.student_id","users.id")
            ->get();

        return $packages;
    }

    public static function clientEarnings(){
        // get all student ids from user from your school
        $today = new \DateTime("now", new \DateTimeZone("UTC"));

        $lastdate = $today->format("Y-m-t 23:59:59");

        $packages = User::where("school_id", auth()->user()->school_id)
            ->where("role_id", 2)
            ->where("orders.status","active")
            ->where("orders.approved_at",">=", $today->format("Y-m-01 00:00:00") )
            ->where("orders.approved_at", "<=", $lastdate)
            ->selectRaw("sum(additional_price) as total_earning, count(student_id) as sold, name")
            ->groupBy("name")
            ->join("orders","orders.student_id","users.id")
            ->get();
        return json_encode($packages);
    }

//admin
        public static function adminRunningTotal($when = "current") {
        $start = ($when=="current") ? DashboardController::cutOffStart() : 
            DashboardController::cutOffStartPrev();
        $end = ($when=="current") ? DashboardController::cutOffEnd() : 
            DashboardController::cutOffEndPrev();
        // get all book_ids of completed lesson memo delay and studentis absent
        $classes = BookClass::whereBetween("book_classes.start_at",[$start, $end])
                    ->whereIn("book_classes.status", ["completed","student is absent","lesson memo delay","penalty 1","penalty 2","penalty 3"])
                    ->where("book_classes.total_fee_status","completed")//not waived
                    ->groupBy("teacher_id")
                    ->selectRaw('teacher_id, count(book_classes.class_fee) as class_count, sum(book_classes.class_fee) as total_class_fee, count(book_classes.penalty_fee) as penalty_count, sum(book_classes.penalty_fee) as total_penalty_fee, count(book_classes.incentive_total) as incentive_count, sum(book_classes.incentive_total) as total_incentive_fee')
                    ->first();

        return $classes;
    }

    // GENERIC FUNCTIONS

    public static function cutOffStart(){
        $now = new \DateTime("now", new \DateTimeZone("UTC"));
        $cutoff_start = "";
        if($now->format("d") <= 15) {
            $cutoff_start = $now->format("Y-m-1 00:00:00");
        }else{
            $cutoff_start = $now->format("Y-m-16 00:00:00");
        }
        return $cutoff_start;
    }
    public static function cutOffEnd(){
        $now = new \DateTime("now", new \DateTimeZone("UTC"));
        $cutoff_end = "";
        if($now->format("d") <= 15) {
            $cutoff_end = $now->format("Y-m-15 23:59:00");
        }else{
            $cutoff_end = $now->format("Y-m-t 23:59:00");
        }
        return $cutoff_end;
    }

    public static function cutOffStartPrev() {
        $now = new \DateTime("now", new \DateTimeZone("UTC"));
        $now->sub(new \DateInterval("P1M"));
        $now->add(new \DateInterval("P16D"));
        $cutoff_start = "";
        if($now->format("d") <= 15) {
            $cutoff_start = $now->format("Y-m-1 00:00:00");
        }else{
            $cutoff_start = $now->format("Y-m-16 00:00:00");
        }
        return $cutoff_start;
    }

    public static function cutOffEndPrev() {
        $now = new \DateTime("now", new \DateTimeZone("UTC"));
        $now->sub(new \DateInterval("P1M"));
        $now->add(new \DateInterval("P16D"));
        $cutoff_end = "";
        if($now->format("d") <= 15) {
            $cutoff_end = $now->format("Y-m-15 23:59:00");
        }else{
            $cutoff_end = $now->format("Y-m-t 23:59:00");
        }
        return $cutoff_end;
    }


}