<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Availability;
use App\User;
use Illuminate\Database\QueryException;

class CalendarController extends Controller
{
    public function store(Request $request) {

    	if($request->ajax()){
	    	$data = json_decode(json_encode($request->post()));
	    	$success = 0;
	    	foreach($data as $item) {
	    		try{
		    		$plot = new Availability();
		    		$plot->start_at = $item->start;
		    		$plot->end_at = $item->end;
		    		$plot->timezone = $item->timezone;
		    		$plot->status = $item->status;
		    		$plot->teacher_id = auth()->user()->id;
		    		$success = $plot->save();
	    		}catch(QueryException $qe) {
	    			// do not echo anything and just continue
	    			continue;
	    		}
	    	}

	    	return "Successful";
    	}
    }

    public function peak(Request $request) {
        $data = json_decode(json_encode($request->post()));

        $user = User::find(auth()->user()->id);
        $user->peak1to15 = $data->peak1to15;
        $user->peak16to31 = $data->peak16to31;
        $result = $user->save();
        if($result == true){
            return "Successful";
        }
        return "Unsuccessful";
    }

    public function list(Request $request){
        // TODO:
        // get data only from this year -> consult to Boss Christian Tantengco
        $list = Availability::where("teacher_id", $request->id)->get();
        $json = [];
        foreach($list as $item){
            $data = [];
            $data["title"] = $item->status;
            $data["status"] = $item->status;
            $data["start"] = date("c",strtotime($item->start_at));
            $data["end"] = $item->end_at;
            $data["id"] = $item->id;
            if($data["status"] == "open"){
                $data["color"] = "#019875";
                $data["textColor"] = "white";
            }
            else if($data["status"] == "booked") {
                $data["color"] = "blue";
            }
            else if($data["status"] == "cancelled") {
                $data["color"] = "red";
            }
            else if($data["status"] == "penalty 1") {
                $data["color"] = "red";
            }
            else if($data["status"] == "penalty 2") {
                $data["color"] = "red";
            }
            else if($data["status"] == "penalty 3") {
                $data["color"] = "red";
            }
            else if($data["status"] == "completed") {
                $data["color"] = "#01730d";
            }
            else if($data["status"] == "teacher is absent") {
                $data["color"] = "#8f3fff"; //violet
            }
            else if($data["status"] == "student is absent") {
                $data["color"] = "#8f3fff"; //violet
            }

            $json[] = $data;
        }
        //2018-04-26T04:21:13.942Z
        //Y-M-dTH:i:s.
        return json_encode($json);
    }

}