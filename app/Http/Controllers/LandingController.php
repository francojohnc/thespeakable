<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LandingController extends Controller
{
    //
    public function teacher(){
    	return view('teacher_landing');
    }
    public function student() {
    	return view('student_landing');
    }
    public function check() {
    	$info = [];
    	$info["logo"] = "/storage/entity/logo.jpg";

    	return view('student_landing', compact($info));
    }
}
