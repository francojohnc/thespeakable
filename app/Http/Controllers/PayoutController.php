<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;
use App\Penalty;
use App\TeacherAccountType;
use App\EnrollmentIncentive;
use App\Payout;
use App\BookClass;
use App\PayoutBatch;


use App\Jobs\SendEmailJob;
use App\Job;

use Illuminate\Contracts\Bus\Dispatcher;



class PayoutController extends Controller
{
    public function routeUserMiddleware(Request $request){
        return $request->user();
    }


    public function previousCutOff(){
        // get start date and end date for this cutoff
        try{
            $start = PayoutController::cutOffStartPrev();
            $end = PayoutController::cutOffEndPrev();


            // Delete Old data for this cutoff and generate new
            $payout = Payout::where("cutoff_start", $start)
                        ->where("cutoff_end",$end)
                        ->where("status","unpaid")
                        ->delete();


            // get all classes that is completed whose
            $all_classes = $this->completedClass($start, $end);
            //return $all_classes;

            $pay = [];
            foreach($all_classes as $class) {

                $payout = new Payout();
                $payout->teacher_id = $class->teacher_id;
                $payout->teacher_name = $class->first_name." ".$class->last_name;
                $payout->teacher_level = $class->level;
                $payout->bank_account_number = $class->bank_account_number;
                $payout->bank_account_name = $class->bank_account_name;
                $payout->bank_name = $class->bank_name;
                $payout->status = "unpaid";

                $payout->total_completed_class = $class->class_count;
                $payout->total_completed_class_fee = $class->total_class_fee;

                $payout->total_penalty_class = $class->penalty_count;
                $payout->total_penalty_fee = $class->total_penalty_fee;
                $payout->total_class_incentives = $class->incentive_count;
                $payout->total_incentive_fee = $class->total_incentive_fee;
                $payout->total_fee = ($payout->total_completed_class_fee - $payout->total_penalty_fee) + $payout->total_incentive_fee;

                $ids = array_values($this->classIds($start, $end, $class->teacher_id));

                $payout->affected_ids = json_encode($ids);
                $payout->cutoff_start = $start;
                $payout->cutoff_end = $end;
                //$pay[] = $payout;
                $payout->save();

            }
            //return $pay;

            return "Payouts for the current cut-off successfully generated";

        }
        catch(Exception $e){
            return "Error";
        }


    }

    public function currentCutOff(){
        // get start date and end date for this cutoff
        try{
            $start = PayoutController::cutOffStart();
            $end = PayoutController::cutOffEnd();


            // Delete Old data for this cutoff and generate new
            $payout = Payout::where("cutoff_start", $start)
                        ->where("cutoff_end",$end)
                        ->where("status","unpaid")
                        ->delete();


            // get all classes that is completed whose
            $all_classes = $this->completedClass($start, $end);
            //return $all_classes;

            $pay = [];
            foreach($all_classes as $class) {

                $payout = new Payout();
                $payout->teacher_id = $class->teacher_id;
                $payout->teacher_name = $class->first_name." ".$class->last_name;
                $payout->teacher_level = $class->level;
                $payout->bank_account_number = $class->bank_account_number;
                $payout->bank_account_name = $class->bank_account_name;
                $payout->bank_name = $class->bank_name;
                $payout->status = "unpaid";

                $payout->total_completed_class = $class->class_count;
                $payout->total_completed_class_fee = $class->total_class_fee;

                $payout->total_penalty_class = $class->penalty_count;
                $payout->total_penalty_fee = $class->total_penalty_fee;
                $payout->total_class_incentives = $class->incentive_count;
                $payout->total_incentive_fee = $class->total_incentive_fee;
                $payout->total_fee = ($payout->total_completed_class_fee - $payout->total_penalty_fee) + $payout->total_incentive_fee;

                $ids = array_values($this->classIds($start, $end, $class->teacher_id));

                $payout->affected_ids = json_encode($ids);
                $payout->cutoff_start = $start;
                $payout->cutoff_end = $end;
                //$pay[] = $payout;
                $payout->save();

            }
            //return $pay;

            return "Payouts for the current cut-off successfully generated";

        }
        catch(Exception $e){
            return "Error";
        }

    }





    public function completedClass($start, $end) {
        // get all book_ids of completed lesson memo delay and studentis absent
        $teacher_ids = BookClass::where("book_classes.start_at",">=",$start)
                    ->where("book_classes.start_at","<=", $end)
                    ->whereIn("book_classes.status",["completed","student is absent","lesson memo delay","penalty 1","penalty 2","penalty 3"])
                    ->where("book_classes.total_fee_status","completed")//not waived
                    ->groupBy("book_classes.teacher_id", "users.first_name", "users.last_name", "users.bank_name", "users.bank_account_name", "users.bank_account_number","teacher_account_types.name")
                    ->selectRaw('book_classes.teacher_id, users.first_name, users.last_name, name as level, users.bank_name, users.bank_account_name, users.bank_account_number, count(book_classes.class_fee) as class_count, sum(book_classes.class_fee) as total_class_fee, count(book_classes.penalty_fee) as penalty_count, sum(book_classes.penalty_fee) as total_penalty_fee, count(book_classes.incentive_total) as incentive_count, sum(book_classes.incentive_total) as total_incentive_fee')
                    ->join('users','users.id','book_classes.teacher_id')
                    ->join("teacher_account_types","teacher_account_type_id","teacher_account_types.id")
                    ->get();

        return $teacher_ids;
    }

    public function classIds($start, $end, $teacher_id) {
        $ids = BookClass::where("start_at",">=",$start)
                    ->where("start_at","<=", $end)
                    ->whereIn("status",["completed","student is absent","lesson memo delay","penalty 1","penalty 2","penalty 3"])
                    ->where("total_fee_status","completed")//not waived
                    ->where("teacher_id", $teacher_id)
                    ->get(["id","status","session"]);
        return $ids->pluck("id")->toArray();
    }



    public function forPayment($id) {
        // get Batch First that is pending
        $now = new \DateTime("now", new \DateTimeZone("UTC"));

        $pb = PayoutBatch::where("batch_date", $now->format("Y-m-d 00:00:00") )->where("status","pending")->count();
        if($pb == 0) {
            $pb = new PayoutBatch();
            $pb->batch_date = $now->format("Y-m-d 00:00:00");
            $pb->status = "pending";
            $pb->save();
        }else{
            $pb = PayoutBatch::where("batch_date",$now->format("Y-m-d 00:00:00"))->where("status","pending")->first();
        }

        $payout = Payout::find($id);
        $payout->batch_id = $pb->id;
        $payout->status = "for payment";
        $result = $payout->save();

        if($result == true) {

            $info = "Successfully added to for payments";
        }else{
            $info = "Error Occured..";
        }
        return $info;
    }


    public function forPaymentPrint() {
        return view('vendor.voyager.payouts.forpayment');
    }


    public function deposited($id) {
        // mark the current id as deposited then 
        $payout = Payout::find($id);
        $payout->status = "deposited";
        $pb = PayoutBatch::find($payout->batch_id);
        $payout->deposited_at = $pb->payment_date;
        $result = $payout->save();

        $ids = json_decode($payout->affected_ids);
        $classes = BookClass::whereIn("id",$ids)->get();

        try{
            $payment_date = $pb->payment_date;
            foreach($classes as $class) {
                $class->total_fee_status = "deposited";
                $class->total_fee_payment_date = $pb->payment_date;
                $res = $class->save();
            }

            $incentives = EnrollmentIncentive::whereIn("class_id", $ids)->where("status","confirmed")->get();

            foreach($incentives as $incentive) {
                $incentive->status = "deposited";
                $incentive->date_paid = $pb->payment_date;
                $incentive->save();
            }

            return "success";

        }catch(Exception $e){

        }

        return "unsucessful";

        // loop to book_class and update payment_status and date_paid
    }

    public function unpaid($id){
        // mark the current id as deposited then 
        $payout = Payout::find($id);
        $payout->status = "unpaid";
        $pb = PayoutBatch::find($payout->batch_id);
        $payout->deposited_at = $pb->payment_date;
        $result = $payout->save();
        if($result == true) {
            return "success";
        }else {
            return "unsuccessful";
        }
        // loop to book_class and update payment_status and date_paid
    }


    public static function cutOffStart() {
        $now = new \DateTime("now", new \DateTimeZone("UTC"));
        $cutoff_start = "";
        if($now->format("d") <= 15) {
            $cutoff_start = $now->format("Y-m-1 00:00:00");
        }else{
            $cutoff_start = $now->format("Y-m-16 00:00:00");
        }
        return $cutoff_start;
    }

    public static function cutOffEnd() {
        $now = new \DateTime("now", new \DateTimeZone("UTC"));
        $cutoff_end = "";
        if($now->format("d") <= 15) {
            $cutoff_end = $now->format("Y-m-15 23:59:00");
        }else{
            $cutoff_end = $now->format("Y-m-t 23:59:00");
        }
        return $cutoff_end;
    }


    public static function cutOffStartPrev() {
        $now = new \DateTime("now", new \DateTimeZone("UTC"));
        $now->sub(new \DateInterval("P1M"));
        $now->add(new \DateInterval("P16D"));
        $cutoff_start = "";
        if($now->format("d") <= 15) {
            $cutoff_start = $now->format("Y-m-1 00:00:00");
        }else{
            $cutoff_start = $now->format("Y-m-16 00:00:00");
        }
        return $cutoff_start;
    }

    public static function cutOffEndPrev() {
        $now = new \DateTime("now", new \DateTimeZone("UTC"));
        $now->sub(new \DateInterval("P1M"));
        $now->add(new \DateInterval("P16D"));
        $cutoff_end = "";
        if($now->format("d") <= 15) {
            $cutoff_end = $now->format("Y-m-15 23:59:00");
        }else{
            $cutoff_end = $now->format("Y-m-t 23:59:00");
        }
        return $cutoff_end;
    }


}
