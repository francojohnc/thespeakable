<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post("/api/calendar/availability/create", 'CalendarController@store');
Route::post("/api/calendar/availability/peak", 'CalendarController@peak');
Route::post("/api/booking/save", 'ApiController@bookClass');
Route::post("/api/booking/update", 'ApiController@updateBookClass');
Route::post("/api/order/approve", "ApiController@approveOrder");
Route::get("/api/vouchers/{id}", "ApiController@vouchersRead");
Route::get("/api/vouchercode/{code}", "ApiController@voucherCode");
Route::post("/api/availabilities/delete", 'ApiController@availabilitiesDelete');


Route::get("/api/calendar/availability/list/{id}", 'CalendarController@list');
Route::get("/api/availabilities/browse/{id}", 'ApiController@availabilitiesBrowse');
Route::get("/api/book_class/browse/student/{id}", 'ApiController@bookClassBrowseStudent');
Route::get("/api/book_class/browse/teacher/{id}", 'ApiController@bookClassBrowseTeacher');

Route::get("/api/book_class/show/{id}", 'ApiController@bookClassShow');
Route::get("/api/book_class/show/student/{id}", 'ApiController@bookClassShowStudent');
Route::get("/api/package/read/{id}", 'ApiController@readPackage');
Route::get("/api/school_package/read/{id}", 'ApiController@readSchoolPackage');
Route::get("/api/teacher/profile/search/{dateandtime}", 'ApiController@searchAvailableTeacher');

Route::get("/api/browse/availabilities/{id}", "ApiController@browseAvailabilities");
Route::get("/api/current/date/{timezone}", "ApiController@getCurrentDate");
Route::get("/api/booking/cancel/{id}", "ApiController@cancelBooking");
Route::post("/api/teacher/booking/cancel/{id}", "ApiController@teacherCancelBooking");
Route::post("/api/teacher/availabilities/cancel/{id}", "ApiController@teacherCancelPlot");
Route::post("/api/teacher/access/cancel/{id}", "ApiController@teacherCancelAccess");
Route::post("/api/teacher/access/enable/{id}", "ApiController@teacherEnableAccess");
Route::get("/api/book_class/teacher/opened/memo/{id}", "ApiController@teacherOpenedMemo");

Route::get("/api/availabilities/list/{id}", "ApiController@availabilityList");


Route::get("/api/tally/class/fee", "ApiController@classFeeTally");
Route::get("/api/tally/penalty/fee", "ApiController@penaltyFeeTally");
Route::get("/api/tally/total/fee", "ApiController@totalFeeTally");
Route::get("/api/tally/class/fee/possible", "ApiController@classFeeTallyPossible");
Route::get("/cutoff", "ApiController@dateCutOff");
Route::get("/cutoffshort", "ApiController@cutOffShort");


Route::get("/cutoff/current", "PayoutController@currentCutOff");
Route::get("/cutoff/previous", "PayoutController@previousCutOff");
Route::get("/cutoff/forpayment/{id}","PayoutController@forPayment");

Route::get("/cutoff/deposited/{id}","PayoutController@deposited");
Route::get("/cutoff/unpaid/{id}","PayoutController@unpaid");



Route::post('paypal', 'PaymentController@payWithpaypal');
Route::get('status', 'PaymentController@getPaymentStatus');

Route::get("/send", "ApiController@sendEmail");
//Route::get("/test", "ApiController@tester");
Route::get("/referred/{referred_id}/redeem", "ApiController@redeemReferral");


Route::get('/cities/{id}', "ApiController@cities");



Route::group(['prefix' => ''], function () {
    Voyager::routes();
});



Route::put("student/update/{id}", "Voyager\UserController@updateStudent");

Route::put("password/update/{id}", "Voyager\UserController@changePass");

Route::post("forgot", "Voyager\UserController@forgot");


Route::get("/cutoff/print/forpayment", "PayoutController@forPaymentPrint");
Route::get("/nick/{nick}", "Voyager\UserController@nickCheck");


Route::get('/backup', "ApiController@backup");

//teacher dashboard computations
Route::get("/dashboard/running/total/completed/{id}/{when}",
	"DashboardController@runningTotalCompleted");
Route::get("/dashboard/running/total/penalty/{id}/{when}",
	"DashboardController@runningTotalPenalty");
Route::get("/dashboard/running/total/incentive/{id}/{when}",
	"DashboardController@runningTotalIncentive");
Route::get("/dashboard/running/total/{id}/{when}",
	"DashboardController@runningTotal");



Route::get("/dashboard/client/total/earning/{id}","DashboardController@clientTotalEarning");



Route::get("/teachers", "LandingController@teacher");

Route::get("/login/{url}", "LandingController@check");

Route::get('/chart/booked', 'ChartController@chartBooked');