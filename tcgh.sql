-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 28, 2018 at 07:31 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tcgh`
--

-- --------------------------------------------------------

--
-- Table structure for table `applications`
--

CREATE TABLE `applications` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unread',
  `body` text COLLATE utf8_unicode_ci,
  `resume` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `availabilities`
--

CREATE TABLE `availabilities` (
  `id` int(10) UNSIGNED NOT NULL,
  `start_at` datetime NOT NULL,
  `end_at` datetime NOT NULL,
  `teacher_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `timezone` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Asia/Shanghai',
  `status` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'open',
  `session` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'regular'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `book_classes`
--

CREATE TABLE `book_classes` (
  `id` int(10) UNSIGNED NOT NULL,
  `start_at` datetime NOT NULL,
  `end_at` datetime NOT NULL,
  `teacher_id` int(10) UNSIGNED NOT NULL,
  `student_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'booked',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `teacher_timezone` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'Asia/Shanghai',
  `student_timezone` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'Asia/Shanghai',
  `book_url` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message_to_teacher` text COLLATE utf8_unicode_ci,
  `session` varchar(30) COLLATE utf8_unicode_ci DEFAULT 'regular',
  `speed_test_1` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `speed_test_2` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `grammar` text COLLATE utf8_unicode_ci,
  `pronunciation` text COLLATE utf8_unicode_ci,
  `areas_for_improvement` text COLLATE utf8_unicode_ci,
  `tips_and_suggestion_for_student` text COLLATE utf8_unicode_ci,
  `class_remarks` longtext COLLATE utf8_unicode_ci,
  `screenshot` longtext COLLATE utf8_unicode_ci,
  `book_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lesson_unit` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page_number` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `current_teacher_rate` double DEFAULT NULL,
  `student_absent` varchar(30) COLLATE utf8_unicode_ci DEFAULT 'no',
  `teacher_absent` varchar(30) COLLATE utf8_unicode_ci DEFAULT 'no',
  `teacher_reason_for_absence` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `student_reason_for_absence` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `student_reason_screenshot` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `teacher_reason_screenshot` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `current_teacher_currency` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `job_ids` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `teacher_opened_memo` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'false',
  `order_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `availability_id` int(10) UNSIGNED DEFAULT NULL,
  `actor` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'system',
  `class_fee` double UNSIGNED DEFAULT NULL,
  `total_fee_status` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'completed',
  `total_fee_payment_date` datetime DEFAULT NULL,
  `penalty_fee` double UNSIGNED DEFAULT NULL,
  `total_fee` double DEFAULT '0',
  `incentive_total` double UNSIGNED DEFAULT NULL,
  `incentive_comment` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2018-04-21 03:12:16', '2018-04-21 03:12:16'),
(2, NULL, 1, 'Category 2', 'category-2', '2018-04-21 03:12:16', '2018-04-21 03:12:16');

-- --------------------------------------------------------

--
-- Table structure for table `city_municipalities`
--

CREATE TABLE `city_municipalities` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `city_municipalities`
--

INSERT INTO `city_municipalities` (`id`, `name`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 'Hong Kong', 1, NULL, NULL),
(2, 'Macau', 1, NULL, NULL),
(3, 'Beijing', 1, NULL, NULL),
(4, 'Chongqing', 1, NULL, NULL),
(5, 'Shanghai', 1, NULL, NULL),
(6, 'Tianjin', 1, NULL, NULL),
(7, 'Anqing', 1, NULL, NULL),
(8, 'Bengbu', 1, NULL, NULL),
(9, 'Bozhou', 1, NULL, NULL),
(10, 'Chaohu', 1, NULL, NULL),
(11, 'Chizhou', 1, NULL, NULL),
(12, 'Chuzhou', 1, NULL, NULL),
(13, 'Fuyang', 1, NULL, NULL),
(14, 'Hefei', 1, NULL, NULL),
(15, 'Huaibei', 1, NULL, NULL),
(16, 'Huainan', 1, NULL, NULL),
(17, 'Huangshan', 1, NULL, NULL),
(18, 'Jieshou', 1, NULL, NULL),
(19, 'Lu\'an', 1, NULL, NULL),
(20, 'Ma\'anshan', 1, NULL, NULL),
(21, 'Mingguang', 1, NULL, NULL),
(22, 'Ningguo', 1, NULL, NULL),
(23, 'Suzhou', 1, NULL, NULL),
(24, 'Tianchang', 1, NULL, NULL),
(25, 'Tongcheng', 1, NULL, NULL),
(26, 'Tongling', 1, NULL, NULL),
(27, 'Wuhu', 1, NULL, NULL),
(28, 'Xuancheng', 1, NULL, NULL),
(29, 'Fu\'an', 1, NULL, NULL),
(30, 'Fuding', 1, NULL, NULL),
(31, 'Fuqing', 1, NULL, NULL),
(32, 'Fuzhou', 1, NULL, NULL),
(33, 'Jian\'ou', 1, NULL, NULL),
(34, 'Jinjiang', 1, NULL, NULL),
(35, 'Longhai', 1, NULL, NULL),
(36, 'Longyan', 1, NULL, NULL),
(37, 'Nan\'an', 1, NULL, NULL),
(38, 'Nanping', 1, NULL, NULL),
(39, 'Ningde', 1, NULL, NULL),
(40, 'Putian', 1, NULL, NULL),
(41, 'Quanzhou', 1, NULL, NULL),
(42, 'Sanming', 1, NULL, NULL),
(43, 'Shaowu', 1, NULL, NULL),
(44, 'Shishi', 1, NULL, NULL),
(45, 'Wuyishan', 1, NULL, NULL),
(46, 'Xiamen', 1, NULL, NULL),
(47, 'Yong\'an', 1, NULL, NULL),
(48, 'Zhangping', 1, NULL, NULL),
(49, 'Zhangzhou', 1, NULL, NULL),
(50, 'Baiyin', 1, NULL, NULL),
(51, 'Dingxi', 1, NULL, NULL),
(52, 'Dunhuang', 1, NULL, NULL),
(53, 'Hezuo', 1, NULL, NULL),
(54, 'Jiayuguan', 1, NULL, NULL),
(55, 'Jinchang', 1, NULL, NULL),
(56, 'Jiuquan', 1, NULL, NULL),
(57, 'Lanzhou', 1, NULL, NULL),
(58, 'Linxia', 1, NULL, NULL),
(59, 'Longnan', 1, NULL, NULL),
(60, 'Pingliang', 1, NULL, NULL),
(61, 'Qingyang', 1, NULL, NULL),
(62, 'Tianshui', 1, NULL, NULL),
(63, 'Wuwei', 1, NULL, NULL),
(64, 'Yumen', 1, NULL, NULL),
(65, 'Zhangye', 1, NULL, NULL),
(66, 'Chaozhou', 1, NULL, NULL),
(67, 'Dongguan', 1, NULL, NULL),
(68, 'Enping', 1, NULL, NULL),
(69, 'Foshan', 1, NULL, NULL),
(70, 'Gaozhou', 1, NULL, NULL),
(71, 'Guangzhou', 1, NULL, NULL),
(72, 'Heshan', 1, NULL, NULL),
(73, 'Heyuan', 1, NULL, NULL),
(74, 'Huazhou', 1, NULL, NULL),
(75, 'Huizhou', 1, NULL, NULL),
(76, 'Jiangmen', 1, NULL, NULL),
(77, 'Jieyang', 1, NULL, NULL),
(78, 'Kaiping', 1, NULL, NULL),
(79, 'Lechang', 1, NULL, NULL),
(80, 'Leizhou', 1, NULL, NULL),
(81, 'Lianjiang', 1, NULL, NULL),
(82, 'Lianzhou', 1, NULL, NULL),
(83, 'Lufeng', 1, NULL, NULL),
(84, 'Luoding', 1, NULL, NULL),
(85, 'Maoming', 1, NULL, NULL),
(86, 'Meizhou', 1, NULL, NULL),
(87, 'Nanxiong', 1, NULL, NULL),
(88, 'Puning', 1, NULL, NULL),
(89, 'Qingyuan', 1, NULL, NULL),
(90, 'Shantou', 1, NULL, NULL),
(91, 'Shanwei', 1, NULL, NULL),
(92, 'Shaoguan', 1, NULL, NULL),
(93, 'Shenzhen', 1, NULL, NULL),
(94, 'Sihui', 1, NULL, NULL),
(95, 'Taishan', 1, NULL, NULL),
(96, 'Wuchuan', 1, NULL, NULL),
(97, 'Xingning', 1, NULL, NULL),
(98, 'Xinyi', 1, NULL, NULL),
(99, 'Yangchun', 1, NULL, NULL),
(100, 'Yangjiang', 1, NULL, NULL),
(101, 'Yingde', 1, NULL, NULL),
(102, 'Yunfu', 1, NULL, NULL),
(103, 'Zhanjiang', 1, NULL, NULL),
(104, 'Zhaoqing', 1, NULL, NULL),
(105, 'Zhongshan', 1, NULL, NULL),
(106, 'Zhuhai', 1, NULL, NULL),
(107, 'Baise', 1, NULL, NULL),
(108, 'Beihai', 1, NULL, NULL),
(109, 'Beiliu', 1, NULL, NULL),
(110, 'Cenxi', 1, NULL, NULL),
(111, 'Chongzuo', 1, NULL, NULL),
(112, 'Dongxing', 1, NULL, NULL),
(113, 'Fangchenggang', 1, NULL, NULL),
(114, 'Guigang', 1, NULL, NULL),
(115, 'Guilin', 1, NULL, NULL),
(116, 'Guiping', 1, NULL, NULL),
(117, 'Hechi', 1, NULL, NULL),
(118, 'Heshan', 1, NULL, NULL),
(119, 'Hezhou', 1, NULL, NULL),
(120, 'Jingxi', 1, NULL, NULL),
(121, 'Laibin', 1, NULL, NULL),
(122, 'Liuzhou', 1, NULL, NULL),
(123, 'Nanning', 1, NULL, NULL),
(124, 'Pingxiang', 1, NULL, NULL),
(125, 'Qinzhou', 1, NULL, NULL),
(126, 'Wuzhou', 1, NULL, NULL),
(127, 'Yulin', 1, NULL, NULL),
(128, 'Anshun', 1, NULL, NULL),
(129, 'Bijie', 1, NULL, NULL),
(130, 'Chishui', 1, NULL, NULL),
(131, 'Duyun', 1, NULL, NULL),
(132, 'Fuquan', 1, NULL, NULL),
(133, 'Guiyang', 1, NULL, NULL),
(134, 'Kaili', 1, NULL, NULL),
(135, 'Liupanshui', 1, NULL, NULL),
(136, 'Panzhou', 1, NULL, NULL),
(137, 'Qingzhen', 1, NULL, NULL),
(138, 'Renhuai', 1, NULL, NULL),
(139, 'Tongren', 1, NULL, NULL),
(140, 'Xingyi', 1, NULL, NULL),
(141, 'Zunyi', 1, NULL, NULL),
(142, 'Danzhou', 1, NULL, NULL),
(143, 'Dongfang', 1, NULL, NULL),
(144, 'Haikou', 1, NULL, NULL),
(145, 'Qionghai', 1, NULL, NULL),
(146, 'Sansha', 1, NULL, NULL),
(147, 'Sanya', 1, NULL, NULL),
(148, 'Wanning', 1, NULL, NULL),
(149, 'Wenchang', 1, NULL, NULL),
(150, 'Wuzhishan', 1, NULL, NULL),
(151, 'Anguo', 1, NULL, NULL),
(152, 'Baoding', 1, NULL, NULL),
(153, 'Bazhou', 1, NULL, NULL),
(154, 'Botou', 1, NULL, NULL),
(155, 'Cangzhou', 1, NULL, NULL),
(156, 'Chengde', 1, NULL, NULL),
(157, 'Dingzhou', 1, NULL, NULL),
(158, 'Gaobeidian', 1, NULL, NULL),
(159, 'Handan', 1, NULL, NULL),
(160, 'Hengshui', 1, NULL, NULL),
(161, 'Hejian', 1, NULL, NULL),
(162, 'Huanghua', 1, NULL, NULL),
(163, 'Jinzhou', 1, NULL, NULL),
(164, 'Langfang', 1, NULL, NULL),
(165, 'Nangong', 1, NULL, NULL),
(166, 'Pingquan', 1, NULL, NULL),
(167, 'Qian\'an', 1, NULL, NULL),
(168, 'Qinhuangdao', 1, NULL, NULL),
(169, 'Renqiu', 1, NULL, NULL),
(170, 'Sanhe', 1, NULL, NULL),
(171, 'Shahe', 1, NULL, NULL),
(172, 'Shenzhou', 1, NULL, NULL),
(173, 'Shijiazhuang', 1, NULL, NULL),
(174, 'Tangshan', 1, NULL, NULL),
(175, 'Xinji', 1, NULL, NULL),
(176, 'Wu\'an', 1, NULL, NULL),
(177, 'Xingtai', 1, NULL, NULL),
(178, 'Xinle', 1, NULL, NULL),
(179, 'Zhangjiakou', 1, NULL, NULL),
(180, 'Zhuozhou', 1, NULL, NULL),
(181, 'Zunhua', 1, NULL, NULL),
(182, 'Anda', 1, NULL, NULL),
(183, 'Bei\'an', 1, NULL, NULL),
(184, 'Daqing', 1, NULL, NULL),
(185, 'Dongning', 1, NULL, NULL),
(186, 'Fujin', 1, NULL, NULL),
(187, 'Fuyuan', 1, NULL, NULL),
(188, 'Hailin', 1, NULL, NULL),
(189, 'Hailun', 1, NULL, NULL),
(190, 'Harbin', 1, NULL, NULL),
(191, 'Hegang', 1, NULL, NULL),
(192, 'Heihe', 1, NULL, NULL),
(193, 'Hulin', 1, NULL, NULL),
(194, 'Jiamusi', 1, NULL, NULL),
(195, 'Jixi', 1, NULL, NULL),
(196, 'Mishan', 1, NULL, NULL),
(197, 'Mohe', 1, NULL, NULL),
(198, 'Mudanjiang', 1, NULL, NULL),
(199, 'Muling', 1, NULL, NULL),
(200, 'Nehe', 1, NULL, NULL),
(201, 'Ning\'an', 1, NULL, NULL),
(202, 'Qiqihar', 1, NULL, NULL),
(203, 'Qitaihe', 1, NULL, NULL),
(204, 'Shangzhi', 1, NULL, NULL),
(205, 'Shuangyashan', 1, NULL, NULL),
(206, 'Suifenhe', 1, NULL, NULL),
(207, 'Suihua', 1, NULL, NULL),
(208, 'Tieli', 1, NULL, NULL),
(209, 'Tongjiang', 1, NULL, NULL),
(210, 'Wuchang', 1, NULL, NULL),
(211, 'Wudalianchi', 1, NULL, NULL),
(212, 'Yichun', 1, NULL, NULL),
(213, 'Zhaodong', 1, NULL, NULL),
(214, 'Anyang', 1, NULL, NULL),
(215, 'Changge', 1, NULL, NULL),
(216, 'Dengfeng', 1, NULL, NULL),
(217, 'Dengzhou', 1, NULL, NULL),
(218, 'Gongyi', 1, NULL, NULL),
(219, 'Hebi', 1, NULL, NULL),
(220, 'Huixian', 1, NULL, NULL),
(221, 'Jiaozuo', 1, NULL, NULL),
(222, 'Jiyuan', 1, NULL, NULL),
(223, 'Kaifeng', 1, NULL, NULL),
(224, 'Lingbao', 1, NULL, NULL),
(225, 'Linzhou', 1, NULL, NULL),
(226, 'Luohe', 1, NULL, NULL),
(227, 'Luoyang', 1, NULL, NULL),
(228, 'Mengzhou', 1, NULL, NULL),
(229, 'Nanyang', 1, NULL, NULL),
(230, 'Pingdingshan', 1, NULL, NULL),
(231, 'Puyang', 1, NULL, NULL),
(232, 'Qinyang', 1, NULL, NULL),
(233, 'Ruzhou', 1, NULL, NULL),
(234, 'Sanmenxia', 1, NULL, NULL),
(235, 'Shangqiu', 1, NULL, NULL),
(236, 'Weihui', 1, NULL, NULL),
(237, 'Wugang', 1, NULL, NULL),
(238, 'Xiangcheng', 1, NULL, NULL),
(239, 'Xingyang', 1, NULL, NULL),
(240, 'Xinmi', 1, NULL, NULL),
(241, 'Xinxiang', 1, NULL, NULL),
(242, 'Xinyang', 1, NULL, NULL),
(243, 'Xinzheng', 1, NULL, NULL),
(244, 'Xuchang', 1, NULL, NULL),
(245, 'Yanshi', 1, NULL, NULL),
(246, 'Yima', 1, NULL, NULL),
(247, 'Yongcheng', 1, NULL, NULL),
(248, 'Yuzhou', 1, NULL, NULL),
(249, 'Zhengzhou', 1, NULL, NULL),
(250, 'Zhoukou', 1, NULL, NULL),
(251, 'Zhumadian', 1, NULL, NULL),
(252, 'Anlu', 1, NULL, NULL),
(253, 'Chibi', 1, NULL, NULL),
(254, 'Dangyang', 1, NULL, NULL),
(255, 'Danjiangkou', 1, NULL, NULL),
(256, 'Daye', 1, NULL, NULL),
(257, 'Enshi', 1, NULL, NULL),
(258, 'Ezhou', 1, NULL, NULL),
(259, 'Guangshui', 1, NULL, NULL),
(260, 'Hanchuan', 1, NULL, NULL),
(261, 'Honghu', 1, NULL, NULL),
(262, 'Huanggang', 1, NULL, NULL),
(263, 'Huangshi', 1, NULL, NULL),
(264, 'Jingmen', 1, NULL, NULL),
(265, 'Jingshan', 1, NULL, NULL),
(266, 'Jingzhou', 1, NULL, NULL),
(267, 'Laohekou', 1, NULL, NULL),
(268, 'Lichuan', 1, NULL, NULL),
(269, 'Macheng', 1, NULL, NULL),
(270, 'Qianjiang', 1, NULL, NULL),
(271, 'Shishou', 1, NULL, NULL),
(272, 'Shiyan', 1, NULL, NULL),
(273, 'Suizhou', 1, NULL, NULL),
(274, 'Songzi', 1, NULL, NULL),
(275, 'Tianmen', 1, NULL, NULL),
(276, 'Wuhan', 1, NULL, NULL),
(277, 'Wuxue', 1, NULL, NULL),
(278, 'Xiangyang', 1, NULL, NULL),
(279, 'Xianning', 1, NULL, NULL),
(280, 'Xiantao', 1, NULL, NULL),
(281, 'Xiaogan', 1, NULL, NULL),
(282, 'Yichang', 1, NULL, NULL),
(283, 'Yicheng', 1, NULL, NULL),
(284, 'Yidu', 1, NULL, NULL),
(285, 'Yingcheng', 1, NULL, NULL),
(286, 'Zaoyang', 1, NULL, NULL),
(287, 'Zhijiang', 1, NULL, NULL),
(288, 'Zhongxiang', 1, NULL, NULL),
(289, 'Changde', 1, NULL, NULL),
(290, 'Changning', 1, NULL, NULL),
(291, 'Changsha', 1, NULL, NULL),
(292, 'Chenzhou', 1, NULL, NULL),
(293, 'Hengyang', 1, NULL, NULL),
(294, 'Hongjiang', 1, NULL, NULL),
(295, 'Huaihua', 1, NULL, NULL),
(296, 'Jinshi', 1, NULL, NULL),
(297, 'Jishou', 1, NULL, NULL),
(298, 'Leiyang', 1, NULL, NULL),
(299, 'Lengshuijiang', 1, NULL, NULL),
(300, 'Lianyuan', 1, NULL, NULL),
(301, 'Liling', 1, NULL, NULL),
(302, 'Linxiang', 1, NULL, NULL),
(303, 'Liuyang', 1, NULL, NULL),
(304, 'Loudi', 1, NULL, NULL),
(305, 'Miluo', 1, NULL, NULL),
(306, 'Ningxiang', 1, NULL, NULL),
(307, 'Shaoshan', 1, NULL, NULL),
(308, 'Shaoyang', 1, NULL, NULL),
(309, 'Wugang', 1, NULL, NULL),
(310, 'Xiangtan', 1, NULL, NULL),
(311, 'Xiangxiang', 1, NULL, NULL),
(312, 'Yiyang', 1, NULL, NULL),
(313, 'Yongzhou', 1, NULL, NULL),
(314, 'Yuanjiang', 1, NULL, NULL),
(315, 'Yueyang', 1, NULL, NULL),
(316, 'Zhangjiajie', 1, NULL, NULL),
(317, 'Zhuzhou', 1, NULL, NULL),
(318, 'Zixing', 1, NULL, NULL),
(319, 'Arxan', 1, NULL, NULL),
(320, 'Baotou', 1, NULL, NULL),
(321, 'Bayannur', 1, NULL, NULL),
(322, 'Chifeng', 1, NULL, NULL),
(323, 'Erenhot', 1, NULL, NULL),
(324, 'Ergun', 1, NULL, NULL),
(325, 'Fengzhen', 1, NULL, NULL),
(326, 'Genhe', 1, NULL, NULL),
(327, 'Hohhot', 1, NULL, NULL),
(328, 'Holingol', 1, NULL, NULL),
(329, 'Hulunbuir', 1, NULL, NULL),
(330, 'Manzhouli', 1, NULL, NULL),
(331, 'Ordos', 1, NULL, NULL),
(332, 'Tongliao', 1, NULL, NULL),
(333, 'Ulanhot', 1, NULL, NULL),
(334, 'Ulanqab', 1, NULL, NULL),
(335, 'Wuhai', 1, NULL, NULL),
(336, 'Xilinhot', 1, NULL, NULL),
(337, 'Yakeshi', 1, NULL, NULL),
(338, 'Zhalantun', 1, NULL, NULL),
(339, 'Changshu', 1, NULL, NULL),
(340, 'Changzhou', 1, NULL, NULL),
(341, 'Danyang', 1, NULL, NULL),
(342, 'Dongtai', 1, NULL, NULL),
(343, 'Gaoyou', 1, NULL, NULL),
(344, 'Hai\'an', 1, NULL, NULL),
(345, 'Haimen', 1, NULL, NULL),
(346, 'Huai\'an', 1, NULL, NULL),
(347, 'Jiangyin', 1, NULL, NULL),
(348, 'Jingjiang', 1, NULL, NULL),
(349, 'Jurong', 1, NULL, NULL),
(350, 'Liyang', 1, NULL, NULL),
(351, 'Lianyungang', 1, NULL, NULL),
(352, 'Kunshan', 1, NULL, NULL),
(353, 'Nanjing', 1, NULL, NULL),
(354, 'Nantong', 1, NULL, NULL),
(355, 'Pizhou', 1, NULL, NULL),
(356, 'Qidong', 1, NULL, NULL),
(357, 'Rugao', 1, NULL, NULL),
(358, 'Suqian', 1, NULL, NULL),
(359, 'Suzhou', 1, NULL, NULL),
(360, 'Taicang', 1, NULL, NULL),
(361, 'Taixing', 1, NULL, NULL),
(362, 'Taizhou', 1, NULL, NULL),
(363, 'Wuxi', 1, NULL, NULL),
(364, 'Xinghua', 1, NULL, NULL),
(365, 'Xinyi', 1, NULL, NULL),
(366, 'Xuzhou', 1, NULL, NULL),
(367, 'Yancheng', 1, NULL, NULL),
(368, 'Yangzhong', 1, NULL, NULL),
(369, 'Yangzhou', 1, NULL, NULL),
(370, 'Yixing', 1, NULL, NULL),
(371, 'Yizheng', 1, NULL, NULL),
(372, 'Zhangjiagang', 1, NULL, NULL),
(373, 'Zhenjiang', 1, NULL, NULL),
(374, 'Dexing', 1, NULL, NULL),
(375, 'Fengcheng', 1, NULL, NULL),
(376, 'Fuzhou', 1, NULL, NULL),
(377, 'Ganzhou', 1, NULL, NULL),
(378, 'Gao\'an', 1, NULL, NULL),
(379, 'Gongqingcheng', 1, NULL, NULL),
(380, 'Guixi', 1, NULL, NULL),
(381, 'Ji\'an', 1, NULL, NULL),
(382, 'Jingdezhen', 1, NULL, NULL),
(383, 'Jinggangshan', 1, NULL, NULL),
(384, 'Jiujiang', 1, NULL, NULL),
(385, 'Leping', 1, NULL, NULL),
(386, 'Lushan', 1, NULL, NULL),
(387, 'Nanchang', 1, NULL, NULL),
(388, 'Pingxiang', 1, NULL, NULL),
(389, 'Ruichang', 1, NULL, NULL),
(390, 'Ruijin', 1, NULL, NULL),
(391, 'Shangrao', 1, NULL, NULL),
(392, 'Xinyu', 1, NULL, NULL),
(393, 'Yichun', 1, NULL, NULL),
(394, 'Yingtan', 1, NULL, NULL),
(395, 'Zhangshu', 1, NULL, NULL),
(396, 'Baicheng', 1, NULL, NULL),
(397, 'Baishan', 1, NULL, NULL),
(398, 'Changchun', 1, NULL, NULL),
(399, 'Da\'an', 1, NULL, NULL),
(400, 'Dehui', 1, NULL, NULL),
(401, 'Dunhua', 1, NULL, NULL),
(402, 'Fuyu', 1, NULL, NULL),
(403, 'Gongzhuling', 1, NULL, NULL),
(404, 'Helong', 1, NULL, NULL),
(405, 'Huadian', 1, NULL, NULL),
(406, 'Hunchun', 1, NULL, NULL),
(407, 'Ji\'an', 1, NULL, NULL),
(408, 'Jiaohe', 1, NULL, NULL),
(409, 'Jilin', 1, NULL, NULL),
(410, 'Liaoyuan', 1, NULL, NULL),
(411, 'Linjiang', 1, NULL, NULL),
(412, 'Longjing', 1, NULL, NULL),
(413, 'Meihekou', 1, NULL, NULL),
(414, 'Panshi', 1, NULL, NULL),
(415, 'Shuangliao', 1, NULL, NULL),
(416, 'Shulan', 1, NULL, NULL),
(417, 'Siping', 1, NULL, NULL),
(418, 'Songyuan', 1, NULL, NULL),
(419, 'Taonan', 1, NULL, NULL),
(420, 'Tonghua', 1, NULL, NULL),
(421, 'Tumen', 1, NULL, NULL),
(422, 'Yanji', 1, NULL, NULL),
(423, 'Yushu', 1, NULL, NULL),
(424, 'Anshan', 1, NULL, NULL),
(425, 'Benxi', 1, NULL, NULL),
(426, 'Beipiao', 1, NULL, NULL),
(427, 'Beizhen', 1, NULL, NULL),
(428, 'Chaoyang', 1, NULL, NULL),
(429, 'Dalian', 1, NULL, NULL),
(430, 'Dandong', 1, NULL, NULL),
(431, 'Dashiqiao', 1, NULL, NULL),
(432, 'Dengta', 1, NULL, NULL),
(433, 'Diaobingshan', 1, NULL, NULL),
(434, 'Donggang', 1, NULL, NULL),
(435, 'Fengcheng', 1, NULL, NULL),
(436, 'Fushun', 1, NULL, NULL),
(437, 'Fuxin', 1, NULL, NULL),
(438, 'Gaizhou', 1, NULL, NULL),
(439, 'Haicheng', 1, NULL, NULL),
(440, 'Huludao', 1, NULL, NULL),
(441, 'Jinzhou', 1, NULL, NULL),
(442, 'Kaiyuan', 1, NULL, NULL),
(443, 'Liaoyang', 1, NULL, NULL),
(444, 'Linghai', 1, NULL, NULL),
(445, 'Lingyuan', 1, NULL, NULL),
(446, 'Panjin', 1, NULL, NULL),
(447, 'Shenyang', 1, NULL, NULL),
(448, 'Tieling', 1, NULL, NULL),
(449, 'Wafangdian', 1, NULL, NULL),
(450, 'Xingcheng', 1, NULL, NULL),
(451, 'Xinmin', 1, NULL, NULL),
(452, 'Yingkou', 1, NULL, NULL),
(453, 'Zhuanghe', 1, NULL, NULL),
(454, 'Guyuan', 1, NULL, NULL),
(455, 'Lingwu', 1, NULL, NULL),
(456, 'Qingtongxia', 1, NULL, NULL),
(457, 'Shizuishan', 1, NULL, NULL),
(458, 'Wuzhong', 1, NULL, NULL),
(459, 'Yinchuan', 1, NULL, NULL),
(460, 'Zhongwei', 1, NULL, NULL),
(461, 'Delingha', 1, NULL, NULL),
(462, 'Golmud', 1, NULL, NULL),
(463, 'Haidong', 1, NULL, NULL),
(464, 'Xining', 1, NULL, NULL),
(465, 'Yushu', 1, NULL, NULL),
(466, 'Ankang', 1, NULL, NULL),
(467, 'Baoji', 1, NULL, NULL),
(468, 'Binzhou', 1, NULL, NULL),
(469, 'Hancheng', 1, NULL, NULL),
(470, 'Hanzhong', 1, NULL, NULL),
(471, 'Huayin', 1, NULL, NULL),
(472, 'Shangluo', 1, NULL, NULL),
(473, 'Shenmu', 1, NULL, NULL),
(474, 'Tongchuan', 1, NULL, NULL),
(475, 'Weinan', 1, NULL, NULL),
(476, 'Xi\'an', 1, NULL, NULL),
(477, 'Xianyang', 1, NULL, NULL),
(478, 'Xingping', 1, NULL, NULL),
(479, 'Yan\'an', 1, NULL, NULL),
(480, 'Yulin', 1, NULL, NULL),
(481, 'Anqiu', 1, NULL, NULL),
(482, 'Binzhou', 1, NULL, NULL),
(483, 'Changyi', 1, NULL, NULL),
(484, 'Dezhou', 1, NULL, NULL),
(485, 'Dongying', 1, NULL, NULL),
(486, 'Feicheng', 1, NULL, NULL),
(487, 'Gaomi', 1, NULL, NULL),
(488, 'Haiyang', 1, NULL, NULL),
(489, 'Heze', 1, NULL, NULL),
(490, 'Jiaozhou', 1, NULL, NULL),
(491, 'Jinan', 1, NULL, NULL),
(492, 'Jining', 1, NULL, NULL),
(493, 'Laiwu', 1, NULL, NULL),
(494, 'Laixi', 1, NULL, NULL),
(495, 'Laiyang', 1, NULL, NULL),
(496, 'Laizhou', 1, NULL, NULL),
(497, 'Leling', 1, NULL, NULL),
(498, 'Liaocheng', 1, NULL, NULL),
(499, 'Linqing', 1, NULL, NULL),
(500, 'Linyi', 1, NULL, NULL),
(501, 'Longkou', 1, NULL, NULL),
(502, 'Penglai', 1, NULL, NULL),
(503, 'Pingdu', 1, NULL, NULL),
(504, 'Qingdao', 1, NULL, NULL),
(505, 'Qingzhou', 1, NULL, NULL),
(506, 'Qixia', 1, NULL, NULL),
(507, 'Qufu', 1, NULL, NULL),
(508, 'Rizhao', 1, NULL, NULL),
(509, 'Rongcheng', 1, NULL, NULL),
(510, 'Rushan', 1, NULL, NULL),
(511, 'Shouguang', 1, NULL, NULL),
(512, 'Tai\'an', 1, NULL, NULL),
(513, 'Tengzhou', 1, NULL, NULL),
(514, 'Weifang', 1, NULL, NULL),
(515, 'Weihai', 1, NULL, NULL),
(516, 'Xintai', 1, NULL, NULL),
(517, 'Yantai', 1, NULL, NULL),
(518, 'Yucheng', 1, NULL, NULL),
(519, 'Zaozhuang', 1, NULL, NULL),
(520, 'Zhaoyuan', 1, NULL, NULL),
(521, 'Zhucheng', 1, NULL, NULL),
(522, 'Zibo', 1, NULL, NULL),
(523, 'Zoucheng', 1, NULL, NULL),
(524, 'Changzhi', 1, NULL, NULL),
(525, 'Datong', 1, NULL, NULL),
(526, 'Fenyang', 1, NULL, NULL),
(527, 'Gaoping', 1, NULL, NULL),
(528, 'Gujiao', 1, NULL, NULL),
(529, 'Hejin', 1, NULL, NULL),
(530, 'Houma', 1, NULL, NULL),
(531, 'Huozhou', 1, NULL, NULL),
(532, 'Jiexiu', 1, NULL, NULL),
(533, 'Jincheng', 1, NULL, NULL),
(534, 'Jinzhong', 1, NULL, NULL),
(535, 'Linfen', 1, NULL, NULL),
(536, 'Lucheng', 1, NULL, NULL),
(537, 'L', 1, NULL, NULL),
(538, 'Shuozhou', 1, NULL, NULL),
(539, 'Taiyuan', 1, NULL, NULL),
(540, 'Xiaoyi', 1, NULL, NULL),
(541, 'Xinzhou', 1, NULL, NULL),
(542, 'Yangquan', 1, NULL, NULL),
(543, 'Yongji', 1, NULL, NULL),
(544, 'Yuncheng', 1, NULL, NULL),
(545, 'Yuanping', 1, NULL, NULL),
(546, 'Barkam', 1, NULL, NULL),
(547, 'Bazhong', 1, NULL, NULL),
(548, 'Chengdu', 1, NULL, NULL),
(549, 'Chongzhou', 1, NULL, NULL),
(550, 'Dazhou', 1, NULL, NULL),
(551, 'Deyang', 1, NULL, NULL),
(552, 'Dujiangyan', 1, NULL, NULL),
(553, 'Emeishan', 1, NULL, NULL),
(554, 'Guang\'an', 1, NULL, NULL),
(555, 'Guanghan', 1, NULL, NULL),
(556, 'Guangyuan', 1, NULL, NULL),
(557, 'Huaying', 1, NULL, NULL),
(558, 'Jiangyou', 1, NULL, NULL),
(559, 'Jianyang', 1, NULL, NULL),
(560, 'Kangding', 1, NULL, NULL),
(561, 'Langzhong', 1, NULL, NULL),
(562, 'Leshan', 1, NULL, NULL),
(563, 'Longchang', 1, NULL, NULL),
(564, 'Luzhou', 1, NULL, NULL),
(565, 'Mianzhu', 1, NULL, NULL),
(566, 'Meishan', 1, NULL, NULL),
(567, 'Mianyang', 1, NULL, NULL),
(568, 'Nanchong', 1, NULL, NULL),
(569, 'Neijiang', 1, NULL, NULL),
(570, 'Panzhihua', 1, NULL, NULL),
(571, 'Pengzhou', 1, NULL, NULL),
(572, 'Qionglai', 1, NULL, NULL),
(573, 'Shifang', 1, NULL, NULL),
(574, 'Suining', 1, NULL, NULL),
(575, 'Wanyuan', 1, NULL, NULL),
(576, 'Xichang', 1, NULL, NULL),
(577, 'Ya\'an', 1, NULL, NULL),
(578, 'Yibin', 1, NULL, NULL),
(579, 'Zigong', 1, NULL, NULL),
(580, 'Ziyang', 1, NULL, NULL),
(581, 'Lhasa', 1, NULL, NULL),
(582, 'Nagqu', 1, NULL, NULL),
(583, 'Nyingchi', 1, NULL, NULL),
(584, 'Qamdo', 1, NULL, NULL),
(585, 'Shannan', 1, NULL, NULL),
(586, 'Xigaz', 1, NULL, NULL),
(587, 'Aksu', 1, NULL, NULL),
(588, 'Alashankou', 1, NULL, NULL),
(589, 'Altay', 1, NULL, NULL),
(590, 'Aral', 1, NULL, NULL),
(591, 'Artux', 1, NULL, NULL),
(592, 'Beitun', 1, NULL, NULL),
(593, 'Bole', 1, NULL, NULL),
(594, 'Changji', 1, NULL, NULL),
(595, 'Fukang', 1, NULL, NULL),
(596, 'Hami', 1, NULL, NULL),
(597, 'Hotan', 1, NULL, NULL),
(598, 'Karamay', 1, NULL, NULL),
(599, 'Kashgar', 1, NULL, NULL),
(600, 'Khorgas', 1, NULL, NULL),
(601, 'Kokdala', 1, NULL, NULL),
(602, 'Korla', 1, NULL, NULL),
(603, 'Kuytun', 1, NULL, NULL),
(604, 'Kunyu', 1, NULL, NULL),
(605, 'Shihezi', 1, NULL, NULL),
(606, 'Shuanghe', 1, NULL, NULL),
(607, 'Tacheng', 1, NULL, NULL),
(608, 'Tiemenguan', 1, NULL, NULL),
(609, 'Tumxuk', 1, NULL, NULL),
(610, 'Turpan', 1, NULL, NULL),
(611, 'Ürümqi', 1, NULL, NULL),
(612, 'Wujiaqu', 1, NULL, NULL),
(613, 'Wusu', 1, NULL, NULL),
(614, 'Yining', 1, NULL, NULL),
(615, 'Anning', 1, NULL, NULL),
(616, 'Baoshan', 1, NULL, NULL),
(617, 'Chuxiong', 1, NULL, NULL),
(618, 'Dali', 1, NULL, NULL),
(619, 'Gejiu', 1, NULL, NULL),
(620, 'Jinghong', 1, NULL, NULL),
(621, 'Kaiyuan', 1, NULL, NULL),
(622, 'Kunming', 1, NULL, NULL),
(623, 'Lincang', 1, NULL, NULL),
(624, 'Lijiang', 1, NULL, NULL),
(625, 'Lushui', 1, NULL, NULL),
(626, 'Mang', 1, NULL, NULL),
(627, 'Mengzi', 1, NULL, NULL),
(628, 'Mile', 1, NULL, NULL),
(629, 'Pu\'er', 1, NULL, NULL),
(630, 'Qujing', 1, NULL, NULL),
(631, 'Ruili', 1, NULL, NULL),
(632, 'Shangri', 1, NULL, NULL),
(633, 'Tengchong', 1, NULL, NULL),
(634, 'Wenshan', 1, NULL, NULL),
(635, 'Xuanwei', 1, NULL, NULL),
(636, 'Yuxi', 1, NULL, NULL),
(637, 'Zhaotong', 1, NULL, NULL),
(638, 'Cixi', 1, NULL, NULL),
(639, 'Dongyang', 1, NULL, NULL),
(640, 'Haining', 1, NULL, NULL),
(641, 'Hangzhou', 1, NULL, NULL),
(642, 'Huzhou', 1, NULL, NULL),
(643, 'Jiande', 1, NULL, NULL),
(644, 'Jiangshan', 1, NULL, NULL),
(645, 'Jiaxing', 1, NULL, NULL),
(646, 'Jinhua', 1, NULL, NULL),
(647, 'Lanxi', 1, NULL, NULL),
(648, 'Linhai', 1, NULL, NULL),
(649, 'Lishui', 1, NULL, NULL),
(650, 'Longquan', 1, NULL, NULL),
(651, 'Ningbo', 1, NULL, NULL),
(652, 'Pinghu', 1, NULL, NULL),
(653, 'Quzhou', 1, NULL, NULL),
(654, 'Ruian', 1, NULL, NULL),
(655, 'Shaoxing', 1, NULL, NULL),
(656, 'Shengzhou', 1, NULL, NULL),
(657, 'Taizhou', 1, NULL, NULL),
(658, 'Tongxiang', 1, NULL, NULL),
(659, 'Wenling', 1, NULL, NULL),
(660, 'Wenzhou', 1, NULL, NULL),
(661, 'Yiwu', 1, NULL, NULL),
(662, 'Yongkang', 1, NULL, NULL),
(663, 'Yueqing', 1, NULL, NULL),
(664, 'Yuhuan', 1, NULL, NULL),
(665, 'Yuyao', 1, NULL, NULL),
(666, 'Zhoushan', 1, NULL, NULL),
(667, 'Zhuji', 1, NULL, NULL),
(668, 'ADAMS', 2, NULL, NULL),
(669, 'BACARRA', 2, NULL, NULL),
(670, 'BADOC', 2, NULL, NULL),
(671, 'BANGUI', 2, NULL, NULL),
(672, 'CITY OF BATAC', 2, NULL, NULL),
(673, 'BURGOS', 2, NULL, NULL),
(674, 'CARASI', 2, NULL, NULL),
(675, 'CURRIMAO', 2, NULL, NULL),
(676, 'DINGRAS', 2, NULL, NULL),
(677, 'DUMALNEG', 2, NULL, NULL),
(678, 'BANNA (ESPIRITU)', 2, NULL, NULL),
(679, 'LAOAG CITY (Capital)', 2, NULL, NULL),
(680, 'MARCOS', 2, NULL, NULL),
(681, 'NUEVA ERA', 2, NULL, NULL),
(682, 'PAGUDPUD', 2, NULL, NULL),
(683, 'PAOAY', 2, NULL, NULL),
(684, 'PASUQUIN', 2, NULL, NULL),
(685, 'PIDDIG', 2, NULL, NULL),
(686, 'PINILI', 2, NULL, NULL),
(687, 'SAN NICOLAS', 2, NULL, NULL),
(688, 'SARRAT', 2, NULL, NULL),
(689, 'SOLSONA', 2, NULL, NULL),
(690, 'VINTAR', 2, NULL, NULL),
(691, 'ALILEM', 2, NULL, NULL),
(692, 'BANAYOYO', 2, NULL, NULL),
(693, 'BANTAY', 2, NULL, NULL),
(694, 'BURGOS', 2, NULL, NULL),
(695, 'CABUGAO', 2, NULL, NULL),
(696, 'CITY OF CANDON', 2, NULL, NULL),
(697, 'CAOAYAN', 2, NULL, NULL),
(698, 'CERVANTES', 2, NULL, NULL),
(699, 'GALIMUYOD', 2, NULL, NULL),
(700, 'GREGORIO DEL PILAR (CONCEPCION)', 2, NULL, NULL),
(701, 'LIDLIDDA', 2, NULL, NULL),
(702, 'MAGSINGAL', 2, NULL, NULL),
(703, 'NAGBUKEL', 2, NULL, NULL),
(704, 'NARVACAN', 2, NULL, NULL),
(705, 'QUIRINO (ANGKAKI)', 2, NULL, NULL),
(706, 'SALCEDO (BAUGEN)', 2, NULL, NULL),
(707, 'SAN EMILIO', 2, NULL, NULL),
(708, 'SAN ESTEBAN', 2, NULL, NULL),
(709, 'SAN ILDEFONSO', 2, NULL, NULL),
(710, 'SAN JUAN (LAPOG)', 2, NULL, NULL),
(711, 'SAN VICENTE', 2, NULL, NULL),
(712, 'SANTA', 2, NULL, NULL),
(713, 'SANTA CATALINA', 2, NULL, NULL),
(714, 'SANTA CRUZ', 2, NULL, NULL),
(715, 'SANTA LUCIA', 2, NULL, NULL),
(716, 'SANTA MARIA', 2, NULL, NULL),
(717, 'SANTIAGO', 2, NULL, NULL),
(718, 'SANTO DOMINGO', 2, NULL, NULL),
(719, 'SIGAY', 2, NULL, NULL),
(720, 'SINAIT', 2, NULL, NULL),
(721, 'SUGPON', 2, NULL, NULL),
(722, 'SUYO', 2, NULL, NULL),
(723, 'TAGUDIN', 2, NULL, NULL),
(724, 'CITY OF VIGAN (Capital)', 2, NULL, NULL),
(725, 'AGOO', 2, NULL, NULL),
(726, 'ARINGAY', 2, NULL, NULL),
(727, 'BACNOTAN', 2, NULL, NULL),
(728, 'BAGULIN', 2, NULL, NULL),
(729, 'BALAOAN', 2, NULL, NULL),
(730, 'BANGAR', 2, NULL, NULL),
(731, 'BAUANG', 2, NULL, NULL),
(732, 'BURGOS', 2, NULL, NULL),
(733, 'CABA', 2, NULL, NULL),
(734, 'LUNA', 2, NULL, NULL),
(735, 'NAGUILIAN', 2, NULL, NULL),
(736, 'PUGO', 2, NULL, NULL),
(737, 'ROSARIO', 2, NULL, NULL),
(738, 'CITY OF SAN FERNANDO (Capital)', 2, NULL, NULL),
(739, 'SAN GABRIEL', 2, NULL, NULL),
(740, 'SAN JUAN', 2, NULL, NULL),
(741, 'SANTO TOMAS', 2, NULL, NULL),
(742, 'SANTOL', 2, NULL, NULL),
(743, 'SUDIPEN', 2, NULL, NULL),
(744, 'TUBAO', 2, NULL, NULL),
(745, 'AGNO', 2, NULL, NULL),
(746, 'AGUILAR', 2, NULL, NULL),
(747, 'CITY OF ALAMINOS', 2, NULL, NULL),
(748, 'ALCALA', 2, NULL, NULL),
(749, 'ANDA', 2, NULL, NULL),
(750, 'ASINGAN', 2, NULL, NULL),
(751, 'BALUNGAO', 2, NULL, NULL),
(752, 'BANI', 2, NULL, NULL),
(753, 'BASISTA', 2, NULL, NULL),
(754, 'BAUTISTA', 2, NULL, NULL),
(755, 'BAYAMBANG', 2, NULL, NULL),
(756, 'BINALONAN', 2, NULL, NULL),
(757, 'BINMALEY', 2, NULL, NULL),
(758, 'BOLINAO', 2, NULL, NULL),
(759, 'BUGALLON', 2, NULL, NULL),
(760, 'BURGOS', 2, NULL, NULL),
(761, 'CALASIAO', 2, NULL, NULL),
(762, 'DAGUPAN CITY', 2, NULL, NULL),
(763, 'DASOL', 2, NULL, NULL),
(764, 'INFANTA', 2, NULL, NULL),
(765, 'LABRADOR', 2, NULL, NULL),
(766, 'LINGAYEN (Capital)', 2, NULL, NULL),
(767, 'MABINI', 2, NULL, NULL),
(768, 'MALASIQUI', 2, NULL, NULL),
(769, 'MANAOAG', 2, NULL, NULL),
(770, 'MANGALDAN', 2, NULL, NULL),
(771, 'MANGATAREM', 2, NULL, NULL),
(772, 'MAPANDAN', 2, NULL, NULL),
(773, 'NATIVIDAD', 2, NULL, NULL),
(774, 'POZORRUBIO', 2, NULL, NULL),
(775, 'ROSALES', 2, NULL, NULL),
(776, 'SAN CARLOS CITY', 2, NULL, NULL),
(777, 'SAN FABIAN', 2, NULL, NULL),
(778, 'SAN JACINTO', 2, NULL, NULL),
(779, 'SAN MANUEL', 2, NULL, NULL),
(780, 'SAN NICOLAS', 2, NULL, NULL),
(781, 'SAN QUINTIN', 2, NULL, NULL),
(782, 'SANTA BARBARA', 2, NULL, NULL),
(783, 'SANTA MARIA', 2, NULL, NULL),
(784, 'SANTO TOMAS', 2, NULL, NULL),
(785, 'SISON', 2, NULL, NULL),
(786, 'SUAL', 2, NULL, NULL),
(787, 'TAYUG', 2, NULL, NULL),
(788, 'UMINGAN', 2, NULL, NULL),
(789, 'URBIZTONDO', 2, NULL, NULL),
(790, 'CITY OF URDANETA', 2, NULL, NULL),
(791, 'VILLASIS', 2, NULL, NULL),
(792, 'LAOAC', 2, NULL, NULL),
(793, 'BASCO (Capital)', 2, NULL, NULL),
(794, 'ITBAYAT', 2, NULL, NULL),
(795, 'IVANA', 2, NULL, NULL),
(796, 'MAHATAO', 2, NULL, NULL),
(797, 'SABTANG', 2, NULL, NULL),
(798, 'UYUGAN', 2, NULL, NULL),
(799, 'ABULUG', 2, NULL, NULL),
(800, 'ALCALA', 2, NULL, NULL),
(801, 'ALLACAPAN', 2, NULL, NULL),
(802, 'AMULUNG', 2, NULL, NULL),
(803, 'APARRI', 2, NULL, NULL),
(804, 'BAGGAO', 2, NULL, NULL),
(805, 'BALLESTEROS', 2, NULL, NULL),
(806, 'BUGUEY', 2, NULL, NULL),
(807, 'CALAYAN', 2, NULL, NULL),
(808, 'CAMALANIUGAN', 2, NULL, NULL),
(809, 'CLAVERIA', 2, NULL, NULL),
(810, 'ENRILE', 2, NULL, NULL),
(811, 'GATTARAN', 2, NULL, NULL),
(812, 'GONZAGA', 2, NULL, NULL),
(813, 'IGUIG', 2, NULL, NULL),
(814, 'LAL-LO', 2, NULL, NULL),
(815, 'LASAM', 2, NULL, NULL),
(816, 'PAMPLONA', 2, NULL, NULL),
(817, 'PEÑABLANCA', 2, NULL, NULL),
(818, 'PIAT', 2, NULL, NULL),
(819, 'RIZAL', 2, NULL, NULL),
(820, 'SANCHEZ-MIRA', 2, NULL, NULL),
(821, 'SANTA ANA', 2, NULL, NULL),
(822, 'SANTA PRAXEDES', 2, NULL, NULL),
(823, 'SANTA TERESITA', 2, NULL, NULL),
(824, 'SANTO NIÑO (FAIRE)', 2, NULL, NULL),
(825, 'SOLANA', 2, NULL, NULL),
(826, 'TUAO', 2, NULL, NULL),
(827, 'TUGUEGARAO CITY (Capital)', 2, NULL, NULL),
(828, 'ALICIA', 2, NULL, NULL),
(829, 'ANGADANAN', 2, NULL, NULL),
(830, 'AURORA', 2, NULL, NULL),
(831, 'BENITO SOLIVEN', 2, NULL, NULL),
(832, 'BURGOS', 2, NULL, NULL),
(833, 'CABAGAN', 2, NULL, NULL),
(834, 'CABATUAN', 2, NULL, NULL),
(835, 'CITY OF CAUAYAN', 2, NULL, NULL),
(836, 'CORDON', 2, NULL, NULL),
(837, 'DINAPIGUE', 2, NULL, NULL),
(838, 'DIVILACAN', 2, NULL, NULL),
(839, 'ECHAGUE', 2, NULL, NULL),
(840, 'GAMU', 2, NULL, NULL),
(841, 'ILAGAN CITY (Capital)', 2, NULL, NULL),
(842, 'JONES', 2, NULL, NULL),
(843, 'LUNA', 2, NULL, NULL),
(844, 'MACONACON', 2, NULL, NULL),
(845, 'DELFIN ALBANO (MAGSAYSAY)', 2, NULL, NULL),
(846, 'MALLIG', 2, NULL, NULL),
(847, 'NAGUILIAN', 2, NULL, NULL),
(848, 'PALANAN', 2, NULL, NULL),
(849, 'QUEZON', 2, NULL, NULL),
(850, 'QUIRINO', 2, NULL, NULL),
(851, 'RAMON', 2, NULL, NULL),
(852, 'REINA MERCEDES', 2, NULL, NULL),
(853, 'ROXAS', 2, NULL, NULL),
(854, 'SAN AGUSTIN', 2, NULL, NULL),
(855, 'SAN GUILLERMO', 2, NULL, NULL),
(856, 'SAN ISIDRO', 2, NULL, NULL),
(857, 'SAN MANUEL', 2, NULL, NULL),
(858, 'SAN MARIANO', 2, NULL, NULL),
(859, 'SAN MATEO', 2, NULL, NULL),
(860, 'SAN PABLO', 2, NULL, NULL),
(861, 'SANTA MARIA', 2, NULL, NULL),
(862, 'CITY OF SANTIAGO', 2, NULL, NULL),
(863, 'SANTO TOMAS', 2, NULL, NULL),
(864, 'TUMAUINI', 2, NULL, NULL),
(865, 'AMBAGUIO', 2, NULL, NULL),
(866, 'ARITAO', 2, NULL, NULL),
(867, 'BAGABAG', 2, NULL, NULL),
(868, 'BAMBANG', 2, NULL, NULL),
(869, 'BAYOMBONG (Capital)', 2, NULL, NULL),
(870, 'DIADI', 2, NULL, NULL),
(871, 'DUPAX DEL NORTE', 2, NULL, NULL),
(872, 'DUPAX DEL SUR', 2, NULL, NULL),
(873, 'KASIBU', 2, NULL, NULL),
(874, 'KAYAPA', 2, NULL, NULL),
(875, 'QUEZON', 2, NULL, NULL),
(876, 'SANTA FE', 2, NULL, NULL),
(877, 'SOLANO', 2, NULL, NULL),
(878, 'VILLAVERDE', 2, NULL, NULL),
(879, 'ALFONSO CASTANEDA', 2, NULL, NULL),
(880, 'AGLIPAY', 2, NULL, NULL),
(881, 'CABARROGUIS (Capital)', 2, NULL, NULL),
(882, 'DIFFUN', 2, NULL, NULL),
(883, 'MADDELA', 2, NULL, NULL),
(884, 'SAGUDAY', 2, NULL, NULL),
(885, 'NAGTIPUNAN', 2, NULL, NULL),
(886, 'ABUCAY', 2, NULL, NULL),
(887, 'BAGAC', 2, NULL, NULL),
(888, 'CITY OF BALANGA (Capital)', 2, NULL, NULL),
(889, 'DINALUPIHAN', 2, NULL, NULL),
(890, 'HERMOSA', 2, NULL, NULL),
(891, 'LIMAY', 2, NULL, NULL),
(892, 'MARIVELES', 2, NULL, NULL),
(893, 'MORONG', 2, NULL, NULL),
(894, 'ORANI', 2, NULL, NULL),
(895, 'ORION', 2, NULL, NULL),
(896, 'PILAR', 2, NULL, NULL),
(897, 'SAMAL', 2, NULL, NULL),
(898, 'ANGAT', 2, NULL, NULL),
(899, 'BALAGTAS (BIGAA)', 2, NULL, NULL),
(900, 'BALIUAG', 2, NULL, NULL),
(901, 'BOCAUE', 2, NULL, NULL),
(902, 'BULACAN', 2, NULL, NULL),
(903, 'BUSTOS', 2, NULL, NULL),
(904, 'CALUMPIT', 2, NULL, NULL),
(905, 'GUIGUINTO', 2, NULL, NULL),
(906, 'HAGONOY', 2, NULL, NULL),
(907, 'CITY OF MALOLOS (Capital)', 2, NULL, NULL),
(908, 'MARILAO', 2, NULL, NULL),
(909, 'CITY OF MEYCAUAYAN', 2, NULL, NULL),
(910, 'NORZAGARAY', 2, NULL, NULL),
(911, 'OBANDO', 2, NULL, NULL),
(912, 'PANDI', 2, NULL, NULL),
(913, 'PAOMBONG', 2, NULL, NULL),
(914, 'PLARIDEL', 2, NULL, NULL),
(915, 'PULILAN', 2, NULL, NULL),
(916, 'SAN ILDEFONSO', 2, NULL, NULL),
(917, 'CITY OF SAN JOSE DEL MONTE', 2, NULL, NULL),
(918, 'SAN MIGUEL', 2, NULL, NULL),
(919, 'SAN RAFAEL', 2, NULL, NULL),
(920, 'SANTA MARIA', 2, NULL, NULL),
(921, 'DOÑA REMEDIOS TRINIDAD', 2, NULL, NULL),
(922, 'ALIAGA', 2, NULL, NULL),
(923, 'BONGABON', 2, NULL, NULL),
(924, 'CABANATUAN CITY', 2, NULL, NULL),
(925, 'CABIAO', 2, NULL, NULL),
(926, 'CARRANGLAN', 2, NULL, NULL),
(927, 'CUYAPO', 2, NULL, NULL),
(928, 'GABALDON (BITULOK & SABANI)', 2, NULL, NULL),
(929, 'CITY OF GAPAN', 2, NULL, NULL),
(930, 'GENERAL MAMERTO NATIVIDAD', 2, NULL, NULL),
(931, 'GENERAL TINIO (PAPAYA)', 2, NULL, NULL),
(932, 'GUIMBA', 2, NULL, NULL),
(933, 'JAEN', 2, NULL, NULL),
(934, 'LAUR', 2, NULL, NULL),
(935, 'LICAB', 2, NULL, NULL),
(936, 'LLANERA', 2, NULL, NULL),
(937, 'LUPAO', 2, NULL, NULL),
(938, 'SCIENCE CITY OF MUÑOZ', 2, NULL, NULL),
(939, 'NAMPICUAN', 2, NULL, NULL),
(940, 'PALAYAN CITY (Capital)', 2, NULL, NULL),
(941, 'PANTABANGAN', 2, NULL, NULL),
(942, 'PEÑARANDA', 2, NULL, NULL),
(943, 'QUEZON', 2, NULL, NULL),
(944, 'RIZAL', 2, NULL, NULL),
(945, 'SAN ANTONIO', 2, NULL, NULL),
(946, 'SAN ISIDRO', 2, NULL, NULL),
(947, 'SAN JOSE CITY', 2, NULL, NULL),
(948, 'SAN LEONARDO', 2, NULL, NULL),
(949, 'SANTA ROSA', 2, NULL, NULL),
(950, 'SANTO DOMINGO', 2, NULL, NULL),
(951, 'TALAVERA', 2, NULL, NULL),
(952, 'TALUGTUG', 2, NULL, NULL),
(953, 'ZARAGOZA', 2, NULL, NULL),
(954, 'ANGELES CITY', 2, NULL, NULL),
(955, 'APALIT', 2, NULL, NULL),
(956, 'ARAYAT', 2, NULL, NULL),
(957, 'BACOLOR', 2, NULL, NULL),
(958, 'CANDABA', 2, NULL, NULL),
(959, 'FLORIDABLANCA', 2, NULL, NULL),
(960, 'GUAGUA', 2, NULL, NULL),
(961, 'LUBAO', 2, NULL, NULL),
(962, 'MABALACAT CITY', 2, NULL, NULL),
(963, 'MACABEBE', 2, NULL, NULL),
(964, 'MAGALANG', 2, NULL, NULL),
(965, 'MASANTOL', 2, NULL, NULL),
(966, 'MEXICO', 2, NULL, NULL),
(967, 'MINALIN', 2, NULL, NULL),
(968, 'PORAC', 2, NULL, NULL),
(969, 'CITY OF SAN FERNANDO (Capital)', 2, NULL, NULL),
(970, 'SAN LUIS', 2, NULL, NULL),
(971, 'SAN SIMON', 2, NULL, NULL),
(972, 'SANTA ANA', 2, NULL, NULL),
(973, 'SANTA RITA', 2, NULL, NULL),
(974, 'SANTO TOMAS', 2, NULL, NULL),
(975, 'SASMUAN (Sexmoan)', 2, NULL, NULL),
(976, 'ANAO', 2, NULL, NULL),
(977, 'BAMBAN', 2, NULL, NULL),
(978, 'CAMILING', 2, NULL, NULL),
(979, 'CAPAS', 2, NULL, NULL),
(980, 'CONCEPCION', 2, NULL, NULL),
(981, 'GERONA', 2, NULL, NULL),
(982, 'LA PAZ', 2, NULL, NULL),
(983, 'MAYANTOC', 2, NULL, NULL),
(984, 'MONCADA', 2, NULL, NULL),
(985, 'PANIQUI', 2, NULL, NULL),
(986, 'PURA', 2, NULL, NULL),
(987, 'RAMOS', 2, NULL, NULL),
(988, 'SAN CLEMENTE', 2, NULL, NULL),
(989, 'SAN MANUEL', 2, NULL, NULL),
(990, 'SANTA IGNACIA', 2, NULL, NULL),
(991, 'CITY OF TARLAC (Capital)', 2, NULL, NULL),
(992, 'VICTORIA', 2, NULL, NULL),
(993, 'SAN JOSE', 2, NULL, NULL),
(994, 'BOTOLAN', 2, NULL, NULL),
(995, 'CABANGAN', 2, NULL, NULL),
(996, 'CANDELARIA', 2, NULL, NULL),
(997, 'CASTILLEJOS', 2, NULL, NULL),
(998, 'IBA (Capital)', 2, NULL, NULL),
(999, 'MASINLOC', 2, NULL, NULL),
(1000, 'OLONGAPO CITY', 2, NULL, NULL),
(1001, 'PALAUIG', 2, NULL, NULL),
(1002, 'SAN ANTONIO', 2, NULL, NULL),
(1003, 'SAN FELIPE', 2, NULL, NULL),
(1004, 'SAN MARCELINO', 2, NULL, NULL),
(1005, 'SAN NARCISO', 2, NULL, NULL),
(1006, 'SANTA CRUZ', 2, NULL, NULL),
(1007, 'SUBIC', 2, NULL, NULL),
(1008, 'BALER (Capital)', 2, NULL, NULL),
(1009, 'CASIGURAN', 2, NULL, NULL),
(1010, 'DILASAG', 2, NULL, NULL),
(1011, 'DINALUNGAN', 2, NULL, NULL),
(1012, 'DINGALAN', 2, NULL, NULL),
(1013, 'DIPACULAO', 2, NULL, NULL),
(1014, 'MARIA AURORA', 2, NULL, NULL),
(1015, 'SAN LUIS', 2, NULL, NULL),
(1016, 'AGONCILLO', 2, NULL, NULL),
(1017, 'ALITAGTAG', 2, NULL, NULL),
(1018, 'BALAYAN', 2, NULL, NULL),
(1019, 'BALETE', 2, NULL, NULL),
(1020, 'BATANGAS CITY (Capital)', 2, NULL, NULL),
(1021, 'BAUAN', 2, NULL, NULL),
(1022, 'CALACA', 2, NULL, NULL),
(1023, 'CALATAGAN', 2, NULL, NULL),
(1024, 'CUENCA', 2, NULL, NULL),
(1025, 'IBAAN', 2, NULL, NULL),
(1026, 'LAUREL', 2, NULL, NULL),
(1027, 'LEMERY', 2, NULL, NULL),
(1028, 'LIAN', 2, NULL, NULL),
(1029, 'LIPA CITY', 2, NULL, NULL),
(1030, 'LOBO', 2, NULL, NULL),
(1031, 'MABINI', 2, NULL, NULL),
(1032, 'MALVAR', 2, NULL, NULL),
(1033, 'MATAASNAKAHOY', 2, NULL, NULL),
(1034, 'NASUGBU', 2, NULL, NULL),
(1035, 'PADRE GARCIA', 2, NULL, NULL),
(1036, 'ROSARIO', 2, NULL, NULL),
(1037, 'SAN JOSE', 2, NULL, NULL),
(1038, 'SAN JUAN', 2, NULL, NULL),
(1039, 'SAN LUIS', 2, NULL, NULL),
(1040, 'SAN NICOLAS', 2, NULL, NULL),
(1041, 'SAN PASCUAL', 2, NULL, NULL),
(1042, 'SANTA TERESITA', 2, NULL, NULL),
(1043, 'SANTO TOMAS', 2, NULL, NULL),
(1044, 'TAAL', 2, NULL, NULL),
(1045, 'TALISAY', 2, NULL, NULL),
(1046, 'CITY OF TANAUAN', 2, NULL, NULL),
(1047, 'TAYSAN', 2, NULL, NULL),
(1048, 'TINGLOY', 2, NULL, NULL),
(1049, 'TUY', 2, NULL, NULL),
(1050, 'ALFONSO', 2, NULL, NULL),
(1051, 'AMADEO', 2, NULL, NULL),
(1052, 'BACOOR CITY', 2, NULL, NULL),
(1053, 'CARMONA', 2, NULL, NULL),
(1054, 'CAVITE CITY', 2, NULL, NULL),
(1055, 'CITY OF DASMARIÑAS', 2, NULL, NULL),
(1056, 'GENERAL EMILIO AGUINALDO', 2, NULL, NULL),
(1057, 'GENERAL TRIAS', 2, NULL, NULL),
(1058, 'IMUS CITY', 2, NULL, NULL),
(1059, 'INDANG', 2, NULL, NULL),
(1060, 'KAWIT', 2, NULL, NULL),
(1061, 'MAGALLANES', 2, NULL, NULL),
(1062, 'MARAGONDON', 2, NULL, NULL),
(1063, 'MENDEZ (MENDEZ-NUÑEZ)', 2, NULL, NULL),
(1064, 'NAIC', 2, NULL, NULL),
(1065, 'NOVELETA', 2, NULL, NULL),
(1066, 'ROSARIO', 2, NULL, NULL),
(1067, 'SILANG', 2, NULL, NULL),
(1068, 'TAGAYTAY CITY', 2, NULL, NULL),
(1069, 'TANZA', 2, NULL, NULL),
(1070, 'TERNATE', 2, NULL, NULL),
(1071, 'TRECE MARTIRES CITY (Capital)', 2, NULL, NULL),
(1072, 'GEN. MARIANO ALVAREZ', 2, NULL, NULL),
(1073, 'ALAMINOS', 2, NULL, NULL),
(1074, 'BAY', 2, NULL, NULL),
(1075, 'CITY OF BIÑAN', 2, NULL, NULL),
(1076, 'CABUYAO CITY', 2, NULL, NULL),
(1077, 'CITY OF CALAMBA', 2, NULL, NULL),
(1078, 'CALAUAN', 2, NULL, NULL),
(1079, 'CAVINTI', 2, NULL, NULL),
(1080, 'FAMY', 2, NULL, NULL),
(1081, 'KALAYAAN', 2, NULL, NULL),
(1082, 'LILIW', 2, NULL, NULL),
(1083, 'LOS BAÑOS', 2, NULL, NULL),
(1084, 'LUISIANA', 2, NULL, NULL),
(1085, 'LUMBAN', 2, NULL, NULL),
(1086, 'MABITAC', 2, NULL, NULL),
(1087, 'MAGDALENA', 2, NULL, NULL),
(1088, 'MAJAYJAY', 2, NULL, NULL),
(1089, 'NAGCARLAN', 2, NULL, NULL),
(1090, 'PAETE', 2, NULL, NULL),
(1091, 'PAGSANJAN', 2, NULL, NULL),
(1092, 'PAKIL', 2, NULL, NULL),
(1093, 'PANGIL', 2, NULL, NULL),
(1094, 'PILA', 2, NULL, NULL),
(1095, 'RIZAL', 2, NULL, NULL),
(1096, 'SAN PABLO CITY', 2, NULL, NULL),
(1097, 'CITY OF SAN PEDRO', 2, NULL, NULL),
(1098, 'SANTA CRUZ (Capital)', 2, NULL, NULL),
(1099, 'SANTA MARIA', 2, NULL, NULL),
(1100, 'CITY OF SANTA ROSA', 2, NULL, NULL),
(1101, 'SINILOAN', 2, NULL, NULL),
(1102, 'VICTORIA', 2, NULL, NULL),
(1103, 'AGDANGAN', 2, NULL, NULL),
(1104, 'ALABAT', 2, NULL, NULL),
(1105, 'ATIMONAN', 2, NULL, NULL),
(1106, 'BUENAVISTA', 2, NULL, NULL),
(1107, 'BURDEOS', 2, NULL, NULL),
(1108, 'CALAUAG', 2, NULL, NULL),
(1109, 'CANDELARIA', 2, NULL, NULL),
(1110, 'CATANAUAN', 2, NULL, NULL),
(1111, 'DOLORES', 2, NULL, NULL),
(1112, 'GENERAL LUNA', 2, NULL, NULL),
(1113, 'GENERAL NAKAR', 2, NULL, NULL),
(1114, 'GUINAYANGAN', 2, NULL, NULL),
(1115, 'GUMACA', 2, NULL, NULL),
(1116, 'INFANTA', 2, NULL, NULL),
(1117, 'JOMALIG', 2, NULL, NULL),
(1118, 'LOPEZ', 2, NULL, NULL),
(1119, 'LUCBAN', 2, NULL, NULL),
(1120, 'LUCENA CITY (Capital)', 2, NULL, NULL),
(1121, 'MACALELON', 2, NULL, NULL),
(1122, 'MAUBAN', 2, NULL, NULL),
(1123, 'MULANAY', 2, NULL, NULL),
(1124, 'PADRE BURGOS', 2, NULL, NULL),
(1125, 'PAGBILAO', 2, NULL, NULL),
(1126, 'PANUKULAN', 2, NULL, NULL),
(1127, 'PATNANUNGAN', 2, NULL, NULL),
(1128, 'PEREZ', 2, NULL, NULL),
(1129, 'PITOGO', 2, NULL, NULL),
(1130, 'PLARIDEL', 2, NULL, NULL),
(1131, 'POLILLO', 2, NULL, NULL),
(1132, 'QUEZON', 2, NULL, NULL),
(1133, 'REAL', 2, NULL, NULL),
(1134, 'SAMPALOC', 2, NULL, NULL),
(1135, 'SAN ANDRES', 2, NULL, NULL),
(1136, 'SAN ANTONIO', 2, NULL, NULL),
(1137, 'SAN FRANCISCO (AURORA)', 2, NULL, NULL),
(1138, 'SAN NARCISO', 2, NULL, NULL),
(1139, 'SARIAYA', 2, NULL, NULL),
(1140, 'TAGKAWAYAN', 2, NULL, NULL),
(1141, 'CITY OF TAYABAS', 2, NULL, NULL),
(1142, 'TIAONG', 2, NULL, NULL),
(1143, 'UNISAN', 2, NULL, NULL),
(1144, 'ANGONO', 2, NULL, NULL),
(1145, 'CITY OF ANTIPOLO', 2, NULL, NULL),
(1146, 'BARAS', 2, NULL, NULL),
(1147, 'BINANGONAN', 2, NULL, NULL),
(1148, 'CAINTA', 2, NULL, NULL),
(1149, 'CARDONA', 2, NULL, NULL),
(1150, 'JALA-JALA', 2, NULL, NULL),
(1151, 'RODRIGUEZ (MONTALBAN)', 2, NULL, NULL),
(1152, 'MORONG', 2, NULL, NULL),
(1153, 'PILILLA', 2, NULL, NULL),
(1154, 'SAN MATEO', 2, NULL, NULL),
(1155, 'TANAY', 2, NULL, NULL),
(1156, 'TAYTAY', 2, NULL, NULL),
(1157, 'TERESA', 2, NULL, NULL),
(1158, 'BOAC (Capital)', 2, NULL, NULL),
(1159, 'BUENAVISTA', 2, NULL, NULL),
(1160, 'GASAN', 2, NULL, NULL),
(1161, 'MOGPOG', 2, NULL, NULL),
(1162, 'SANTA CRUZ', 2, NULL, NULL),
(1163, 'TORRIJOS', 2, NULL, NULL),
(1164, 'ABRA DE ILOG', 2, NULL, NULL),
(1165, 'CALINTAAN', 2, NULL, NULL),
(1166, 'LOOC', 2, NULL, NULL),
(1167, 'LUBANG', 2, NULL, NULL),
(1168, 'MAGSAYSAY', 2, NULL, NULL),
(1169, 'MAMBURAO (Capital)', 2, NULL, NULL),
(1170, 'PALUAN', 2, NULL, NULL),
(1171, 'RIZAL', 2, NULL, NULL),
(1172, 'SABLAYAN', 2, NULL, NULL),
(1173, 'SAN JOSE', 2, NULL, NULL),
(1174, 'SANTA CRUZ', 2, NULL, NULL),
(1175, 'BACO', 2, NULL, NULL),
(1176, 'BANSUD', 2, NULL, NULL),
(1177, 'BONGABONG', 2, NULL, NULL),
(1178, 'BULALACAO (SAN PEDRO)', 2, NULL, NULL),
(1179, 'CITY OF CALAPAN (Capital)', 2, NULL, NULL),
(1180, 'GLORIA', 2, NULL, NULL),
(1181, 'MANSALAY', 2, NULL, NULL),
(1182, 'NAUJAN', 2, NULL, NULL),
(1183, 'PINAMALAYAN', 2, NULL, NULL),
(1184, 'POLA', 2, NULL, NULL),
(1185, 'PUERTO GALERA', 2, NULL, NULL),
(1186, 'ROXAS', 2, NULL, NULL),
(1187, 'SAN TEODORO', 2, NULL, NULL),
(1188, 'SOCORRO', 2, NULL, NULL),
(1189, 'VICTORIA', 2, NULL, NULL),
(1190, 'ABORLAN', 2, NULL, NULL),
(1191, 'AGUTAYA', 2, NULL, NULL),
(1192, 'ARACELI', 2, NULL, NULL),
(1193, 'BALABAC', 2, NULL, NULL),
(1194, 'BATARAZA', 2, NULL, NULL),
(1195, 'BROOKE\'S POINT', 2, NULL, NULL),
(1196, 'BUSUANGA', 2, NULL, NULL),
(1197, 'CAGAYANCILLO', 2, NULL, NULL),
(1198, 'CORON', 2, NULL, NULL),
(1199, 'CUYO', 2, NULL, NULL),
(1200, 'DUMARAN', 2, NULL, NULL),
(1201, 'EL NIDO (BACUIT)', 2, NULL, NULL),
(1202, 'LINAPACAN', 2, NULL, NULL),
(1203, 'MAGSAYSAY', 2, NULL, NULL),
(1204, 'NARRA', 2, NULL, NULL),
(1205, 'PUERTO PRINCESA CITY (Capital)', 2, NULL, NULL),
(1206, 'QUEZON', 2, NULL, NULL),
(1207, 'ROXAS', 2, NULL, NULL),
(1208, 'SAN VICENTE', 2, NULL, NULL),
(1209, 'TAYTAY', 2, NULL, NULL),
(1210, 'KALAYAAN', 2, NULL, NULL),
(1211, 'CULION', 2, NULL, NULL),
(1212, 'RIZAL (MARCOS)', 2, NULL, NULL),
(1213, 'SOFRONIO ESPAÑOLA', 2, NULL, NULL),
(1214, 'ALCANTARA', 2, NULL, NULL),
(1215, 'BANTON', 2, NULL, NULL),
(1216, 'CAJIDIOCAN', 2, NULL, NULL),
(1217, 'CALATRAVA', 2, NULL, NULL),
(1218, 'CONCEPCION', 2, NULL, NULL),
(1219, 'CORCUERA', 2, NULL, NULL),
(1220, 'LOOC', 2, NULL, NULL),
(1221, 'MAGDIWANG', 2, NULL, NULL),
(1222, 'ODIONGAN', 2, NULL, NULL),
(1223, 'ROMBLON (Capital)', 2, NULL, NULL),
(1224, 'SAN AGUSTIN', 2, NULL, NULL),
(1225, 'SAN ANDRES', 2, NULL, NULL),
(1226, 'SAN FERNANDO', 2, NULL, NULL),
(1227, 'SAN JOSE', 2, NULL, NULL),
(1228, 'SANTA FE', 2, NULL, NULL),
(1229, 'FERROL', 2, NULL, NULL),
(1230, 'SANTA MARIA (IMELDA)', 2, NULL, NULL),
(1231, 'BACACAY', 2, NULL, NULL),
(1232, 'CAMALIG', 2, NULL, NULL),
(1233, 'DARAGA (LOCSIN)', 2, NULL, NULL),
(1234, 'GUINOBATAN', 2, NULL, NULL),
(1235, 'JOVELLAR', 2, NULL, NULL),
(1236, 'LEGAZPI CITY (Capital)', 2, NULL, NULL),
(1237, 'LIBON', 2, NULL, NULL),
(1238, 'CITY OF LIGAO', 2, NULL, NULL),
(1239, 'MALILIPOT', 2, NULL, NULL),
(1240, 'MALINAO', 2, NULL, NULL),
(1241, 'MANITO', 2, NULL, NULL),
(1242, 'OAS', 2, NULL, NULL),
(1243, 'PIO DURAN', 2, NULL, NULL),
(1244, 'POLANGUI', 2, NULL, NULL),
(1245, 'RAPU-RAPU', 2, NULL, NULL),
(1246, 'SANTO DOMINGO (LIBOG)', 2, NULL, NULL),
(1247, 'CITY OF TABACO', 2, NULL, NULL),
(1248, 'TIWI', 2, NULL, NULL),
(1249, 'BASUD', 2, NULL, NULL),
(1250, 'CAPALONGA', 2, NULL, NULL),
(1251, 'DAET (Capital)', 2, NULL, NULL),
(1252, 'SAN LORENZO RUIZ (IMELDA)', 2, NULL, NULL),
(1253, 'JOSE PANGANIBAN', 2, NULL, NULL),
(1254, 'LABO', 2, NULL, NULL),
(1255, 'MERCEDES', 2, NULL, NULL),
(1256, 'PARACALE', 2, NULL, NULL),
(1257, 'SAN VICENTE', 2, NULL, NULL),
(1258, 'SANTA ELENA', 2, NULL, NULL),
(1259, 'TALISAY', 2, NULL, NULL),
(1260, 'VINZONS', 2, NULL, NULL),
(1261, 'BAAO', 2, NULL, NULL),
(1262, 'BALATAN', 2, NULL, NULL),
(1263, 'BATO', 2, NULL, NULL),
(1264, 'BOMBON', 2, NULL, NULL),
(1265, 'BUHI', 2, NULL, NULL),
(1266, 'BULA', 2, NULL, NULL),
(1267, 'CABUSAO', 2, NULL, NULL),
(1268, 'CALABANGA', 2, NULL, NULL),
(1269, 'CAMALIGAN', 2, NULL, NULL),
(1270, 'CANAMAN', 2, NULL, NULL),
(1271, 'CARAMOAN', 2, NULL, NULL),
(1272, 'DEL GALLEGO', 2, NULL, NULL),
(1273, 'GAINZA', 2, NULL, NULL),
(1274, 'GARCHITORENA', 2, NULL, NULL),
(1275, 'GOA', 2, NULL, NULL),
(1276, 'IRIGA CITY', 2, NULL, NULL),
(1277, 'LAGONOY', 2, NULL, NULL),
(1278, 'LIBMANAN', 2, NULL, NULL),
(1279, 'LUPI', 2, NULL, NULL),
(1280, 'MAGARAO', 2, NULL, NULL),
(1281, 'MILAOR', 2, NULL, NULL),
(1282, 'MINALABAC', 2, NULL, NULL),
(1283, 'NABUA', 2, NULL, NULL),
(1284, 'NAGA CITY', 2, NULL, NULL),
(1285, 'OCAMPO', 2, NULL, NULL),
(1286, 'PAMPLONA', 2, NULL, NULL),
(1287, 'PASACAO', 2, NULL, NULL),
(1288, 'PILI (Capital)', 2, NULL, NULL),
(1289, 'PRESENTACION (PARUBCAN)', 2, NULL, NULL),
(1290, 'RAGAY', 2, NULL, NULL),
(1291, 'SAGÑAY', 2, NULL, NULL),
(1292, 'SAN FERNANDO', 2, NULL, NULL),
(1293, 'SAN JOSE', 2, NULL, NULL),
(1294, 'SIPOCOT', 2, NULL, NULL),
(1295, 'SIRUMA', 2, NULL, NULL),
(1296, 'TIGAON', 2, NULL, NULL),
(1297, 'TINAMBAC', 2, NULL, NULL),
(1298, 'BAGAMANOC', 2, NULL, NULL),
(1299, 'BARAS', 2, NULL, NULL),
(1300, 'BATO', 2, NULL, NULL),
(1301, 'CARAMORAN', 2, NULL, NULL),
(1302, 'GIGMOTO', 2, NULL, NULL),
(1303, 'PANDAN', 2, NULL, NULL),
(1304, 'PANGANIBAN (PAYO)', 2, NULL, NULL),
(1305, 'SAN ANDRES (CALOLBON)', 2, NULL, NULL),
(1306, 'SAN MIGUEL', 2, NULL, NULL),
(1307, 'VIGA', 2, NULL, NULL),
(1308, 'VIRAC (Capital)', 2, NULL, NULL),
(1309, 'AROROY', 2, NULL, NULL),
(1310, 'BALENO', 2, NULL, NULL),
(1311, 'BALUD', 2, NULL, NULL),
(1312, 'BATUAN', 2, NULL, NULL),
(1313, 'CATAINGAN', 2, NULL, NULL),
(1314, 'CAWAYAN', 2, NULL, NULL),
(1315, 'CLAVERIA', 2, NULL, NULL),
(1316, 'DIMASALANG', 2, NULL, NULL),
(1317, 'ESPERANZA', 2, NULL, NULL),
(1318, 'MANDAON', 2, NULL, NULL),
(1319, 'CITY OF MASBATE (Capital)', 2, NULL, NULL),
(1320, 'MILAGROS', 2, NULL, NULL),
(1321, 'MOBO', 2, NULL, NULL),
(1322, 'MONREAL', 2, NULL, NULL),
(1323, 'PALANAS', 2, NULL, NULL),
(1324, 'PIO V. CORPUZ (LIMBUHAN)', 2, NULL, NULL),
(1325, 'PLACER', 2, NULL, NULL),
(1326, 'SAN FERNANDO', 2, NULL, NULL),
(1327, 'SAN JACINTO', 2, NULL, NULL),
(1328, 'SAN PASCUAL', 2, NULL, NULL),
(1329, 'USON', 2, NULL, NULL),
(1330, 'BARCELONA', 2, NULL, NULL),
(1331, 'BULAN', 2, NULL, NULL),
(1332, 'BULUSAN', 2, NULL, NULL),
(1333, 'CASIGURAN', 2, NULL, NULL),
(1334, 'CASTILLA', 2, NULL, NULL),
(1335, 'DONSOL', 2, NULL, NULL),
(1336, 'GUBAT', 2, NULL, NULL),
(1337, 'IROSIN', 2, NULL, NULL),
(1338, 'JUBAN', 2, NULL, NULL),
(1339, 'MAGALLANES', 2, NULL, NULL),
(1340, 'MATNOG', 2, NULL, NULL),
(1341, 'PILAR', 2, NULL, NULL),
(1342, 'PRIETO DIAZ', 2, NULL, NULL),
(1343, 'SANTA MAGDALENA', 2, NULL, NULL),
(1344, 'CITY OF SORSOGON (Capital)', 2, NULL, NULL),
(1345, 'ALTAVAS', 2, NULL, NULL),
(1346, 'BALETE', 2, NULL, NULL),
(1347, 'BANGA', 2, NULL, NULL),
(1348, 'BATAN', 2, NULL, NULL),
(1349, 'BURUANGA', 2, NULL, NULL),
(1350, 'IBAJAY', 2, NULL, NULL),
(1351, 'KALIBO (Capital)', 2, NULL, NULL),
(1352, 'LEZO', 2, NULL, NULL),
(1353, 'LIBACAO', 2, NULL, NULL),
(1354, 'MADALAG', 2, NULL, NULL),
(1355, 'MAKATO', 2, NULL, NULL),
(1356, 'MALAY', 2, NULL, NULL),
(1357, 'MALINAO', 2, NULL, NULL),
(1358, 'NABAS', 2, NULL, NULL),
(1359, 'NEW WASHINGTON', 2, NULL, NULL),
(1360, 'NUMANCIA', 2, NULL, NULL),
(1361, 'TANGALAN', 2, NULL, NULL),
(1362, 'ANINI-Y', 2, NULL, NULL),
(1363, 'BARBAZA', 2, NULL, NULL),
(1364, 'BELISON', 2, NULL, NULL),
(1365, 'BUGASONG', 2, NULL, NULL),
(1366, 'CALUYA', 2, NULL, NULL),
(1367, 'CULASI', 2, NULL, NULL),
(1368, 'TOBIAS FORNIER (DAO)', 2, NULL, NULL),
(1369, 'HAMTIC', 2, NULL, NULL),
(1370, 'LAUA-AN', 2, NULL, NULL),
(1371, 'LIBERTAD', 2, NULL, NULL),
(1372, 'PANDAN', 2, NULL, NULL),
(1373, 'PATNONGON', 2, NULL, NULL),
(1374, 'SAN JOSE (Capital)', 2, NULL, NULL),
(1375, 'SAN REMIGIO', 2, NULL, NULL),
(1376, 'SEBASTE', 2, NULL, NULL),
(1377, 'SIBALOM', 2, NULL, NULL),
(1378, 'TIBIAO', 2, NULL, NULL),
(1379, 'VALDERRAMA', 2, NULL, NULL),
(1380, 'CUARTERO', 2, NULL, NULL),
(1381, 'DAO', 2, NULL, NULL),
(1382, 'DUMALAG', 2, NULL, NULL),
(1383, 'DUMARAO', 2, NULL, NULL),
(1384, 'IVISAN', 2, NULL, NULL),
(1385, 'JAMINDAN', 2, NULL, NULL),
(1386, 'MA-AYON', 2, NULL, NULL),
(1387, 'MAMBUSAO', 2, NULL, NULL),
(1388, 'PANAY', 2, NULL, NULL),
(1389, 'PANITAN', 2, NULL, NULL),
(1390, 'PILAR', 2, NULL, NULL),
(1391, 'PONTEVEDRA', 2, NULL, NULL),
(1392, 'PRESIDENT ROXAS', 2, NULL, NULL),
(1393, 'ROXAS CITY (Capital)', 2, NULL, NULL),
(1394, 'SAPI-AN', 2, NULL, NULL),
(1395, 'SIGMA', 2, NULL, NULL),
(1396, 'TAPAZ', 2, NULL, NULL),
(1397, 'AJUY', 2, NULL, NULL),
(1398, 'ALIMODIAN', 2, NULL, NULL),
(1399, 'ANILAO', 2, NULL, NULL),
(1400, 'BADIANGAN', 2, NULL, NULL),
(1401, 'BALASAN', 2, NULL, NULL),
(1402, 'BANATE', 2, NULL, NULL),
(1403, 'BAROTAC NUEVO', 2, NULL, NULL),
(1404, 'BAROTAC VIEJO', 2, NULL, NULL),
(1405, 'BATAD', 2, NULL, NULL),
(1406, 'BINGAWAN', 2, NULL, NULL),
(1407, 'CABATUAN', 2, NULL, NULL),
(1408, 'CALINOG', 2, NULL, NULL),
(1409, 'CARLES', 2, NULL, NULL),
(1410, 'CONCEPCION', 2, NULL, NULL),
(1411, 'DINGLE', 2, NULL, NULL),
(1412, 'DUEÑAS', 2, NULL, NULL),
(1413, 'DUMANGAS', 2, NULL, NULL),
(1414, 'ESTANCIA', 2, NULL, NULL),
(1415, 'GUIMBAL', 2, NULL, NULL),
(1416, 'IGBARAS', 2, NULL, NULL),
(1417, 'ILOILO CITY (Capital)', 2, NULL, NULL),
(1418, 'JANIUAY', 2, NULL, NULL),
(1419, 'LAMBUNAO', 2, NULL, NULL),
(1420, 'LEGANES', 2, NULL, NULL),
(1421, 'LEMERY', 2, NULL, NULL),
(1422, 'LEON', 2, NULL, NULL),
(1423, 'MAASIN', 2, NULL, NULL),
(1424, 'MIAGAO', 2, NULL, NULL),
(1425, 'MINA', 2, NULL, NULL),
(1426, 'NEW LUCENA', 2, NULL, NULL),
(1427, 'OTON', 2, NULL, NULL),
(1428, 'CITY OF PASSI', 2, NULL, NULL),
(1429, 'PAVIA', 2, NULL, NULL),
(1430, 'POTOTAN', 2, NULL, NULL),
(1431, 'SAN DIONISIO', 2, NULL, NULL),
(1432, 'SAN ENRIQUE', 2, NULL, NULL),
(1433, 'SAN JOAQUIN', 2, NULL, NULL),
(1434, 'SAN MIGUEL', 2, NULL, NULL),
(1435, 'SAN RAFAEL', 2, NULL, NULL),
(1436, 'SANTA BARBARA', 2, NULL, NULL),
(1437, 'SARA', 2, NULL, NULL),
(1438, 'TIGBAUAN', 2, NULL, NULL),
(1439, 'TUBUNGAN', 2, NULL, NULL),
(1440, 'ZARRAGA', 2, NULL, NULL),
(1441, 'BACOLOD CITY (Capital)', 2, NULL, NULL),
(1442, 'BAGO CITY', 2, NULL, NULL),
(1443, 'BINALBAGAN', 2, NULL, NULL),
(1444, 'CADIZ CITY', 2, NULL, NULL),
(1445, 'CALATRAVA', 2, NULL, NULL),
(1446, 'CANDONI', 2, NULL, NULL),
(1447, 'CAUAYAN', 2, NULL, NULL),
(1448, 'ENRIQUE B. MAGALONA (SARAVIA)', 2, NULL, NULL),
(1449, 'CITY OF ESCALANTE', 2, NULL, NULL),
(1450, 'CITY OF HIMAMAYLAN', 2, NULL, NULL),
(1451, 'HINIGARAN', 2, NULL, NULL),
(1452, 'HINOBA-AN (ASIA)', 2, NULL, NULL),
(1453, 'ILOG', 2, NULL, NULL),
(1454, 'ISABELA', 2, NULL, NULL),
(1455, 'CITY OF KABANKALAN', 2, NULL, NULL),
(1456, 'LA CARLOTA CITY', 2, NULL, NULL),
(1457, 'LA CASTELLANA', 2, NULL, NULL),
(1458, 'MANAPLA', 2, NULL, NULL),
(1459, 'MOISES PADILLA (MAGALLON)', 2, NULL, NULL),
(1460, 'MURCIA', 2, NULL, NULL),
(1461, 'PONTEVEDRA', 2, NULL, NULL),
(1462, 'PULUPANDAN', 2, NULL, NULL),
(1463, 'SAGAY CITY', 2, NULL, NULL),
(1464, 'SAN CARLOS CITY', 2, NULL, NULL),
(1465, 'SAN ENRIQUE', 2, NULL, NULL),
(1466, 'SILAY CITY', 2, NULL, NULL),
(1467, 'CITY OF SIPALAY', 2, NULL, NULL),
(1468, 'CITY OF TALISAY', 2, NULL, NULL),
(1469, 'TOBOSO', 2, NULL, NULL),
(1470, 'VALLADOLID', 2, NULL, NULL),
(1471, 'CITY OF VICTORIAS', 2, NULL, NULL),
(1472, 'SALVADOR BENEDICTO', 2, NULL, NULL),
(1473, 'BUENAVISTA', 2, NULL, NULL),
(1474, 'JORDAN (Capital)', 2, NULL, NULL),
(1475, 'NUEVA VALENCIA', 2, NULL, NULL),
(1476, 'SAN LORENZO', 2, NULL, NULL),
(1477, 'SIBUNAG', 2, NULL, NULL),
(1478, 'ALBURQUERQUE', 2, NULL, NULL),
(1479, 'ALICIA', 2, NULL, NULL),
(1480, 'ANDA', 2, NULL, NULL),
(1481, 'ANTEQUERA', 2, NULL, NULL),
(1482, 'BACLAYON', 2, NULL, NULL),
(1483, 'BALILIHAN', 2, NULL, NULL),
(1484, 'BATUAN', 2, NULL, NULL),
(1485, 'BILAR', 2, NULL, NULL),
(1486, 'BUENAVISTA', 2, NULL, NULL),
(1487, 'CALAPE', 2, NULL, NULL),
(1488, 'CANDIJAY', 2, NULL, NULL),
(1489, 'CARMEN', 2, NULL, NULL),
(1490, 'CATIGBIAN', 2, NULL, NULL),
(1491, 'CLARIN', 2, NULL, NULL),
(1492, 'CORELLA', 2, NULL, NULL),
(1493, 'CORTES', 2, NULL, NULL),
(1494, 'DAGOHOY', 2, NULL, NULL),
(1495, 'DANAO', 2, NULL, NULL),
(1496, 'DAUIS', 2, NULL, NULL),
(1497, 'DIMIAO', 2, NULL, NULL),
(1498, 'DUERO', 2, NULL, NULL),
(1499, 'GARCIA HERNANDEZ', 2, NULL, NULL),
(1500, 'GUINDULMAN', 2, NULL, NULL),
(1501, 'INABANGA', 2, NULL, NULL),
(1502, 'JAGNA', 2, NULL, NULL),
(1503, 'JETAFE', 2, NULL, NULL),
(1504, 'LILA', 2, NULL, NULL),
(1505, 'LOAY', 2, NULL, NULL),
(1506, 'LOBOC', 2, NULL, NULL),
(1507, 'LOON', 2, NULL, NULL),
(1508, 'MABINI', 2, NULL, NULL),
(1509, 'MARIBOJOC', 2, NULL, NULL),
(1510, 'PANGLAO', 2, NULL, NULL),
(1511, 'PILAR', 2, NULL, NULL),
(1512, 'PRES. CARLOS P. GARCIA (PITOGO)', 2, NULL, NULL),
(1513, 'SAGBAYAN (BORJA)', 2, NULL, NULL),
(1514, 'SAN ISIDRO', 2, NULL, NULL),
(1515, 'SAN MIGUEL', 2, NULL, NULL),
(1516, 'SEVILLA', 2, NULL, NULL),
(1517, 'SIERRA BULLONES', 2, NULL, NULL),
(1518, 'SIKATUNA', 2, NULL, NULL),
(1519, 'TAGBILARAN CITY (Capital)', 2, NULL, NULL),
(1520, 'TALIBON', 2, NULL, NULL),
(1521, 'TRINIDAD', 2, NULL, NULL),
(1522, 'TUBIGON', 2, NULL, NULL),
(1523, 'UBAY', 2, NULL, NULL),
(1524, 'VALENCIA', 2, NULL, NULL),
(1525, 'BIEN UNIDO', 2, NULL, NULL),
(1526, 'ALCANTARA', 2, NULL, NULL),
(1527, 'ALCOY', 2, NULL, NULL),
(1528, 'ALEGRIA', 2, NULL, NULL),
(1529, 'ALOGUINSAN', 2, NULL, NULL),
(1530, 'ARGAO', 2, NULL, NULL),
(1531, 'ASTURIAS', 2, NULL, NULL),
(1532, 'BADIAN', 2, NULL, NULL),
(1533, 'BALAMBAN', 2, NULL, NULL),
(1534, 'BANTAYAN', 2, NULL, NULL),
(1535, 'BARILI', 2, NULL, NULL),
(1536, 'CITY OF BOGO', 2, NULL, NULL),
(1537, 'BOLJOON', 2, NULL, NULL);
INSERT INTO `city_municipalities` (`id`, `name`, `country_id`, `created_at`, `updated_at`) VALUES
(1538, 'BORBON', 2, NULL, NULL),
(1539, 'CITY OF CARCAR', 2, NULL, NULL),
(1540, 'CARMEN', 2, NULL, NULL),
(1541, 'CATMON', 2, NULL, NULL),
(1542, 'CEBU CITY (Capital)', 2, NULL, NULL),
(1543, 'COMPOSTELA', 2, NULL, NULL),
(1544, 'CONSOLACION', 2, NULL, NULL),
(1545, 'CORDOVA', 2, NULL, NULL),
(1546, 'DAANBANTAYAN', 2, NULL, NULL),
(1547, 'DALAGUETE', 2, NULL, NULL),
(1548, 'DANAO CITY', 2, NULL, NULL),
(1549, 'DUMANJUG', 2, NULL, NULL),
(1550, 'GINATILAN', 2, NULL, NULL),
(1551, 'LAPU-LAPU CITY (OPON)', 2, NULL, NULL),
(1552, 'LILOAN', 2, NULL, NULL),
(1553, 'MADRIDEJOS', 2, NULL, NULL),
(1554, 'MALABUYOC', 2, NULL, NULL),
(1555, 'MANDAUE CITY', 2, NULL, NULL),
(1556, 'MEDELLIN', 2, NULL, NULL),
(1557, 'MINGLANILLA', 2, NULL, NULL),
(1558, 'MOALBOAL', 2, NULL, NULL),
(1559, 'CITY OF NAGA', 2, NULL, NULL),
(1560, 'OSLOB', 2, NULL, NULL),
(1561, 'PILAR', 2, NULL, NULL),
(1562, 'PINAMUNGAHAN', 2, NULL, NULL),
(1563, 'PORO', 2, NULL, NULL),
(1564, 'RONDA', 2, NULL, NULL),
(1565, 'SAMBOAN', 2, NULL, NULL),
(1566, 'SAN FERNANDO', 2, NULL, NULL),
(1567, 'SAN FRANCISCO', 2, NULL, NULL),
(1568, 'SAN REMIGIO', 2, NULL, NULL),
(1569, 'SANTA FE', 2, NULL, NULL),
(1570, 'SANTANDER', 2, NULL, NULL),
(1571, 'SIBONGA', 2, NULL, NULL),
(1572, 'SOGOD', 2, NULL, NULL),
(1573, 'TABOGON', 2, NULL, NULL),
(1574, 'TABUELAN', 2, NULL, NULL),
(1575, 'CITY OF TALISAY', 2, NULL, NULL),
(1576, 'TOLEDO CITY', 2, NULL, NULL),
(1577, 'TUBURAN', 2, NULL, NULL),
(1578, 'TUDELA', 2, NULL, NULL),
(1579, 'AMLAN (AYUQUITAN)', 2, NULL, NULL),
(1580, 'AYUNGON', 2, NULL, NULL),
(1581, 'BACONG', 2, NULL, NULL),
(1582, 'BAIS CITY', 2, NULL, NULL),
(1583, 'BASAY', 2, NULL, NULL),
(1584, 'CITY OF BAYAWAN (TULONG)', 2, NULL, NULL),
(1585, 'BINDOY (PAYABON)', 2, NULL, NULL),
(1586, 'CANLAON CITY', 2, NULL, NULL),
(1587, 'DAUIN', 2, NULL, NULL),
(1588, 'DUMAGUETE CITY (Capital)', 2, NULL, NULL),
(1589, 'CITY OF GUIHULNGAN', 2, NULL, NULL),
(1590, 'JIMALALUD', 2, NULL, NULL),
(1591, 'LA LIBERTAD', 2, NULL, NULL),
(1592, 'MABINAY', 2, NULL, NULL),
(1593, 'MANJUYOD', 2, NULL, NULL),
(1594, 'PAMPLONA', 2, NULL, NULL),
(1595, 'SAN JOSE', 2, NULL, NULL),
(1596, 'SANTA CATALINA', 2, NULL, NULL),
(1597, 'SIATON', 2, NULL, NULL),
(1598, 'SIBULAN', 2, NULL, NULL),
(1599, 'CITY OF TANJAY', 2, NULL, NULL),
(1600, 'TAYASAN', 2, NULL, NULL),
(1601, 'VALENCIA (LUZURRIAGA)', 2, NULL, NULL),
(1602, 'VALLEHERMOSO', 2, NULL, NULL),
(1603, 'ZAMBOANGUITA', 2, NULL, NULL),
(1604, 'ENRIQUE VILLANUEVA', 2, NULL, NULL),
(1605, 'LARENA', 2, NULL, NULL),
(1606, 'LAZI', 2, NULL, NULL),
(1607, 'MARIA', 2, NULL, NULL),
(1608, 'SAN JUAN', 2, NULL, NULL),
(1609, 'SIQUIJOR (Capital)', 2, NULL, NULL),
(1610, 'ARTECHE', 2, NULL, NULL),
(1611, 'BALANGIGA', 2, NULL, NULL),
(1612, 'BALANGKAYAN', 2, NULL, NULL),
(1613, 'CITY OF BORONGAN (Capital)', 2, NULL, NULL),
(1614, 'CAN-AVID', 2, NULL, NULL),
(1615, 'DOLORES', 2, NULL, NULL),
(1616, 'GENERAL MACARTHUR', 2, NULL, NULL),
(1617, 'GIPORLOS', 2, NULL, NULL),
(1618, 'GUIUAN', 2, NULL, NULL),
(1619, 'HERNANI', 2, NULL, NULL),
(1620, 'JIPAPAD', 2, NULL, NULL),
(1621, 'LAWAAN', 2, NULL, NULL),
(1622, 'LLORENTE', 2, NULL, NULL),
(1623, 'MASLOG', 2, NULL, NULL),
(1624, 'MAYDOLONG', 2, NULL, NULL),
(1625, 'MERCEDES', 2, NULL, NULL),
(1626, 'ORAS', 2, NULL, NULL),
(1627, 'QUINAPONDAN', 2, NULL, NULL),
(1628, 'SALCEDO', 2, NULL, NULL),
(1629, 'SAN JULIAN', 2, NULL, NULL),
(1630, 'SAN POLICARPO', 2, NULL, NULL),
(1631, 'SULAT', 2, NULL, NULL),
(1632, 'TAFT', 2, NULL, NULL),
(1633, 'ABUYOG', 2, NULL, NULL),
(1634, 'ALANGALANG', 2, NULL, NULL),
(1635, 'ALBUERA', 2, NULL, NULL),
(1636, 'BABATNGON', 2, NULL, NULL),
(1637, 'BARUGO', 2, NULL, NULL),
(1638, 'BATO', 2, NULL, NULL),
(1639, 'CITY OF BAYBAY', 2, NULL, NULL),
(1640, 'BURAUEN', 2, NULL, NULL),
(1641, 'CALUBIAN', 2, NULL, NULL),
(1642, 'CAPOOCAN', 2, NULL, NULL),
(1643, 'CARIGARA', 2, NULL, NULL),
(1644, 'DAGAMI', 2, NULL, NULL),
(1645, 'DULAG', 2, NULL, NULL),
(1646, 'HILONGOS', 2, NULL, NULL),
(1647, 'HINDANG', 2, NULL, NULL),
(1648, 'INOPACAN', 2, NULL, NULL),
(1649, 'ISABEL', 2, NULL, NULL),
(1650, 'JARO', 2, NULL, NULL),
(1651, 'JAVIER (BUGHO)', 2, NULL, NULL),
(1652, 'JULITA', 2, NULL, NULL),
(1653, 'KANANGA', 2, NULL, NULL),
(1654, 'LA PAZ', 2, NULL, NULL),
(1655, 'LEYTE', 2, NULL, NULL),
(1656, 'MACARTHUR', 2, NULL, NULL),
(1657, 'MAHAPLAG', 2, NULL, NULL),
(1658, 'MATAG-OB', 2, NULL, NULL),
(1659, 'MATALOM', 2, NULL, NULL),
(1660, 'MAYORGA', 2, NULL, NULL),
(1661, 'MERIDA', 2, NULL, NULL),
(1662, 'ORMOC CITY', 2, NULL, NULL),
(1663, 'PALO', 2, NULL, NULL),
(1664, 'PALOMPON', 2, NULL, NULL),
(1665, 'PASTRANA', 2, NULL, NULL),
(1666, 'SAN ISIDRO', 2, NULL, NULL),
(1667, 'SAN MIGUEL', 2, NULL, NULL),
(1668, 'SANTA FE', 2, NULL, NULL),
(1669, 'TABANGO', 2, NULL, NULL),
(1670, 'TABONTABON', 2, NULL, NULL),
(1671, 'TACLOBAN CITY (Capital)', 2, NULL, NULL),
(1672, 'TANAUAN', 2, NULL, NULL),
(1673, 'TOLOSA', 2, NULL, NULL),
(1674, 'TUNGA', 2, NULL, NULL),
(1675, 'VILLABA', 2, NULL, NULL),
(1676, 'ALLEN', 2, NULL, NULL),
(1677, 'BIRI', 2, NULL, NULL),
(1678, 'BOBON', 2, NULL, NULL),
(1679, 'CAPUL', 2, NULL, NULL),
(1680, 'CATARMAN (Capital)', 2, NULL, NULL),
(1681, 'CATUBIG', 2, NULL, NULL),
(1682, 'GAMAY', 2, NULL, NULL),
(1683, 'LAOANG', 2, NULL, NULL),
(1684, 'LAPINIG', 2, NULL, NULL),
(1685, 'LAS NAVAS', 2, NULL, NULL),
(1686, 'LAVEZARES', 2, NULL, NULL),
(1687, 'MAPANAS', 2, NULL, NULL),
(1688, 'MONDRAGON', 2, NULL, NULL),
(1689, 'PALAPAG', 2, NULL, NULL),
(1690, 'PAMBUJAN', 2, NULL, NULL),
(1691, 'ROSARIO', 2, NULL, NULL),
(1692, 'SAN ANTONIO', 2, NULL, NULL),
(1693, 'SAN ISIDRO', 2, NULL, NULL),
(1694, 'SAN JOSE', 2, NULL, NULL),
(1695, 'SAN ROQUE', 2, NULL, NULL),
(1696, 'SAN VICENTE', 2, NULL, NULL),
(1697, 'SILVINO LOBOS', 2, NULL, NULL),
(1698, 'VICTORIA', 2, NULL, NULL),
(1699, 'LOPE DE VEGA', 2, NULL, NULL),
(1700, 'ALMAGRO', 2, NULL, NULL),
(1701, 'BASEY', 2, NULL, NULL),
(1702, 'CALBAYOG CITY', 2, NULL, NULL),
(1703, 'CALBIGA', 2, NULL, NULL),
(1704, 'CITY OF CATBALOGAN (Capital)', 2, NULL, NULL),
(1705, 'DARAM', 2, NULL, NULL),
(1706, 'GANDARA', 2, NULL, NULL),
(1707, 'HINABANGAN', 2, NULL, NULL),
(1708, 'JIABONG', 2, NULL, NULL),
(1709, 'MARABUT', 2, NULL, NULL),
(1710, 'MATUGUINAO', 2, NULL, NULL),
(1711, 'MOTIONG', 2, NULL, NULL),
(1712, 'PINABACDAO', 2, NULL, NULL),
(1713, 'SAN JOSE DE BUAN', 2, NULL, NULL),
(1714, 'SAN SEBASTIAN', 2, NULL, NULL),
(1715, 'SANTA MARGARITA', 2, NULL, NULL),
(1716, 'SANTA RITA', 2, NULL, NULL),
(1717, 'SANTO NIÑO', 2, NULL, NULL),
(1718, 'TALALORA', 2, NULL, NULL),
(1719, 'TARANGNAN', 2, NULL, NULL),
(1720, 'VILLAREAL', 2, NULL, NULL),
(1721, 'PARANAS (WRIGHT)', 2, NULL, NULL),
(1722, 'ZUMARRAGA', 2, NULL, NULL),
(1723, 'TAGAPUL-AN', 2, NULL, NULL),
(1724, 'SAN JORGE', 2, NULL, NULL),
(1725, 'PAGSANGHAN', 2, NULL, NULL),
(1726, 'ANAHAWAN', 2, NULL, NULL),
(1727, 'BONTOC', 2, NULL, NULL),
(1728, 'HINUNANGAN', 2, NULL, NULL),
(1729, 'HINUNDAYAN', 2, NULL, NULL),
(1730, 'LIBAGON', 2, NULL, NULL),
(1731, 'LILOAN', 2, NULL, NULL),
(1732, 'CITY OF MAASIN (Capital)', 2, NULL, NULL),
(1733, 'MACROHON', 2, NULL, NULL),
(1734, 'MALITBOG', 2, NULL, NULL),
(1735, 'PADRE BURGOS', 2, NULL, NULL),
(1736, 'PINTUYAN', 2, NULL, NULL),
(1737, 'SAINT BERNARD', 2, NULL, NULL),
(1738, 'SAN FRANCISCO', 2, NULL, NULL),
(1739, 'SAN JUAN (CABALIAN)', 2, NULL, NULL),
(1740, 'SAN RICARDO', 2, NULL, NULL),
(1741, 'SILAGO', 2, NULL, NULL),
(1742, 'SOGOD', 2, NULL, NULL),
(1743, 'TOMAS OPPUS', 2, NULL, NULL),
(1744, 'LIMASAWA', 2, NULL, NULL),
(1745, 'ALMERIA', 2, NULL, NULL),
(1746, 'BILIRAN', 2, NULL, NULL),
(1747, 'CABUCGAYAN', 2, NULL, NULL),
(1748, 'CAIBIRAN', 2, NULL, NULL),
(1749, 'CULABA', 2, NULL, NULL),
(1750, 'KAWAYAN', 2, NULL, NULL),
(1751, 'MARIPIPI', 2, NULL, NULL),
(1752, 'NAVAL (Capital)', 2, NULL, NULL),
(1753, 'DAPITAN CITY', 2, NULL, NULL),
(1754, 'DIPOLOG CITY (Capital)', 2, NULL, NULL),
(1755, 'KATIPUNAN', 2, NULL, NULL),
(1756, 'LA LIBERTAD', 2, NULL, NULL),
(1757, 'LABASON', 2, NULL, NULL),
(1758, 'LILOY', 2, NULL, NULL),
(1759, 'MANUKAN', 2, NULL, NULL),
(1760, 'MUTIA', 2, NULL, NULL),
(1761, 'PIÑAN (NEW PIÑAN)', 2, NULL, NULL),
(1762, 'POLANCO', 2, NULL, NULL),
(1763, 'PRES. MANUEL A. ROXAS', 2, NULL, NULL),
(1764, 'RIZAL', 2, NULL, NULL),
(1765, 'SALUG', 2, NULL, NULL),
(1766, 'SERGIO OSMEÑA SR.', 2, NULL, NULL),
(1767, 'SIAYAN', 2, NULL, NULL),
(1768, 'SIBUCO', 2, NULL, NULL),
(1769, 'SIBUTAD', 2, NULL, NULL),
(1770, 'SINDANGAN', 2, NULL, NULL),
(1771, 'SIOCON', 2, NULL, NULL),
(1772, 'SIRAWAI', 2, NULL, NULL),
(1773, 'TAMPILISAN', 2, NULL, NULL),
(1774, 'JOSE DALMAN (PONOT)', 2, NULL, NULL),
(1775, 'GUTALAC', 2, NULL, NULL),
(1776, 'BALIGUIAN', 2, NULL, NULL),
(1777, 'GODOD', 2, NULL, NULL),
(1778, 'BACUNGAN (Leon T. Postigo)', 2, NULL, NULL),
(1779, 'KALAWIT', 2, NULL, NULL),
(1780, 'AURORA', 2, NULL, NULL),
(1781, 'BAYOG', 2, NULL, NULL),
(1782, 'DIMATALING', 2, NULL, NULL),
(1783, 'DINAS', 2, NULL, NULL),
(1784, 'DUMALINAO', 2, NULL, NULL),
(1785, 'DUMINGAG', 2, NULL, NULL),
(1786, 'KUMALARANG', 2, NULL, NULL),
(1787, 'LABANGAN', 2, NULL, NULL),
(1788, 'LAPUYAN', 2, NULL, NULL),
(1789, 'MAHAYAG', 2, NULL, NULL),
(1790, 'MARGOSATUBIG', 2, NULL, NULL),
(1791, 'MIDSALIP', 2, NULL, NULL),
(1792, 'MOLAVE', 2, NULL, NULL),
(1793, 'PAGADIAN CITY (Capital)', 2, NULL, NULL),
(1794, 'RAMON MAGSAYSAY (LIARGO)', 2, NULL, NULL),
(1795, 'SAN MIGUEL', 2, NULL, NULL),
(1796, 'SAN PABLO', 2, NULL, NULL),
(1797, 'TABINA', 2, NULL, NULL),
(1798, 'TAMBULIG', 2, NULL, NULL),
(1799, 'TUKURAN', 2, NULL, NULL),
(1800, 'ZAMBOANGA CITY', 2, NULL, NULL),
(1801, 'LAKEWOOD', 2, NULL, NULL),
(1802, 'JOSEFINA', 2, NULL, NULL),
(1803, 'PITOGO', 2, NULL, NULL),
(1804, 'SOMINOT (DON MARIANO MARCOS)', 2, NULL, NULL),
(1805, 'VINCENZO A. SAGUN', 2, NULL, NULL),
(1806, 'GUIPOS', 2, NULL, NULL),
(1807, 'TIGBAO', 2, NULL, NULL),
(1808, 'ALICIA', 2, NULL, NULL),
(1809, 'BUUG', 2, NULL, NULL),
(1810, 'DIPLAHAN', 2, NULL, NULL),
(1811, 'IMELDA', 2, NULL, NULL),
(1812, 'IPIL (Capital)', 2, NULL, NULL),
(1813, 'KABASALAN', 2, NULL, NULL),
(1814, 'MABUHAY', 2, NULL, NULL),
(1815, 'MALANGAS', 2, NULL, NULL),
(1816, 'NAGA', 2, NULL, NULL),
(1817, 'OLUTANGA', 2, NULL, NULL),
(1818, 'PAYAO', 2, NULL, NULL),
(1819, 'ROSELLER LIM', 2, NULL, NULL),
(1820, 'SIAY', 2, NULL, NULL),
(1821, 'TALUSAN', 2, NULL, NULL),
(1822, 'TITAY', 2, NULL, NULL),
(1823, 'TUNGAWAN', 2, NULL, NULL),
(1824, 'CITY OF ISABELA', 2, NULL, NULL),
(1825, 'BAUNGON', 2, NULL, NULL),
(1826, 'DAMULOG', 2, NULL, NULL),
(1827, 'DANGCAGAN', 2, NULL, NULL),
(1828, 'DON CARLOS', 2, NULL, NULL),
(1829, 'IMPASUG-ONG', 2, NULL, NULL),
(1830, 'KADINGILAN', 2, NULL, NULL),
(1831, 'KALILANGAN', 2, NULL, NULL),
(1832, 'KIBAWE', 2, NULL, NULL),
(1833, 'KITAOTAO', 2, NULL, NULL),
(1834, 'LANTAPAN', 2, NULL, NULL),
(1835, 'LIBONA', 2, NULL, NULL),
(1836, 'CITY OF MALAYBALAY (Capital)', 2, NULL, NULL),
(1837, 'MALITBOG', 2, NULL, NULL),
(1838, 'MANOLO FORTICH', 2, NULL, NULL),
(1839, 'MARAMAG', 2, NULL, NULL),
(1840, 'PANGANTUCAN', 2, NULL, NULL),
(1841, 'QUEZON', 2, NULL, NULL),
(1842, 'SAN FERNANDO', 2, NULL, NULL),
(1843, 'SUMILAO', 2, NULL, NULL),
(1844, 'TALAKAG', 2, NULL, NULL),
(1845, 'CITY OF VALENCIA', 2, NULL, NULL),
(1846, 'CABANGLASAN', 2, NULL, NULL),
(1847, 'CATARMAN', 2, NULL, NULL),
(1848, 'GUINSILIBAN', 2, NULL, NULL),
(1849, 'MAHINOG', 2, NULL, NULL),
(1850, 'MAMBAJAO (Capital)', 2, NULL, NULL),
(1851, 'SAGAY', 2, NULL, NULL),
(1852, 'BACOLOD', 2, NULL, NULL),
(1853, 'BALOI', 2, NULL, NULL),
(1854, 'BAROY', 2, NULL, NULL),
(1855, 'ILIGAN CITY', 2, NULL, NULL),
(1856, 'KAPATAGAN', 2, NULL, NULL),
(1857, 'SULTAN NAGA DIMAPORO (KAROMATAN)', 2, NULL, NULL),
(1858, 'KAUSWAGAN', 2, NULL, NULL),
(1859, 'KOLAMBUGAN', 2, NULL, NULL),
(1860, 'LALA', 2, NULL, NULL),
(1861, 'LINAMON', 2, NULL, NULL),
(1862, 'MAGSAYSAY', 2, NULL, NULL),
(1863, 'MAIGO', 2, NULL, NULL),
(1864, 'MATUNGAO', 2, NULL, NULL),
(1865, 'MUNAI', 2, NULL, NULL),
(1866, 'NUNUNGAN', 2, NULL, NULL),
(1867, 'PANTAO RAGAT', 2, NULL, NULL),
(1868, 'POONA PIAGAPO', 2, NULL, NULL),
(1869, 'SALVADOR', 2, NULL, NULL),
(1870, 'SAPAD', 2, NULL, NULL),
(1871, 'TAGOLOAN', 2, NULL, NULL),
(1872, 'TANGCAL', 2, NULL, NULL),
(1873, 'TUBOD (Capital)', 2, NULL, NULL),
(1874, 'PANTAR', 2, NULL, NULL),
(1875, 'ALORAN', 2, NULL, NULL),
(1876, 'BALIANGAO', 2, NULL, NULL),
(1877, 'BONIFACIO', 2, NULL, NULL),
(1878, 'CALAMBA', 2, NULL, NULL),
(1879, 'CLARIN', 2, NULL, NULL),
(1880, 'CONCEPCION', 2, NULL, NULL),
(1881, 'JIMENEZ', 2, NULL, NULL),
(1882, 'LOPEZ JAENA', 2, NULL, NULL),
(1883, 'OROQUIETA CITY (Capital)', 2, NULL, NULL),
(1884, 'OZAMIS CITY', 2, NULL, NULL),
(1885, 'PANAON', 2, NULL, NULL),
(1886, 'PLARIDEL', 2, NULL, NULL),
(1887, 'SAPANG DALAGA', 2, NULL, NULL),
(1888, 'SINACABAN', 2, NULL, NULL),
(1889, 'TANGUB CITY', 2, NULL, NULL),
(1890, 'TUDELA', 2, NULL, NULL),
(1891, 'DON VICTORIANO CHIONGBIAN  (DON MARIANO MARCOS)', 2, NULL, NULL),
(1892, 'ALUBIJID', 2, NULL, NULL),
(1893, 'BALINGASAG', 2, NULL, NULL),
(1894, 'BALINGOAN', 2, NULL, NULL),
(1895, 'BINUANGAN', 2, NULL, NULL),
(1896, 'CAGAYAN DE ORO CITY (Capital)', 2, NULL, NULL),
(1897, 'CLAVERIA', 2, NULL, NULL),
(1898, 'CITY OF EL SALVADOR', 2, NULL, NULL),
(1899, 'GINGOOG CITY', 2, NULL, NULL),
(1900, 'GITAGUM', 2, NULL, NULL),
(1901, 'INITAO', 2, NULL, NULL),
(1902, 'JASAAN', 2, NULL, NULL),
(1903, 'KINOGUITAN', 2, NULL, NULL),
(1904, 'LAGONGLONG', 2, NULL, NULL),
(1905, 'LAGUINDINGAN', 2, NULL, NULL),
(1906, 'LIBERTAD', 2, NULL, NULL),
(1907, 'LUGAIT', 2, NULL, NULL),
(1908, 'MAGSAYSAY (LINUGOS)', 2, NULL, NULL),
(1909, 'MANTICAO', 2, NULL, NULL),
(1910, 'MEDINA', 2, NULL, NULL),
(1911, 'NAAWAN', 2, NULL, NULL),
(1912, 'OPOL', 2, NULL, NULL),
(1913, 'SALAY', 2, NULL, NULL),
(1914, 'SUGBONGCOGON', 2, NULL, NULL),
(1915, 'TAGOLOAN', 2, NULL, NULL),
(1916, 'TALISAYAN', 2, NULL, NULL),
(1917, 'VILLANUEVA', 2, NULL, NULL),
(1918, 'ASUNCION (SAUG)', 2, NULL, NULL),
(1919, 'CARMEN', 2, NULL, NULL),
(1920, 'KAPALONG', 2, NULL, NULL),
(1921, 'NEW CORELLA', 2, NULL, NULL),
(1922, 'CITY OF PANABO', 2, NULL, NULL),
(1923, 'ISLAND GARDEN CITY OF SAMAL', 2, NULL, NULL),
(1924, 'SANTO TOMAS', 2, NULL, NULL),
(1925, 'CITY OF TAGUM (Capital)', 2, NULL, NULL),
(1926, 'TALAINGOD', 2, NULL, NULL),
(1927, 'BRAULIO E. DUJALI', 2, NULL, NULL),
(1928, 'SAN ISIDRO', 2, NULL, NULL),
(1929, 'BANSALAN', 2, NULL, NULL),
(1930, 'DAVAO CITY', 2, NULL, NULL),
(1931, 'CITY OF DIGOS (Capital)', 2, NULL, NULL),
(1932, 'HAGONOY', 2, NULL, NULL),
(1933, 'KIBLAWAN', 2, NULL, NULL),
(1934, 'MAGSAYSAY', 2, NULL, NULL),
(1935, 'MALALAG', 2, NULL, NULL),
(1936, 'MATANAO', 2, NULL, NULL),
(1937, 'PADADA', 2, NULL, NULL),
(1938, 'SANTA CRUZ', 2, NULL, NULL),
(1939, 'SULOP', 2, NULL, NULL),
(1940, 'BAGANGA', 2, NULL, NULL),
(1941, 'BANAYBANAY', 2, NULL, NULL),
(1942, 'BOSTON', 2, NULL, NULL),
(1943, 'CARAGA', 2, NULL, NULL),
(1944, 'CATEEL', 2, NULL, NULL),
(1945, 'GOVERNOR GENEROSO', 2, NULL, NULL),
(1946, 'LUPON', 2, NULL, NULL),
(1947, 'MANAY', 2, NULL, NULL),
(1948, 'CITY OF MATI (Capital)', 2, NULL, NULL),
(1949, 'SAN ISIDRO', 2, NULL, NULL),
(1950, 'TARRAGONA', 2, NULL, NULL),
(1951, 'COMPOSTELA', 2, NULL, NULL),
(1952, 'LAAK (SAN VICENTE)', 2, NULL, NULL),
(1953, 'MABINI (DOÑA ALICIA)', 2, NULL, NULL),
(1954, 'MACO', 2, NULL, NULL),
(1955, 'MARAGUSAN (SAN MARIANO)', 2, NULL, NULL),
(1956, 'MAWAB', 2, NULL, NULL),
(1957, 'MONKAYO', 2, NULL, NULL),
(1958, 'MONTEVISTA', 2, NULL, NULL),
(1959, 'NABUNTURAN (Capital)', 2, NULL, NULL),
(1960, 'NEW BATAAN', 2, NULL, NULL),
(1961, 'PANTUKAN', 2, NULL, NULL),
(1962, 'DON MARCELINO', 2, NULL, NULL),
(1963, 'JOSE ABAD SANTOS (TRINIDAD)', 2, NULL, NULL),
(1964, 'MALITA', 2, NULL, NULL),
(1965, 'SANTA MARIA', 2, NULL, NULL),
(1966, 'SARANGANI', 2, NULL, NULL),
(1967, 'ALAMADA', 2, NULL, NULL),
(1968, 'CARMEN', 2, NULL, NULL),
(1969, 'KABACAN', 2, NULL, NULL),
(1970, 'CITY OF KIDAPAWAN (Capital)', 2, NULL, NULL),
(1971, 'LIBUNGAN', 2, NULL, NULL),
(1972, 'MAGPET', 2, NULL, NULL),
(1973, 'MAKILALA', 2, NULL, NULL),
(1974, 'MATALAM', 2, NULL, NULL),
(1975, 'MIDSAYAP', 2, NULL, NULL),
(1976, 'M\'LANG', 2, NULL, NULL),
(1977, 'PIGKAWAYAN', 2, NULL, NULL),
(1978, 'PIKIT', 2, NULL, NULL),
(1979, 'PRESIDENT ROXAS', 2, NULL, NULL),
(1980, 'TULUNAN', 2, NULL, NULL),
(1981, 'ANTIPAS', 2, NULL, NULL),
(1982, 'BANISILAN', 2, NULL, NULL),
(1983, 'ALEOSAN', 2, NULL, NULL),
(1984, 'ARAKAN', 2, NULL, NULL),
(1985, 'BANGA', 2, NULL, NULL),
(1986, 'GENERAL SANTOS CITY (DADIANGAS)', 2, NULL, NULL),
(1987, 'CITY OF KORONADAL (Capital)', 2, NULL, NULL),
(1988, 'NORALA', 2, NULL, NULL),
(1989, 'POLOMOLOK', 2, NULL, NULL),
(1990, 'SURALLAH', 2, NULL, NULL),
(1991, 'TAMPAKAN', 2, NULL, NULL),
(1992, 'TANTANGAN', 2, NULL, NULL),
(1993, 'T\'BOLI', 2, NULL, NULL),
(1994, 'TUPI', 2, NULL, NULL),
(1995, 'SANTO NIÑO', 2, NULL, NULL),
(1996, 'LAKE SEBU', 2, NULL, NULL),
(1997, 'BAGUMBAYAN', 2, NULL, NULL),
(1998, 'COLUMBIO', 2, NULL, NULL),
(1999, 'ESPERANZA', 2, NULL, NULL),
(2000, 'ISULAN (Capital)', 2, NULL, NULL),
(2001, 'KALAMANSIG', 2, NULL, NULL),
(2002, 'LEBAK', 2, NULL, NULL),
(2003, 'LUTAYAN', 2, NULL, NULL),
(2004, 'LAMBAYONG (MARIANO MARCOS)', 2, NULL, NULL),
(2005, 'PALIMBANG', 2, NULL, NULL),
(2006, 'PRESIDENT QUIRINO', 2, NULL, NULL),
(2007, 'CITY OF TACURONG', 2, NULL, NULL),
(2008, 'SEN. NINOY AQUINO', 2, NULL, NULL),
(2009, 'ALABEL (Capital)', 2, NULL, NULL),
(2010, 'GLAN', 2, NULL, NULL),
(2011, 'KIAMBA', 2, NULL, NULL),
(2012, 'MAASIM', 2, NULL, NULL),
(2013, 'MAITUM', 2, NULL, NULL),
(2014, 'MALAPATAN', 2, NULL, NULL),
(2015, 'MALUNGON', 2, NULL, NULL),
(2016, 'COTABATO CITY', 2, NULL, NULL),
(2017, 'TONDO I / II', 2, NULL, NULL),
(2018, 'BINONDO', 2, NULL, NULL),
(2019, 'QUIAPO', 2, NULL, NULL),
(2020, 'SAN NICOLAS', 2, NULL, NULL),
(2021, 'SANTA CRUZ', 2, NULL, NULL),
(2022, 'SAMPALOC', 2, NULL, NULL),
(2023, 'SAN MIGUEL', 2, NULL, NULL),
(2024, 'ERMITA', 2, NULL, NULL),
(2025, 'INTRAMUROS', 2, NULL, NULL),
(2026, 'MALATE', 2, NULL, NULL),
(2027, 'PACO', 2, NULL, NULL),
(2028, 'PANDACAN', 2, NULL, NULL),
(2029, 'PORT AREA', 2, NULL, NULL),
(2030, 'SANTA ANA', 2, NULL, NULL),
(2031, 'CITY OF MANDALUYONG', 2, NULL, NULL),
(2032, 'CITY OF MARIKINA', 2, NULL, NULL),
(2033, 'CITY OF PASIG', 2, NULL, NULL),
(2034, 'QUEZON CITY', 2, NULL, NULL),
(2035, 'CITY OF SAN JUAN', 2, NULL, NULL),
(2036, 'CALOOCAN CITY', 2, NULL, NULL),
(2037, 'CITY OF MALABON', 2, NULL, NULL),
(2038, 'CITY OF NAVOTAS', 2, NULL, NULL),
(2039, 'CITY OF VALENZUELA', 2, NULL, NULL),
(2040, 'CITY OF LAS PIÑAS', 2, NULL, NULL),
(2041, 'CITY OF MAKATI', 2, NULL, NULL),
(2042, 'CITY OF MUNTINLUPA', 2, NULL, NULL),
(2043, 'CITY OF PARAÑAQUE', 2, NULL, NULL),
(2044, 'PASAY CITY', 2, NULL, NULL),
(2045, 'PATEROS', 2, NULL, NULL),
(2046, 'TAGUIG CITY', 2, NULL, NULL),
(2047, 'BANGUED (Capital)', 2, NULL, NULL),
(2048, 'BOLINEY', 2, NULL, NULL),
(2049, 'BUCAY', 2, NULL, NULL),
(2050, 'BUCLOC', 2, NULL, NULL),
(2051, 'DAGUIOMAN', 2, NULL, NULL),
(2052, 'DANGLAS', 2, NULL, NULL),
(2053, 'DOLORES', 2, NULL, NULL),
(2054, 'LA PAZ', 2, NULL, NULL),
(2055, 'LACUB', 2, NULL, NULL),
(2056, 'LAGANGILANG', 2, NULL, NULL),
(2057, 'LAGAYAN', 2, NULL, NULL),
(2058, 'LANGIDEN', 2, NULL, NULL),
(2059, 'LICUAN-BAAY (LICUAN)', 2, NULL, NULL),
(2060, 'LUBA', 2, NULL, NULL),
(2061, 'MALIBCONG', 2, NULL, NULL),
(2062, 'MANABO', 2, NULL, NULL),
(2063, 'PEÑARRUBIA', 2, NULL, NULL),
(2064, 'PIDIGAN', 2, NULL, NULL),
(2065, 'PILAR', 2, NULL, NULL),
(2066, 'SALLAPADAN', 2, NULL, NULL),
(2067, 'SAN ISIDRO', 2, NULL, NULL),
(2068, 'SAN JUAN', 2, NULL, NULL),
(2069, 'SAN QUINTIN', 2, NULL, NULL),
(2070, 'TAYUM', 2, NULL, NULL),
(2071, 'TINEG', 2, NULL, NULL),
(2072, 'TUBO', 2, NULL, NULL),
(2073, 'VILLAVICIOSA', 2, NULL, NULL),
(2074, 'ATOK', 2, NULL, NULL),
(2075, 'BAGUIO CITY', 2, NULL, NULL),
(2076, 'BAKUN', 2, NULL, NULL),
(2077, 'BOKOD', 2, NULL, NULL),
(2078, 'BUGUIAS', 2, NULL, NULL),
(2079, 'ITOGON', 2, NULL, NULL),
(2080, 'KABAYAN', 2, NULL, NULL),
(2081, 'KAPANGAN', 2, NULL, NULL),
(2082, 'KIBUNGAN', 2, NULL, NULL),
(2083, 'LA TRINIDAD (Capital)', 2, NULL, NULL),
(2084, 'MANKAYAN', 2, NULL, NULL),
(2085, 'SABLAN', 2, NULL, NULL),
(2086, 'TUBA', 2, NULL, NULL),
(2087, 'TUBLAY', 2, NULL, NULL),
(2088, 'BANAUE', 2, NULL, NULL),
(2089, 'HUNGDUAN', 2, NULL, NULL),
(2090, 'KIANGAN', 2, NULL, NULL),
(2091, 'LAGAWE (Capital)', 2, NULL, NULL),
(2092, 'LAMUT', 2, NULL, NULL),
(2093, 'MAYOYAO', 2, NULL, NULL),
(2094, 'ALFONSO LISTA (POTIA)', 2, NULL, NULL),
(2095, 'AGUINALDO', 2, NULL, NULL),
(2096, 'HINGYON', 2, NULL, NULL),
(2097, 'TINOC', 2, NULL, NULL),
(2098, 'ASIPULO', 2, NULL, NULL),
(2099, 'BALBALAN', 2, NULL, NULL),
(2100, 'LUBUAGAN', 2, NULL, NULL),
(2101, 'PASIL', 2, NULL, NULL),
(2102, 'PINUKPUK', 2, NULL, NULL),
(2103, 'RIZAL (LIWAN)', 2, NULL, NULL),
(2104, 'CITY OF TABUK (Capital)', 2, NULL, NULL),
(2105, 'TANUDAN', 2, NULL, NULL),
(2106, 'TINGLAYAN', 2, NULL, NULL),
(2107, 'BARLIG', 2, NULL, NULL),
(2108, 'BAUKO', 2, NULL, NULL),
(2109, 'BESAO', 2, NULL, NULL),
(2110, 'BONTOC (Capital)', 2, NULL, NULL),
(2111, 'NATONIN', 2, NULL, NULL),
(2112, 'PARACELIS', 2, NULL, NULL),
(2113, 'SABANGAN', 2, NULL, NULL),
(2114, 'SADANGA', 2, NULL, NULL),
(2115, 'SAGADA', 2, NULL, NULL),
(2116, 'TADIAN', 2, NULL, NULL),
(2117, 'CALANASAN (BAYAG)', 2, NULL, NULL),
(2118, 'CONNER', 2, NULL, NULL),
(2119, 'FLORA', 2, NULL, NULL),
(2120, 'KABUGAO (Capital)', 2, NULL, NULL),
(2121, 'LUNA', 2, NULL, NULL),
(2122, 'PUDTOL', 2, NULL, NULL),
(2123, 'SANTA MARCELA', 2, NULL, NULL),
(2124, 'CITY OF LAMITAN', 2, NULL, NULL),
(2125, 'LANTAWAN', 2, NULL, NULL),
(2126, 'MALUSO', 2, NULL, NULL),
(2127, 'SUMISIP', 2, NULL, NULL),
(2128, 'TIPO-TIPO', 2, NULL, NULL),
(2129, 'TUBURAN', 2, NULL, NULL),
(2130, 'AKBAR', 2, NULL, NULL),
(2131, 'AL-BARKA', 2, NULL, NULL),
(2132, 'HADJI MOHAMMAD AJUL', 2, NULL, NULL),
(2133, 'UNGKAYA PUKAN', 2, NULL, NULL),
(2134, 'HADJI MUHTAMAD', 2, NULL, NULL),
(2135, 'TABUAN-LASA', 2, NULL, NULL),
(2136, 'BACOLOD-KALAWI (BACOLOD GRANDE)', 2, NULL, NULL),
(2137, 'BALABAGAN', 2, NULL, NULL),
(2138, 'BALINDONG (WATU)', 2, NULL, NULL),
(2139, 'BAYANG', 2, NULL, NULL),
(2140, 'BINIDAYAN', 2, NULL, NULL),
(2141, 'BUBONG', 2, NULL, NULL),
(2142, 'BUTIG', 2, NULL, NULL),
(2143, 'GANASSI', 2, NULL, NULL),
(2144, 'KAPAI', 2, NULL, NULL),
(2145, 'LUMBA-BAYABAO (MAGUING)', 2, NULL, NULL),
(2146, 'LUMBATAN', 2, NULL, NULL),
(2147, 'MADALUM', 2, NULL, NULL),
(2148, 'MADAMBA', 2, NULL, NULL),
(2149, 'MALABANG', 2, NULL, NULL),
(2150, 'MARANTAO', 2, NULL, NULL),
(2151, 'MARAWI CITY (Capital)', 2, NULL, NULL),
(2152, 'MASIU', 2, NULL, NULL),
(2153, 'MULONDO', 2, NULL, NULL),
(2154, 'PAGAYAWAN (TATARIKAN)', 2, NULL, NULL),
(2155, 'PIAGAPO', 2, NULL, NULL),
(2156, 'POONA BAYABAO (GATA)', 2, NULL, NULL),
(2157, 'PUALAS', 2, NULL, NULL),
(2158, 'DITSAAN-RAMAIN', 2, NULL, NULL),
(2159, 'SAGUIARAN', 2, NULL, NULL),
(2160, 'TAMPARAN', 2, NULL, NULL),
(2161, 'TARAKA', 2, NULL, NULL),
(2162, 'TUBARAN', 2, NULL, NULL),
(2163, 'TUGAYA', 2, NULL, NULL),
(2164, 'WAO', 2, NULL, NULL),
(2165, 'MAROGONG', 2, NULL, NULL),
(2166, 'CALANOGAS', 2, NULL, NULL),
(2167, 'BUADIPOSO-BUNTONG', 2, NULL, NULL),
(2168, 'MAGUING', 2, NULL, NULL),
(2169, 'PICONG (SULTAN GUMANDER)', 2, NULL, NULL),
(2170, 'LUMBAYANAGUE', 2, NULL, NULL),
(2171, 'BUMBARAN', 2, NULL, NULL),
(2172, 'TAGOLOAN II', 2, NULL, NULL),
(2173, 'KAPATAGAN', 2, NULL, NULL),
(2174, 'SULTAN DUMALONDONG', 2, NULL, NULL),
(2175, 'LUMBACA-UNAYAN', 2, NULL, NULL),
(2176, 'AMPATUAN', 2, NULL, NULL),
(2177, 'BULDON', 2, NULL, NULL),
(2178, 'BULUAN', 2, NULL, NULL),
(2179, 'DATU PAGLAS', 2, NULL, NULL),
(2180, 'DATU PIANG', 2, NULL, NULL),
(2181, 'DATU ODIN SINSUAT (DINAIG)', 2, NULL, NULL),
(2182, 'SHARIFF AGUAK (MAGANOY) (Capital)', 2, NULL, NULL),
(2183, 'MATANOG', 2, NULL, NULL),
(2184, 'PAGALUNGAN', 2, NULL, NULL),
(2185, 'PARANG', 2, NULL, NULL),
(2186, 'SULTAN KUDARAT (NULING)', 2, NULL, NULL),
(2187, 'SULTAN SA BARONGIS (LAMBAYONG)', 2, NULL, NULL),
(2188, 'KABUNTALAN (TUMBAO)', 2, NULL, NULL),
(2189, 'UPI', 2, NULL, NULL),
(2190, 'TALAYAN', 2, NULL, NULL),
(2191, 'SOUTH UPI', 2, NULL, NULL),
(2192, 'BARIRA', 2, NULL, NULL),
(2193, 'GEN. S. K. PENDATUN', 2, NULL, NULL),
(2194, 'MAMASAPANO', 2, NULL, NULL),
(2195, 'TALITAY', 2, NULL, NULL),
(2196, 'PAGAGAWAN', 2, NULL, NULL),
(2197, 'PAGLAT', 2, NULL, NULL),
(2198, 'SULTAN MASTURA', 2, NULL, NULL),
(2199, 'GUINDULUNGAN', 2, NULL, NULL),
(2200, 'DATU SAUDI-AMPATUAN', 2, NULL, NULL),
(2201, 'DATU UNSAY', 2, NULL, NULL),
(2202, 'DATU ABDULLAH SANGKI', 2, NULL, NULL),
(2203, 'RAJAH BUAYAN', 2, NULL, NULL),
(2204, 'DATU BLAH T. SINSUAT', 2, NULL, NULL),
(2205, 'DATU ANGGAL MIDTIMBANG', 2, NULL, NULL),
(2206, 'MANGUDADATU', 2, NULL, NULL),
(2207, 'PANDAG', 2, NULL, NULL),
(2208, 'NORTHERN KABUNTALAN', 2, NULL, NULL),
(2209, 'DATU HOFFER AMPATUAN', 2, NULL, NULL),
(2210, 'DATU SALIBO', 2, NULL, NULL),
(2211, 'SHARIFF SAYDONA MUSTAPHA', 2, NULL, NULL),
(2212, 'INDANAN', 2, NULL, NULL),
(2213, 'JOLO (Capital)', 2, NULL, NULL),
(2214, 'KALINGALAN CALUANG', 2, NULL, NULL),
(2215, 'LUUK', 2, NULL, NULL),
(2216, 'MAIMBUNG', 2, NULL, NULL),
(2217, 'HADJI PANGLIMA TAHIL (MARUNGGAS)', 2, NULL, NULL),
(2218, 'OLD PANAMAO', 2, NULL, NULL),
(2219, 'PANGUTARAN', 2, NULL, NULL),
(2220, 'PARANG', 2, NULL, NULL),
(2221, 'PATA', 2, NULL, NULL),
(2222, 'PATIKUL', 2, NULL, NULL),
(2223, 'SIASI', 2, NULL, NULL),
(2224, 'TALIPAO', 2, NULL, NULL),
(2225, 'TAPUL', 2, NULL, NULL),
(2226, 'TONGKIL', 2, NULL, NULL),
(2227, 'PANGLIMA ESTINO (NEW PANAMAO)', 2, NULL, NULL),
(2228, 'LUGUS', 2, NULL, NULL),
(2229, 'PANDAMI', 2, NULL, NULL),
(2230, 'OMAR', 2, NULL, NULL),
(2231, 'PANGLIMA SUGALA (BALIMBING)', 2, NULL, NULL),
(2232, 'BONGAO (Capital)', 2, NULL, NULL),
(2233, 'MAPUN (CAGAYAN DE TAWI-TAWI)', 2, NULL, NULL),
(2234, 'SIMUNUL', 2, NULL, NULL),
(2235, 'SITANGKAI', 2, NULL, NULL),
(2236, 'SOUTH UBIAN', 2, NULL, NULL),
(2237, 'TANDUBAS', 2, NULL, NULL),
(2238, 'TURTLE ISLANDS', 2, NULL, NULL),
(2239, 'LANGUYAN', 2, NULL, NULL),
(2240, 'SAPA-SAPA', 2, NULL, NULL),
(2241, 'SIBUTU', 2, NULL, NULL),
(2242, 'BUENAVISTA', 2, NULL, NULL),
(2243, 'BUTUAN CITY (Capital)', 2, NULL, NULL),
(2244, 'CITY OF CABADBARAN', 2, NULL, NULL),
(2245, 'CARMEN', 2, NULL, NULL),
(2246, 'JABONGA', 2, NULL, NULL),
(2247, 'KITCHARAO', 2, NULL, NULL),
(2248, 'LAS NIEVES', 2, NULL, NULL),
(2249, 'MAGALLANES', 2, NULL, NULL),
(2250, 'NASIPIT', 2, NULL, NULL),
(2251, 'SANTIAGO', 2, NULL, NULL),
(2252, 'TUBAY', 2, NULL, NULL),
(2253, 'REMEDIOS T. ROMUALDEZ', 2, NULL, NULL),
(2254, 'CITY OF BAYUGAN', 2, NULL, NULL),
(2255, 'BUNAWAN', 2, NULL, NULL),
(2256, 'ESPERANZA', 2, NULL, NULL),
(2257, 'LA PAZ', 2, NULL, NULL),
(2258, 'LORETO', 2, NULL, NULL),
(2259, 'PROSPERIDAD (Capital)', 2, NULL, NULL),
(2260, 'ROSARIO', 2, NULL, NULL),
(2261, 'SAN FRANCISCO', 2, NULL, NULL),
(2262, 'SAN LUIS', 2, NULL, NULL),
(2263, 'SANTA JOSEFA', 2, NULL, NULL),
(2264, 'TALACOGON', 2, NULL, NULL),
(2265, 'TRENTO', 2, NULL, NULL),
(2266, 'VERUELA', 2, NULL, NULL),
(2267, 'SIBAGAT', 2, NULL, NULL),
(2268, 'ALEGRIA', 2, NULL, NULL),
(2269, 'BACUAG', 2, NULL, NULL),
(2270, 'BURGOS', 2, NULL, NULL),
(2271, 'CLAVER', 2, NULL, NULL),
(2272, 'DAPA', 2, NULL, NULL),
(2273, 'DEL CARMEN', 2, NULL, NULL),
(2274, 'GENERAL LUNA', 2, NULL, NULL),
(2275, 'GIGAQUIT', 2, NULL, NULL),
(2276, 'MAINIT', 2, NULL, NULL),
(2277, 'MALIMONO', 2, NULL, NULL),
(2278, 'PILAR', 2, NULL, NULL),
(2279, 'PLACER', 2, NULL, NULL),
(2280, 'SAN BENITO', 2, NULL, NULL),
(2281, 'SAN FRANCISCO (ANAO-AON)', 2, NULL, NULL),
(2282, 'SAN ISIDRO', 2, NULL, NULL),
(2283, 'SANTA MONICA (SAPAO)', 2, NULL, NULL),
(2284, 'SISON', 2, NULL, NULL),
(2285, 'SOCORRO', 2, NULL, NULL),
(2286, 'SURIGAO CITY (Capital)', 2, NULL, NULL),
(2287, 'TAGANA-AN', 2, NULL, NULL),
(2288, 'TUBOD', 2, NULL, NULL),
(2289, 'BAROBO', 2, NULL, NULL),
(2290, 'BAYABAS', 2, NULL, NULL),
(2291, 'CITY OF BISLIG', 2, NULL, NULL),
(2292, 'CAGWAIT', 2, NULL, NULL),
(2293, 'CANTILAN', 2, NULL, NULL),
(2294, 'CARMEN', 2, NULL, NULL),
(2295, 'CARRASCAL', 2, NULL, NULL),
(2296, 'CORTES', 2, NULL, NULL),
(2297, 'HINATUAN', 2, NULL, NULL),
(2298, 'LANUZA', 2, NULL, NULL),
(2299, 'LIANGA', 2, NULL, NULL),
(2300, 'LINGIG', 2, NULL, NULL),
(2301, 'MADRID', 2, NULL, NULL),
(2302, 'MARIHATAG', 2, NULL, NULL),
(2303, 'SAN AGUSTIN', 2, NULL, NULL),
(2304, 'SAN MIGUEL', 2, NULL, NULL),
(2305, 'TAGBINA', 2, NULL, NULL),
(2306, 'TAGO', 2, NULL, NULL),
(2307, 'CITY OF TANDAG (Capital)', 2, NULL, NULL),
(2308, 'BASILISA (RIZAL)', 2, NULL, NULL),
(2309, 'CAGDIANAO', 2, NULL, NULL),
(2310, 'DINAGAT', 2, NULL, NULL),
(2311, 'LIBJO (ALBOR)', 2, NULL, NULL),
(2312, 'LORETO', 2, NULL, NULL),
(2313, 'SAN JOSE (Capital)', 2, NULL, NULL),
(2314, 'TUBAJON', 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

CREATE TABLE `complaints` (
  `id` int(10) UNSIGNED NOT NULL,
  `accuser_id` int(10) UNSIGNED DEFAULT NULL,
  `accused_id` int(10) UNSIGNED DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'open',
  `action_taken` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `other_comments` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `complaint_categories`
--

CREATE TABLE `complaint_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `complaint_categories`
--

INSERT INTO `complaint_categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Classes', '2018-07-17 11:01:21', '2018-07-17 11:01:21'),
(2, 'Teacher', '2018-07-17 11:01:32', '2018-07-17 11:01:32'),
(3, 'Student', '2018-07-17 11:01:44', '2018-07-17 11:01:44'),
(4, 'System', '2018-07-17 11:02:00', '2018-07-17 11:02:00');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name`, `short_code`, `created_at`, `updated_at`) VALUES
(1, 'China', 'CH', '2018-07-15 04:25:01', '2018-07-15 04:25:01'),
(2, 'Philippines', 'PH', '2018-07-15 04:25:17', '2018-07-15 04:25:17');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 6),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 7),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 8),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 9),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 10),
(8, 1, 'avatar', 'image', 'Avatar', 1, 1, 1, 1, 1, 1, NULL, 2),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 13),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 28),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 5),
(21, 1, 'role_id', 'text', 'Role', 0, 1, 1, 1, 1, 1, NULL, 12),
(22, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(23, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(24, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(25, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 4),
(26, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(27, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '', 6),
(28, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 7),
(29, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(30, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, '', 2),
(31, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, '', 3),
(32, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '', 4),
(33, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, '', 5),
(34, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 6),
(35, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(36, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(37, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, '', 9),
(38, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, '', 10),
(39, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(40, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '', 12),
(41, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 13),
(42, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, '', 14),
(43, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, '', 15),
(44, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(45, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, '', 2),
(46, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '', 3),
(47, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, '', 4),
(48, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 5),
(49, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(50, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, '', 7),
(51, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, '', 8),
(52, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(53, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, '', 10),
(54, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, '', 11),
(55, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, '', 12),
(56, 1, 'first_name', 'text', 'First Name', 1, 1, 1, 1, 1, 1, NULL, 3),
(57, 1, 'settings', 'text', 'Settings', 0, 1, 1, 1, 1, 1, NULL, 11),
(58, 1, 'last_name', 'text', 'Last Name', 0, 1, 1, 1, 1, 1, NULL, 4),
(59, 1, 'middle_name', 'text', 'Middle Name', 0, 1, 1, 1, 1, 1, NULL, 5),
(60, 1, 'birth_date', 'date', 'Birth Date', 0, 1, 1, 1, 1, 1, NULL, 14),
(61, 1, 'gender', 'text', 'Gender', 0, 1, 1, 1, 1, 1, NULL, 15),
(62, 1, 'qq', 'text', 'QQ', 0, 1, 1, 1, 1, 1, NULL, 16),
(63, 1, 'mobile', 'text', 'Mobile', 0, 1, 1, 1, 1, 1, NULL, 17),
(64, 1, 'wechat', 'text', 'Wechat', 0, 1, 1, 1, 1, 1, NULL, 18),
(65, 1, 'address', 'text_area', 'Address', 0, 1, 1, 1, 1, 1, NULL, 19),
(68, 1, 'timezone', 'text', 'Timezone', 1, 1, 1, 1, 1, 1, NULL, 22),
(70, 1, 'immortal', 'number', 'Immortal', 0, 1, 1, 1, 1, 1, NULL, 25),
(71, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(72, 7, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(73, 7, 'address', 'text', 'Address', 0, 1, 1, 1, 1, 1, NULL, 3),
(74, 1, 'user_belongsto_school_relationship', 'relationship', 'schools', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\School\",\"table\":\"schools\",\"type\":\"belongsTo\",\"column\":\"school_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 29),
(75, 1, 'school_id', 'text', 'School', 0, 1, 1, 1, 1, 1, NULL, 23),
(76, 7, 'school_hasmany_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"hasMany\",\"column\":\"school_id\",\"key\":\"id\",\"label\":\"email\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(77, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(78, 8, 'duration_in_days', 'number', 'Duration In Days', 1, 1, 1, 1, 1, 1, NULL, 3),
(80, 8, 'number_of_classes', 'number', 'Number Of Classes', 1, 1, 1, 1, 1, 1, NULL, 6),
(81, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 9),
(82, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 10),
(83, 8, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 2),
(84, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(85, 9, 'package_id', 'text', 'Package Id', 0, 1, 1, 1, 1, 1, NULL, 2),
(87, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 9),
(88, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 10),
(90, 9, 'school_package_belongsto_package_relationship', 'relationship', 'packages', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Package\",\"table\":\"packages\",\"type\":\"belongsTo\",\"column\":\"package_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(91, 10, 'id', 'text', 'Order Number', 1, 0, 0, 0, 0, 0, NULL, 1),
(92, 10, 'school_package_id', 'text', 'School Package Id', 0, 1, 1, 1, 1, 1, NULL, 6),
(96, 10, 'order_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"student_id\",\"key\":\"id\",\"label\":\"email\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 14),
(97, 1, 'user_hasmany_order_relationship', 'relationship', 'orders', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Order\",\"table\":\"orders\",\"type\":\"hasMany\",\"column\":\"student_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\",\"taggable\":\"0\"}', 41),
(98, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 0),
(99, 11, 'start_at', 'text', 'Start At', 1, 1, 1, 0, 1, 1, NULL, 2),
(100, 11, 'end_at', 'text', 'End At', 1, 1, 1, 0, 1, 1, NULL, 3),
(101, 11, 'teacher_id', 'text', 'Teacher Id', 1, 1, 1, 0, 1, 1, NULL, 4),
(102, 11, 'student_id', 'text', 'Student Id', 1, 1, 1, 0, 1, 1, NULL, 5),
(103, 11, 'status', 'text', 'Status', 1, 1, 1, 1, 1, 1, NULL, 6),
(104, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 1, 1, NULL, 7),
(105, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 8),
(106, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(107, 12, 'start_at', 'text', 'Start At', 1, 1, 1, 1, 1, 1, NULL, 2),
(108, 12, 'end_at', 'text', 'End At', 1, 1, 1, 1, 1, 1, NULL, 3),
(109, 12, 'teacher_id', 'text', 'Teacher Id', 1, 1, 1, 1, 1, 1, NULL, 4),
(110, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 5),
(111, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
(112, 12, 'availability_belongsto_user_relationship', 'relationship', 'users', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"teacher_id\",\"key\":\"id\",\"label\":\"email\",\"pivot_table\":\"availabilities\",\"pivot\":\"0\",\"taggable\":null}', 7),
(113, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(114, 13, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, NULL, 2),
(115, 13, 'status', 'text', 'Status', 1, 1, 1, 1, 1, 1, NULL, 3),
(116, 13, 'body', 'text', 'Body', 0, 1, 1, 1, 1, 1, NULL, 4),
(117, 13, 'resume', 'file', 'Resume', 0, 1, 1, 1, 1, 1, NULL, 5),
(118, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 6),
(119, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(120, 14, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(121, 14, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 2),
(122, 14, 'email', 'text', 'Email', 0, 1, 1, 1, 1, 1, NULL, 3),
(123, 14, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 4),
(124, 14, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 5),
(125, 1, 'user_hasmany_availability_relationship', 'relationship', 'availabilities', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Availability\",\"table\":\"availabilities\",\"type\":\"hasMany\",\"column\":\"teacher_id\",\"key\":\"id\",\"label\":\"start_at\",\"pivot_table\":\"applications\",\"pivot\":\"0\",\"taggable\":\"0\"}', 42),
(126, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 8),
(127, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 9),
(128, 10, 'student_id', 'text', 'Student Id', 0, 1, 1, 1, 1, 1, NULL, 12),
(130, 10, 'status', 'text', 'Status', 0, 1, 1, 1, 1, 1, NULL, 4),
(131, 9, 'school_package_hasmany_order_relationship', 'relationship', 'orders', 0, 1, 1, 1, 0, 1, '{\"model\":\"App\\\\Order\",\"table\":\"orders\",\"type\":\"hasMany\",\"column\":\"school_package_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"applications\",\"pivot\":\"0\",\"taggable\":\"0\"}', 15),
(132, 10, 'order_belongsto_school_package_relationship', 'relationship', 'school_packages', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\SchoolPackage\",\"table\":\"school_packages\",\"type\":\"belongsTo\",\"column\":\"school_package_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"applications\",\"pivot\":\"0\",\"taggable\":\"0\"}', 21),
(133, 10, 'remaining_reservation', 'number', 'Remaining Reservation', 0, 1, 1, 1, 1, 1, NULL, 5),
(134, 9, 'price', 'number', 'Price', 1, 1, 1, 1, 1, 1, NULL, 8),
(135, 9, 'number_of_classes', 'text', 'Number Of Classes', 0, 1, 1, 1, 1, 1, NULL, 4),
(136, 9, 'duration_in_days', 'text', 'Duration In Days', 0, 1, 1, 1, 1, 1, NULL, 5),
(137, 9, 'base_price', 'number', 'Base Price', 0, 1, 1, 1, 1, 1, NULL, 6),
(138, 9, 'additional_price', 'number', 'Additional Price', 0, 1, 1, 1, 1, 1, NULL, 7),
(139, 9, 'school_id', 'text', 'School Id', 0, 1, 1, 1, 1, 1, NULL, 11),
(140, 10, 'name', 'text', 'Name', 0, 0, 1, 1, 1, 1, NULL, 3),
(141, 10, 'total_price', 'text', 'Total Price', 0, 1, 1, 1, 1, 1, NULL, 11),
(142, 10, 'duration', 'text', 'Duration', 0, 1, 1, 1, 1, 1, NULL, 13),
(143, 14, 'party_at', 'time', 'Party At', 0, 1, 1, 1, 1, 1, NULL, 6),
(144, 11, 'teacher_timezone', 'text', 'Teacher Timezone', 0, 1, 1, 0, 1, 1, NULL, 9),
(145, 11, 'student_timezone', 'text', 'Student Timezone', 0, 1, 1, 0, 1, 1, NULL, 10),
(146, 10, 'effective_until', 'date', 'Effective Until', 0, 1, 1, 1, 1, 1, NULL, 7),
(147, 11, 'book_url', 'file', 'Upload Book', 0, 1, 1, 1, 1, 1, NULL, 11),
(149, 11, 'message_to_teacher', 'text_area', 'Message To Teacher', 0, 1, 1, 1, 1, 1, NULL, 12),
(150, 11, 'session', 'radio_btn', 'Session', 0, 1, 1, 1, 1, 1, '{\"default\":\"regular\",\"options\":{\"free-trial\":\"Free Trial\",\"regular\":\"Regular\"}}', 13),
(151, 11, 'speed_test_1', 'text', 'Speed Test 1', 0, 1, 1, 1, 1, 1, NULL, 14),
(152, 11, 'speed_test_2', 'text', 'Speed Test 2', 0, 1, 1, 1, 1, 1, NULL, 15),
(153, 11, 'grammar', 'text_area', 'Grammar', 0, 1, 1, 1, 1, 1, NULL, 16),
(154, 11, 'pronunciation', 'text_area', 'Pronunciation', 0, 1, 1, 1, 1, 1, NULL, 17),
(155, 11, 'areas_for_improvement', 'text_area', 'Areas For Improvement', 0, 1, 1, 1, 1, 1, NULL, 18),
(156, 11, 'tips_and_suggestion_for_student', 'text_area', 'Tips And Suggestion For Student', 0, 1, 1, 1, 1, 1, NULL, 19),
(157, 11, 'class_remarks', 'rich_text_box', 'Class Remarks', 0, 1, 1, 1, 1, 1, NULL, 20),
(158, 11, 'screenshot', 'rich_text_box', 'Screenshot', 0, 1, 1, 1, 1, 1, NULL, 21),
(159, 11, 'book_name', 'text', 'Book Name', 0, 1, 1, 1, 1, 1, NULL, 22),
(160, 11, 'lesson_unit', 'text', 'Lesson Unit', 0, 1, 1, 1, 1, 1, NULL, 23),
(161, 11, 'page_number', 'text', 'Page Number', 0, 1, 1, 1, 1, 1, NULL, 24),
(163, 11, 'student_absent', 'radio_btn', 'Student Absent', 0, 1, 1, 1, 1, 1, '{\"default\":\"no\",\"options\":{\"no\":\"No\",\"yes\":\"Yes\"}}', 29),
(164, 11, 'teacher_absent', 'radio_btn', 'Teacher Absent', 0, 1, 1, 1, 1, 1, '{\"default\":\"no\",\"options\":{\"no\":\"No\",\"yes\":\"Yes\"}}', 26),
(165, 11, 'teacher_reason_for_absence', 'text', 'Teacher Reason For Absence', 0, 1, 1, 1, 1, 1, NULL, 27),
(166, 11, 'student_reason_for_absence', 'text_area', 'Student Reason For Absence', 0, 1, 1, 1, 1, 1, NULL, 30),
(167, 11, 'student_reason_screenshot', 'image', 'Student Reason Screenshot', 0, 1, 1, 1, 1, 1, NULL, 31),
(168, 11, 'teacher_reason_screenshot', 'image', 'Teacher Reason Screenshot', 0, 1, 1, 1, 1, 1, NULL, 28),
(169, 9, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 12),
(170, 8, 'unit_price', 'text', 'Unit Price', 1, 1, 1, 1, 1, 1, NULL, 7),
(172, 8, 'currency', 'text', 'Currency', 0, 1, 1, 1, 1, 1, NULL, 5),
(173, 8, 'base_price', 'text', 'Total Price', 0, 1, 1, 1, 1, 1, NULL, 8),
(174, 9, 'currency', 'text', 'Currency', 0, 1, 1, 1, 1, 1, NULL, 13),
(175, 10, 'currency', 'text', 'Currency', 0, 1, 1, 1, 1, 1, NULL, 10),
(176, 10, 'transaction_id', 'text', 'Paypal Transaction Number', 0, 0, 1, 1, 1, 1, NULL, 16),
(177, 10, 'conversion_rate', 'text', 'Conversion Rate', 0, 0, 1, 1, 1, 1, NULL, 17),
(179, 11, 'current_teacher_rate', 'text', 'Current Teacher Rate', 0, 1, 1, 1, 1, 1, NULL, 25),
(180, 11, 'current_teacher_currency', 'text', 'Current Teacher Currency', 0, 1, 1, 1, 1, 1, NULL, 32),
(181, 10, 'additional_price', 'text', 'Additional Price', 0, 0, 1, 1, 1, 1, NULL, 18),
(182, 10, 'base_price', 'text', 'Base Price', 0, 0, 1, 1, 1, 1, NULL, 20),
(183, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 4),
(184, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 5),
(185, 7, 'prefix', 'text', 'Prefix', 0, 1, 1, 1, 1, 1, NULL, 6),
(186, 10, 'order_number', 'text', 'Order Number', 0, 1, 1, 1, 1, 1, NULL, 2),
(191, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(192, 16, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 2),
(193, 16, 'rate', 'text', 'Rate', 0, 1, 1, 1, 1, 1, NULL, 3),
(194, 16, 'converted_to', 'text', 'Converted To', 0, 1, 1, 1, 1, 1, NULL, 4),
(195, 16, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 5),
(196, 16, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
(203, 10, 'converted_from', 'text', 'Converted From', 0, 0, 1, 1, 1, 1, NULL, 15),
(204, 10, 'approved_at', 'text', 'Approved At', 0, 1, 1, 0, 0, 1, NULL, 19),
(205, 1, 'peak1to15', 'text', 'Peak1to15', 0, 1, 1, 1, 1, 1, NULL, 24),
(206, 1, 'peak16to31', 'text', 'Peak16to31', 0, 1, 1, 1, 1, 1, NULL, 26),
(207, 17, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(208, 17, 'queue', 'text', 'Queue', 1, 1, 1, 1, 1, 1, NULL, 2),
(209, 17, 'payload', 'text', 'Payload', 1, 1, 1, 1, 1, 1, NULL, 3),
(210, 17, 'attempts', 'text', 'Attempts', 1, 1, 1, 1, 1, 1, NULL, 4),
(211, 17, 'reserved_at', 'text', 'Reserved At', 0, 1, 1, 1, 1, 1, NULL, 5),
(212, 17, 'available_at', 'text', 'Available At', 1, 1, 1, 1, 1, 1, NULL, 6),
(213, 17, 'created_at', 'text', 'Created At', 1, 1, 1, 1, 1, 1, NULL, 7),
(214, 11, 'job_ids', 'text', 'Job Ids', 0, 1, 1, 1, 1, 1, NULL, 33),
(215, 11, 'teacher_opened_memo', 'text', 'Teacher Opened Memo', 0, 1, 1, 1, 1, 1, NULL, 34),
(219, 1, 'password_changed', 'text', 'Password Changed', 0, 1, 1, 1, 1, 1, NULL, 37),
(220, 1, 'special_plotting_indefinite', 'text', 'Special Plotting Indefinite', 0, 1, 1, 1, 1, 1, NULL, 27),
(221, 1, 'special_plotting', 'text', 'Special Plotting', 0, 1, 1, 1, 1, 1, NULL, 34),
(226, 20, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(227, 20, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 2),
(228, 20, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 3),
(229, 20, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(230, 21, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(231, 21, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 2),
(232, 21, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 3),
(233, 21, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(234, 1, 'student_account_type_id', 'text', 'Student Account Type Id', 0, 1, 1, 1, 1, 1, NULL, 30),
(235, 1, 'teacher_account_type_id', 'text', 'Teacher Account Type Id', 0, 1, 1, 1, 1, 1, NULL, 32),
(236, 1, 'free_trial_consumable', 'text', 'Free Trial Consumable', 0, 1, 1, 1, 1, 1, NULL, 36),
(237, 1, 'free_trial_valid_until', 'text', 'Free Trial Valid Until', 0, 1, 1, 1, 1, 1, NULL, 38),
(238, 22, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(239, 22, 'referral_id', 'text', 'Referral Id', 0, 1, 1, 1, 1, 1, NULL, 2),
(240, 22, 'referrer_id', 'text', 'Referrer Id', 0, 1, 1, 1, 1, 1, NULL, 3),
(241, 22, 'referral_consumable', 'text', 'Referral Consumable', 0, 1, 1, 1, 1, 1, NULL, 4),
(242, 22, 'referral_valid_until', 'text', 'Referral Valid Until', 0, 1, 1, 1, 1, 1, NULL, 5),
(243, 22, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 6),
(244, 22, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(245, 23, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(246, 23, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 2),
(247, 23, 'actor', 'text', 'Actor', 0, 1, 1, 1, 1, 1, NULL, 3),
(248, 23, 'value_in_php', 'text', 'Value In Php', 0, 1, 1, 1, 1, 1, NULL, 4),
(249, 23, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 5),
(250, 23, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
(251, 24, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(252, 24, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 2),
(253, 24, 'rate', 'text', 'Rate', 0, 1, 1, 1, 1, 1, NULL, 3),
(254, 24, 'minimum_student', 'text', 'Minimum Student', 0, 1, 1, 1, 1, 1, NULL, 4),
(255, 24, 'minimum_class', 'text', 'Minimum Class', 0, 1, 1, 1, 1, 1, NULL, 5),
(256, 24, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 6),
(257, 24, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(258, 25, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(259, 25, 'class_id', 'text', 'Class Id', 0, 1, 1, 1, 1, 1, NULL, 2),
(260, 25, 'student_id', 'text', 'Student Id', 0, 1, 1, 1, 1, 1, NULL, 3),
(261, 25, 'teacher_id', 'text', 'Teacher Id', 0, 1, 1, 1, 1, 1, NULL, 4),
(262, 25, 'status', 'text', 'Status', 0, 1, 1, 1, 1, 1, NULL, 5),
(263, 25, 'date_paid', 'text', 'Date Paid', 0, 1, 1, 1, 1, 1, NULL, 6),
(264, 25, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 7),
(265, 25, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 8),
(266, 11, 'order_id', 'text', 'Order Id', 0, 1, 1, 1, 1, 1, NULL, 35),
(267, 11, 'availability_id', 'text', 'Availability Id', 0, 1, 1, 1, 1, 1, NULL, 36),
(268, 11, 'actor', 'text', 'Actor', 0, 1, 1, 1, 1, 1, NULL, 37),
(269, 11, 'class_fee', 'text', 'Class Fee', 0, 1, 1, 1, 1, 1, NULL, 38),
(270, 11, 'total_fee_status', 'text', 'Total Fee Status', 0, 1, 1, 1, 1, 1, NULL, 39),
(271, 11, 'total_fee_payment_date', 'text', 'Total Fee Payment Date', 0, 1, 1, 1, 1, 1, NULL, 40),
(272, 11, 'penalty_fee', 'text', 'Penalty Fee', 0, 1, 1, 1, 1, 1, NULL, 41),
(273, 11, 'total_fee', 'text', 'Total Fee', 0, 1, 1, 1, 1, 1, NULL, 42),
(274, 26, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(275, 26, 'teacher_id', 'text', 'Teacher Id', 0, 1, 1, 1, 1, 1, NULL, 2),
(276, 26, 'teacher_name', 'text', 'Teacher Name', 0, 1, 1, 1, 1, 1, NULL, 3),
(277, 26, 'teacher_level', 'text', 'Teacher Level', 0, 1, 1, 1, 1, 1, NULL, 4),
(278, 26, 'bank_account_number', 'text', 'Bank Account Number', 0, 1, 1, 1, 1, 1, NULL, 5),
(279, 26, 'bank_account_name', 'text', 'Bank Account Name', 0, 1, 1, 1, 1, 1, NULL, 6),
(280, 26, 'bank_name', 'text', 'Bank Name', 0, 1, 1, 1, 1, 1, NULL, 7),
(281, 26, 'status', 'text', 'Status', 0, 1, 1, 1, 1, 1, NULL, 8),
(282, 26, 'total_penalty_class', 'text', 'Total Penalty Class', 0, 1, 1, 1, 1, 1, NULL, 9),
(283, 26, 'total_penalty_fee', 'text', 'Total Penalty Fee', 0, 1, 1, 1, 1, 1, NULL, 10),
(284, 26, 'total_class_incentives', 'text', 'Total Class Incentives', 0, 1, 1, 1, 1, 1, NULL, 11),
(285, 26, 'total_incentive_fee', 'text', 'Total Incentive Fee', 0, 1, 1, 1, 1, 1, NULL, 12),
(288, 26, 'total_fee', 'text', 'Total Fee', 0, 1, 1, 1, 1, 1, NULL, 15),
(289, 26, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 16),
(290, 26, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 17),
(292, 26, 'cutoff_start', 'text', 'Cutoff Start', 0, 1, 1, 1, 1, 1, NULL, 19),
(293, 26, 'cutoff_end', 'text', 'Cutoff End', 0, 1, 1, 1, 1, 1, NULL, 20),
(294, 26, 'total_completed_class', 'text', 'Total Completed Class', 0, 1, 1, 1, 1, 1, NULL, 13),
(295, 26, 'total_completed_class_fee', 'text', 'Total Completed Class Fee', 0, 1, 1, 1, 1, 1, NULL, 14),
(296, 26, 'affected_ids', 'text', 'Affected Ids', 0, 1, 1, 1, 1, 1, NULL, 18),
(297, 1, 'bank_name', 'text', 'Bank Name', 0, 1, 1, 1, 1, 1, NULL, 31),
(298, 1, 'bank_account_number', 'text', 'Bank Account Number', 0, 1, 1, 1, 1, 1, NULL, 33),
(299, 1, 'bank_account_name', 'text', 'Bank Account Name', 0, 1, 1, 1, 1, 1, NULL, 35),
(300, 29, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(301, 29, 'payment_date', 'date', 'Payment Date', 0, 1, 1, 1, 1, 1, NULL, 2),
(302, 29, 'proof', 'multiple_images', 'Proof', 0, 1, 1, 1, 1, 1, NULL, 3),
(303, 29, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 1, NULL, 4),
(304, 29, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 5),
(305, 29, 'batch_date', 'date', 'Batch Date', 0, 1, 1, 0, 1, 1, NULL, 6),
(308, 26, 'batch_id', 'text', 'Batch Id', 0, 1, 1, 1, 1, 1, NULL, 21),
(309, 26, 'deposited_at', 'text', 'Deposited At', 0, 1, 1, 1, 1, 1, NULL, 22),
(310, 11, 'incentive_total', 'text', 'Incentive Total', 0, 1, 1, 1, 1, 1, NULL, 43),
(311, 11, 'incentive_comment', 'text', 'Incentive Comment', 0, 1, 1, 1, 1, 1, NULL, 44),
(312, 30, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(313, 30, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 2),
(314, 30, 'short_code', 'text', 'Short Code', 0, 1, 1, 1, 1, 1, NULL, 3),
(315, 30, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 4),
(316, 30, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 5),
(317, 31, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(318, 31, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 2),
(319, 31, 'country_id', 'text', 'Country Id', 0, 1, 1, 1, 1, 1, NULL, 3),
(320, 31, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 4),
(321, 31, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 5),
(322, 1, 'city_id', 'text', 'City', 0, 1, 1, 1, 1, 1, NULL, 20),
(323, 1, 'country_id', 'text', 'Country', 0, 1, 1, 1, 1, 1, NULL, 21),
(324, 8, 'country_id', 'text', 'Country Id', 0, 1, 1, 1, 1, 1, NULL, 8),
(325, 8, 'school_id', 'text', 'School Id', 0, 1, 1, 1, 1, 1, NULL, 11),
(326, 1, 'nick', 'text', 'Nick', 0, 1, 1, 1, 1, 1, NULL, 40),
(327, 32, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(328, 32, 'accuser_id', 'text', 'Accuser', 0, 1, 1, 1, 1, 1, NULL, 4),
(329, 32, 'accused_id', 'text', 'Accused', 0, 1, 1, 1, 1, 1, NULL, 5),
(330, 32, 'content', 'text_area', 'Content', 0, 1, 1, 1, 1, 1, NULL, 3),
(331, 32, 'category_id', 'text', 'Category', 0, 1, 1, 1, 1, 1, NULL, 2),
(332, 32, 'status', 'text', 'Status', 0, 1, 1, 0, 0, 1, NULL, 7),
(333, 32, 'action_taken', 'text_area', 'Action Taken', 0, 1, 1, 1, 1, 1, NULL, 6),
(334, 32, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 8),
(335, 32, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 9),
(336, 32, 'other_comments', 'text', 'Other Comments', 0, 1, 1, 1, 1, 1, NULL, 10),
(337, 33, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(338, 33, 'name', 'text', 'Name', 0, 1, 1, 1, 1, 1, NULL, 2),
(339, 33, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 3),
(340, 33, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(341, 1, 'lang', 'text', 'Lang', 0, 1, 1, 1, 1, 1, NULL, 39),
(342, 9, 'status', 'text', 'Status', 0, 1, 1, 1, 1, 1, NULL, 14),
(343, 10, 'discount_price', 'text', 'Discount Price', 0, 1, 1, 1, 1, 1, NULL, 24),
(344, 10, 'voucher_id', 'text', 'Voucher Id', 0, 1, 1, 1, 1, 1, NULL, 22),
(345, 34, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(346, 34, 'school_id', 'text', 'School Id', 0, 0, 1, 1, 1, 1, NULL, 2),
(347, 34, 'code', 'text', 'Code', 0, 1, 1, 0, 1, 1, NULL, 3),
(348, 34, 'fixed_rate', 'text', 'Fixed Rate', 0, 1, 1, 1, 1, 1, NULL, 4),
(349, 34, 'percent', 'text', 'Percent', 0, 1, 1, 1, 1, 1, NULL, 5),
(350, 34, 'quantity', 'text', 'Quantity', 0, 1, 1, 1, 1, 1, NULL, 6),
(351, 34, 'total_quantity', 'text', 'Total Quantity', 0, 1, 1, 1, 1, 1, NULL, 7),
(352, 34, 'status', 'text', 'Status', 0, 1, 1, 1, 1, 1, NULL, 8),
(353, 34, 'valid_from', 'date', 'Valid From', 0, 1, 1, 1, 1, 1, NULL, 9),
(354, 34, 'valid_until', 'date', 'Valid Until', 0, 1, 1, 1, 1, 1, NULL, 10),
(355, 34, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 1, NULL, 11),
(356, 34, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 12),
(357, 34, 'school_package_id', 'text', 'School Package Id', 0, 1, 1, 1, 1, 1, NULL, 13),
(358, 34, 'voucher_belongsto_school_package_relationship', 'relationship', 'school_packages', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\SchoolPackage\",\"table\":\"school_packages\",\"type\":\"belongsTo\",\"column\":\"school_package_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"applications\",\"pivot\":\"0\",\"taggable\":null}', 14),
(359, 10, 'order_belongsto_voucher_relationship', 'relationship', 'vouchers', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Voucher\",\"table\":\"vouchers\",\"type\":\"belongsTo\",\"column\":\"voucher_id\",\"key\":\"id\",\"label\":\"code\",\"pivot_table\":\"applications\",\"pivot\":\"0\",\"taggable\":\"0\"}', 23);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'App\\User', 'TCG\\Voyager\\Policies\\UserPolicy', 'Voyager\\UserController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-04-21 03:12:08', '2018-06-21 23:35:33'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2018-04-21 03:12:09', '2018-04-21 03:12:09'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2018-04-21 03:12:09', '2018-04-21 03:12:09'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2018-04-21 03:12:15', '2018-04-21 03:12:15'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2018-04-21 03:12:17', '2018-04-21 03:12:17'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2018-04-21 03:12:19', '2018-04-21 03:12:19'),
(7, 'schools', 'schools', 'School', 'Schools', NULL, 'App\\School', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-04-21 08:16:02', '2018-04-21 08:16:02'),
(8, 'packages', 'packages', 'Package', 'Packages', NULL, 'App\\Package', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-04-21 10:33:20', '2018-06-21 23:33:12'),
(9, 'school_packages', 'school-packages', 'School Package', 'School Packages', NULL, 'App\\SchoolPackage', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-04-21 11:02:49', '2018-04-21 11:02:49'),
(10, 'orders', 'orders', 'Order', 'Orders', NULL, 'App\\Order', NULL, 'Voyager\\OrderController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-04-21 11:33:19', '2018-07-01 05:44:23'),
(11, 'book_classes', 'book-classes', 'Book Class', 'Book Classes', NULL, 'App\\BookClass', NULL, 'Voyager\\BookClassController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-04-21 12:07:56', '2018-06-23 21:26:00'),
(12, 'availabilities', 'availabilities', 'Availability', 'Availabilities', NULL, 'App\\Availability', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-04-21 12:20:47', '2018-04-21 12:20:47'),
(13, 'applications', 'applications', 'Application', 'Applications', NULL, 'App\\Application', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-04-21 21:23:04', '2018-04-21 21:23:04'),
(14, 'employees', 'employees', 'Employee', 'Employees', NULL, 'App\\Employee', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-04-22 00:55:12', '2018-04-22 00:55:12'),
(16, 'paypal_exchange_rate', 'paypal-exchange-rate', 'Paypal Exchange Rate', 'Paypal Exchange Rates', NULL, 'App\\PaypalExchangeRate', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-06-20 07:45:48', '2018-06-20 07:45:48'),
(17, 'jobs', 'jobs', 'Job', 'Jobs', NULL, 'App\\Job', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-06-23 05:18:57', '2018-06-23 05:18:57'),
(20, 'student_account_type', 'student-account-type', 'Student Account Type', 'Student Account Types', NULL, 'App\\StudentAccountType', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-07-01 01:24:47', '2018-07-01 01:24:47'),
(21, 'student_account_types', 'student-account-types', 'Student Account Type', 'Student Account Types', NULL, 'App\\StudentAccountType', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-07-01 01:25:45', '2018-07-01 01:25:45'),
(22, 'referrals', 'referrals', 'Referral', 'Referrals', NULL, 'App\\Referral', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-07-01 07:47:44', '2018-07-01 07:47:44'),
(23, 'penalties', 'penalties', 'Penalty', 'Penalties', NULL, 'App\\Penalty', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-07-03 05:03:20', '2018-07-03 05:03:20'),
(24, 'teacher_account_types', 'teacher-account-types', 'Teacher Account Type', 'Teacher Account Types', NULL, 'App\\TeacherAccountType', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-07-03 06:29:24', '2018-07-03 06:29:24'),
(25, 'enrollment_incentives', 'enrollment-incentives', 'Enrollment Incentive', 'Enrollment Incentives', NULL, 'App\\EnrollmentIncentive', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-07-03 07:07:29', '2018-07-03 07:07:29'),
(26, 'payouts', 'payouts', 'Payout', 'Payouts', NULL, 'App\\Payout', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-07-05 01:11:00', '2018-07-05 01:11:00'),
(29, 'payout_batches', 'payout-batches', 'Payout Batch', 'Payout Batches', NULL, 'App\\PayoutBatch', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-07-06 03:17:28', '2018-07-06 03:17:28'),
(30, 'countries', 'countries', 'Country', 'Countries', NULL, 'App\\Country', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-07-15 04:21:23', '2018-07-15 04:21:23'),
(31, 'city_municipalities', 'city-municipalities', 'City Municipality', 'City Municipalities', NULL, 'App\\CityMunicipality', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-07-15 04:21:45', '2018-07-15 04:21:45'),
(32, 'complaints', 'complaints', 'Complaint', 'Complaints', NULL, 'App\\Complaint', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-07-17 10:11:23', '2018-07-17 10:11:23'),
(33, 'complaint_categories', 'complaint-categories', 'Complaint Category', 'Complaint Categories', NULL, 'App\\ComplaintCategory', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-07-17 11:00:51', '2018-07-17 11:00:51'),
(34, 'vouchers', 'vouchers', 'Voucher', 'Vouchers', NULL, 'App\\Voucher', NULL, 'Voyager\\VoucherController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null}', '2018-07-28 00:57:56', '2018-07-28 00:57:56');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `party_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `enrollment_incentives`
--

CREATE TABLE `enrollment_incentives` (
  `id` int(10) UNSIGNED NOT NULL,
  `class_id` int(10) UNSIGNED DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `teacher_id` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_paid` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2018-04-21 03:12:09', '2018-04-21 03:12:09'),
(2, 'client', '2018-04-21 05:44:54', '2018-04-21 05:44:54'),
(3, 'student', '2018-06-29 00:24:47', '2018-06-29 00:24:47'),
(4, 'teacher', '2018-06-29 01:04:54', '2018-06-29 01:04:54'),
(5, 'student-zh-cn', '2018-07-26 08:27:02', '2018-07-26 08:27:02');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2018-04-21 03:12:09', '2018-04-21 03:12:09', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 10, '2018-04-21 03:12:10', '2018-05-30 21:49:37', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 6, '2018-04-21 03:12:10', '2018-05-30 21:39:53', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2018-04-21 03:12:10', '2018-04-21 03:12:10', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 14, '2018-04-21 03:12:10', '2018-05-30 21:49:37', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2018-04-21 03:12:10', '2018-05-30 21:21:21', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2018-04-21 03:12:10', '2018-05-30 21:25:21', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2018-04-21 03:12:10', '2018-05-30 21:25:21', 'voyager.compass.index', NULL),
(9, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 15, '2018-04-21 03:12:10', '2018-05-30 21:49:37', 'voyager.settings.index', NULL),
(10, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 13, '2018-04-21 03:12:16', '2018-05-30 21:49:37', 'voyager.categories.index', NULL),
(11, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 11, '2018-04-21 03:12:19', '2018-05-30 21:49:37', 'voyager.posts.index', NULL),
(12, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 12, '2018-04-21 03:12:20', '2018-05-30 21:49:37', 'voyager.pages.index', NULL),
(13, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 4, '2018-04-21 03:12:22', '2018-05-30 21:25:21', 'voyager.hooks', NULL),
(14, 2, 'Student', '', '_self', 'voyager-person', '#000000', NULL, 3, '2018-04-21 05:45:47', '2018-06-29 03:59:04', 'voyager.users.index', 'null'),
(15, 1, 'Schools', '', '_self', 'voyager-study', '#000000', NULL, 3, '2018-04-21 08:16:02', '2018-05-30 21:41:40', 'voyager.schools.index', 'null'),
(16, 1, 'Packages', '', '_self', 'voyager-backpack', '#000000', NULL, 4, '2018-04-21 10:33:20', '2018-05-30 21:41:40', 'voyager.packages.index', 'null'),
(17, 1, 'School Packages', '', '_self', 'voyager-study voyager-backpack', '#000000', NULL, 5, '2018-04-21 11:02:49', '2018-05-30 21:39:53', 'voyager.school-packages.index', 'null'),
(18, 1, 'Orders', '', '_self', 'voyager-buy', '#000000', NULL, 9, '2018-04-21 11:33:19', '2018-05-30 21:49:37', 'voyager.orders.index', 'null'),
(19, 1, 'Book a class', '', '_self', 'voyager-book', '#000000', NULL, 8, '2018-04-21 12:07:56', '2018-06-28 02:30:40', 'voyager.book-classes.index', 'null'),
(20, 1, 'Schedule', '', '_self', 'voyager-window-list', '#000000', NULL, 7, '2018-04-21 12:20:47', '2018-06-28 02:29:59', 'voyager.availabilities.index', 'null'),
(21, 1, 'Applications', '', '_self', NULL, NULL, 23, 1, '2018-04-21 21:23:04', '2018-05-30 21:26:22', 'voyager.applications.index', NULL),
(22, 1, 'Employees', '', '_self', NULL, NULL, 23, 2, '2018-04-22 00:55:12', '2018-05-30 21:26:22', 'voyager.employees.index', NULL),
(23, 1, 'Others', '#', '_self', 'voyager-pirate-hat', '#000000', NULL, 16, '2018-05-30 21:26:11', '2018-05-30 21:49:37', NULL, ''),
(26, 1, 'Jobs', '', '_self', NULL, NULL, NULL, 17, '2018-06-23 05:18:58', '2018-07-01 01:26:14', 'voyager.jobs.index', NULL),
(27, 3, 'Ordered Packages', '', '_self', 'voyager-list', '#000000', 28, 2, '2018-06-29 00:29:46', '2018-06-29 00:39:25', 'voyager.orders.index', 'null'),
(28, 3, 'Packages', 'javascript:;', '_self', 'voyager-buy', '#000000', NULL, 2, '2018-06-29 00:36:33', '2018-07-15 07:38:17', NULL, ''),
(29, 3, 'Buy a Package', '', '_self', 'voyager-plus', '#000000', 28, 1, '2018-06-29 00:37:34', '2018-06-29 00:39:01', 'voyager.orders.create', 'null'),
(30, 3, 'Book Classes', '', '_self', 'voyager-plus', '#000000', 32, 1, '2018-06-29 00:41:33', '2018-06-29 00:47:55', 'voyager.book-classes.create', 'null'),
(31, 3, 'Class Schedule', '', '_self', 'voyager-list', '#000000', 32, 2, '2018-06-29 00:42:16', '2018-06-29 00:47:45', 'voyager.book-classes.index', 'null'),
(32, 3, 'Schedule', 'javascript:;', '_self', 'voyager-book', '#000000', NULL, 3, '2018-06-29 00:44:22', '2018-07-15 07:38:17', NULL, ''),
(33, 3, 'dashboard', '', '_self', 'voyager-boat', '#000000', NULL, 1, '2018-06-29 00:49:43', '2018-07-15 07:38:16', 'voyager.dashboard', 'null'),
(34, 4, 'dashboard', '', '_self', 'voyager-boat', '#000000', NULL, 1, '2018-06-29 01:06:37', '2018-06-29 01:09:47', 'voyager.dashboard', 'null'),
(35, 4, 'Schedule', 'javascript:;', '_self', 'voyager-book', '#000000', NULL, 2, '2018-06-29 01:07:17', '2018-06-29 01:09:47', NULL, ''),
(36, 4, 'Class Schedule', '', '_self', 'voyager-list', '#000000', 35, 2, '2018-06-29 01:07:46', '2018-06-29 01:09:49', 'voyager.book-classes.index', 'null'),
(37, 4, 'Plot Schedule', '', '_self', 'voyager-plus', '#000000', 35, 1, '2018-06-29 01:08:26', '2018-06-29 01:54:39', 'voyager.availabilities.index', 'null'),
(38, 2, 'Dashboard', '', '_self', 'voyager-boat', '#000000', NULL, 1, '2018-06-29 03:47:06', '2018-06-29 03:47:24', 'voyager.dashboard', 'null'),
(39, 2, 'Orders', '', '_self', 'voyager-buy', '#000000', NULL, 4, '2018-06-29 03:47:54', '2018-06-29 03:59:04', 'voyager.orders.index', 'null'),
(40, 2, 'Packages', '', '_self', 'voyager-backpack', '#000000', NULL, 2, '2018-06-29 03:48:21', '2018-06-29 03:59:04', 'voyager.school-packages.index', 'null'),
(43, 1, 'Student Account Types', '', '_self', NULL, NULL, NULL, 20, '2018-07-01 01:25:45', '2018-07-01 01:26:14', 'voyager.student-account-types.index', NULL),
(44, 1, 'Referrals', '', '_self', NULL, NULL, NULL, 21, '2018-07-01 07:47:44', '2018-07-01 07:47:44', 'voyager.referrals.index', NULL),
(45, 1, 'Penalties', '', '_self', NULL, NULL, NULL, 22, '2018-07-03 05:03:21', '2018-07-03 05:03:21', 'voyager.penalties.index', NULL),
(46, 1, 'Teacher Account Types', '', '_self', NULL, NULL, NULL, 23, '2018-07-03 06:29:24', '2018-07-03 06:29:24', 'voyager.teacher-account-types.index', NULL),
(47, 1, 'Enrollment Incentives', '', '_self', NULL, NULL, NULL, 24, '2018-07-03 07:07:29', '2018-07-03 07:07:29', 'voyager.enrollment-incentives.index', NULL),
(48, 1, 'Payouts', '', '_self', NULL, NULL, NULL, 25, '2018-07-05 01:11:01', '2018-07-05 01:11:01', 'voyager.payouts.index', NULL),
(49, 1, 'Payout Batches', '', '_self', NULL, NULL, NULL, 26, '2018-07-06 03:17:29', '2018-07-06 03:17:29', 'voyager.payout-batches.index', NULL),
(50, 1, 'Countries', '', '_self', NULL, NULL, NULL, 27, '2018-07-15 04:21:23', '2018-07-15 04:21:23', 'voyager.countries.index', NULL),
(51, 1, 'City Municipalities', '', '_self', NULL, NULL, NULL, 28, '2018-07-15 04:21:46', '2018-07-15 04:21:46', 'voyager.city-municipalities.index', NULL),
(52, 1, 'Complaints', '', '_self', NULL, NULL, NULL, 29, '2018-07-17 10:11:23', '2018-07-17 10:11:23', 'voyager.complaints.index', NULL),
(53, 1, 'Complaint Categories', '', '_self', NULL, NULL, NULL, 30, '2018-07-17 11:00:51', '2018-07-17 11:00:51', 'voyager.complaint-categories.index', NULL),
(54, 5, '帐号信息', '', '_self', 'voyager-boat', '#000000', NULL, 1, '2018-07-26 08:28:07', '2018-07-26 08:29:52', 'voyager.dashboard', 'null'),
(56, 5, '订购A学习包', '', '_self', 'voyager-plus', '#000000', 66, 1, '2018-07-26 08:29:47', '2018-07-26 09:35:10', 'voyager.orders.create', NULL),
(57, 5, '已购学习包', '', '_self', 'voyager-list', '#000000', 66, 2, '2018-07-26 08:30:30', '2018-07-26 09:35:11', 'voyager.orders.index', 'null'),
(59, 5, '预约课程', '', '_self', 'voyager-plus', '#000000', 65, 1, '2018-07-26 08:32:05', '2018-07-26 09:34:39', 'voyager.book-classes.create', 'null'),
(60, 5, '课程表', '', '_self', 'voyager-list', '#000000', 65, 2, '2018-07-26 08:33:05', '2018-07-26 09:34:41', 'voyager.book-classes.index', 'null'),
(65, 5, '课程信息', '', '_self', 'voyager-book', '#000000', NULL, 3, '2018-07-26 09:28:58', '2018-07-26 09:35:18', NULL, ''),
(66, 5, '学习包', '', '_self', 'voyager-buy', '#000000', NULL, 2, '2018-07-26 09:35:01', '2018-07-26 09:35:08', NULL, ''),
(67, 1, 'Vouchers', '', '_self', NULL, NULL, NULL, 31, '2018-07-28 00:57:56', '2018-07-28 00:57:56', 'voyager.vouchers.index', NULL),
(68, 2, 'Vouchers', '', '_self', 'voyager-smile', '#000000', NULL, 32, '2018-07-28 01:07:28', '2018-07-28 09:22:18', 'voyager.vouchers.index', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2016_01_01_000000_create_pages_table', 2),
(24, '2016_01_01_000000_create_posts_table', 2),
(25, '2016_02_15_204651_create_categories_table', 2),
(26, '2017_04_11_000000_alter_post_nullable_fields_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `school_package_id` int(10) UNSIGNED DEFAULT NULL,
  `remaining_reservation` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `student_id` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'pending',
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_price` double UNSIGNED DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `effective_until` datetime DEFAULT NULL,
  `currency` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transaction_id` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conversion_rate` double DEFAULT NULL,
  `converted_from` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `additional_price` double UNSIGNED DEFAULT '0',
  `base_price` double UNSIGNED DEFAULT '0',
  `order_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approved_at` datetime DEFAULT NULL,
  `discount_price` double UNSIGNED DEFAULT '0',
  `voucher_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `packages`
--

CREATE TABLE `packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `duration_in_days` int(11) NOT NULL,
  `unit_price` double NOT NULL,
  `number_of_classes` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `currency` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `base_price` double DEFAULT NULL,
  `school_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2018-04-21 03:12:20', '2018-04-21 03:12:20');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payouts`
--

CREATE TABLE `payouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `teacher_id` int(10) UNSIGNED DEFAULT NULL,
  `teacher_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `teacher_level` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_number` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_account_name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bank_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_penalty_class` int(10) UNSIGNED DEFAULT '0',
  `total_penalty_fee` double UNSIGNED DEFAULT '0',
  `total_class_incentives` int(10) UNSIGNED DEFAULT '0',
  `total_incentive_fee` double UNSIGNED DEFAULT '0',
  `total_completed_class` int(10) UNSIGNED DEFAULT '0',
  `total_completed_class_fee` double UNSIGNED DEFAULT '0',
  `total_fee` double DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `affected_ids` longtext COLLATE utf8_unicode_ci,
  `cutoff_start` datetime DEFAULT NULL,
  `cutoff_end` datetime DEFAULT NULL,
  `batch_id` int(10) UNSIGNED DEFAULT NULL,
  `deposited_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payout_batches`
--

CREATE TABLE `payout_batches` (
  `id` int(10) UNSIGNED NOT NULL,
  `payment_date` datetime DEFAULT NULL,
  `proof` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `batch_date` datetime DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci DEFAULT 'pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `penalties`
--

CREATE TABLE `penalties` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `actor` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `value_in_php` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `penalties`
--

INSERT INTO `penalties` (`id`, `name`, `actor`, `value_in_php`, `created_at`, `updated_at`) VALUES
(1, 'penalty 1', 'teacher', 70, '2018-07-03 05:08:50', '2018-07-03 05:08:50'),
(2, 'penalty 2', 'teacher', 140, '2018-07-03 05:09:12', '2018-07-03 05:09:12'),
(3, 'penalty 3', 'teacher', 210, '2018-07-03 05:10:53', '2018-07-03 05:10:53'),
(4, 'penalty 3', 'student', 210, '2018-07-03 05:11:28', '2018-07-03 05:11:28'),
(5, 'lesson memo delay', 'teacher', 35, '2018-07-03 05:12:06', '2018-07-03 05:12:06'),
(6, 'penalty 3', 'system', 210, '2018-07-03 05:12:41', '2018-07-03 05:12:41');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2018-04-21 03:12:10', '2018-04-21 03:12:10'),
(2, 'browse_bread', NULL, '2018-04-21 03:12:10', '2018-04-21 03:12:10'),
(3, 'browse_database', NULL, '2018-04-21 03:12:10', '2018-04-21 03:12:10'),
(4, 'browse_media', NULL, '2018-04-21 03:12:10', '2018-04-21 03:12:10'),
(5, 'browse_compass', NULL, '2018-04-21 03:12:10', '2018-04-21 03:12:10'),
(6, 'browse_menus', 'menus', '2018-04-21 03:12:10', '2018-04-21 03:12:10'),
(7, 'read_menus', 'menus', '2018-04-21 03:12:10', '2018-04-21 03:12:10'),
(8, 'edit_menus', 'menus', '2018-04-21 03:12:10', '2018-04-21 03:12:10'),
(9, 'add_menus', 'menus', '2018-04-21 03:12:10', '2018-04-21 03:12:10'),
(10, 'delete_menus', 'menus', '2018-04-21 03:12:10', '2018-04-21 03:12:10'),
(11, 'browse_roles', 'roles', '2018-04-21 03:12:10', '2018-04-21 03:12:10'),
(12, 'read_roles', 'roles', '2018-04-21 03:12:10', '2018-04-21 03:12:10'),
(13, 'edit_roles', 'roles', '2018-04-21 03:12:10', '2018-04-21 03:12:10'),
(14, 'add_roles', 'roles', '2018-04-21 03:12:10', '2018-04-21 03:12:10'),
(15, 'delete_roles', 'roles', '2018-04-21 03:12:10', '2018-04-21 03:12:10'),
(16, 'browse_users', 'users', '2018-04-21 03:12:10', '2018-04-21 03:12:10'),
(17, 'read_users', 'users', '2018-04-21 03:12:10', '2018-04-21 03:12:10'),
(18, 'edit_users', 'users', '2018-04-21 03:12:10', '2018-04-21 03:12:10'),
(19, 'add_users', 'users', '2018-04-21 03:12:11', '2018-04-21 03:12:11'),
(20, 'delete_users', 'users', '2018-04-21 03:12:11', '2018-04-21 03:12:11'),
(21, 'browse_settings', 'settings', '2018-04-21 03:12:11', '2018-04-21 03:12:11'),
(22, 'read_settings', 'settings', '2018-04-21 03:12:11', '2018-04-21 03:12:11'),
(23, 'edit_settings', 'settings', '2018-04-21 03:12:11', '2018-04-21 03:12:11'),
(24, 'add_settings', 'settings', '2018-04-21 03:12:11', '2018-04-21 03:12:11'),
(25, 'delete_settings', 'settings', '2018-04-21 03:12:11', '2018-04-21 03:12:11'),
(26, 'browse_categories', 'categories', '2018-04-21 03:12:16', '2018-04-21 03:12:16'),
(27, 'read_categories', 'categories', '2018-04-21 03:12:16', '2018-04-21 03:12:16'),
(28, 'edit_categories', 'categories', '2018-04-21 03:12:16', '2018-04-21 03:12:16'),
(29, 'add_categories', 'categories', '2018-04-21 03:12:16', '2018-04-21 03:12:16'),
(30, 'delete_categories', 'categories', '2018-04-21 03:12:16', '2018-04-21 03:12:16'),
(31, 'browse_posts', 'posts', '2018-04-21 03:12:19', '2018-04-21 03:12:19'),
(32, 'read_posts', 'posts', '2018-04-21 03:12:19', '2018-04-21 03:12:19'),
(33, 'edit_posts', 'posts', '2018-04-21 03:12:19', '2018-04-21 03:12:19'),
(34, 'add_posts', 'posts', '2018-04-21 03:12:19', '2018-04-21 03:12:19'),
(35, 'delete_posts', 'posts', '2018-04-21 03:12:19', '2018-04-21 03:12:19'),
(36, 'browse_pages', 'pages', '2018-04-21 03:12:20', '2018-04-21 03:12:20'),
(37, 'read_pages', 'pages', '2018-04-21 03:12:20', '2018-04-21 03:12:20'),
(38, 'edit_pages', 'pages', '2018-04-21 03:12:20', '2018-04-21 03:12:20'),
(39, 'add_pages', 'pages', '2018-04-21 03:12:20', '2018-04-21 03:12:20'),
(40, 'delete_pages', 'pages', '2018-04-21 03:12:20', '2018-04-21 03:12:20'),
(41, 'browse_hooks', NULL, '2018-04-21 03:12:22', '2018-04-21 03:12:22'),
(42, 'browse_schools', 'schools', '2018-04-21 08:16:02', '2018-04-21 08:16:02'),
(43, 'read_schools', 'schools', '2018-04-21 08:16:02', '2018-04-21 08:16:02'),
(44, 'edit_schools', 'schools', '2018-04-21 08:16:02', '2018-04-21 08:16:02'),
(45, 'add_schools', 'schools', '2018-04-21 08:16:02', '2018-04-21 08:16:02'),
(46, 'delete_schools', 'schools', '2018-04-21 08:16:02', '2018-04-21 08:16:02'),
(47, 'browse_packages', 'packages', '2018-04-21 10:33:20', '2018-04-21 10:33:20'),
(48, 'read_packages', 'packages', '2018-04-21 10:33:20', '2018-04-21 10:33:20'),
(49, 'edit_packages', 'packages', '2018-04-21 10:33:20', '2018-04-21 10:33:20'),
(50, 'add_packages', 'packages', '2018-04-21 10:33:20', '2018-04-21 10:33:20'),
(51, 'delete_packages', 'packages', '2018-04-21 10:33:20', '2018-04-21 10:33:20'),
(52, 'browse_school_packages', 'school_packages', '2018-04-21 11:02:49', '2018-04-21 11:02:49'),
(53, 'read_school_packages', 'school_packages', '2018-04-21 11:02:49', '2018-04-21 11:02:49'),
(54, 'edit_school_packages', 'school_packages', '2018-04-21 11:02:49', '2018-04-21 11:02:49'),
(55, 'add_school_packages', 'school_packages', '2018-04-21 11:02:49', '2018-04-21 11:02:49'),
(56, 'delete_school_packages', 'school_packages', '2018-04-21 11:02:49', '2018-04-21 11:02:49'),
(57, 'browse_orders', 'orders', '2018-04-21 11:33:19', '2018-04-21 11:33:19'),
(58, 'read_orders', 'orders', '2018-04-21 11:33:19', '2018-04-21 11:33:19'),
(59, 'edit_orders', 'orders', '2018-04-21 11:33:19', '2018-04-21 11:33:19'),
(60, 'add_orders', 'orders', '2018-04-21 11:33:19', '2018-04-21 11:33:19'),
(61, 'delete_orders', 'orders', '2018-04-21 11:33:19', '2018-04-21 11:33:19'),
(62, 'browse_book_classes', 'book_classes', '2018-04-21 12:07:56', '2018-04-21 12:07:56'),
(63, 'read_book_classes', 'book_classes', '2018-04-21 12:07:56', '2018-04-21 12:07:56'),
(64, 'edit_book_classes', 'book_classes', '2018-04-21 12:07:56', '2018-04-21 12:07:56'),
(65, 'add_book_classes', 'book_classes', '2018-04-21 12:07:56', '2018-04-21 12:07:56'),
(66, 'delete_book_classes', 'book_classes', '2018-04-21 12:07:56', '2018-04-21 12:07:56'),
(67, 'browse_availabilities', 'availabilities', '2018-04-21 12:20:47', '2018-04-21 12:20:47'),
(68, 'read_availabilities', 'availabilities', '2018-04-21 12:20:47', '2018-04-21 12:20:47'),
(69, 'edit_availabilities', 'availabilities', '2018-04-21 12:20:47', '2018-04-21 12:20:47'),
(70, 'add_availabilities', 'availabilities', '2018-04-21 12:20:47', '2018-04-21 12:20:47'),
(71, 'delete_availabilities', 'availabilities', '2018-04-21 12:20:47', '2018-04-21 12:20:47'),
(72, 'browse_applications', 'applications', '2018-04-21 21:23:04', '2018-04-21 21:23:04'),
(73, 'read_applications', 'applications', '2018-04-21 21:23:04', '2018-04-21 21:23:04'),
(74, 'edit_applications', 'applications', '2018-04-21 21:23:04', '2018-04-21 21:23:04'),
(75, 'add_applications', 'applications', '2018-04-21 21:23:04', '2018-04-21 21:23:04'),
(76, 'delete_applications', 'applications', '2018-04-21 21:23:04', '2018-04-21 21:23:04'),
(77, 'browse_employees', 'employees', '2018-04-22 00:55:12', '2018-04-22 00:55:12'),
(78, 'read_employees', 'employees', '2018-04-22 00:55:12', '2018-04-22 00:55:12'),
(79, 'edit_employees', 'employees', '2018-04-22 00:55:12', '2018-04-22 00:55:12'),
(80, 'add_employees', 'employees', '2018-04-22 00:55:12', '2018-04-22 00:55:12'),
(81, 'delete_employees', 'employees', '2018-04-22 00:55:12', '2018-04-22 00:55:12'),
(87, 'browse_paypal_exchange_rate', 'paypal_exchange_rate', '2018-06-20 07:45:49', '2018-06-20 07:45:49'),
(88, 'read_paypal_exchange_rate', 'paypal_exchange_rate', '2018-06-20 07:45:49', '2018-06-20 07:45:49'),
(89, 'edit_paypal_exchange_rate', 'paypal_exchange_rate', '2018-06-20 07:45:49', '2018-06-20 07:45:49'),
(90, 'add_paypal_exchange_rate', 'paypal_exchange_rate', '2018-06-20 07:45:49', '2018-06-20 07:45:49'),
(91, 'delete_paypal_exchange_rate', 'paypal_exchange_rate', '2018-06-20 07:45:49', '2018-06-20 07:45:49'),
(92, 'browse_jobs', 'jobs', '2018-06-23 05:18:58', '2018-06-23 05:18:58'),
(93, 'read_jobs', 'jobs', '2018-06-23 05:18:58', '2018-06-23 05:18:58'),
(94, 'edit_jobs', 'jobs', '2018-06-23 05:18:58', '2018-06-23 05:18:58'),
(95, 'add_jobs', 'jobs', '2018-06-23 05:18:58', '2018-06-23 05:18:58'),
(96, 'delete_jobs', 'jobs', '2018-06-23 05:18:58', '2018-06-23 05:18:58'),
(102, 'browse_student_account_type', 'student_account_type', '2018-07-01 01:24:47', '2018-07-01 01:24:47'),
(103, 'read_student_account_type', 'student_account_type', '2018-07-01 01:24:47', '2018-07-01 01:24:47'),
(104, 'edit_student_account_type', 'student_account_type', '2018-07-01 01:24:47', '2018-07-01 01:24:47'),
(105, 'add_student_account_type', 'student_account_type', '2018-07-01 01:24:47', '2018-07-01 01:24:47'),
(106, 'delete_student_account_type', 'student_account_type', '2018-07-01 01:24:47', '2018-07-01 01:24:47'),
(107, 'browse_student_account_types', 'student_account_types', '2018-07-01 01:25:45', '2018-07-01 01:25:45'),
(108, 'read_student_account_types', 'student_account_types', '2018-07-01 01:25:45', '2018-07-01 01:25:45'),
(109, 'edit_student_account_types', 'student_account_types', '2018-07-01 01:25:45', '2018-07-01 01:25:45'),
(110, 'add_student_account_types', 'student_account_types', '2018-07-01 01:25:45', '2018-07-01 01:25:45'),
(111, 'delete_student_account_types', 'student_account_types', '2018-07-01 01:25:45', '2018-07-01 01:25:45'),
(112, 'browse_referrals', 'referrals', '2018-07-01 07:47:44', '2018-07-01 07:47:44'),
(113, 'read_referrals', 'referrals', '2018-07-01 07:47:44', '2018-07-01 07:47:44'),
(114, 'edit_referrals', 'referrals', '2018-07-01 07:47:44', '2018-07-01 07:47:44'),
(115, 'add_referrals', 'referrals', '2018-07-01 07:47:44', '2018-07-01 07:47:44'),
(116, 'delete_referrals', 'referrals', '2018-07-01 07:47:44', '2018-07-01 07:47:44'),
(117, 'browse_penalties', 'penalties', '2018-07-03 05:03:20', '2018-07-03 05:03:20'),
(118, 'read_penalties', 'penalties', '2018-07-03 05:03:20', '2018-07-03 05:03:20'),
(119, 'edit_penalties', 'penalties', '2018-07-03 05:03:20', '2018-07-03 05:03:20'),
(120, 'add_penalties', 'penalties', '2018-07-03 05:03:20', '2018-07-03 05:03:20'),
(121, 'delete_penalties', 'penalties', '2018-07-03 05:03:20', '2018-07-03 05:03:20'),
(122, 'browse_teacher_account_types', 'teacher_account_types', '2018-07-03 06:29:24', '2018-07-03 06:29:24'),
(123, 'read_teacher_account_types', 'teacher_account_types', '2018-07-03 06:29:24', '2018-07-03 06:29:24'),
(124, 'edit_teacher_account_types', 'teacher_account_types', '2018-07-03 06:29:24', '2018-07-03 06:29:24'),
(125, 'add_teacher_account_types', 'teacher_account_types', '2018-07-03 06:29:24', '2018-07-03 06:29:24'),
(126, 'delete_teacher_account_types', 'teacher_account_types', '2018-07-03 06:29:24', '2018-07-03 06:29:24'),
(127, 'browse_enrollment_incentives', 'enrollment_incentives', '2018-07-03 07:07:29', '2018-07-03 07:07:29'),
(128, 'read_enrollment_incentives', 'enrollment_incentives', '2018-07-03 07:07:29', '2018-07-03 07:07:29'),
(129, 'edit_enrollment_incentives', 'enrollment_incentives', '2018-07-03 07:07:29', '2018-07-03 07:07:29'),
(130, 'add_enrollment_incentives', 'enrollment_incentives', '2018-07-03 07:07:29', '2018-07-03 07:07:29'),
(131, 'delete_enrollment_incentives', 'enrollment_incentives', '2018-07-03 07:07:29', '2018-07-03 07:07:29'),
(132, 'browse_payouts', 'payouts', '2018-07-05 01:11:01', '2018-07-05 01:11:01'),
(133, 'read_payouts', 'payouts', '2018-07-05 01:11:01', '2018-07-05 01:11:01'),
(134, 'edit_payouts', 'payouts', '2018-07-05 01:11:01', '2018-07-05 01:11:01'),
(135, 'add_payouts', 'payouts', '2018-07-05 01:11:01', '2018-07-05 01:11:01'),
(136, 'delete_payouts', 'payouts', '2018-07-05 01:11:01', '2018-07-05 01:11:01'),
(137, 'browse_payout_batches', 'payout_batches', '2018-07-06 03:17:29', '2018-07-06 03:17:29'),
(138, 'read_payout_batches', 'payout_batches', '2018-07-06 03:17:29', '2018-07-06 03:17:29'),
(139, 'edit_payout_batches', 'payout_batches', '2018-07-06 03:17:29', '2018-07-06 03:17:29'),
(140, 'add_payout_batches', 'payout_batches', '2018-07-06 03:17:29', '2018-07-06 03:17:29'),
(141, 'delete_payout_batches', 'payout_batches', '2018-07-06 03:17:29', '2018-07-06 03:17:29'),
(142, 'browse_countries', 'countries', '2018-07-15 04:21:23', '2018-07-15 04:21:23'),
(143, 'read_countries', 'countries', '2018-07-15 04:21:23', '2018-07-15 04:21:23'),
(144, 'edit_countries', 'countries', '2018-07-15 04:21:23', '2018-07-15 04:21:23'),
(145, 'add_countries', 'countries', '2018-07-15 04:21:23', '2018-07-15 04:21:23'),
(146, 'delete_countries', 'countries', '2018-07-15 04:21:23', '2018-07-15 04:21:23'),
(147, 'browse_city_municipalities', 'city_municipalities', '2018-07-15 04:21:45', '2018-07-15 04:21:45'),
(148, 'read_city_municipalities', 'city_municipalities', '2018-07-15 04:21:45', '2018-07-15 04:21:45'),
(149, 'edit_city_municipalities', 'city_municipalities', '2018-07-15 04:21:45', '2018-07-15 04:21:45'),
(150, 'add_city_municipalities', 'city_municipalities', '2018-07-15 04:21:45', '2018-07-15 04:21:45'),
(151, 'delete_city_municipalities', 'city_municipalities', '2018-07-15 04:21:45', '2018-07-15 04:21:45'),
(152, 'browse_complaints', 'complaints', '2018-07-17 10:11:23', '2018-07-17 10:11:23'),
(153, 'read_complaints', 'complaints', '2018-07-17 10:11:23', '2018-07-17 10:11:23'),
(154, 'edit_complaints', 'complaints', '2018-07-17 10:11:23', '2018-07-17 10:11:23'),
(155, 'add_complaints', 'complaints', '2018-07-17 10:11:23', '2018-07-17 10:11:23'),
(156, 'delete_complaints', 'complaints', '2018-07-17 10:11:23', '2018-07-17 10:11:23'),
(157, 'browse_complaint_categories', 'complaint_categories', '2018-07-17 11:00:51', '2018-07-17 11:00:51'),
(158, 'read_complaint_categories', 'complaint_categories', '2018-07-17 11:00:51', '2018-07-17 11:00:51'),
(159, 'edit_complaint_categories', 'complaint_categories', '2018-07-17 11:00:51', '2018-07-17 11:00:51'),
(160, 'add_complaint_categories', 'complaint_categories', '2018-07-17 11:00:51', '2018-07-17 11:00:51'),
(161, 'delete_complaint_categories', 'complaint_categories', '2018-07-17 11:00:51', '2018-07-17 11:00:51'),
(162, 'browse_vouchers', 'vouchers', '2018-07-28 00:57:56', '2018-07-28 00:57:56'),
(163, 'read_vouchers', 'vouchers', '2018-07-28 00:57:56', '2018-07-28 00:57:56'),
(164, 'edit_vouchers', 'vouchers', '2018-07-28 00:57:56', '2018-07-28 00:57:56'),
(165, 'add_vouchers', 'vouchers', '2018-07-28 00:57:56', '2018-07-28 00:57:56'),
(166, 'delete_vouchers', 'vouchers', '2018-07-28 00:57:56', '2018-07-28 00:57:56');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 1),
(3, 1),
(4, 1),
(4, 3),
(5, 1),
(5, 3),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(11, 3),
(12, 1),
(12, 3),
(13, 1),
(13, 3),
(14, 1),
(14, 3),
(15, 1),
(16, 1),
(16, 2),
(16, 3),
(16, 4),
(16, 5),
(17, 1),
(17, 2),
(17, 3),
(17, 4),
(17, 5),
(18, 1),
(18, 2),
(18, 3),
(18, 4),
(18, 5),
(19, 1),
(19, 3),
(19, 4),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(26, 3),
(27, 1),
(27, 3),
(28, 1),
(28, 3),
(29, 1),
(29, 3),
(30, 1),
(30, 3),
(31, 1),
(31, 3),
(32, 1),
(32, 3),
(33, 1),
(33, 3),
(34, 1),
(34, 3),
(35, 1),
(35, 3),
(36, 1),
(36, 3),
(37, 1),
(37, 3),
(38, 1),
(38, 3),
(39, 1),
(39, 3),
(40, 1),
(40, 3),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(52, 4),
(53, 1),
(53, 4),
(54, 1),
(54, 4),
(55, 1),
(55, 4),
(56, 1),
(57, 1),
(57, 2),
(57, 4),
(58, 1),
(58, 4),
(59, 1),
(59, 4),
(60, 1),
(60, 2),
(61, 1),
(62, 1),
(62, 2),
(62, 5),
(63, 1),
(63, 2),
(63, 5),
(64, 1),
(64, 2),
(64, 5),
(65, 1),
(65, 2),
(66, 1),
(67, 1),
(67, 5),
(68, 1),
(68, 5),
(69, 1),
(70, 1),
(70, 5),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1),
(137, 1),
(138, 1),
(139, 1),
(142, 1),
(143, 1),
(144, 1),
(145, 1),
(146, 1),
(147, 1),
(148, 1),
(149, 1),
(150, 1),
(151, 1),
(152, 1),
(153, 1),
(154, 1),
(155, 1),
(156, 1),
(157, 1),
(158, 1),
(159, 1),
(160, 1),
(161, 1),
(162, 1),
(162, 4),
(163, 1),
(163, 4),
(164, 1),
(164, 4),
(165, 1),
(165, 4),
(166, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-04-21 03:12:19', '2018-04-21 03:12:19'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-04-21 03:12:19', '2018-04-21 03:12:19'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-04-21 03:12:19', '2018-04-21 03:12:19'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-04-21 03:12:19', '2018-04-21 03:12:19');

-- --------------------------------------------------------

--
-- Table structure for table `referrals`
--

CREATE TABLE `referrals` (
  `id` int(10) UNSIGNED NOT NULL,
  `referral_id` int(10) UNSIGNED DEFAULT NULL,
  `referrer_id` int(10) UNSIGNED DEFAULT NULL,
  `referral_consumable` int(11) DEFAULT '5',
  `referral_valid_until` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'superadmin', 'Super Administrator', '2018-04-21 03:12:10', '2018-04-21 05:14:42'),
(2, 'student', 'Student', '2018-04-21 03:12:10', '2018-04-21 05:14:22'),
(3, 'admin', 'Administrator', '2018-04-21 05:15:31', '2018-04-21 05:15:31'),
(4, 'client', 'Client', '2018-04-21 05:16:02', '2018-04-21 05:16:37'),
(5, 'teacher', 'Teacher', '2018-04-21 05:16:52', '2018-04-21 05:16:52');

-- --------------------------------------------------------

--
-- Table structure for table `schools`
--

CREATE TABLE `schools` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `prefix` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `school_packages`
--

CREATE TABLE `school_packages` (
  `id` int(10) UNSIGNED NOT NULL,
  `package_id` int(10) UNSIGNED DEFAULT NULL,
  `price` decimal(10,0) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `number_of_classes` int(11) DEFAULT NULL,
  `duration_in_days` int(11) DEFAULT NULL,
  `base_price` decimal(10,0) DEFAULT NULL,
  `additional_price` decimal(10,0) DEFAULT NULL,
  `school_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `currency` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Speakable Me', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Learn English online from certified English Teacher', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', NULL, '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', NULL, '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Speakable Me!', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Learn English with Ease', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', NULL, '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', NULL, '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `student_account_types`
--

CREATE TABLE `student_account_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `student_account_types`
--

INSERT INTO `student_account_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Free-Trial', '2018-07-01 01:26:48', '2018-07-01 01:26:48'),
(2, 'Regular', '2018-07-01 01:26:59', '2018-07-01 01:26:59');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_account_types`
--

CREATE TABLE `teacher_account_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rate` int(10) UNSIGNED DEFAULT NULL,
  `minimum_student` int(10) UNSIGNED DEFAULT NULL,
  `minimum_class` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teacher_account_types`
--

INSERT INTO `teacher_account_types` (`id`, `name`, `rate`, `minimum_student`, `minimum_class`, `created_at`, `updated_at`) VALUES
(1, 'Level 1', 60, 5, 20, '2018-07-03 06:30:30', '2018-07-03 06:30:30'),
(2, 'Level 2', 65, 10, 20, '2018-07-03 06:30:46', '2018-07-03 06:30:46'),
(3, 'Level 3', 70, 20, 20, '2018-07-03 06:31:00', '2018-07-03 06:31:25');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2018-04-21 03:12:20', '2018-04-21 03:12:20'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2018-04-21 03:12:20', '2018-04-21 03:12:20'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2018-04-21 03:12:20', '2018-04-21 03:12:20'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2018-04-21 03:12:20', '2018-04-21 03:12:20'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2018-04-21 03:12:20', '2018-04-21 03:12:20'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2018-04-21 03:12:20', '2018-04-21 03:12:20'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2018-04-21 03:12:20', '2018-04-21 03:12:20'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2018-04-21 03:12:20', '2018-04-21 03:12:20'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2018-04-21 03:12:20', '2018-04-21 03:12:20'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2018-04-21 03:12:20', '2018-04-21 03:12:20'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2018-04-21 03:12:20', '2018-04-21 03:12:20'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2018-04-21 03:12:21', '2018-04-21 03:12:21'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2018-04-21 03:12:21', '2018-04-21 03:12:21'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2018-04-21 03:12:21', '2018-04-21 03:12:21'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2018-04-21 03:12:21', '2018-04-21 03:12:21'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2018-04-21 03:12:21', '2018-04-21 03:12:21'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2018-04-21 03:12:21', '2018-04-21 03:12:21'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2018-04-21 03:12:21', '2018-04-21 03:12:21'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2018-04-21 03:12:21', '2018-04-21 03:12:21'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2018-04-21 03:12:21', '2018-04-21 03:12:21'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2018-04-21 03:12:21', '2018-04-21 03:12:21'),
(22, 'menu_items', 'title', 11, 'pt', 'Publicações', '2018-04-21 03:12:21', '2018-04-21 03:12:21'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2018-04-21 03:12:21', '2018-04-21 03:12:21'),
(24, 'menu_items', 'title', 10, 'pt', 'Categorias', '2018-04-21 03:12:21', '2018-04-21 03:12:21'),
(25, 'menu_items', 'title', 12, 'pt', 'Páginas', '2018-04-21 03:12:21', '2018-04-21 03:12:21'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2018-04-21 03:12:21', '2018-04-21 03:12:21'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2018-04-21 03:12:21', '2018-04-21 03:12:21'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2018-04-21 03:12:21', '2018-04-21 03:12:21'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2018-04-21 03:12:21', '2018-04-21 03:12:21'),
(30, 'menu_items', 'title', 9, 'pt', 'Configurações', '2018-04-21 03:12:21', '2018-04-21 03:12:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'users/default.png',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `middle_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` datetime DEFAULT NULL,
  `gender` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qq` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `wechat` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `city_id` int(10) UNSIGNED DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL,
  `timezone` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Asia/Shanghai',
  `immortal` int(11) DEFAULT '0',
  `school_id` int(10) UNSIGNED DEFAULT NULL,
  `peak1to15` int(11) DEFAULT '0',
  `peak16to31` int(11) DEFAULT '0',
  `password_changed` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'false',
  `special_plotting_indefinite` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'false',
  `special_plotting` datetime DEFAULT NULL,
  `student_account_type_id` int(10) UNSIGNED DEFAULT NULL,
  `teacher_account_type_id` int(10) UNSIGNED DEFAULT NULL,
  `free_trial_consumable` int(10) UNSIGNED DEFAULT '1',
  `free_trial_valid_until` datetime DEFAULT NULL,
  `bank_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Bank of the Philippine Islands',
  `bank_account_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bank_account_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nick` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT 'en'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `first_name`, `email`, `avatar`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `last_name`, `middle_name`, `birth_date`, `gender`, `qq`, `mobile`, `wechat`, `address`, `city_id`, `country_id`, `timezone`, `immortal`, `school_id`, `peak1to15`, `peak16to31`, `password_changed`, `special_plotting_indefinite`, `special_plotting`, `student_account_type_id`, `teacher_account_type_id`, `free_trial_consumable`, `free_trial_valid_until`, `bank_name`, `bank_account_number`, `bank_account_name`, `nick`, `lang`) VALUES
(1, 1, 'Louie', 'foronda@ymail.com', 'users\\April2018\\kHOZrK5rdMXz1Lsj26Wx.jpg', '$2y$10$s6MYI/Hhn93sgCpH3ih8N.udKIkGO4ZFVTfta16hARAnryqs3B9Pe', 'L1iVRg64CcLAeMIYgbYXTzGuPslUjcvkL8hCxq0pp6hxryIOIjbTMMbV2a8m', NULL, '2018-04-20 13:07:26', '2018-07-28 09:30:35', 'Foronda', 'Bacani', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, 'Asia/Manila', NULL, NULL, 0, 0, 'true', 'false', NULL, NULL, NULL, 1, NULL, 'Bank of the Philippine Islands', NULL, NULL, NULL, 'en');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vouchers`
--

CREATE TABLE `vouchers` (
  `id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED DEFAULT NULL,
  `code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fixed_rate` double UNSIGNED DEFAULT NULL,
  `percent` int(10) UNSIGNED DEFAULT NULL,
  `quantity` int(10) UNSIGNED DEFAULT '0',
  `total_quantity` int(10) UNSIGNED DEFAULT '0',
  `status` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT 'active',
  `valid_from` datetime DEFAULT NULL,
  `valid_until` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `school_package_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applications`
--
ALTER TABLE `applications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `availabilities`
--
ALTER TABLE `availabilities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `start_at` (`start_at`,`teacher_id`);

--
-- Indexes for table `book_classes`
--
ALTER TABLE `book_classes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `book_classes_job_ids_index` (`job_ids`),
  ADD KEY `start_at` (`start_at`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `city_municipalities`
--
ALTER TABLE `city_municipalities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complaints`
--
ALTER TABLE `complaints`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `complaint_categories`
--
ALTER TABLE `complaint_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enrollment_incentives`
--
ALTER TABLE `enrollment_incentives`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payouts`
--
ALTER TABLE `payouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payout_batches`
--
ALTER TABLE `payout_batches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penalties`
--
ALTER TABLE `penalties`
  ADD PRIMARY KEY (`id`),
  ADD KEY `penalties_name_index` (`name`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `referrals`
--
ALTER TABLE `referrals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `schools`
--
ALTER TABLE `schools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `school_packages`
--
ALTER TABLE `school_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `student_account_types`
--
ALTER TABLE `student_account_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_account_types`
--
ALTER TABLE `teacher_account_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`),
  ADD KEY `users_nick_index` (`nick`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indexes for table `vouchers`
--
ALTER TABLE `vouchers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `vouchers_code_unique` (`code`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applications`
--
ALTER TABLE `applications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `availabilities`
--
ALTER TABLE `availabilities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `book_classes`
--
ALTER TABLE `book_classes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `city_municipalities`
--
ALTER TABLE `city_municipalities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2315;
--
-- AUTO_INCREMENT for table `complaints`
--
ALTER TABLE `complaints`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `complaint_categories`
--
ALTER TABLE `complaint_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=360;
--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `enrollment_incentives`
--
ALTER TABLE `enrollment_incentives`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1802;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `payouts`
--
ALTER TABLE `payouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payout_batches`
--
ALTER TABLE `payout_batches`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `penalties`
--
ALTER TABLE `penalties`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `referrals`
--
ALTER TABLE `referrals`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `schools`
--
ALTER TABLE `schools`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `school_packages`
--
ALTER TABLE `school_packages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `student_account_types`
--
ALTER TABLE `student_account_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `teacher_account_types`
--
ALTER TABLE `teacher_account_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `vouchers`
--
ALTER TABLE `vouchers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
