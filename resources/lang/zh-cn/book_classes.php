<?php

return [



    'failed' => '您输入的密码有误，请重新输入',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

];