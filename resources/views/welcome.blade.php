<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
    <title>Speakable Me!!</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/slick/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/slick/slick-theme.css') }}">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/full.css') }}" rel="stylesheet">
</head>
<body>
    <section class="lazy" style="margin: 0 0 0 0;">
        <div class="full" style="background: url('{{ asset('images/1.jpg') }}') no-repeat center center fixed;"></div>
        <div class="full" style="background: url('{{ asset('images/2.jpg') }}') no-repeat center center fixed;"></div>
        <div class="full" style="background: url('{{ asset('images/3.jpg') }}') no-repeat center center fixed;"></div>
    </section>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">Speakable Me!!</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="#">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">About</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Services</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Contact</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ url('login') }}">Sign-in</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Page Content -->

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script src="{{ asset('vendor/slick/slick.min.js') }}" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript">
    $(document).on('ready', function() {
      $(".lazy").slick({
        autoplay: true, 
        autoplaySpeed: 1000,
        infinite: true,
        dots: false,
        prevArrow: false,
        nextArrow: false,
        fade: true,
        pauseOnHover: false,
        pauseOnFocus: false
      });
    });
</script>

</body>
</html>