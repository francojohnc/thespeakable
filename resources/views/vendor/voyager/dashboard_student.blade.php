        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">

        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">{{ (auth()->user()->lang=="zh-cn") ? "剩余课次":"Regular Balance" }}</span>
              <span class="info-box-number">{{ App\Http\Controllers\DashboardController::regularBalance() }}</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    {{ (auth()->user()->lang=="zh-cn") ? "有效期":"Expiration" }}: {{ App\Http\Controllers\DashboardController::regularBalanceLatestDate() }}
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">{{ (auth()->user()->lang=="zh-cn") ? "赠课课次":"Incentive Balance" }}</span>
              <span class="info-box-number">{{ App\Http\Controllers\DashboardController::referralBalance() }}</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    {{ (auth()->user()->lang=="zh-cn") ? "有效期":"Expiration" }}: {{ App\Http\Controllers\DashboardController::referralBalanceLatestDate() }}
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">{{ (auth()->user()->lang=="zh-cn") ? "补课课次":"Immortal Balance" }}</span>
              <span class="info-box-number">{{ App\Http\Controllers\DashboardController::immortalBalance() }}</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    {{ (auth()->user()->lang=="zh-cn") ? "永久有效":"No Expiration Date" }}
                    
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-red">
            <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">{{ (auth()->user()->lang=="zh-cn") ? "累计听说英语课次":"Completed Classes" }}</span>
              <span class="info-box-number">{{ App\Http\Controllers\DashboardController::totalStudentCompletedClass() }}</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    起始日
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
                       
                    </div>
                </div>
            </div>
        </div>
