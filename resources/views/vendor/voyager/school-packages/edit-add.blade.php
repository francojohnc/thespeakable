@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data" id="save_edit" name="save_edit">
                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                            @endphp


                            @foreach($dataTypeRows as $row)

                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $options = json_decode($row->details);
                                    $display_options = isset($options->display) ? $options->display : NULL;
                                @endphp
                                @if ($options && isset($options->legend) && isset($options->legend->text))
                                    <legend class="text-{{$options->legend->align or 'center'}}" style="background-color: {{$options->legend->bgcolor or '#f0f0f0'}};padding: 5px;">{{$options->legend->text}}</legend>
                                @endif
                                @if ($options && isset($options->formfields_custom))
                                    @include('voyager::formfields.custom.' . $options->formfields_custom)
                                @else
                                    @if($row->field == "school_package_belongsto_package_relationship")
                                    @php
                                        $packages = App\Package::where("school_id",auth()->user()->school_id)->get(["name","id"]);
                                    @endphp
                                    <div class="form-group col-md-12">
                                        <label for="package_id">Packages</label>
                                        <select name="package_id" id="package_id" class="form-control">
                                            <option selected="" value="">None</option>
                                            @foreach($packages as $package)
                                                @if($package->id == $dataTypeContent->package_id)
                                                    <option value="{{ $package->id }}" selected>{{ $package->name }}</option>
                                                @else
                                                    <option value="{{ $package->id }}">{{ $package->name }}</option>
                                                @endif
                                            @endforeach
                                        </select>                                        
                                    </div>

                                    @elseif($row->field == "status")
                                    <div class="form-group col-md-12">
                                        <label for="status">Status</label>
                                        <select name="status" id="status" class="form-control">
                                            @if($dataTypeContent->status == "inactive")
                                            <option value="active">active</option>
                                            <option value="inactive" selected="">inactive</option>
                                            @else
                                            <option selected="" value="active">active</option>
                                            <option value="inactive">inactive</option>
                                            @endif
                                        </select>                                        
                                    </div>

                                    @elseif($row->field == "base_price")
                                        <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                            <label>Base Price: <span id="base_price_label">{{ "USD ".$dataTypeContent->base_price }}</span></label>
                                            <input type="hidden" value="{{ $dataTypeContent->base_price }}" name="base_price" id="base_price">
                                        </div>
                                    @elseif($row->field == "name")
                                        <input type="hidden" name="name" id="name" value="{{ $dataTypeContent->name }}">
                                    @elseif($row->field == "price")
                                        <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                            <label>Total Price: <span id="price_label">{{ "USD ".$dataTypeContent->price }}</span></label>
                                            <input type="hidden" value="{{ $dataTypeContent->price }}" name="price" id="price">
                                        </div>
                                    @elseif($row->field == "additional_price")
                                        <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                            <label for="price">Additional Price:</label>
                                            <input type="number" value="{{ ($dataTypeContent->additional_price) ? $dataTypeContent->additional_price: 0 }}" name="additional_price" id="additional_price" class="form-control">
                                        </div>
                                    @elseif($row->field == "duration_in_days")
                                        <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                            <label>Duration: <span id="duration_in_days_label">{{ $dataTypeContent->duration_in_days." days" }}</span></label>
                                            <input type="hidden" value="{{ $dataTypeContent->duration_in_days }}" name="duration_in_days" id="duration_in_days">
                                        </div>
                                    @elseif($row->field == "number_of_classes")
                                        <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                            <label>Number of Classes: <span id="number_of_classes_label">{{ ($dataTypeContent->number_of_classes)? $dataTypeContent->number_of_classes:0 }} bookable classes</span></label>
                                            <input type="hidden" value="{{ $dataTypeContent->number_of_classes }}" name="number_of_classes" id="number_of_classes">
                                        </div>
                                    @elseif($row->field == "currency")
                                            <input type="hidden" value="{{$dataTypeContent->currency}}" name="currency" id="currency">
                                    @elseif($row->field == "school_id")
                                            <input type="hidden" value="{{ auth()->user()->school_id }}" name="school_id" id="school_id">
                                    @elseif($row->field == "school_package_hasmany_order_relationship")
                                    @elseif($row->field == "created_at")
                                    @elseif($row->field == "is_group")
                                        <input type="hidden" name="is_group" id="is_group" value="{{$dataTypeContent->is_group}}">
                                    @else

                                    <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                        {{ $row->slugify }}
                                        <label for="name">{{ $row->display_name }}</label>
                                        @include('voyager::multilingual.input-hidden-bread-edit-add')
                                        @if($row->type == 'relationship')
                                            @include('voyager::formfields.relationship')
                                        @else
                                            {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                        @endif

                                        @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                            {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                        @endforeach
                                    </div>  
                                    @endif                                  
                                @endif
                            @endforeach

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <a href="javascript::" class="btn btn-primary save" onclick="save()">{{ __('voyager::generic.save') }}</a>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {}
        var $image

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });

        $(document).ready(function(){
            $("#package_id").change(function(){
                var id = $(this).val();
                $.ajax({
                  url: "{{ url('api/package/read') }}/"+id,
                  method: "GET",
                }).done(function(data) {

                    $("#base_price").val(data.base_price);
                    $("#name").val(data.name);
                    $("#currency").val(data.currency);
                    $("#base_price_label").html(data.currency+" "+data.base_price);
                    $("#duration_in_days").val(data.duration_in_days);
                    $("#duration_in_days_label").html(data.duration_in_days+" days");
                    $("#number_of_classes").val(data.number_of_classes);
                    $("#number_of_classes_label").html(data.number_of_classes+" bookable classes");
                    
                    var price = parseFloat(data.base_price) + parseFloat($("#additional_price").val());
                    $("#price").val(price);
                    $("#price_label").html(data.currency+" "+price);

                });

            });

            $("#additional_price").blur(function(){
                var id = $(this).id;
                var base_price = $("#base_price").val();
                var additional_price = ($("#additional_price").val()) ?$("#additional_price").val():0;
                var price = parseFloat(base_price) + parseFloat(additional_price);
                var currency = $("#currency").val();
                $("#price").val(price);
                $("#price_label").html(currency+" "+price);
            
            });
        });

        function save() {
            var id = $(this).id;
            var base_price = $("#base_price").val();
            var additional_price = ($("#additional_price").val()) ?$("#additional_price").val():0;
            var price = parseFloat(base_price) + parseFloat(additional_price);
            var currency = $("#currency").val();
            $("#price").val(price);
            $("#price_label").html(currency+" "+price);
            
            $("#save_edit").submit();
        }
    </script>
@stop
