        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">

        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Potential Earning</span>
              <span class="info-box-number">PHP {{ App\Http\Controllers\DashboardController::classFeeTallyPossible() }}</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">Opened: {{ App\Http\Controllers\DashboardController::totalOpenedSlots() }}
                    
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box" style="color:#ffffff;background-color: #499fbc">
            <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Booked Classes</span>
              <span class="info-box-number">{{ App\Http\Controllers\DashboardController::bookedClass() }}/{{ App\Http\Controllers\DashboardController::totalOpenedSlots() }}</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    Booking Rate: {{ App\Http\Controllers\DashboardController::bookedClassRate() }}%
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Teacher Score</span>
              <span class="info-box-number">98%</span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    Teacher Level: {{ App\Http\Controllers\DashboardController::teacherLevel() }}
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box" style="color:#ffffff;background-color: #20987a">
            <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Running Total Fee</span>
              <span class="info-box-number"><a href="javascript:;" style="color:#ffffff" onclick="cutoff()">PHP {{ App\Http\Controllers\DashboardController::totalFeeTally() }}</a></span>

              <div class="progress">
                <div class="progress-bar" style="width: 70%"></div>
              </div>
                  <span class="progress-description">
                    {{ App\Http\Controllers\DashboardController::cutOffShort() }}
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

                    </div>
                </div>
            </div>
        </div>