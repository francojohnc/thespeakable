@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> Plot Overview
        </h1>
        @can('delete',app($dataType->model_name))
            @include('voyager::partials.bulk-delete')
        @endcan
        @can('edit',app($dataType->model_name))
        @if(isset($dataType->order_column) && isset($dataType->order_display_column))
            <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary">
                <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
            </a>
        @endif
        @endcan
        @include('voyager::multilingual.language-selector')
        <div class="alert alert-warning" style="background-color:#FF9E0F66;color:#444444">Note: Please make sure to plot atleast 40 classes within the peak hour (18:00-22:00).</div>
    </div>
@stop

@section('content')
@php
    $userTimeZone = auth()->user()->timezone;
    $display = "display:none";
    if(auth()->user()->special_plotting != null) {
        $now = new \DateTime(null, new \DateTimeZone($userTimeZone));
        $special = new \DateTime(auth()->user()->special_plotting, new \DateTimeZone($userTimeZone));
        if($now->getTimestamp() < $special->getTimestamp()) {
            $display = "";
        }
    }
    // if indefinite is true then always show the button
    if(auth()->user()->special_plotting_indefinite == "true") {
        $display = "";
    }
@endphp
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div><h4>Current Cut-off Peak Hour Classes: <span id="span-peak1">0</span></h4></div>
                        <div><h4>Next Cut-off Peak Hour Classes: <span id="span-peak2">0</span></h4></div>
                        <div class="table-responsive"><div id="calendar"></div></div>
                    </div>
                    <button id="submitDate" class="btn btn-primary pull-right save" >
                        {{ __('voyager::generic.save') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
<link rel="stylesheet" href="{{ asset('vendor/fullcalendar/fullcalendar.min.css') }}">
@stop

@section('javascript')
    <script src="{{ asset('vendor/fullcalendar/lib/moment.min.js') }}"></script>
    <script src="{{ asset('vendor/fullcalendar/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('vendor/fullcalendar/lib/moment-with-data.min.js') }}"></script>
    <script src="{{ asset('vendor/bootbox/bootbox.min.js') }}"></script>

    <script type="text/javascript">
    var special_plotting = "{{ (auth()->user()->special_plotting_indefinite == 'true') ? 'true':'false' }}";
    var clickedEvents = [];
    var totalPeakEvents1to15 = {!! auth()->user()->peak1to15 !!};
    var totalPeakEvents16to31 = {!! auth()->user()->peak16to31 !!};

    $(function() { // document ready

        if((totalPeakEvents1to15 > 39) || (totalPeakEvents16to31 > 39)) {
            $("#submitDate").show();
        }

        var mn = moment().tz("{{ auth()->user()->timezone }}").toISOString().split("T")[0].split("-")[2];

        if( (mn >= 1) && (mn<=15) ) {
            $("#span-peak1").html(totalPeakEvents1to15);
            $("#span-peak2").html(totalPeakEvents16to31);
        }
        else if((mn >= 16) && (mn <= 31)) {
            $("#span-peak1").html(totalPeakEvents16to31);
            $("#span-peak2").html(totalPeakEvents1to15);
        }
        // onload we have to check if you already have totalPeakEvents

        init();

    }); // end document ready

    $("#submitDate").click(function() {
        var now = moment();
        var addToTime = now.tz("{{ $userTimeZone }}").format('Z').split(":");
        var operation = addToTime[0].toString();
        var hr = Math.abs(parseInt(addToTime[0]));
        var min = Math.abs(parseInt(addToTime[1]));
        var rightNowOnYourLocal;
        if(operation[0] == "-") {
            rightNowOnYourLocal = now.subtract( (hr-1), "hours");
            rightNowOnYourLocal = now.subtract( min, "minutes");
        }
        else {
            rightNowOnYourLocal = now.add( (hr+1), "hours");            
            rightNowOnYourLocal = now.add( min, "minutes");
        }

        for(i=0; i<clickedEvents.length; i++) {
            if(clickedEvents[i].start.isBefore(rightNowOnYourLocal)){
                clickedEvents.splice(i, 1);
            }
        }

        //convert startdate and enddates to mysql ready for insert
        for(i=0; i<clickedEvents.length; i++) {
            clickedEvents[i].start = clickedEvents[i].start.format("YYYY-MM-DD HH:mm:ss");
            clickedEvents[i].end = clickedEvents[i].end.format("YYYY-MM-DD HH:mm:ss");
        }

        //var data = JSON.stringify(clickedEvents);
        //console.log(data);

        var url = "{{ url('api/calendar/availability/create') }}";
        $.ajax({
          type: "POST",
          url: url,
          contentType: "application/json",
          data: JSON.stringify(clickedEvents),
          success: function(data){
            //alert(data);

            url = "{{ url('api/calendar/availability/peak') }}";
            var peak = {
                peak1to15: totalPeakEvents1to15,
                peak16to31: totalPeakEvents16to31
            }
            $.ajax({
              type: "POST",
              url: url,
              contentType: "application/json",
              data: JSON.stringify(peak),
              success: function(data) {
                bootbox.alert(data,function(){
                    window.location.replace("{{ url('availabilities') }}");
                });
              },
              error: function(error){
                console.log(error);
              }
            });

            //window.location.replace("{{ url('availabilities') }}");

          },
          error: function(error){
            console.log(error);
          }
        });

    });

    function leapYear(year)
    {
      return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
    }



    function getFirst() {
        // new constraint
        // if date is 13 open the next plotting 16-31
        // if date is 27 open the next plotting 1-15
        // reset old data 
        // open next 

        var dt = "";
        var d = moment().toISOString().split("T")[0].split("-");


        if( (d[2] >= 1) && (d[2]<13) ) {
            totalPeakEvents16to31 = 0;
            dt = d[0]+"-"+d[1]+"-01";
        }
        else if( (d[2] >= 13) && (d[2]<=15) ){
            dt = d[0]+"-"+d[1]+"-13";
        }
        else if( (d[2] >= 16) && (d[2]<27) ) {
            totalPeakEvents1to15 = 0;
            dt = d[0]+"-"+d[1]+"-16";
        }
        else if( (d[2] >= 27) && (d[2]<=31) ) {
            dt = d[0]+"-"+d[1]+"-27";
        }
        return dt;
    }

    function getLast() {
        var dt = "";
        var today = moment();
        //console.log(today);
        var d = today.toISOString().split("T")[0].split("-");
        var end = today.endOf('month').add(1,'days').toISOString().split("T")[0].split("-");
        //console.log(end);

        if( (d[2] >= 1) && (d[2]<13) ) {
            dt = d[0]+"-"+d[1]+"-16";
        }
        else if( (d[2] >= 13) && (d[2]<=15) ){
            dt = end[0]+"-"+end[1]+"-"+end[2];
        }
        else if( (d[2] >= 16) && (d[2]<27) ) {
            dt = end[0]+"-"+end[1]+"-"+end[2];
        }
        else if( (d[2] >= 27) && (d[2]<=31) ) {
            if(d[1] == 12) { d[1] = 1; }
            else { d[1]++; }
            dt = end[0]+"-"+end[1]+"-16";
        }
        return dt;
    }


function init(){
    $("#calendar").fullCalendar("destroy");
            var calendar = $('#calendar').fullCalendar({
            header: {
                left: 'prev,next,today',
                center: 'title',
                right: 'agendaWeek,agendaCutoff'
            },
            columnHeaderFormat: "ddd M/D",
            contentHeight: 'auto',
            timeFormat: 'H:mm',
            slotLabelFormat: 'H:mm',
            selectConstraint: "businessHours",
            defaultView: 'agendaCutoff',
            defaultTimedEventDuration: '0:50',
            eventStartEditable: false,
            allDaySlot: false,
            scrollTime: '00:00',
            lang: /^en-/.test(navigator.language) ? 'en' : 'zh-cn',
            timezone: '{{ $userTimeZone }}',
            editable: true,
            selectable: true,
            selectHelper: true,
            disableDragging: true,
            disableResizing: true,
            businessHours: {
              dow: [ 0, 1, 2, 3, 4, 5, 6 ],
              start: '6:00',
              end: '24:00',
            },
            validRange: {
                start: getFirst(),
                end: getLast()
            },
            views: {
                agendaCutoff: {
                  type: 'agenda',
                  duration: { days: 16 },
                  buttonText: '15 days'
                }
            },
            viewRender: function() {
                $("#calendar").fullCalendar( 'removeEvents' );                
                $("#calendar").fullCalendar( 'addEventSource', clickedEvents );

                var container = $('.fc thead');

                $(".cloned-head").remove();

                var cloneHead = $('<tr class="fc-first fc-last cloned-head"></tr>').css({
                    zIndex: 5,
                    display: 'none'
                }).appendTo(container);

                $('#calendar thead th').each(function() {
                    var cloned = $(this).clone();
                    cloned.css('width', $(this).outerWidth());
                    cloned.appendTo(cloneHead);
                });

                $(document).on('scroll', function() {
                    var scroll = $(document).scrollTop();
                    var show = (container.offset().top) + cloneHead.outerHeight() < scroll;
                    if (show) {
                        cloneHead.show().css({
                          position: 'absolute',
                          backgroundColor: '#cbcdda',
                          top: scroll - container.offset().top +65
                        });
                    } else {
                        cloneHead.hide();
                    }
                });

            },
            eventSources: [
                {
                    url: "{{ url('api/calendar/availability/list') }}/{{ auth()->user()->id }}",
                    color: "#019875",
                    textColor: "white"
                }
            ],
            eventOverlap: function(stillEvent, movingEvent) {
                return false;//stillEvent.allDay && movingEvent.allDay;
            },
            select: function(start, end) {
                // check if this start has already been taken
                // force end to 30 minutes...
                var end = moment(start).add(50,"minutes");

                var inClickedEvents = false;
                if(clickedEvents.length > 0) {
                    for(var i=0; i<clickedEvents.length; i++){
                        //console.log(clickedEvents[i].start.unix() +"   "+ start.unix());
                        if(clickedEvents[i].start.unix() == start.unix()) {
                            inClickedEvents = true;
                            calendar.fullCalendar('unselect');
                            break;
                        }
                    }
                }

                if(inClickedEvents == false){
                    //console.log(end);
                    // force events to only 50 minutes even if dragged
                    var now = moment();
                    var addToTime = now.tz("{{ $userTimeZone }}").format('Z').split(":");
                    var operation = addToTime[0].toString();
                    var hr = Math.abs(parseInt(addToTime[0]));
                    var min = Math.abs(parseInt(addToTime[1]));
                    var rightNowOnYourLocal;
                    
                    if(operation[0] == "-") {
                        rightNowOnYourLocal = now.subtract( (hr-1), "hours");
                        rightNowOnYourLocal = now.subtract( min, "minutes");
                    }
                    else {
                        rightNowOnYourLocal = now.add( (hr+1), "hours");            
                        rightNowOnYourLocal = now.subtract( min, "minutes");
                    }
                    

                    if( start.isBefore( rightNowOnYourLocal ) ) {
                        alert('You cannot open slots which are less than one hour from now.');
                        calendar.fullCalendar('unselect');
                    } else {
                        // CODE FOR ADDING EVENTS
                        // set default duration to 1 hr.
                        end = end.subtract(5,"minutes");
                        // console.log(end);
                        // event ok
                        var title = end.format("H:mm")+" - open";
                        var status = "open";
                        var eventData;
                        if (title && title.trim()) {
                            eventData = {
                              title: title,
                              start: start,
                              end: end,
                              status: status,
                              timezone: "{{ $userTimeZone }}",
                              teacher_id: teacher_id
                            };
                            clickedEvents.push(eventData);
                            //clickedEvents[clickedEvents.length] = eventData;
                            calendar.fullCalendar('renderEvent', eventData);
                        }
                        calendar.fullCalendar('unselect');
                    }

                }
            },
            eventDurationEditable: false,
            eventClick: function(calEvent, jsEvent, view) {
                //console.log(calEvent.status);
                if(calEvent.status == "open" && calEvent.color == undefined)
                {
                    var d = calEvent.start.toISOString().split("T")[0].split("-")[2];
                    
                    var m = calEvent.start.toISOString().split("T")[1].split(":")[0];

                    // delete events only 
                    var now = moment();
                    var addToTime = now.tz("{{ $userTimeZone }}").format('Z').split(":");
                    var operation = addToTime[0].toString();
                    var hr = Math.abs(parseInt(addToTime[0]));
                    var min = Math.abs(parseInt(addToTime[1]));
                    var rightNowOnYourLocal;
                    if(operation[0] == "-"){
                        rightNowOnYourLocal = now.subtract( (hr-1), "hours");
                        rightNowOnYourLocal = now.subtract( min, "minutes");
                    }
                    else {
                        rightNowOnYourLocal = now.add( (hr+1), "hours");            
                        rightNowOnYourLocal = now.add( min, "minutes");
                    }

                    if( calEvent.start.isBefore( rightNowOnYourLocal ) ) {
                        alert('You cannot open slots which are less than one hour from now, if you are saving, only valid timeslots 1 hr from now will be saved..');
                        calendar.fullCalendar('unselect');
                    } else {
                        // CODE FOR REMOVING EVENTS on click
                        calendar.fullCalendar('removeEvents', calEvent._id);
                    }//end else
                }// end if status is open
                else if( (calEvent.status == "booked" || calEvent.status == "open") && calEvent.color != undefined) {
                    deleteAvailability(calEvent.id);
                }

            }// end eventclick
        });// end fullcalendar
}

        function deleteAvailability(id) {

            bootbox.confirm("Do you really want to close this schedule? ", function(ans){
                if(ans == true){
                    var url = "{{ url('api/availabilities/delete') }}";
                    var peak = {
                        id: id
                    }

                    $.ajax({
                      type: "POST",
                      url: url,
                      contentType: "application/json",
                      data: JSON.stringify(peak),
                      success: function(data){
                        bootbox.alert(data, function(){
                            init();    
                        });
                      },
                      error: function(error){
                        console.log(error);
                      }
                    });                    
                }
            });

        }


    </script>
@stop

@section('css')
<style>
.fc-view-container { 
  overflow-x: scroll !important; 
}
.fc-view.fc-agendaDay-view.fc-agenda-view{
  width: 500% !important;
}
/* **For 2 day view** */
.fc-view.fc-agendaTwoDay-view.fc-agenda-view{
  width: 500% !important;
} 
</style>
@stop