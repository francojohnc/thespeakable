@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                            @endphp

                            <div class="form-group col-md-12">
                                <label for="fixed_or_percent">Please choose one:</label>
                                @if( $dataTypeContent->percent==null )
                                    <label><input type="radio" value="fixed" name="fixed_or_percent" id="lbl-fixed" checked> Fixed Rate</label>
                                    <label><input type="radio" value="percent" name="fixed_or_percent" id="lbl-percent"> Percentage of Price</label>
                                @else                                
                                    <label><input type="radio" value="fixed" name="fixed_or_percent" id="lbl-fixed"> Fixed Rate</label>
                                    <label><input type="radio" value="percent" name="fixed_or_percent" id="lbl-percent" checked> Percentage of Price</label>
                                @endif
                            </div>


                            @foreach($dataTypeRows as $row)


                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $options = json_decode($row->details);
                                    $display_options = isset($options->display) ? $options->display : NULL;
                                @endphp
                                @if ($options && isset($options->legend) && isset($options->legend->text))
                                    <legend class="text-{{$options->legend->align or 'center'}}" style="background-color: {{$options->legend->bgcolor or '#f0f0f0'}};padding: 5px;">{{$options->legend->text}}</legend>
                                @endif
                                @if ($options && isset($options->formfields_custom))
                                    @include('voyager::formfields.custom.' . $options->formfields_custom)
                                @else

                                    @if( $row->field == "school_id" )
                                        <input type="hidden" value="{{ auth()->user()->school_id }}" name="school_id" id="school_id">
                                    @elseif( $row->field == "code" )
                                        <div class="form-group col-md-12">
                                            <label for="code">Voucher Code <small>Please create your own code: ex. KS-PROMO-5, maximum 20 characters only.</small></label>
                                            <input type="text" class="form-control" value="" name="code" id="code" required="true" max="20" min="3">
                                        </div>
                                    @elseif( $row->field == "fixed_rate" )
                                        <div class="form-group col-md-12" id="div-fixed-rate">
                                            <label for="fixed_rate">Fixed Rate in USD <small>ex. $2.35 USD</small></label>
                                            <input type="number" class="form-control" value="{{ $dataTypeContent->fixed_rate }}" name="fixed_rate" id="fixed-rate" min="0">
                                        </div>
                                    @elseif( $row->field == "percent" )
                                        <div class="form-group col-md-12" id="div-percent" style="display:none">
                                            <label for="percent">Percentage <small>Percent of total amount.</small></label>
                                            <input type="number" class="form-control" value="{{ $dataTypeContent->percent }}" name="percent" id="percent" min="0" max="100.00">
                                        </div>
                                    @elseif( $row->field == "quantity" )
                                        <div class="form-group col-md-12">
                                            <label for="quantity">Quantity <small>How many student can avail of this voucher?</small></label>
                                            <input type="number" class="form-control" value="{{ $dataTypeContent->quantity }}" name="quantity" id="quantity" min="0">
                                        </div>
                                    @elseif( $row->field == "total_quantity" )
                                        <input type="hidden" name="total_quantity" id="total_quantity" value="{{ $dataTypeContent->total_quantity }}">
                                    @elseif( $row->field == "voucher_belongsto_school_package_relationship" )
                                        @php
                                            $packages = App\SchoolPackage::where("school_id", auth()->user()->school_id)->where("status","active")->get();
                                        @endphp
                                        <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                            <label for="name">Package: <small>Choose the package where you want to apply the voucher.</small></label>
                                            <select class="form-control" id="school_package_id" name="school_package_id">
                                                <option value="">All</option>
                                                @foreach($packages as $package)
                                                    <option value="{{ $package->id }}">{{ $package->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                         

                                    @elseif( $row->field == "status" )
                                        <div class="form-group col-md-12">
                                            <label for="status">Status <small>Inactive vouchers will not show on orders</small></label>
                                            <select id="status" name="status" class="form-control">
                                                @if($dataTypeContent->status == "active" || $dataTypeContent->status == "")
                                                    <option value="active" selected>Active</option>
                                                    <option value="inactive">Inactive</option>
                                                @elseif($dataTypeContent->status == "inactive")
                                                    <option value="active">Active</option>
                                                    <option value="inactive" selected>Inactive</option>
                                                @endif
                                            </select>
                                        </div>
                                    @else
                                    <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                        {{ $row->slugify }}
                                        <label for="name">{{ $row->display_name }}</label>
                                        @include('voyager::multilingual.input-hidden-bread-edit-add')
                                        @if($row->type == 'relationship')
                                            @include('voyager::formfields.relationship')
                                        @else
                                            {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                        @endif

                                        @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                            {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                        @endforeach
                                    </div>
                                    @endif

                                @endif
                            @endforeach

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {}
        var $image

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });

        $(document).ready(function(){

            $('input:radio[name="fixed_or_percent"]').change(function(){
                if ( ($(this).is(':checked')) && ($(this).val() == 'percent') ) {
                    $("#div-percent").show();
                    $("#div-fixed-rate").hide();
                    $("#fixed-rate").val("");
                }else{
                    
                    $("#div-percent").hide();
                    $("#percent").val("");
                    $("#div-fixed-rate").show();
                
                }
            });


            @if(url()->current() == url("/vouchers/create"))
            $("#quantity").blur(function(){
                var q = $(this).val() ? $(this).val():0;
                $("#total_quantity").val(q);
                //console.log($("#total_quantity").val());
            });
            @else
            $("#quantity").blur(function(){
                var q = $(this).val() ? $(this).val():0;
                var tq = $("#total_quantity").val()?$("#total_quantity").val():0;
                if(q>tq){
                    $("#total_quantity").val(q);
                }
                //console.log($("#total_quantity").val());
            });
            @endif

            @if( $dataTypeContent->percent!=null )
                $("#div-percent").show();
                $("#div-fixed-rate").hide();
                $("#fixed-rate").val("");
            @endif



        });
    </script>
@stop
