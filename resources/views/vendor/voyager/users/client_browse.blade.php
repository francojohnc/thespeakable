@php
$fields = [
"avatar"=>"Avatar",
"first_name"=>"First Name",
"last_name"=>"Last Name",
"nick"=>"English Name",
"email"=>"Email",
"created_at"=>"Created On",
"gender"=>"Gender",
"qq"=>"QQ",
"wechat"=>"WeChat",
"immortal"=>"Immortal Balance",
"student_account_type_id"=>"Account Type",
"free_trial_valid_until"=>"Free Trial Valid Until",
"free_trial_consumable"=>"Free-Trial Balance",
"user_hasmany_order_relationship"=>"Orders"

];

@endphp


<div class="table-responsive">
    <table id="dataTable" class="table table-hover table-striped table-bordered" style="text-align:center">
        <thead style="text-align:center">
            <tr>
                @can('delete',app($dataType->model_name))
                    <th style="text-align:center; vertical-align: top; min-width: 80px">
                        <input type="checkbox" class="select_all">
                    </th>
                @endcan
                @foreach($dataType->browseRows as $row)
                    @if( array_key_exists($row->field,$fields) )
                    <th style="text-align:center; vertical-align: top; min-width: 80px">
                        @if ($isServerSide)
                            <a href="{{ $row->sortByUrl() }}">
                        @endif
                        {{ $fields[$row->field] }}
                        @if ($isServerSide)
                            @if ($row->isCurrentSortField())
                                @if (!isset($_GET['sort_order']) || $_GET['sort_order'] == 'asc')
                                    <i class="voyager-angle-up pull-right"></i>
                                @else
                                    <i class="voyager-angle-down pull-right"></i>
                                @endif
                            @endif
                            </a>
                        @endif
                    </th>
                    @endif
                @endforeach
                <th class="actions" style="text-align:center; vertical-align: top; min-width: 110px">{{ __('voyager::generic.actions') }}</th>
            </tr>
        </thead>
        <tbody>
            @foreach($dataTypeContent as $data)                                        
            <tr>
                @can('delete',app($dataType->model_name))
                    <td>
                        <input type="checkbox" name="row_id" id="checkbox_{{ $data->getKey() }}" value="{{ $data->getKey() }}">
                    </td>
                @endcan
                @foreach($dataType->browseRows as $row)

                    @if( array_key_exists($row->field,$fields) )

                    <td>
                        <?php $options = json_decode($row->details); ?>
                        @if($row->type == 'image')
                            <img src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif" style="width:50px">
                        @elseif($row->type == 'relationship')
                            @include('voyager::formfields.relationship', ['view' => 'browse'])
                        @elseif($row->type == 'select_multiple')
                            @if(property_exists($options, 'relationship'))

                                @foreach($data->{$row->field} as $item)
                                    @if($item->{$row->field . '_page_slug'})
                                    <a href="{{ $item->{$row->field . '_page_slug'} }}">{{ $item->{$row->field} }}</a>@if(!$loop->last), @endif
                                    @else
                                    {{ $item->{$row->field} }}
                                    @endif
                                @endforeach

                                {{-- $data->{$row->field}->implode($options->relationship->label, ', ') --}}
                            @elseif(property_exists($options, 'options'))
                                @foreach($data->{$row->field} as $item)
                                 {{ $options->options->{$item} . (!$loop->last ? ', ' : '') }}
                                @endforeach
                            @endif

                        @elseif($row->type == 'select_dropdown' && property_exists($options, 'options'))

                            @if($data->{$row->field . '_page_slug'})
                                <a href="{{ $data->{$row->field . '_page_slug'} }}">{!! $options->options->{$data->{$row->field}} !!}</a>
                            @else
                                {!! $options->options->{$data->{$row->field}} !!}
                            @endif


                        @elseif($row->type == 'select_dropdown' && $data->{$row->field . '_page_slug'})
                            <a href="{{ $data->{$row->field . '_page_slug'} }}">{{ $data->{$row->field} }}</a>
                        @elseif($row->type == 'date' || $row->type == 'timestamp')
                        {{ $options && property_exists($options, 'format') ? \Carbon\Carbon::parse($data->{$row->field})->formatLocalized($options->format) : $data->{$row->field} }}
                        @elseif($row->type == 'checkbox')
                            @if($options && property_exists($options, 'on') && property_exists($options, 'off'))
                                @if($data->{$row->field})
                                <span class="label label-info">{{ $options->on }}</span>
                                @else
                                <span class="label label-primary">{{ $options->off }}</span>
                                @endif
                            @else
                            {{ $data->{$row->field} }}
                            @endif
                        @elseif($row->type == 'color')
                            <span class="badge badge-lg" style="background-color: {{ $data->{$row->field} }}">{{ $data->{$row->field} }}</span>
                        @elseif($row->type == 'text')
                            @if($row->field == "student_account_type_id")
                                @php
                                    $res = App\StudentAccountType::find($data->student_account_type_id);
                                @endphp
                                <span>{{ $res->name }}</span>
                            @elseif($row->field == "city_id")
                                @php
                                    $res = App\CityMunicipality::find($data->city_id);
                                @endphp
                                <span>{{ $res->name }}</span>
                            @elseif($row->field == "country_id")
                                @php
                                    $res = App\Country::find($data->country_id);
                                @endphp
                                <span>{{ $res->name }}</span>
                            @else
                                @include('voyager::multilingual.input-hidden-bread-browse')
                                <div class="readmore">{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                            @endif
                        @elseif($row->type == 'text_area')
                            @include('voyager::multilingual.input-hidden-bread-browse')
                            <div class="readmore">{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                        @elseif($row->type == 'file' && !empty($data->{$row->field}) )
                            @include('voyager::multilingual.input-hidden-bread-browse')
                            @if(json_decode($data->{$row->field}))
                                @foreach(json_decode($data->{$row->field}) as $file)
                                    <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}" target="_blank">
                                        {{ $file->original_name ?: '' }}
                                    </a>
                                    <br/>
                                @endforeach
                            @else
                                <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($data->{$row->field}) }}" target="_blank">
                                    Download
                                </a>
                            @endif
                        @elseif($row->type == 'rich_text_box')
                            @include('voyager::multilingual.input-hidden-bread-browse')
                            <div class="readmore">{{ mb_strlen( strip_tags($data->{$row->field}, '<b><i><u>') ) > 200 ? mb_substr(strip_tags($data->{$row->field}, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($data->{$row->field}, '<b><i><u>') }}</div>
                        @elseif($row->type == 'coordinates')
                            @include('voyager::partials.coordinates-static-image')
                        @elseif($row->type == 'multiple_images')
                            @php $images = json_decode($data->{$row->field}); @endphp
                            @if($images)
                                @php $images = array_slice($images, 0, 3); @endphp
                                @foreach($images as $image)
                                    <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif" style="width:50px">
                                @endforeach
                            @endif
                        @else

                            @include('voyager::multilingual.input-hidden-bread-browse')
                            <span>{{ $data->{$row->field} }}</span>
                        @endif
                    </td>
                    @endif
                @endforeach
                <td class="no-sort no-click" id="bread-actions">
                    @foreach(Voyager::actions() as $action)

                        @include('voyager::bread.partials.actions', ['action' => $action])
                    @endforeach
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@if ($isServerSide)
    <div class="pull-left">
        <div role="status" class="show-res" aria-live="polite">{{ trans_choice(
            'voyager::generic.showing_entries', $dataTypeContent->total(), [
                'from' => $dataTypeContent->firstItem(),
                'to' => $dataTypeContent->lastItem(),
                'all' => $dataTypeContent->total()
            ]) }}</div>
    </div>
    <div class="pull-right">
        {{ $dataTypeContent->appends([
            's' => $search->value,
            'filter' => $search->filter,
            'key' => $search->key,
            'order_by' => $orderBy,
            'sort_order' => $sortOrder
        ])->links() }}
    </div>
@endif
