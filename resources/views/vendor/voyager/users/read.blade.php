@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> {{ __('voyager::generic.viewing') }} {{ ucfirst($dataType->display_name_singular) }} &nbsp;

        @can('edit', $dataTypeContent)
        <a href="{{ route('voyager.'.$dataType->slug.'.edit', $dataTypeContent->getKey()) }}" class="btn btn-info">
            <span class="glyphicon glyphicon-pencil"></span>&nbsp;
            {{ __('voyager::generic.edit') }}
        </a>
        @endcan
        @can('delete', $dataTypeContent)
            <a href="javascript:;" title="{{ __('voyager::generic.delete') }}" class="btn btn-danger delete" data-id="{{ $dataTypeContent->getKey() }}" id="delete-{{ $dataTypeContent->getKey() }}">
                <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">{{ __('voyager::generic.delete') }}</span>
            </a>
        @endcan

        <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager::generic.return_to_list') }}
        </a>
                @if( auth()->user()->hasRole('admin') || auth()->user()->hasRole('superadmin') )

                        @if( $dataTypeContent->disabled )
                            <button class="btn btn-success" onclick="unban()">Unban</button>
                        @else
                            <button class="btn btn-danger" onclick="ban()">Ban</button>
                        @endif
                @endif
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered" style="padding-bottom:5px;">
                    <!-- form start -->
                    @foreach($dataType->readRows as $row)
                        @php $rowDetails = json_decode($row->details);
                         if($rowDetails === null){
                                $rowDetails=new stdClass();
                                $rowDetails->options=new stdClass();
                         }
                        @endphp

                        @if(auth()->user()->hasRole('client'))
                            @include('vendor.voyager.users.client_read')
                        @else
                            @include('vendor.voyager.users.superadmin_read')                        
                        @endif


                    @endforeach

                </div>

            </div>
        </div>
    </div>
    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('voyager.'.$dataType->slug.'.index') }}" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                               value="{{ __('voyager::generic.delete_confirm') }} {{ strtolower($dataType->display_name_singular) }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('javascript')
    @if ($isModelTranslatable)
    <script>
        $(document).ready(function () {
            $('.side-body').multilingual();
        });
    </script>
    <script src="{{ voyager_asset('js/multilingual.js') }}"></script>
    @endif
    <script>
        var deleteFormAction;
        $('.delete').on('click', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) { // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');
            console.log(form.action);

            $('#delete_modal').modal('show');
        });

    </script>
    <script src="{{ asset('vendor/bootbox/bootbox.min.js') }}"></script>
    @if(auth()->user()->hasRole('superadmin') || auth()->user()->hasRole('admin'))
    <script>
        var id = "{{$dataTypeContent->id}}";

        function removePlotting()
        {
            var url = "{{ url('/api/teacher/availabilities/cancel/') }}/"+id;

            $.ajax({
              type: "POST",
              url: url
            }).done(function(data) {
                console.log(data);
                //bootbox.alert(data, function() {
                //    init();
                //});
            });
        }

        function cancelBooking()
        {
            var url = "{{ url('/api/teacher/booking/cancel/') }}/"+id;
            //console.log(url);
            $.ajax({
              type: "POST",
              url: url
            }).done(function(data) {
                console.log(data);
            });
        }


        function ban()
        {
            bootbox.confirm("Banning a user will remove all his/her open and booked schedule.. ",
                function(ans) {
                if(ans == true) {
                    removePlotting();
                    cancelBooking();
                    disableFromSystem();
                }
            });
        }

        function disableFromSystem()
        {
            var url = "{{ url('/api/teacher/access/cancel/') }}/"+id;

            $.ajax({
              type: "POST",
              url: url
            }).done(function(data) {
                bootbox.alert("This user is now banned from the system");
            });
        }

        function enableFromSystem()
        {
            var url = "{{ url('/api/teacher/access/enable/') }}/"+id;

            $.ajax({
              type: "POST",
              url: url
            }).done(function(data) {
                bootbox.alert("User can now login..");
            });
        }

        function unban()
        {
            bootbox.confirm("Enable User to Login",
                function(ans) {
                if(ans == true) {
                    enableFromSystem();
                }
            });
        }
    </script>
    @endif
@stop
