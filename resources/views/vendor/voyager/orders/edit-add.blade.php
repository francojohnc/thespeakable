@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>{{auth()->user()->lang=="zh-cn" ? "订购信息":"Order Form"}}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    @php
        $isAdmin = false;
        if( auth()->user()->hasRole('superadmin') || auth()->user()->hasRole('admin') ){
            $isAdmin = true;
        }
        $school = App\School::find(auth()->user()->school_id);
        $isCh = auth()->user()->lang=="zh-cn" ? "true":"false";
    @endphp
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">
                @if( auth()->user()->hasRole('client') )
                <div class="alert alert-warning" style="background-color: #fcf3c3; color: #8a6d3b">
                    <p>{{ (auth()->user()->lang=="zh-cn") ? "请确认付款，我们将尽快为您激活学习包。":'If you want to select from your vouchers please check "Use Vouchers" otherwise you can directly input discounts in USD.. A Save button will appear after pressing compute'}}</p>
                </div>
                @endif

                <div class="panel panel-bordered">



                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data" id="save_package" name="save_package">
                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                            @endphp





                            @foreach($dataTypeRows as $row)


                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $options = json_decode($row->details);
                                    $display_options = isset($options->display) ? $options->display : NULL;
                                @endphp
                                @if ($options && isset($options->legend) && isset($options->legend->text))
                                    <legend class="text-{{$options->legend->align or 'center'}}" style="background-color: {{$options->legend->bgcolor or '#f0f0f0'}};padding: 5px;">{{$options->legend->text}}</legend>
                                @endif
                                @if ($options && isset($options->formfields_custom))
                                    @include('voyager::formfields.custom.' . $options->formfields_custom)
                                @else
                                    @if( auth()->user()->hasRole('student') )
                                        @if( $row->field == "remaining_reservation" )
                                            <input type="hidden" name="{{ $row->field }}" id="{{ $row->field }}" value="">
                                        @elseif( $row->field == "order_number" )
                                            <input type="hidden" name="{{ $row->field }}" id="{{ $row->field }}" value="">
                                        @elseif($row->field == "status")
                                            <input type="hidden" name="status" id="status" value="pending">
                                        @elseif($row->field == "currency")
                                            <input type="hidden" name="currency" id="currency" value="">
                                        @elseif($row->field == "effective_until")
                                            <input type="hidden" name="effective_until" id="effective_until" value="">
                                        @elseif($row->field == "additional_price")
                                            <input type="hidden" name="additional_price" id="additional_price" value="">
                                        @elseif($row->field == "approved_at")
                                            <input type="hidden" name="approved_at" id="approved_at" value="">
                                        @elseif($row->field == "base_price")
                                            <input type="hidden" name="base_price" id="base_price" value="">
                                        @elseif($row->field == "name")
                                            <input type="hidden" name="name" id="name" value="">
                                        @elseif($row->field == "duration")
                                            <input type="hidden" name="duration" id="duration" value="">
                                        @elseif($row->field == "total_price" )
                                            <input type="hidden" id="original_total_price" value="">
                                            <input type="hidden" name="total_price" id="total_price" value="">
                                        @elseif($row->field == "transaction_id")
                                            <input type="hidden" name="transaction_id" id="transaction_id" value="">
                                        @elseif($row->field == "conversion_rate")
                                            <input type="hidden" name="conversion_rate" id="conversion_rate" value="">
                                        @elseif($row->field == "converted_from")
                                            <input type="hidden" name="converted_from" id="converted_from" value="">
                                        @elseif($row->field == "order_belongsto_user_relationship")
                                            <input type="hidden" name="student_id" value="{{ auth()->user()->id }}">
                                        @elseif($row->field == "discount_price")
                                            <input type="hidden" name="discount_price" id="discount_price">
                                        @elseif( $row->field == "order_belongsto_voucher_relationship" )
                                            <div class="form-group col-md-12" style="display:none" id="div-voucher">
                                                <label for="voucher_code">{{ $isCh ? "优惠码":"Voucher" }}: <small></small></label>
                                                <input type="hidden" name="voucher_id" id="voucher_id">
                                                <input type="text" name="voucher_code" id="voucher_code" class="form-control">
                                            </div>
                                        @elseif($row->field == "order_belongsto_school_package_relationship" )
                                            @php
                                                $packages = App\SchoolPackage::where("school_id", auth()->user()->school_id)->where("status","active")->get();
                                            @endphp
                                            <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                                <label for="name">{{$isCh ? "请选择学习包":"School Package"}}:</label>
                                                <select class="form-control" id="school_package_id" name="school_package_id">
                                                    <option value="">{{$isCh ? "无":"None"}}</option>
                                                    @foreach($packages as $package)
                                                        <option value="{{ $package->id }}">{{ $isCh ? str_replace("Package","学习包",$package->name):$package->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        @else
                                            <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                                {{ $row->slugify }}
                                                <label for="name">{{ $row->display_name }}</label>
                                                @include('voyager::multilingual.input-hidden-bread-edit-add')
                                                @if($row->type == 'relationship')
                                                    @include('voyager::formfields.relationship')
                                                @else
                                                    {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                                @endif

                                                @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                                    {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                                @endforeach
                                            </div>
                                        @endif
                                    @elseif( auth()->user()->hasRole('client') )
                                        @if( $dataTypeContent->status == "pending"  )
                                            @if($row->field == "discount_price")
                                                <div class="form-group col-md-12">
                                                    <label for="discount_price">Discount Price in USD: <span id="lbl-discount-price"></span></label>
                                                    <input type="decimal" min="0.00" step=".01" name="discount_price" id="discount_price" class="form-control" value="{{ $dataTypeContent->discount_price }}">
                                                    <a href="javascript:;" id="compute" class="btn btn-warning">Compute</a>
                                                </div>
                                            @elseif( $row->field == "order_belongsto_voucher_relationship" )
                                                @php
                                                    $date = new \DateTime("now", new \DateTimeZone(auth()->user()->timezone));
                                                    $vouchers = App\Voucher::where("status","active")
                                                        ->where("valid_from","<=",$date->format("Y-m-d H:i:s") )
                                                        ->where("valid_until",">=",$date->format("Y-m-d H:i:s"))
                                                        ->whereRaw("school_package_id is null or school_package_id = '$dataTypeContent->school_package_id'")
                                                        ->get();
                                                @endphp
                                                <div class="form-group col-md-12">
                                                    {{ $dataTypeContent->voucher_id }}
                                                    <label><input type="checkbox" name="use_voucher"> Use Voucher</label>
                                                    <select name="voucher_id" id="voucher_id" class="form-control" style="display:none">
                                                        <option selected value="">None</option>
                                                        @foreach($vouchers as $voucher)
                                                            <option value="{{ $voucher->id }}">{{ $voucher->code }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            @elseif( $row->field == "total_price" )
                                                <input type="hidden" id="original_total_price" value="{{ $dataTypeContent->total_price }}">
                                                <input type="hidden" name="total_price" id="total_price" value="{{ $dataTypeContent->total_price }}">
                                            @endif
                                        @else
                                        @endif
                                    @else

                                        <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                            {{ $row->slugify }}
                                            <label for="name">{{ $row->display_name }}</label>
                                            @include('voyager::multilingual.input-hidden-bread-edit-add')
                                            @if($row->type == 'relationship')
                                                @include('voyager::formfields.relationship')
                                            @else
                                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                            @endif

                                            @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                                {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                            @endforeach
                                        </div>                                        
                                        
                                    @endif
                                @endif
                            @endforeach
                            @if( auth()->user()->hasRole('student') )
                            <div class="form-group col-md-12">
                                <label>{{ $isCh ? "课次":"Number of Classes" }}: <span id="number_of_classes_label"></span></label>
                            </div>
                            <div class="form-group col-md-12">
                                <label>{{ $isCh ? "有效天数":"Duration" }}: <span id="duration_label"></span></label>
                            </div>
                            <div class="form-group col-md-12">
                                <label>{{ $isCh ? "价格":"Price" }}: <span id="price_label"></span></label>
                            </div>
                            @elseif(auth()->user()->hasRole('client'))
                                @if( $dataTypeContent->status == "pending" )
                                    <div class="form-group col-md-12">
                                        <label>Original Price: $<span id="lbl-original-price">{{ floatval($dataTypeContent->total_price) }}</span> USD</label>
                                    </div>
                                    <div class="form-group col-md-12">
                                        <label>Total Price After Discount: $<span id="lbl-total-price">{{ floatval($dataTypeContent->total_price) }}</span> USD</label>
                                    </div>
                                @else
                                    <div class="alert alert-info">You can't update orders that has been paid already..</div>                                
                                @endif
                            @endif

                            

                        </div><!-- panel-body -->
                        @if( auth()->user()->hasRole('client') )
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save" id="save" style="display:none">Save</button>
                        </div>
                        @endif
                    </form>
                        @if( auth()->user()->hasRole('student') )
                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save" id="save" onClick="orderPackage()" style="display:none">{{ $isCh ? "订购课程":"Order" }}</button>
                        </div>

                        @endif

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script src="{{ asset('vendor/bootbox/bootbox.min.js') }}"></script>

    <script>
        var params = {}
        var $image

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });

        $(document).ready(function(){
            $("#school_package_id").change(function(){
                var id = $(this).val();
                $.ajax({
                  url: "{{ url('api/school_package/read') }}/"+id,
                  method: "GET",
                }).done(function(data){
                    $("#div-voucher").show();
                    $("#save").show();

                    $("#base_price").val(data.base_price);
                    $("#additional_price").val(data.additional_price);
                    $("#currency").val(data.currency);
                    $("#price_label").html("$"+data.price+".00 "+data.currency);
                    $("#duration_label").html(data.duration_in_days+" days");
                    $("#number_of_classes_label").html(data.number_of_classes);
                    $("#remaining_reservation").val(data.number_of_classes);
                    $("#total_price").val(data.price);
                    $("#original_total_price").val(data.price);
                    $("#name").val(data.name);
                    $("#duration").val(data.duration_in_days);
                    var number = "{!! $school->prefix.auth()->user()->id !!}-"+ (Math.floor(Math.random()*90000) + 10000);
                    $("#order_number").val(number);

                    //console.log(data);
                    //console.log( $("#currency").val() );
                });

            });
        });

        function orderPackage() {

            var info = {
                order_number: $("#order_number").val(),
                name: $("#name").val(),
                status: $("#status").val(),
                remaining_reservation: $("#remaining_reservation").val(),
                effective_until: $("#effective_until").val(),
                currency: $("#currency").val(),
                total_price: $("#total_price").val(),
                duration: $("#duration").val(),
                student_id: "{{ auth()->user()->id }}",
                school_package_id: $("#school_package_id").val(),
                converted_from: $("#converted_from").val(),
                transaction_id: $("#transaction_id").val(),
                conversion_rate: $("#conversion_rate").val(),
                additional_price: $("#additional_price").val(),
                base_price: $("#base_price").val(),
                discount_price: $("#discount_price").val(),
                voucher_id:$("#voucher_id").val()
            }
            console.log(info);

            
            var url = "{{ url('orders') }}";

            $.ajax({
              type: "POST",
              contentType: "application/json",
              data: JSON.stringify(info),
              url: url
            }).done(function(data) {
                    // window.location.replace("{{ url('orders') }}");
            });
            
        }


        $(document).ready(function() {
            @if( auth()->user()->hasRole('client') )
            $("#voucher_id").change(function(){
                var id = $(this).val();
                if(id != "" && id!=null && id!=undefined){
                    var url = "{{ url('/api/vouchers/') }}/"+id;
                    //console.log(url);
                    
                    $.ajax({
                      type: "GET",
                      url: url
                    }).done(function(data) {
                        //console.log(data);
                        var original_total_price = $("#original_total_price").val();
                        
                        var discount_price = 0.00;
                        if(data.percent == null || data.percent == undefined) {
                           discount_price = parseFloat(data.fixed_rate); 
                        }else{
                           discount_price = parseFloat(original_total_price) * (parseFloat(data.percent)/100);
                        }
                        $("#discount_price").val(discount_price);
                        $("#total_price").val(parseFloat(original_total_price) - parseFloat(discount_price));
                        $("#lbl-discount-price").html(discount_price);
                        $("#lbl-total-price").html($("#total_price").val());
                        $("#save").show();
                        //console.log($("#total_price").val());
                        
                    });                    
                }else{
                    $("#total_price").val($("#original_total_price").val());
                    $("#lbl-discount-price").html("0.00");
                    $("#lbl-total-price").html($("#original_total_price").val());
                    $("#save").hide();
                    //console.log($("#total_price").val());
                }
            });

            $("#compute").click(function(){
                var original_total_price = $("#original_total_price").val();
                var discount_price = $("#discount_price").val();

                $("#total_price").val(parseFloat(original_total_price) - parseFloat(discount_price));
                $("#lbl-discount-price").html(discount_price);
                $("#lbl-total-price").html($("#total_price").val());
                $("#save").show();
            });
            $('input:checkbox[name="use_voucher"]').change(function(){
                if ( $(this).is(':checked') ) {
                    $("#voucher_id").show();
                    $("#discount_price").hide();
                    $("#compute").hide();
                }else{
                    $("#voucher_id").hide();
                    $("#voucher_id").val("");
                    $("#discount_price").show();
                    
                    $("#total_price").val($("#original_total_price").val());
                    $("#lbl-discount-price").html("");
                    $("#lbl-total-price").html($("#original_total_price").val());
                    $("#discount_price").val("0");
                    $("#compute").show();
                }
                $("#save").hide();

            });

            @elseif( auth()->user()->hasRole('student') )
            $("#voucher_code").blur(function(){
                var code = $(this).val();
                if( (code!="") && (code!=null) && (code!=undefined) ) {
                    var url = "{{ url('/api/vouchercode/') }}/"+code;
                    //console.log(url);
                    
                    $.ajax({
                      type: "GET",
                      url: url
                    }).done(function(data) {
                        //console.log(data);
                        if(data != "" && data!= null && data != undefined) {
                            $("#voucher_id").val(data.id);
                            //console.log("Voucher ID: "+$("#voucher_id").val());
                            var original_total_price = $("#original_total_price").val();
                            //console.log(original_total_price);
                            var discount_price = 0.00;
                            if(data.percent == null || data.percent == undefined) {
                               discount_price = parseFloat(data.fixed_rate); 
                            }else{
                               discount_price = parseFloat(original_total_price) * (parseFloat(data.percent)/100);
                            }
                            $("#discount_price").val(discount_price);
                            //console.log("discount price: "+$("#discount_price").val() );
                            $("#total_price").val(parseFloat(original_total_price) - parseFloat(discount_price));
                            //console.log("total price: "+$("#total_price").val() );

                            //$("#lbl-discount-price").html(discount_price);
                            $("#price_label").html("$"+parseFloat($("#total_price").val())+" USD");
                        }
                        //console.log($("#total_price").val());
                    });                    
                } else {
                    $("#total_price").val($("#original_total_price").val());
                    $("#lbl-discount-price").html("0.00");
                    $("#price_label").html($("#original_total_price").val());
                    $("#save").hide();
                    //console.log($("#total_price").val());
                }
            });
            @endif

        });
    </script>
@stop
