@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{auth()->user()->lang=="zh-cn"?"已购学习包":"Ordered Packages"}}
        </h1>
        @can('add',app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{auth()->user()->lang=="zh-cn"?"立即订购课程":"Order Now"}}</span>
            </a>
        @endcan
        @can('delete',app($dataType->model_name))
            @include('voyager::partials.bulk-delete')
        @endcan
        @can('edit',app($dataType->model_name))
        @if(isset($dataType->order_column) && isset($dataType->order_display_column))
            <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary">
                <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
            </a>
        @endif
        @endcan

        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')

    @php

        $isAdmin = false;
        if( auth()->user()->hasRole('superadmin') || auth()->user()->hasRole('admin') ){
            $isAdmin = true;
        }

        $isClient = false;
        if( auth()->user()->hasRole('client') ){
            $isClient = true;
        }

        $isStudent = false;
        if( auth()->user()->hasRole('student') ){
            $isStudent = true;
            $dataTypeContent = App\Order::where("student_id", auth()->user()->id)->get();
        }

        $isCh = auth()->user()->lang=="zh-cn" ? "true":"false";

    @endphp
    <div class="page-content browse container-fluid">
        
        @include('voyager::alerts')
        <div class="row">
@if ($message = Session::get('success'))
<div class="w3-panel w3-green w3-display-container">
    <span onclick="this.parentElement.style.display='none'"
            class="w3-button w3-green w3-large w3-display-topright">&times;</span>
    <p>{!! $message !!}</p>
</div>
@php
    Session::forget('success');
@endphp
@endif

@if ($message = Session::get('error'))
<div class="w3-panel w3-red w3-display-container">
    <span onclick="this.parentElement.style.display='none'"
            class="w3-button w3-red w3-large w3-display-topright">&times;</span>
    <p>{!! $message !!}</p>
</div>
@php
    Session::forget('error');
@endphp
@endif
            <div class="col-md-12">
                @if( auth()->user()->hasRole('student') )
                <div class="alert alert-warning" style="background-color: #fcf3c3; color: #8a6d3b">
                    <p>{{ (auth()->user()->lang=="zh-cn") ? "请确认付款，我们将尽快为您激活学习包。":"Please settle your payment so we can activate your package asap."}}</p>
                </div>
                @elseif( auth()->user()->hasRole('client') )
                <div class="alert alert-warning" style="background-color: #fcf3c3; color: #8a6d3b">
                    <p>{{ (auth()->user()->lang=="zh-cn") ? "请确认付款，我们将尽快为您激活学习包。":"You can add Discounts when you edit orders.."}}</p>
                </div>
                @endif
                <div class="panel panel-bordered">
                    <div class="panel-body">
@if($isClient == true)
<form method="POST" name="payment_form" id="payment_form" action="{!! URL::to('paypal') !!}">
{{ csrf_field() }}
<input type="hidden" name="payables" id="payables" value="">
<div class="col-md-12" style="margin-bottom:20px">
      <div class="pull-right"><a href="javascript:;" id="cart"><i class="fa fa-shopping-cart"></i> Cart <span class="badge">0</span></a></div>
</div>

<div class="row">
  <div class="shopping-cart">
    <div class="shopping-cart-header">
      <i class="fa fa-shopping-cart cart-icon"></i><span class="badge">0</span>
      <div class="shopping-cart-total">
        <span class="lighter-text">Total:</span>
        <span class="main-color-text">USD <span id="cart-total">0</span></span>
      </div>
    </div> <!--end shopping-cart-header -->
    <a href="javascript:;" onclick="formSubmit();" class="button">PAY</a>
    <ul class="shopping-cart-items" id="cart-contents"></ul>
  </div> <!--end shopping-cart -->
</div>
</form>
@endif
                       

                        @if ($isServerSide)
                            <form method="get" class="form-search">
                                <div id="search-input">
                                    <select id="search_key" name="key">
                                        @foreach($searchable as $key)
                                                <option value="{{ $key }}" @if($search->key == $key){{ 'selected' }}@endif>{{ ucwords(str_replace('_', ' ', $key)) }}</option>
                                        @endforeach
                                    </select>
                                    <select id="filter" name="filter">
                                        <option value="contains" @if($search->filter == "contains"){{ 'selected' }}@endif>contains</option>
                                        <option value="equals" @if($search->filter == "equals"){{ 'selected' }}@endif>=</option>
                                    </select>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="{{ __('voyager::generic.search') }}" name="s" value="{{ $search->value }}">
                                        <span class="input-group-btn">
                                            <button class="btn btn-info btn-lg" type="submit">
                                                <i class="voyager-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        @endif
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover table-striped table-bordered" style="text-align:center">
                                <thead style="text-align:center">
                                    <tr>
                                        @can('delete',app($dataType->model_name))
                                            <th class="table-layout">
                                                <input type="checkbox" class="select_all">
                                            </th>
                                        @endcan
                                        @foreach($dataType->browseRows as $row)

                                        @if( auth()->user()->hasRole('student') )
                                            @if($row->field=="order_belongsto_user_relationship")
                                            @elseif($row->field=="duration")
                                            @elseif($row->field=="order_number")
                                                <th class="table-layout">{{ $isCh ? "课程编号":"Order Number"}}</th>
                                            @elseif($row->field=="status")
                                                <th class="table-layout">{{ $isCh ? "状态":"Status"}}</th>
                                            @elseif($row->field=="effective_until")
                                                <th class="table-layout">{{ $isCh ? "有效期":"Valid Until"}}</th>
                                            @elseif($row->field=="remaining_reservation")
                                                <th class="table-layout">{{ $isCh ? "剩余课次":"Balance"}}</th>
                                            @elseif($row->field=="created_at")
                                                <th class="table-layout">{{ $isCh ? "创建日期":"Created On"}}</th>
                                            @elseif($row->field=="currency")
                                                <th class="table-layout">{{ $isCh ? "货币":"Currency"}}</th>
                                            @elseif($row->field=="total_price")
                                                <th class="table-layout">{{ $isCh ? "总价":"Total Price"}}</th>
                                            @elseif($row->field=="approved_at")
                                                <th class="table-layout">{{ $isCh ? "激活日期":"Approved On"}}</th>
                                            @elseif($row->field=="order_belongsto_school_package_relationship")                                                

                                                <th class="table-layout">
                                                    @if ($isServerSide)
                                                        <a href="{{ $row->sortByUrl() }}">
                                                    @endif
                                                    {{ $isCh ? "学习包":"Package"}}
                                                    @if ($isServerSide)
                                                        @if ($row->isCurrentSortField())
                                                            @if (!isset($_GET['sort_order']) || $_GET['sort_order'] == 'asc')
                                                                <i class="voyager-angle-up pull-right"></i>
                                                            @else
                                                                <i class="voyager-angle-down pull-right"></i>
                                                            @endif
                                                        @endif
                                                        </a>
                                                    @endif
                                                </th>
                                            @else
                                                <th class="table-layout">
                                                    @if ($isServerSide)
                                                        <a href="{{ $row->sortByUrl() }}">
                                                    @endif
                                                    {{ $row->display_name }}
                                                    @if ($isServerSide)
                                                        @if ($row->isCurrentSortField())
                                                            @if (!isset($_GET['sort_order']) || $_GET['sort_order'] == 'asc')
                                                                <i class="voyager-angle-up pull-right"></i>
                                                            @else
                                                                <i class="voyager-angle-down pull-right"></i>
                                                            @endif
                                                        @endif
                                                        </a>
                                                    @endif
                                                </th>

                                            @endif
                                        @else

                                        <th style="">
                                            @if ($isServerSide)
                                                <a href="{{ $row->sortByUrl() }}">
                                            @endif
                                            {{ $row->display_name }}
                                            @if ($isServerSide)
                                                @if ($row->isCurrentSortField())
                                                    @if (!isset($_GET['sort_order']) || $_GET['sort_order'] == 'asc')
                                                        <i class="voyager-angle-up pull-right"></i>
                                                    @else
                                                        <i class="voyager-angle-down pull-right"></i>
                                                    @endif
                                                @endif
                                                </a>
                                            @endif
                                        </th>
                                        @endif
                                        @endforeach

                                        @if(auth()->user()->hasRole('student'))
                                        
                                        @else
                                        <th class="actions">{{ __('voyager::generic.actions') }}</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($dataTypeContent as $data)
                                    <tr>
                                        @can('delete',app($dataType->model_name))
                                            <td>
                                                <input type="checkbox" name="row_id" id="checkbox_{{ $data->getKey() }}" value="{{ $data->getKey() }}">
                                            </td>
                                        @endcan
                                        @foreach($dataType->browseRows as $row)
                                            @if(auth()->user()->hasRole('student'))
                                                @if($row->field=="order_belongsto_user_relationship")
                                                @elseif($row->field=="duration")
                                                @else
                                            <td>

                                                <?php $options = json_decode($row->details); ?>
                                                @if($row->type == 'image')
                                                    <img src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif" style="width:100px">
                                                @elseif($row->type == 'relationship')
                                                    @include('voyager::formfields.relationship', ['view' => 'browse'])


                                                    
                                                @elseif($row->type == 'select_multiple')
                                                    @if(property_exists($options, 'relationship'))

                                                        @foreach($data->{$row->field} as $item)
                                                            @if($item->{$row->field . '_page_slug'})
                                                            <a href="{{ $item->{$row->field . '_page_slug'} }}">{{ $item->{$row->field} }}</a>@if(!$loop->last), @endif
                                                            @else
                                                            {{ $item->{$row->field} }}
                                                            @endif
                                                        @endforeach

                                                        {{-- $data->{$row->field}->implode($options->relationship->label, ', ') --}}
                                                    @elseif(property_exists($options, 'options'))
                                                        @foreach($data->{$row->field} as $item)
                                                         {{ $options->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                        @endforeach
                                                    @endif

                                                @elseif($row->type == 'select_dropdown' && property_exists($options, 'options'))

                                                    @if($data->{$row->field . '_page_slug'})
                                                        <a href="{{ $data->{$row->field . '_page_slug'} }}">{!! $options->options->{$data->{$row->field}} !!}</a>
                                                    @else
                                                        {!! $options->options->{$data->{$row->field}} !!}
                                                    @endif


                                                @elseif($row->type == 'select_dropdown' && $data->{$row->field . '_page_slug'})
                                                    <a href="{{ $data->{$row->field . '_page_slug'} }}">{{ $data->{$row->field} }}</a>
                                                @elseif($row->type == 'date' || $row->type == 'timestamp')
                                                {{ $options && property_exists($options, 'format') ? \Carbon\Carbon::parse($data->{$row->field})->formatLocalized($options->format) : $data->{$row->field} }}
                                                @elseif($row->type == 'checkbox')
                                                    @if($options && property_exists($options, 'on') && property_exists($options, 'off'))
                                                        @if($data->{$row->field})
                                                        <span class="label label-info">{{ $options->on }}</span>
                                                        @else
                                                        <span class="label label-primary">{{ $options->off }}</span>
                                                        @endif
                                                    @else
                                                    {{ $data->{$row->field} }}
                                                    @endif
                                                @elseif($row->type == 'color')
                                                    <span class="badge badge-lg" style="background-color: {{ $data->{$row->field} }}">{{ $data->{$row->field} }}</span>
                                                @elseif($row->type == 'text')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div class="readmore">{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                @elseif($row->type == 'text_area')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div class="readmore">{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                @elseif($row->type == 'file' && !empty($data->{$row->field}) )
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    @if(json_decode($data->{$row->field}))
                                                        @foreach(json_decode($data->{$row->field}) as $file)
                                                            <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}" target="_blank">
                                                                {{ $file->original_name ?: '' }}
                                                            </a>
                                                            <br/>
                                                        @endforeach
                                                    @else
                                                        <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($data->{$row->field}) }}" target="_blank">
                                                            Download
                                                        </a>
                                                    @endif
                                                @elseif($row->type == 'rich_text_box')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div class="readmore">{{ mb_strlen( strip_tags($data->{$row->field}, '<b><i><u>') ) > 200 ? mb_substr(strip_tags($data->{$row->field}, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($data->{$row->field}, '<b><i><u>') }}</div>
                                                @elseif($row->type == 'coordinates')
                                                    @include('voyager::partials.coordinates-static-image')
                                                @elseif($row->type == 'multiple_images')
                                                    @php $images = json_decode($data->{$row->field}); @endphp
                                                    @if($images)
                                                        @php $images = array_slice($images, 0, 3); @endphp
                                                        @foreach($images as $image)
                                                            <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif" style="width:50px">
                                                        @endforeach
                                                    @endif
                                                @else
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <span>{{ $data->{$row->field} }}</span>
                                                @endif
                                            </td>
                                                @endif
                                            @else
                                            <td>

                                                <?php $options = json_decode($row->details); ?>
                                                @if($row->type == 'image')
                                                    <img src="@if( !filter_var($data->{$row->field}, FILTER_VALIDATE_URL)){{ Voyager::image( $data->{$row->field} ) }}@else{{ $data->{$row->field} }}@endif" style="width:100px">
                                                @elseif($row->type == 'relationship')
                                                    @include('voyager::formfields.relationship', ['view' => 'browse'])
                                                    @if( $row->field == "order_belongsto_user_relationship" )
                                                        {{ $data->{$row->field} }}
                                                        {{ $data->nick }}
                                                    @endif
                                                @elseif($row->type == 'select_multiple')
                                                    @if(property_exists($options, 'relationship'))

                                                        @foreach($data->{$row->field} as $item)
                                                            @if($item->{$row->field . '_page_slug'})
                                                            <a href="{{ $item->{$row->field . '_page_slug'} }}">{{ $item->{$row->field} }}</a>@if(!$loop->last), @endif
                                                            @else
                                                            {{ $item->{$row->field} }}
                                                            @endif
                                                        @endforeach

                                                        {{-- $data->{$row->field}->implode($options->relationship->label, ', ') --}}
                                                    @elseif(property_exists($options, 'options'))
                                                        @foreach($data->{$row->field} as $item)
                                                         {{ $options->options->{$item} . (!$loop->last ? ', ' : '') }}
                                                        @endforeach
                                                    @endif

                                                @elseif($row->type == 'select_dropdown' && property_exists($options, 'options'))

                                                    @if($data->{$row->field . '_page_slug'})
                                                        <a href="{{ $data->{$row->field . '_page_slug'} }}">{!! $options->options->{$data->{$row->field}} !!}</a>
                                                    @else
                                                        {!! $options->options->{$data->{$row->field}} !!}
                                                    @endif


                                                @elseif($row->type == 'select_dropdown' && $data->{$row->field . '_page_slug'})
                                                    <a href="{{ $data->{$row->field . '_page_slug'} }}">{{ $data->{$row->field} }}</a>
                                                @elseif($row->type == 'date' || $row->type == 'timestamp')
                                                {{ $options && property_exists($options, 'format') ? \Carbon\Carbon::parse($data->{$row->field})->formatLocalized($options->format) : $data->{$row->field} }}
                                                @elseif($row->type == 'checkbox')
                                                    @if($options && property_exists($options, 'on') && property_exists($options, 'off'))
                                                        @if($data->{$row->field})
                                                        <span class="label label-info">{{ $options->on }}</span>
                                                        @else
                                                        <span class="label label-primary">{{ $options->off }}</span>
                                                        @endif
                                                    @else
                                                    {{ $data->{$row->field} }}
                                                    @endif
                                                @elseif($row->type == 'color')
                                                    <span class="badge badge-lg" style="background-color: {{ $data->{$row->field} }}">{{ $data->{$row->field} }}</span>
                                                @elseif($row->type == 'text')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div class="readmore">{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                @elseif($row->type == 'text_area')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div class="readmore">{{ mb_strlen( $data->{$row->field} ) > 200 ? mb_substr($data->{$row->field}, 0, 200) . ' ...' : $data->{$row->field} }}</div>
                                                @elseif($row->type == 'file' && !empty($data->{$row->field}) )
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    @if(json_decode($data->{$row->field}))
                                                        @foreach(json_decode($data->{$row->field}) as $file)
                                                            <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}" target="_blank">
                                                                {{ $file->original_name ?: '' }}
                                                            </a>
                                                            <br/>
                                                        @endforeach
                                                    @else
                                                        <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($data->{$row->field}) }}" target="_blank">
                                                            Download
                                                        </a>
                                                    @endif
                                                @elseif($row->type == 'rich_text_box')
                                                    @include('voyager::multilingual.input-hidden-bread-browse')
                                                    <div class="readmore">{{ mb_strlen( strip_tags($data->{$row->field}, '<b><i><u>') ) > 200 ? mb_substr(strip_tags($data->{$row->field}, '<b><i><u>'), 0, 200) . ' ...' : strip_tags($data->{$row->field}, '<b><i><u>') }}</div>
                                                @elseif($row->type == 'coordinates')
                                                    @include('voyager::partials.coordinates-static-image')
                                                @elseif($row->type == 'multiple_images')
                                                    @php $images = json_decode($data->{$row->field}); @endphp
                                                    @if($images)
                                                        @php $images = array_slice($images, 0, 3); @endphp
                                                        @foreach($images as $image)
                                                            <img src="@if( !filter_var($image, FILTER_VALIDATE_URL)){{ Voyager::image( $image ) }}@else{{ $image }}@endif" style="width:50px">
                                                        @endforeach
                                                    @endif
                                                @else
                                                    @include('voyager::multilingual.input-hidden-bread-browse')

                                                    <span>{{ $data->{$row->field} }}</span>
                                                @endif
                                            </td>

                                            @endif

                                        @endforeach
                                        @if(auth()->user()->hasRole('student'))
                                        @else
                                        <td class="no-sort no-click" id="bread-actions">
                                            @php
                                                $payment_status = $data->status;
                                            @endphp
                                            @if($isAdmin == true && $payment_status == "paid")
                                            <a class="btn btn-sm btn-primary pull-right approve" onclick="approve('{{ $data->id }}')" title="approve">
                                                <i class="voyager-edit"></i> <span class="hidden-xs hidden-sm">Approve</span>
                                            </a>
                                            @elseif($isClient == true && $payment_status == "pending")
                                            <a class="btn btn-sm btn-success pull-right approve" onclick="addToPayment('{{ $data->order_number }}','{{ $data->name }}','{{ $data->base_price }}','{{ $data->school_package_id }}')" title="approve" id="btn-pay-{{ $data->order_number }}">
                                                <i class="voyager-credit-card"></i> <span class="hidden-xs hidden-sm">Add To Payment</span>
                                            </a>
                                            @endif
                                            @foreach(Voyager::actions() as $action)
                                                @include('voyager::bread.partials.actions', ['action' => $action])
                                            @endforeach
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @if ($isServerSide)
                            <div class="pull-left">
                                <div role="status" class="show-res" aria-live="polite">{{ trans_choice(
                                    'voyager::generic.showing_entries', $dataTypeContent->total(), [
                                        'from' => $dataTypeContent->firstItem(),
                                        'to' => $dataTypeContent->lastItem(),
                                        'all' => $dataTypeContent->total()
                                    ]) }}</div>
                            </div>
                            <div class="pull-right">
                                {{ $dataTypeContent->appends([
                                    's' => $search->value,
                                    'filter' => $search->filter,
                                    'key' => $search->key,
                                    'order_by' => $orderBy,
                                    'sort_order' => $sortOrder
                                ])->links() }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
@if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
<link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
@endif
<style type="text/css">
@import url(https://fonts.googleapis.com/css?family=Lato:300,400,700);
@import url(https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css);
*, *:before, *:after {
  box-sizing: border-box;
}

body {
  font: 14px/22px "Lato", Arial, sans-serif;
  background: #6394F8;
}

.lighter-text {
  color: #ABB0BE;
}

.main-color-text {
  color: #6394F8;
}

.cart-nav {
  padding: 20px 0 40px 0;
  background: #F8F8F8;
  font-size: 16px;
}
.cart-nav .navbar-left {
  float: left;
}
.cart-nav .navbar-right {
  float: right;
}
.cart-nav ul li {
  display: inline;
  padding-left: 20px;
}
.cart-nav ul li a {
  color: #777777;
  text-decoration: none;
}
.cart-nav ul li a:hover {
  color: black;
}

.container {
  margin: auto;
  width: 80%;
}

.badge {
  background-color: #6394F8;
  border-radius: 10px;
  color: white;
  display: inline-block;
  font-size: 12px;
  line-height: 1;
  padding: 3px 7px;
  text-align: center;
  vertical-align: middle;
  white-space: nowrap;
}

.shopping-cart {
  margin: 20px 0;
  float: right;
  background: white;
  width: 320px;
  border-radius: 3px;
  padding: 20px;
  display:none;
  position:absolute;
  z-index:9999;
  right:20px;
  top:50px
}
.shopping-cart .shopping-cart-header {
  border-bottom: 1px solid #E8E8E8;
  padding-bottom: 15px;
}
.shopping-cart .shopping-cart-header .shopping-cart-total {
  float: right;
}
.shopping-cart .shopping-cart-items {
  padding-top: 20px;
}
.shopping-cart .shopping-cart-items li {
  margin-bottom: 18px;
}
.shopping-cart .shopping-cart-items img {
  float: left;
  margin-right: 12px;
}
.shopping-cart .shopping-cart-items .item-name {
  display: block;
  padding-top: 10px;
  font-size: 16px;
}
.shopping-cart .shopping-cart-items .item-price {
  color: #6394F8;
  margin-right: 8px;
}
.shopping-cart .shopping-cart-items .item-quantity {
  color: #ABB0BE;
}

.shopping-cart:after {
  bottom: 100%;
  left: 89%;
  border: solid transparent;
  content: " ";
  height: 0;
  width: 0;
  position: absolute;
  pointer-events: none;
  border-bottom-color: white;
  border-width: 8px;
  margin-left: -8px;
}

.cart-icon {
  color: #515783;
  font-size: 24px;
  margin-right: 7px;
  float: left;
}

.button {
  background: #6394F8;
  color: white;
  text-align: center;
  padding: 12px;
  text-decoration: none;
  display: block;
  border-radius: 3px;
  font-size: 16px;
}

.button:hover {
  background: #729ef9;
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}

.table-layout {
    text-align:center;
    vertical-align:top !important;
    min-width: 80px;
}


</style>
@stop

@section('javascript')
    <script src="{{ asset('vendor/bootbox/bootbox.min.js') }}"></script>
    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    <script>
var to_pay = [];
var total = 0.000;
var index = -1;
        (function(){
 
          $("#cart").on("click", function() {
            $(".shopping-cart").fadeToggle( "fast");
          });

          $("#payment_form").submit(function(e){
            //alert("trigger");
            var payable = {
                total: total,
                items: to_pay
            }
            if(to_pay.length > 0){
                $("#payables").val(JSON.stringify(payable));
            }
            else {
                e.preventDefault();
                bootbox.alert("Nothing to pay..");
            }
          });
          
        })();

        $(document).ready(function () {

            @if (!$dataType->server_side)
                var table = $('#dataTable').DataTable({!! json_encode(
                    array_merge([
                        "order" => [],
                        "language" => __('voyager::datatable'),
                        "columnDefs" => [['searchable' =>  false, 'targets' => -1 ]],
                    ],
                    config('voyager.dashboard.data_tables', []))
                , true) !!});
            @else
                $('#search-input select').select2({
                    minimumResultsForSearch: Infinity
                });
            @endif

            @if ($isModelTranslatable)
                $('.side-body').multilingual();
                //Reinitialise the multilingual features when they change tab
                $('#dataTable').on('draw.dt', function(){
                    $('.side-body').data('multilingual').init();
                })
            @endif
            $('.select_all').on('click', function(e) {
                $('input[name="row_id"]').prop('checked', $(this).prop('checked'));
            });
        });


        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

        $("#dataTable th").addClass("table-layout");

function approve(id) {
    var url = "{{ url('/api/order/approve') }}";
    //console.log(url);
    var info = {
        id: id
    }
    
    $.ajax({
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(info),
      url: url
    }).done(function(data){
        //console.log(data);
        bootbox.alert(data,function(){
            window.location = "{{ url('/orders') }}";
        });
        
    });

}



function addToPayment(id, name, price, package_id) {
    var data = {
        id:id,
        name:name,
        price:price,
        package_id: package_id
    }
    //console.log(price);
    to_pay.push(data);
    index++;
    total += parseFloat(price);
    var cartChild = document.createElement("li");
    cartChild.setAttribute("id", "cart-items-"+id);
    cartChild.setAttribute("data-index", index);
    cartChild.innerHTML = "<span id='item-"+id+"' class='item-name'>"+id+"</span>"+
        "<span id='item-price-"+id+"' class='item-price'>"+price+"</span>"+
        "<span id='item-name-"+id+"' class='item-quantity'>"+name+"</span>"+
        "<div><a href='#' class='item-remove' onclick=\"removeToCart('"+id+"')\">remove</a></div>";
    $("#cart-contents").append(cartChild);
    $("#btn-pay-"+id).hide();
    $("#cart-total").html(total);
    $(".badge").html(to_pay.length);
    //$().val(JSON.stringify());
    //console.log(to_pay);
    //console.log("Total: "+total);
}

function removeToCart(id){
    var item = $("#cart-items-"+id);
    var index2 = item.dataset.index;
    var price = to_pay[index2].price;
    total -= price;
    if (index2 > -1) {
      to_pay.splice(index2, 1);
    }
    index--;
    item.remove(item);
    $("#btn-pay-"+id).show();
    $("#cart-total").html(total);
    $(".badge").html(to_pay.length);
}

function formSubmit(){
    $("#payment_form").submit();
}

    
    </script>

@stop
