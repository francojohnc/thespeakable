@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
    <div class="container-fluid">

        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i>  {{ (auth()->user()->lang == "zh-cn")? "课程表":"Class Schedule" }}
        </h1>
        @can('add',app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ (auth()->user()->lang=="zh-cn") ? "预约课程":"Book a class"}}</span>
            </a>
        @endcan
        @can('delete',app($dataType->model_name))
            @include('voyager::partials.bulk-delete')
        @endcan
        @can('edit',app($dataType->model_name))
        @if(isset($dataType->order_column) && isset($dataType->order_display_column))
            <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary">
                <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
            </a>
        @endif
        @endcan
        @include('voyager::multilingual.language-selector')
    </div>
@stop

@section('content')
    @php
        $isAdmin = false;
        $isTeacher = false;
        $isStudent = false;
        if( auth()->user()->hasRole('superadmin') || auth()->user()->hasRole('admin') ){
            $isAdmin = true;
        }
        if( auth()->user()->hasRole('teacher')){
            $isTeacher = true;
        }
        if( auth()->user()->hasRole('student')){
            $isStudent = true;
        }

        $timezone = auth()->user()->timezone;
        $now = new \DateTime(null, new \DateTimeZone($timezone));


        $lbl_upload_book = (auth()->user()->lang=="zh-cn") ? "请上传pdf格式的教材文件 (40 MB 以下)" : "Upload book in pdf format (40MB or less)";
        $lbl_message_to_teacher = (auth()->user()->lang=="zh-cn") ? "向外教留言" : "Message to Teacher";
        $lbl_teacher_absent = (auth()->user()->lang=="zh-cn") ? "外教缺席" : "Teacher Absent";
        $lbl_teacher_reason = (auth()->user()->lang=="zh-cn") ? "外教缺席原因" : "Teacher's Reason For Absence:";
        $lbl_provide_link = (auth()->user()->lang=="zh-cn") ? "请您保存并提供截屏" : "Please provide a screenshot link as proof.:";

        

    @endphp

    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div id="calendar"></div>
                            <div style="color:green">{{ (auth()->user()->lang == "zh-cn") ? "绿色 = 已完成课程" : "Green = Completed" }}</div>
                            <div style="color:blue">{{ (auth()->user()->lang == "zh-cn") ? "蓝色 = 已预约课程" : "Blue = Booked" }}</div>
                            <div style="color:red">{{ (auth()->user()->lang == "zh-cn") ? "红色 = 外教缺席或学生缺席" : "Red = Teacher is Absent, or Student is Absent" }}</div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
@if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
<link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">

@endif
<link rel="stylesheet" href="{{ asset('vendor/datetimepicker/jquery.datetimepicker.css') }}"/>
<link rel="stylesheet" href="{{ asset('vendor/fullcalendar/fullcalendar.min.css') }}">
@stop

@section('javascript')
    <script src="{{ asset('vendor/fullcalendar/lib/moment.min.js') }}"></script>
    <script src="{{ asset('vendor/bootbox/bootbox.min.js') }}"></script>
    <script src="{{ asset('vendor/fullcalendar/lib/moment-with-data.min.js') }}"></script>
    <script src="{{ asset('vendor/fullcalendar/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('vendor/php-date-formatter/js/php-date-formatter.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('vendor/datetimepicker/jquery.datetimepicker.full.min.js') }}"></script>
    @if(auth()->user()->lang == "zh-cn")
        <script src="{{ asset('vendor/fullcalendar/locale/zh-cn.js') }}"></script>
    @endif
    <script type="text/javascript">
        var calendarEvent;
        //console.log("{{ url('api/book_class/browse/student') }}/{{ auth()->user()->id }}");
        init();
        function mobilecheck(){
          var check = false;
          (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
          return check;

        }
window.mobilecheck = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
}
        function cancelBooking(id)
        {
            bootbox.confirm("Are you sure you want to cancel this booking? ", function(ans) {
                if(ans == true){
                    var url = "{{ url('/api/booking/cancel/') }}/"+id;

                    $.ajax({
                      type: "GET",
                      url: url
                    }).done(function(data){
                        //console.log(data);
                        bootbox.alert(data, function(){
                            init();
                        });
                    });
                }
            });
        }
        
        function init() {
            $("#calendar").fullCalendar("destroy");
            var calendar = $('#calendar').fullCalendar({
            header: {
                left: 'prev,next,today',
                center: 'title',
                right: 'agendaWeek,agendaCutoff,basicDay'
            },
            //eventColor: '#378006',
            columnHeaderFormat: "ddd M/D",
            slotDuration: '00:30:00',
            slotLabelInterval: 30,
            slotLabelFormat: 'h(:mm)a',
            contentHeight: 'auto',
            timeFormat: 'H:mm',
            eventLongPressDelay: 1000,
            slotLabelFormat: 'H:mm',
            selectConstraint: "businessHours",
            defaultView: mobilecheck() ? "basicDay" : "agendaWeek",
            eventLongPressDelay: 1000,
            defaultTimedEventDuration: '0:25',
            eventStartEditable: false,
            allDaySlot: false,
            scrollTime: '00:00',
            lang: /^en-/.test(navigator.language) ? 'en' : 'zh-cn',
            timezone: '{{ $timezone }}',
            editable: true,
            selectable: false,
            selectHelper: false,
            disableDragging: true,
            disableResizing: true,
            businessHours: {
              dow: [ 0, 1, 2, 3, 4, 5, 6 ],
              start: '6:00',
              end: '24:00',
            },
            views: {
                agendaCutoff: {
                  type: 'agenda',
                  duration: { days: 16 },
                  buttonText: '15 days'
                }
            },
            viewRender: function(){
                if (GetIEVersion() == 0) {
                    var container = $('.fc thead');

                    $(".cloned-head").remove();

                    var cloneHead = $('<tr class="fc-first fc-last cloned-head"></tr>').css({
                        zIndex: 5,
                        display: 'none'
                    }).appendTo(container);

                    $('#calendar thead th').each(function() {
                        var cloned = $(this).clone();
                        cloned.css('width', $(this).outerWidth());
                        cloned.appendTo(cloneHead);
                    });

                    $(document).on('scroll', function() {
                        var scroll = $(document).scrollTop();
                        var show = (container.offset().top) + cloneHead.outerHeight() < scroll;
                        if (show) {
                            cloneHead.show().css({
                              position: 'absolute',
                              backgroundColor: '#cbcdda',
                              top: scroll - container.offset().top +65
                            });
                        } else {
                            cloneHead.hide();
                        }
                    });
                }



            },
            eventSources: [{
                @if($isStudent)
                url: "{{ url('api/book_class/browse/student') }}/{{ auth()->user()->id }}",
                @else
                url: "{{ url('api/book_class/browse/teacher') }}/{{ auth()->user()->id }}",
                @endif
            }
            @if($isTeacher)
            ,{
                    url: "{{ url('api/availabilities/list') }}/{{ auth()->user()->id }}",
            }
            @endif
            ],
            eventOverlap: function(stillEvent, movingEvent) {
                return false;//stillEvent.allDay && movingEvent.allDay;
            },
            //select: function(start, end) {
            //},
            eventDurationEditable: false,
            
            eventClick: function(calEvent, jsEvent, view) {
                calendarEvent = calEvent;
                // get data from server
                @if($isStudent)
                    var url = "{{ url('/api/book_class/show') }}/"+calEvent.id;
                    //console.log(url);
                    $.ajax({
                      type: "GET",
                      url: url
                    }).done(function(data) {
                        //console.log(data);
                        //console.log(data.teacher_opened_memo);
                        var data = JSON.parse(data);
                        if(data.status == "booked") {

                            // get current date time

                            var now = moment().tz("{{ $timezone }}");

                                if(data.book_url != null){
                                    var ebook = JSON.parse(data.book_url);
                                }
                                //console.log(ebook[0].download_link);
                                var action = "{{ url('book-classes') }}/"+calEvent.id;
                                var sts = data.status;
                                if(data.teacher_absent == "yes") {
                                    sts = "penalty 3";
                                }

                                var view = '<form role="form" class="form-edit-add" action="'+action+'" name="form-edit-add" id="form-edit-add" method="POST" enctype="multipart/form-data">'+
                                    '{{ method_field("PUT") }}'+
                                    '{{ csrf_field() }}'+
                                    '<div class="panel-body">'+
                                    '<div class="form-group"><b>Previous Book:</b> '+data.previous_book+'</div>'+

                                        '<input name="status" id="status" value="'+sts+'" type="hidden">';

                                        // you can't upload book 30 minutes before class time
                                        var min30upload = moment(data.start_at).tz("{{ $timezone }}").subtract(30,"minutes");

                                        if(now.unix() < min30upload.unix()){
                                            view += '<div class="form-group  col-md-12">'+
                                                '<label for="name">{{$lbl_upload_book}}</label>'+
                                                '<input name="book_url" multiple="multiple" type="file">';

                                                if(data.book_url != null) {
                                                    view += '<a href="/storage/'+ebook[0].download_link+'">'+ebook[0].original_name+'</a>'
                                                }
                                            view += '</div>'+
                                            '<div class="form-group col-md-12">'+
                                                '<label for="name">{{$lbl_message_to_teacher}}</label>'+
                                                '<textarea class="form-control" name="message_to_teacher" rows="5">'+removeNull(data.message_to_teacher)+'</textarea>'+
                                            '</div>';
                                        }
 
                                        // you can't upload book 30 minutes before class time
                                        var min5teacher_absent = moment(data.start_at).tz("{{ $timezone }}").add(5,"minutes");

                                        if(now.unix() > min5teacher_absent.unix() ){
                                            if(data.teacher_opened_memo == "false" ) {

                                                view += '<label for="name">{{$lbl_teacher_absent}}</label>'+
                                                '<ul class="radio">'+
                                                '<li>';

                                                var checked ="";
                                                //console.log(data.teacher_absent);
                                                if(data.teacher_absent == "no"){
                                                    checked = "checked";
                                                }
                                                view += '<input id="option-teacher-absent-no" name="teacher_absent" value="no" '+checked+' type="radio" onChange="toggleTeacher(this.value,\''+data.start_at+'\')">'+
                                                '<label for="option-teacher-absent-no">{{auth()->user()->lang=="zh-cn" ? "否":"No"}}</label>'+
                                                '<div class="check"></div>'+
                                                '</li>'+
                                                '<li>';
                                                checked="";
                                                var display = "display:none";
                                                if(data.teacher_absent == "yes"){
                                                    checked="checked";
                                                    display = "display:block"
                                                }
                                                view += '<input id="option-teacher-absent-yes" name="teacher_absent" '+checked+' value="yes" type="radio" onChange="toggleTeacher(this.value,\''+data.start_at+'\')">'+
                                                '<label for="option-teacher-absent-yes">{{auth()->user()->lang=="zh-cn" ? "是":"yes"}}</label>'+
                                                '<div class="check"></div>'+
                                                '</li>'+
                                                '</ul>'+

                                                '<div style="'+display+'" id="teacher_absent"><div class="form-group col-md-12">'+
                                                    '<label for="name">{{$lbl_teacher_reason}}</label>'+
                                                    '<input class="form-control" name="teacher_reason_for_absence" id="teacher_reason_for_absence" placeholder="{{$lbl_teacher_reason}}" value="" type="text">'+
                                                '</div>'+
                                                '<div class="form-group col-md-12">'+
                                                    '<label for="name">{{$lbl_provide_link}}</label>'+
                                                    '<textarea class="form-control" name="teacher_reason_screenshot"></textarea>'+
                                                '</div></div>';
                                            }
                                        }

                                    view += '</div>'+
                                    '</form>';
                        }//end if
                        else {

                            view = '<div class="panel panel-bordered">';
                            for(var key in data){
                                //console.log(key+" "+data);
                                if( key == "start_at" || key == "end_at" ||key == "grammar" || key == "pronunciation" ||key == "areas_for_improvement" ||key == "tips_and_suggestion_for_student" || key == "class_remarks" || key == "book_name" ||key == "lesson_unit" || key == "page_number" ||key == "teacher_absent" || key =="status") {
                                    var field = "";
                                    if(key == "start_at"){field = "Starts at";}
                                    else if(key == "end_at"){field = "Ends at";}
                                    else if(key == "status"){field = "Status";}
                                    else if(key == "grammar"){field = "Grammar";}
                                    else if(key == "pronunciation"){field = "Pronunciation";}
                                    else if(key == "areas_for_improvement"){field = "Areas for improvement";}
                                    else if(key == "tips_and_suggestion_for_student"){field = "Tips and suggestions";}
                                    else if(key == "class_remarks"){field = "Class remarks";}
                                    else if(key == "book_name"){field = "Book Title";}
                                    else if(key == "lesson_unit"){field = "Lesson or Unit";}
                                    else if(key == "page_number"){field = "Page Number";}

                                    else if(key == "teacher_absent"){field = "Teacher absent?";}

                                    view += '<div class="panel-heading" style="border-bottom:0;">'+
                                        '<h3 class="panel-title">'+field+'</h3>'+
                                        '</div>'+

                                        '<div class="panel-body" style="padding-top:0;">'+
                                        '<p>'+removeNull(data[key])+'</p>'+
                                        '</div>'+
                                        '<hr style="margin:0;">';

                                }
                                
                            } 
                            view += "</div>";
                        }//end else

                        var buttons = "";
                        if(data.status == "booked"){
                            buttons = {
                                        saveData: {
                                        label: "{{ (auth()->user()->lang=='zh-cn')? '保存':'Save'}}",
                                        className: 'btn-info bootbox-save',
                                        callback: function() {
                                                bootbox.alert("{{ (auth()->user()->lang == 'zh-cn') ? '正在为您更新课程信息，请稍候':'Updating your class details, please wait.' }}");
                                                $("#form-edit-add").submit();
                                            }
                                        },
                                        cancelBooking: {
                                        label: "{{ auth()->user()->lang=='zh-cn' ? '取消课程': 'Cancel Class'}}",
                                        className: 'btn-warning',
                                        callback: function(){
                                                var now = moment().tz("{{ $timezone }}");
                                                var start = moment(data.start_at).tz("{{ $timezone }}").subtract(1,"hours");
                                                //console.log(now +" < "+ start);
                                                if(now < start) {
                                                    cancelBooking(calEvent.id);
                                                } else {
                                                    bootbox.alert("You can't cancel this class anymore..");
                                                }
                                            }
                                        },
                                        ok: {
                                            label: "{{ (auth()->user()->lang=='zh-cn') ? '关闭':'Close'}}",
                                            className: 'btn-danger'
                                        }

                                    }
                            $(".bootbox-save").attr('id', 'btn-save');
                        }else{
                            buttons = {
                                        ok: {
                                            label: "{{ (auth()->user()->lang=='zh-cn') ? '关闭':'Close'}}",
                                            className: 'btn-danger'
                                        }

                                    }
                        }

                        var medium = "";
                        if(data.status == "booked"){
                            medium = (data.teacher_qq != undefined) ? "<br/> QQ Number: "+data.teacher_qq : "<br/> WeChat ID: "+data.teacher_wechat;
                        }


                        var dialog = bootbox.dialog({
                            title: 'Class ID: ' + moment(data.start_at).format("DD-MMM-YY-HH:mm") +"-"+moment(data.end_at).format("HH:mm") + "-"+data.id +"<br/>Teacher "+ data.teacher_nick + "<br/>Teacher ID: "+ data.teacher_id + " " + medium,
                            message: view,
                            size: 'large',
                            buttons: buttons
                        });

                        //bootbox.alert(data);
                    });
                    //console.log(calEvent);
                

                @else
                    if(calEvent.status != "open"){
                    //console.log(calEvent.start.timezone);
                    var cdt = moment().tz("{{ auth()->user()->timezone }}");
                    var csa = calEvent.start.utcOffset(cdt.format("Z"),true).add(1,"minutes"); // the calevent start to be +8

                    if(csa <= cdt) {
                        //console.log("true");
                        //send data to server that teacher_opened_memo
                        var url = "{{ url('/api/book_class/teacher/opened/memo') }}/"+calEvent.id;
                        //console.log(url);
                        $.ajax({
                          type: "GET",
                          url: url
                        }).done(function(data){
                            //console.log(data);
                        });
                    }

                    // TEACHER DATA
                    var url = "{{ url('/api/book_class/show') }}/"+calEvent.id;
                    //console.log(url);
                    $.ajax({
                      type: "GET",
                      url: url
                    }).done(function(data) {
                        //console.log(data);
                        var data = JSON.parse(data);
                        if(data.book_url != null){
                            var ebook = JSON.parse(data.book_url);
                        }
                        if(data.status == "booked") {

                            //console.log(ebook[0].download_link);
                            var action = "{{ url('book-classes') }}/"+calEvent.id;
                            var sts = data.status;
                            if(data.student_absent == "yes"){
                                sts = "student is absent";
                            }

                            var view = '<form role="form" class="form-edit-add" action="'+action+'" name="form-edit-add" id="form-edit-add" method="POST" enctype="multipart/form-data">'+
                                '{{ method_field("PUT") }}'+
                                '{{ csrf_field() }}'+
                                '<div class="panel-body">'+
                                    '<div class="form-group"><b>Previous Book:</b> '+data.previous_book+'</div>'+
                                    '<input name="status" id="status" value="completed" type="hidden">';
                                    view+='<div class="form-group ">'+
                                        '<label for="book_url"><b>Book:</b> </label> ';
                                        if(data.book_url != null) {
                                            view += '<span><a href="/storage/'+ebook[0].download_link+'">'+ebook[0].original_name+'</a></span>';
                                        }
                                    view += '</div>'+
                                    '<div class="form-group">'+
                                        '<label for="name"><b>Message To Teacher:</b></label> '+
                                        '<span>'+removeNull(data.message_to_teacher)+'</span>'+
                                    '</div>';
                                    if(data.book_url != null){
                                        view += '<div class="form-group"><label for="book_name">Book Name</label><input class="form-control" id="book_name" name="book_name" placeholder="Book Name" value="'+ebook[0].original_name.replace(".pdf","") +'" type="text"></div>';
                                    }else{
                                        view += '<div class="form-group"><label for="book_name">Book Name</label><input class="form-control" id="book_name" name="book_name" placeholder="Book Name" value="" type="text"></div>';
                                    }
                                    view += '<div class="form-group"><label for="name"><b>Page Number </b></label><br/><small><i>(Put the page number where you stopped.)</i></small><input class="form-control" id="page_number" name="page_number" placeholder="Page Number" value="" type="text"></div>'+
                                    '<div class="form-group"><label for="speed_test_1"><b>Speed Test 1 </b></label><br/><small><i>(Must be within 5 minutes before the class time to be considered valid, please use the link of your speedtest result. Make sure to use Beijing server and the timezone should be UTC+8 Hongkong. The speedtest results will be used for complaints/issues relating to internet connection. Unable to provide a valid speedtest will automatically make the complaint valid. A valid speedtest result should have a download speed of at least 1.0 Mbps and upload speed of at least 0.5 Mbps.)</i></small><input class="form-control" id="speed_test_1" name="speed_test_1" placeholder="Speed Test 1" value="" type="text"></div>'+
                                    '<div class="form-group"><label for="pronunciation"><b>Pronunciation</b></label><br/><small><i>(Please input at least three corrections. For example: • rising /ˈraɪzɪŋ/ verb, • vegetarian /ˌvɛʤəˈterijən/ noun, • healthy /ˈhɛlθi/ adjective. These corrections will be sent to the students on a separate email. Please make sure that these corrections actually happened to avoid complaints from the students.)</i></small><textarea class="form-control" name="pronunciation" id="pronunciation" rows="5">• \n• \n• </textarea></div>'+
                                    '<div class="form-group"><label for="grammar"><b>Grammar</b></label><br/><small><i>(Please write at least 2 corrections. For example: You said: She`s mother is my good friend, too. Better say: Her mother is my good friend, too. These corrections will be sent to the students on a separate email. Please make sure that these corrections actually happened to avoid complaints from the students.)</i></small><textarea class="form-control" id="grammar" name="grammar" rows="10" placeholder="You said:\nBetter say:\n\nYou said:\nBetter say:\n\nYou said:\nBetter say:"></textarea></div>'+
                                    '<div class="form-group"><label for="areas_for_improvement"><b>Student\'s Areas For Improvement (AFI ) in this class:</b></label><select name="areas_for_improvement" id="areas_for_improvement" class="form-control"><option value="" selected></option><option value="Vocabulary">Vocabulary</option><option value="Grammar">Grammar</option><option value="Pronunciation">Pronunciation</option><option value="Listening">Listening</option><option value="Fluency">Fluency</option></select></div>'+
                                    '<div class="form-group"><label for="tips_and_suggestion_for_student"><b>Tips And Suggestion For The Student:</b></label><br/><small><i>(Please put suggested activities/exercises relevant to their AFIs. These tips and suggestions will also be sent to students.)</i></small><textarea class="form-control" id="tips_and_suggestion_for_student" name="tips_and_suggestion_for_student" rows="5"></textarea></div>'+
                                    '<div class="form-group"><label for="speed_test_2"><b>Speed Test 2</b></label><br/><small><i>(Must be during the class time or within 5 minutes after the class time to be considered valid, please use the link of your speedtest result. Make sure to use Beijing server and the timezone should be UTC+8 Hongkong. The speedtest results will be used for complaints/issues relating to internet connection. Unable to provide a valid speedtest will automatically make the complaint valid. A valid speedtest result should have a download speed of at least 1.0 Mbps and upload speed of at least 0.5 Mbps.)</i></small><input class="form-control" name="speed_test_2" id="speed_test_2" placeholder="Speed Test 2" value="" type="text"></div>'+
                                    '<div class="form-group"><label for="class_remarks"><b>Class Remarks</b></label><br/><small><i>(This part is for class documentation. If there\'s a problem or anything not normal in the middle of the class, make sure to put it here. Make sure to secure a screenshot with your computer time to serve as time stamp. Otherwise, just put “The class went well.”)</i></small><textarea class="form-control" id="class_remarks" name="class_remarks" rows="5"></textarea></div>'+
                                    '<div class="form-group"><label for="screenshot"><b>Screenshot</b></label><br/><small><i>(Please use Lightshot (<a href="https://app.prntscr.com/en/index.html" target="_blank">https://app.prntscr.com/en/index.html</a>). Please input the link of the screenshot. You are required to TAKE A SCREENSHOT AT THE START AND END OF EVERY CLASS. Always send screenshots IF THE STUDENT IS ABSENT OR LATE AND IF THE CALL WAS INTERRUPTED. These screenshots MUST SHOW THE WHOLE WINDOW OF YOUR COMPUTER with the correct COMPUTER DATE & TIME, TIMESTAMPS ON QQ, CALL DURATION, STUDENT\'S MESSAGE AND VIDEO CALL WITH YOUR STUDENT. Also, send screenshots of the lesson would there be any possible complaints from the students. Save a copy on your computer as well for future reference.)</i></small><textarea class="form-control" name="screenshot" id="screenshot"></textarea></div>'+
                                    '<label for="name">Student Absent</label>'+
                                    '<ul class="radio">'+
                                    '<li>';

                                    var checked ="";
                                    //console.log(data.student_absent);
                                    if(data.student_absent == "no"){
                                        checked = "checked";
                                    }
                                    view += '<input id="option-student-absent-no" name="student_absent" value="no" '+checked+' type="radio" onChange="toggleStudent(this.value)">'+
                                    '<label for="option-student-absent-no">No</label>'+
                                    '<div class="check"></div>'+
                                    '</li>'+
                                    '<li>';
                                    checked="";
                                    var display = "display:none";
                                    if(data.student_absent == "yes"){
                                        checked="checked";
                                        display = "display:block"
                                    }
                                    view += '<input id="option-student-absent-yes" name="student_absent" '+checked+' value="yes" type="radio" onChange="toggleStudent(this.value)">'+
                                    '<label for="option-student-absent-yes">Yes</label>'+
                                    '<div class="check"></div>'+
                                    '</li>'+
                                    '</ul>'+

                                    '<div style="'+display+'" id="student_absent"><div class="form-group col-md-12">'+
                                        '<label for="name">Student Reason For Absence:</label>'+
                                        '<input class="form-control" name="student_reason_for_absence" id="student_reason_for_absence" placeholder="Student Reason For Absence" value="" type="text">'+
                                    '</div>'+
                                    '<div class="form-group col-md-12">'+
                                        '<label for="name">Student Reason Screenshot:</label>'+
                                        '<textarea name="student_reason_screenshot" id="student_reason_screenshot" class="form-control"></textarea>'+
                                    '</div></div>';

                                view += '</div>';

                                view += '</form>';

                        }//end if
                        else {

                            view = '<div class="panel panel-bordered">';
                            for(var key in data) {
                                if(data.status == "completed" || data.status == "lesson memo delay") {

                                    if( key == "start_at" || key == "end_at" ||key == "grammar" || key == "pronunciation" ||key == "areas_for_improvement" ||key == "tips_and_suggestion_for_student" || key == "class_remarks" || key == "book_name" ||key == "lesson_unit" || key == "page_number" || key =="status" || key =="speed_test_1" || key == "speed_test_2" || key == "screenshot" || key == "book_url") {
                                        var field = "";
                                        if(key == "start_at"){field = "Starts at";}
                                        else if(key == "end_at"){field = "Ends at";}
                                        else if(key == "status"){field = "Status";}
                                        else if(key == "grammar"){field = "Grammar";}
                                        else if(key == "pronunciation"){field = "Pronunciation";}
                                        else if(key == "areas_for_improvement"){field = "Areas for improvement";}
                                        else if(key == "tips_and_suggestion_for_student"){field = "Tips and suggestions";}
                                        else if(key == "class_remarks"){field = "Class remarks";}
                                        else if(key == "book_name"){field = "Book Title";}
                                        else if(key == "lesson_unit"){field = "Lesson or Unit";}
                                        else if(key == "page_number"){field = "Page Number";}

                                        else if(data.status != "completed" && data.status != "lesson memo delay") {
                                            if(key == "student_absent"){field = "Student absent?";}
                                            else if(key == "student_reason_for_absence"){field = "Student's reason for absence";}
                                            else if(key == "student_reason_screenshot"){field = "Reason screenshot";}

                                        }

                                        else if(key == "screenshot"){field = "Screenshot";}
                                        else if(key == "speed_test_1"){field = "Speed Test 1";}
                                        else if(key == "speed_test_2"){field = "Speed Test 2";}
                                        else if(key == "book_url"){field = "Book Download";}

                                        view += '<div class="panel-heading" style="border-bottom:0;">'+
                                            '<h3 class="panel-title">'+field+'</h3>'+
                                            '</div>'+

                                            '<div class="panel-body" style="padding-top:0;">';
                                            if(key == "book_url" && data.book_url != null){
                                                view += '<span><a href="/storage/'+ebook[0].download_link+'">'+ebook[0].original_name+'</a></span>';
                                            }else{
                                                view += '<p style="white-space: pre-line;">'+removeNull(data[key])+'</p>';
                                            }

                                        view += '</div>'+
                                            '<hr style="margin:0;">';

                                    }

                                }else if(data.status == "student is absent"){
                                    if( key == "start_at" || key == "end_at" ||key == "grammar" || key == "pronunciation" ||key == "areas_for_improvement" ||key == "tips_and_suggestion_for_student" || key == "class_remarks" || key == "book_name" ||key == "lesson_unit" || key == "page_number" || key =="status" || key =="speed_test_1" || key == "speed_test_2" || key == "screenshot" || key == "book_url" || key == "student_absent" ||key == "student_reason_for_absence" ||key == "student_reason_screenshot") {
                                        var field = "";
                                        if(key == "start_at"){field = "Starts at";}
                                        else if(key == "end_at"){field = "Ends at";}
                                        else if(key == "status"){field = "Status";}
                                        else if(key == "grammar"){field = "Grammar";}
                                        else if(key == "pronunciation"){field = "Pronunciation";}
                                        else if(key == "areas_for_improvement"){field = "Areas for improvement";}
                                        else if(key == "tips_and_suggestion_for_student"){field = "Tips and suggestions";}
                                        else if(key == "class_remarks"){field = "Class remarks";}
                                        else if(key == "book_name"){field = "Book Title";}
                                        else if(key == "lesson_unit"){field = "Lesson or Unit";}
                                        else if(key == "page_number"){field = "Page Number";}

                                        else if(key == "student_absent"){field = "Student absent?";}
                                        else if(key == "student_reason_for_absence"){field = "Student's reason for absence";}
                                        else if(key == "student_reason_screenshot"){field = "Reason screenshot";}


                                        else if(key == "screenshot"){field = "Screenshot";}
                                        else if(key == "speed_test_1"){field = "Speed Test 1";}
                                        else if(key == "speed_test_2"){field = "Speed Test 2";}
                                        else if(key == "book_url"){field = "Book Download";}

                                        //console.log(key);

                                        view += '<div class="panel-heading" style="border-bottom:0;">'+
                                            '<h3 class="panel-title">'+field+'</h3>'+
                                            '</div>'+

                                            '<div class="panel-body" style="padding-top:0;">';
                                            if(key == "book_url" && data.book_url != null){
                                                view += '<span><a href="/storage/'+ebook[0].download_link+'">'+ebook[0].original_name+'</a></span>';
                                            }else{
                                                view += '<p>'+removeNull(data[key])+'</p>';
                                            }

                                        view += '</div>'+
                                            '<hr style="margin:0;">';

                                    }
                                }else{

                                    if( key == "start_at" || key == "end_at" ||key == "grammar" || key == "pronunciation" ||key == "areas_for_improvement" ||key == "tips_and_suggestion_for_student" || key == "class_remarks" || key == "book_name" ||key == "lesson_unit" || key == "page_number" || key =="status" || key =="speed_test_1" || key == "speed_test_2" || key == "screenshot" || key == "book_url" || key == "student_absent" ||key == "student_reason_for_absence" ||key == "student_reason_screenshot") {
                                        var field = "";
                                        if(key == "start_at"){field = "Starts at";}
                                        else if(key == "end_at"){field = "Ends at";}
                                        else if(key == "status"){field = "Status";}
                                        else if(key == "grammar"){field = "Grammar";}
                                        else if(key == "pronunciation"){field = "Pronunciation";}
                                        else if(key == "areas_for_improvement"){field = "Areas for improvement";}
                                        else if(key == "tips_and_suggestion_for_student"){field = "Tips and suggestions";}
                                        else if(key == "class_remarks"){field = "Class remarks";}
                                        else if(key == "book_name"){field = "Book Title";}
                                        else if(key == "lesson_unit"){field = "Lesson or Unit";}
                                        else if(key == "page_number"){field = "Page Number";}

                                        else if(key == "student_absent"){field = "Student absent?";}
                                        else if(key == "student_reason_for_absence"){field = "Student's reason for absence";}
                                        else if(key == "student_reason_screenshot"){field = "Reason screenshot";}


                                        else if(key == "screenshot"){field = "Screenshot";}
                                        else if(key == "speed_test_1"){field = "Speed Test 1";}
                                        else if(key == "speed_test_2"){field = "Speed Test 2";}
                                        else if(key == "book_url"){field = "Book Download";}

                                        //console.log(key);

                                        view += '<div class="panel-heading" style="border-bottom:0;">'+
                                            '<h3 class="panel-title">'+field+'</h3>'+
                                            '</div>'+

                                            '<div class="panel-body" style="padding-top:0;">';
                                            if(key == "book_url" && data.book_url != null){
                                                view += '<span><a href="/storage/'+ebook[0].download_link+'">'+ebook[0].original_name+'</a></span>';
                                            }else{
                                                view += '<p>'+removeNull(data[key])+'</p>';
                                            }

                                        view += '</div>'+
                                            '<hr style="margin:0;">';

                                    }


                                }


                                
                            }

                            view += "</div>";
                        }//end else

                        view += "</div>";
                        view += '<div class="panel-heading" style="border-bottom:0;margin-top:50px" data-toggle="collapse" data-target="#accordion" class="clickable">Previous Lesson Memos <small style="color:blue">[Display]</small>'+
                            '</div>'+
                            '<div class="panel-body" style="padding-top:10px;" >';
                                view += '<table class="table table-striped table-bordered table-hover">';
                                view += '<thead>';
                                    view += '<tr data-toggle="collapse" data-target="#accordion" class="clickable"><th>Class Time</th><th>Book</th><th>Grammar</th><th>Pronunciation</th><th>Areas for Improvement</th><th>Tips and Suggestions</th><th>Remarks</th></tr>';
                                view += '</thead>';
                                view += '<tbody id="accordion" class="collapse">';
                                for(var i=0; i< data.last30.length; i++) {
                                    var ebook = "";
                                    if(data.last30[i].book_url != null) {
                                        //console.log("not null");
                                        book = JSON.parse(data.last30[i].book_url);
                                        ebook = book[0].original_name
                                        //console.log(ebook[0].original_name);
                                    }
                                    view += '<tr>';
                                        view += '<td>'+moment(data.last30[i].start_at).format("MMMM D YYYY HH:mm")+" - "+moment(data.last30[i].end_at).format("HH:mm")+'</td>';
                                        view += '<td>'+ ebook +'</td>';
                                        view += '<td>'+removeNull(data.last30[i].grammar)+'</td>';
                                        view += '<td>'+removeNull(data.last30[i].pronunciation)+'</td>';
                                        view += '<td>'+removeNull(data.last30[i].areas_for_improvement)+'</td>';
                                        view += '<td>'+removeNull(data.last30[i].tips_and_suggestion_for_student)+'</td>';
                                        view += '<td>'+removeNull(data.last30[i].class_remarks)+'</td>';
                                    view +='</tr>';
                                }
                                view += '</tbody>';
                                view += '</table>';
                            view += '</div>'+
                            '<hr style="margin:0;">';

                        var medium = "";
                        if(data.status == "booked"){
                            medium = (data.student_qq != undefined) ? "<br/> QQ Number: "+data.student_qq : "<br/> WeChat ID: "+data.student_wechat;
                        }

                        var btns;
                        if(calendarEvent.status == "booked"){
                            btns = {
                                    cancelBooking: {
                                        label: "Notify as absent",
                                        className: 'btn-warning',
                                        callback: function(){
                                            cancelBooking(calEvent.id);
                                        }
                                    },
                                    close: {
                                        label: "Close",
                                        className: 'btn-danger'
                                    },
                                    ok: {
                                        label: "Save",
                                        className: 'btn-info',
                                        callback: function(){
                                            teacherSave();
                                            return false;
                                        }
                                    }

                                };
                        } else {
                            btns = {
                                    close: {
                                        label: "Close",
                                        className: 'btn-danger'
                                    }
                                };
                        }

                        
                        var dialog = bootbox.dialog({
                            title: 'Class ID: ' + moment(data.start_at).format("DD-MMM-YY-HH:mm") +"-"+moment(data.end_at).format("HH:mm") + "-"+data.id +"<br/>Student: "+ data.student_nick + "<br/>Student ID: "+ data.student_id + " " + medium,
                            message: view,
                            size: 'large',
                            buttons: btns
                        });

                        //bootbox.alert(data);
                    });
                    //console.log(calEvent);
                    }
                @endif
            }// end eventclick
            
        });// end fullcalendar



        }// init


        function toggleTeacher(value, class_start_at){

            //console.log("class start: "+class_start_at);
            if(value == "yes"){
                $("#status").val("penalty 3");
                $("#teacher_absent").show();
                var now = moment().tz( "{{auth()->user()->timezone}}" );
                var start_at = moment(class_start_at).tz("{{ auth()->user()->timezone }}").add(5,"minutes");
                //console.log("Current Time: "+now);
                //console.log("Class Start: "+start_at);
                //console.log("Current Time: "+now.format("H:mm:s"));
                //console.log("Class Start: "+start_at.format("H:mm:s"));
                //console.log(start_at + " > " + now);
                
                if(start_at >= now) {
                    //hide btn-save

                    $("#btn-save").hide();
                }
                // lets check if its 15 minutes, or more for the said class
            }
            else {
                $("#status").val("booked");
                $("#teacher_absent").hide();
                $("#btn-save").show();
            }
        }

        function removeNull(value) {
            return (value == null) ? "" : value;
        }

        @if($isTeacher)
            function toggleStudent(value) {
                if(value == "yes"){
                    $("#status").val("student is absent");
                    $("#student_absent").show();
                }
                else {
                    $("#status").val("completed");
                    $("#student_absent").hide();
                }
            }
            
            function teacherSave() {
                
                var pronunciation = $("#pronunciation").val();
                //console.log("pronunciation: "+pronunciation);
                if(pronunciation == "• • •") {
                    $("#pronunciation").val("");
                }
                
                var cdt = moment().tz("{{ auth()->user()->timezone }}");
                var incdt = moment().tz("{{ auth()->user()->timezone }}");
                var incsa = calendarEvent.start.utcOffset(cdt.format("Z"),true).add(20, "minutes");

                //console.log("checking time:"+incdt.format("hh:mm:ss")+ " incsa:"+incsa.format("hh:mm:ss"));

                if(incdt >= incsa) {
                    //check some fields
                    var complete = true;
                    if($("#grammar").val() == "")
                    {
                        complete = false;
                    }
                    if($("#pronunciation").val() == "") {
                        complete = false;
                    }

                    if($("#page_number").val() == "") {
                        complete = false;
                    }

                    if(complete == false){
                        alert("Please fill, grammar, pronunciation, page number.. put n/a if not applicable..");
                    }else{
                        var info = {
                            id: calendarEvent.id,
                            status: $("#status").val(),
                            pronunciation: $("#pronunciation").val(),
                            grammar: $("#grammar").val(),
                            areas_for_improvement: $("#areas_for_improvement").val(),
                            tips_and_suggestion_for_student: $("#tips_and_suggestion_for_student").val(),
                            class_remarks: $("#class_remarks").val(),
                            speed_test_1: $("#speed_test_1").val(),
                            speed_test_2: $("#speed_test_2").val(),
                            book_name: $("#book_name").val(),
                            page_number: $("#page_number").val(),
                            student_absent: $('input[name=student_absent]:checked').val(),
                            student_reason_for_absence: $('#student_reason_for_absence').val(),
                            student_reason_screenshot: $('#student_reason_screenshot').val(),
                        }

                        var url = "{{ url('api/booking/update') }}";

                        $.ajax({
                          type: "POST",
                          contentType: "application/json",
                          data: JSON.stringify(info),
                          url: url
                        }).done(function(data) {
                            bootbox.hideAll();
                            bootbox.alert(data, function() {
                                window.location.replace("{{ url('book-classes') }}");
                            });
                            //console.log(data);                
                        });

                    }

                }else{
                    alert("Please save later after class..");
                }

            }
        @endif





        function GetIEVersion() {
          var sAgent = window.navigator.userAgent;
          var Idx = sAgent.indexOf("MSIE");

          // If IE, return version number.
          if (Idx > 0) 
            return parseInt(sAgent.substring(Idx+ 5, sAgent.indexOf(".", Idx)));

          // If IE 11 then look for Updated user agent string.
          else if (!!navigator.userAgent.match(/Trident\/7\./)) 
            return 11;

          else
            return 0; //It is not IE
        }



    </script>
@stop


