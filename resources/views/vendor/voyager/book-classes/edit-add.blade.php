@extends('voyager::master')


@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ (auth()->user()->lang == "zh-cn")? "预约课程":"Book Classes" }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
@php

    $userTimeZone = auth()->user()->timezone;
    $now = new \DateTime(null, new \DateTimeZone($userTimeZone));
    $end_at = "";
    $no_package = "false";

    $orders = App\Order::where('student_id', auth()->user()->id )->where("status","active")->where("effective_until", ">=", $now)->where("remaining_reservation", ">", 0)->get(['name','id','remaining_reservation','effective_until']);

    $referrals = App\Referral::where('referrer_id', auth()->user()->id )->where("referral_valid_until", ">=", $now)->where("referral_consumable", ">", 0)->get(['id','referral_consumable','referral_valid_until']);

    $isStudent = false;
    if( auth()->user()->hasRole('student') ){
        $isStudent = true;
    }

    $isTeacher = false;
    if( auth()->user()->hasRole('teacher') ){
        $isTeacher = true;
    }

@endphp
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    @if(url()->current() == url("/book-classes/create"))
                    <div class="panel-body">

                        @php
                            
                            $valid_until = new \DateTime(auth()->user()->free_trial_valid_until, new DateTimeZone($userTimeZone));
                            
                            // package and immortal
                            if( count($orders) <= 0 && auth()->user()->immortal <= 0 && count($referrals) <= 0 ) {
                                $no_package = "true";
                            }

                            // free trial if free trial
                            if( auth()->user()->student_account_type_id == "1"){
                                $no_package = "false";

                                if( $now->getTimestamp() < $valid_until->getTimestamp() ) {
                                    if( (auth()->user()->free_trial_consumable == null) || (auth()->user()->free_trial_consumable <= 0) ) {
                                        // add referral
                                        $no_package = "true";
                                    }
                                }
                                if(auth()->user()->immortal > 0) {
                                    $no_package = "false";
                                }
                            }

                        @endphp

                        @if($no_package == "false")
                        <div class="form-group col-md-12" id="ordered_packages">
                            @php
                                $lbl_remaining = (auth()->user()->lang=="zh-cn")?"剩余课次":"remaining classes";
                                $lbl_imm_exp = (auth()->user()->lang=="zh-cn")?"永久有效":"no expiration date";
                                $lbl_expiry = (auth()->user()->lang=="zh-cn")?"截止日期":"will expire on";

                            @endphp
                            <label for="orders">{{ (auth()->user()->lang=="zh-cn") ? "选择希望使用的课程类型":"Select Which Balance You Wish To Use"}}:</label>
                            <select name="orders" id="orders" class="form-control">
                                @if(count($orders) > 0)
                                    @foreach($orders as $order)
                                        <option value="{{ $order->id }}:{{$order->remaining_reservation}}">{{ $order->name }} - {{ $order->remaining_reservation }} {{ $lbl_remaining }}, {{$lbl_expiry}} {{ $order->effective_until }}</option>
                                    @endforeach
                                @endif

                                @if(count($referrals) > 0)
                                    @foreach($referrals as $ref)
                                        <option value="{{ 'r-'.$ref->id }}:{{$ref->referral_consumable}}">
                                            Active Referrals: {{ $ref->referral_consumable }} {{$lbl_remaining}}, {{$lbl_expiry}} {{ $ref->referral_valid_until }}
                                        </option>
                                    @endforeach
                                @endif

                                @if( (auth()->user()->immortal != null) && (auth()->user()->immortal > 0) )
                                    <option value="0:{{ auth()->user()->immortal }}">Immortal: {{ auth()->user()->immortal }} {{$lbl_remaining}}, {{$lbl_imm_exp}}</option>
                                @endif

                                @if(auth()->user()->student_account_type_id == 1)
                                    @php
                                        $valid_until = new \DateTime(auth()->user()->free_trial_valid_until, new DateTimeZone($userTimeZone));
                                    @endphp
                                    @if( $now->getTimestamp() < $valid_until->getTimestamp() )
                                        @if( (auth()->user()->free_trial_consumable != null) && (auth()->user()->free_trial_consumable > 0) )
                                            <option value="free:{{ auth()->user()->free_trial_consumable }}" selected>Free Trial: {{ auth()->user()->free_trial_consumable }} {{$lbl_remaining}}, valid until {{ auth()->user()->free_trial_valid_until }}</option>
                                        @endif
                                    @endif
                                @endif
                            </select>
                        </div>
                        @endif


                        <div class="form-group col-md-12">
                            <label for="when">{{ (auth()->user()->lang=="zh-cn") ? "请选择日期":"Select Date and Time"}}:</label>
                            <input type="text" id="when" name="when" class="form-control" />
                            <button id="search" class="btn btn-primary">{{ (auth()->user()->lang=="zh-cn") ? "搜索":"Search" }}</button>
                        </div>                       
                        <div class="col-md-12" id="parent-profile">
                            <div class="form-group">
                                @php
                                    $lbl_noteacher = (auth()->user()->lang =="zh-cn") ? "此日期无可预约外教":"No Teachers available at this date..";
                                @endphp
                                <div class='col-md-12 alert alert-danger text-center' id="noteachers" style="display:none">{{ $lbl_noteacher }}</div>
                                <div id="profile">
                                </div>
                            </div>
                        </div>



                        <div class="row col-md-12">
                            <div id="calendar"></div>
                        </div>
                        <div class="row col-md-12">
                            <div id="divSubmit" style="display:none">
                                <!-- Trigger the modal with a button -->
                                <button type="button" class="btn btn-primary save" onclick="saveBookedClasses()" id="save-booked-classes">{{ (auth()->user()->lang =="zh-cn") ? "保存":"Save"}}</button>
                                <!--<button id="saveBookedSlots" type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Save</button>
                                    -->
                            </div>
                        </div>
                        
                    </div>

                    @else
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" name="save_edit" id="save_edit" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $options = json_decode($row->details);
                                    $display_options = isset($options->display) ? $options->display : NULL;
                                @endphp
                                @if ($options && isset($options->legend) && isset($options->legend->text))
                                    <legend class="text-{{$options->legend->align or 'center'}}" style="background-color: {{$options->legend->bgcolor or '#f0f0f0'}};padding: 5px;">{{$options->legend->text}}</legend>
                                @endif
                                @if ($options && isset($options->formfields_custom))
                                    @include('voyager::formfields.custom.' . $options->formfields_custom)
                                @else
                                        @if( $row->field == "status" && $isTeacher == true)
                                            <input type="hidden" name="status" id="status" value="completed">
                                        @elseif( $row->field == "status" && $isStudent == true)
                                            <!--<input type="hidden" name="status" id="status" value="booked">-->
                                        @elseif( ($row->field == "session" || $row->field == "") && $isStudent == true)

                                        @elseif( ($row->field == "teacher_reason_for_absence" || $row->field == "current_teacher_rate" || $row->field=="current_teacher_currency" || $row->field=="teacher_absent" || $row->field=="teacher_reason_screenshot" || $row->field=="job_ids") && $isTeacher == true)

                                        @elseif( ($row->field == "student_reason_for_absence" || $row->field == "current_teacher_rate" || $row->field=="current_teacher_currency" || $row->field=="student_absent" || $row->field=="student_reason_screenshot" || $row->field=="speed_test_1"|| $row->field=="speed_test_2" || $row->field=="grammar"|| $row->field=="pronunciation"|| $row->field=="areas_for_improvement"|| $row->field=="tips_and_suggestion_for_student"|| $row->field=="class_remarks"|| $row->field=="screenshot"|| $row->field=="book_name" || $row->field=="lesson_unit" || $row->field=="page_number"|| $row->field=="current_teacher_rate" || $row->field=="job_ids") && $isStudent == true)

                                        @elseif($row->field == "teacher_reason_for_absence" && $isStudent == true)
                                            <div style="display:none" id="teacher_reason_for_absence" class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                                <label for="name">{{ $row->display_name }}:</label>
                                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                            </div>
                                        @elseif($row->field == "teacher_reason_screenshot" && $isStudent == true)
                                            <div style="display:none" id="teacher_reason_screenshot" class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                                <label for="name">{{ $row->display_name }}:</label>
                                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                            </div>

                                        @elseif($row->field == "student_reason_for_absence" && $isTeacher == true)
                                            <div style="display:none" id="student_reason_for_absence" class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                                <label for="name">{{ $row->display_name }}:</label>
                                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                            </div>
                                        @elseif($row->field == "student_reason_screenshot" && $isTeacher == true)
                                            <div style="display:none" id="student_reason_screenshot" class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                                <label for="name">{{ $row->display_name }}:</label>
                                                {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                            </div>

                                        @elseif($row->field == "book_url" && $isTeacher == true)
                                            @php
                                                $data = json_decode($dataTypeContent->book_url);
                                                $url = "";
                                                $name = "";
                                                if($data[0] != null){
                                                    $url = url("/storage/".$data[0]->download_link);
                                                    $name = $data[0]->original_name;
                                                }
                                            @endphp
                                            <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                                <label for="name">{{ $row->display_name }}:</label>
                                                <a href="{!! $url !!}" target="_blank">{!! $name !!}</a>
                                            </div>
                                        @elseif($row->field == "message_to_teacher" && $isTeacher == true)
                                            <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                                <label for="name">{{ $row->display_name }}:</label>
                                                {{ $dataTypeContent->message_to_teacher }}
                                            </div>
                                        @else
                                    
                                            <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                                {{ $row->slugify }}
                                                <label for="name">{{ $row->display_name }}</label>
                                                @include('voyager::multilingual.input-hidden-bread-edit-add')
                                                @if($row->type == 'relationship')
                                                    @include('voyager::formfields.relationship')
                                                @else
                                                    {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                                @endif

                                                @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                                    {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                                @endforeach
                                            </div>                                           
                                        @endif
                                @endif
                            @endforeach
                            <div class="panel-footer">
                                @if($isTeacher)
                                <button type="button" class="btn btn-primary pull-right save" onclick="checkTime()">Save</button>
                                @else
                                <button type="submit" class="btn btn-primary pull-right save">Save</button>
                                @endif
                            </div>

                        </div><!-- panel-body -->

                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                    @endif


                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script src="{{ asset('vendor/fullcalendar/lib/moment.min.js') }}"></script>
    <script src="{{ asset('vendor/bootbox/bootbox.min.js') }}"></script>
    @if($isStudent == true && url()->current() == url("/book-classes/create") )
    <script src="{{ asset('vendor/fullcalendar/lib/moment-with-data.min.js') }}"></script>
    <script src="{{ asset('vendor/fullcalendar/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('vendor/php-date-formatter/js/php-date-formatter.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('vendor/datetimepicker/jquery.datetimepicker.full.min.js') }}"></script>
    @endif
    @if(auth()->user()->lang == "zh-cn")
        <script src="{{ asset('vendor/fullcalendar/locale/zh-cn.js') }}"></script>
    @endif

    <script type="text/javascript">
        var no_package = "{{ $no_package }}";
        var params = {}
        var $image

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();

            $('#option-student-absent-no').on("change", function (e){ 
                if(this.checked){
                    $("#status").val("completed");
                    $("#student_reason_for_absence").hide();
                    $("#student_reason_screenshot").hide();
                }
            });

            $('#option-student-absent-yes').on("change", function (e){ 
                if(this.checked){
                    $("#status").val("student is absent");
                    $("#student_reason_for_absence").show();
                    $("#student_reason_screenshot").show();
                }
            });
            // teacher
            $('#option-teacher-absent-no').on("change", function (e){ 
                if(this.checked){
                    $("#status").val("booked");
                    $("#teacher_reason_for_absence").hide();
                    $("#teacher_reason_screenshot").hide();
                }
            });

            $('#option-teacher-absent-yes').on("change", function (e){ 
                if(this.checked){
                    $("#status").val("teacher is absent");
                    $("#teacher_reason_for_absence").show();
                    $("#teacher_reason_screenshot").show();
                }
            });

        });
    </script>
    @if($isStudent == true && url()->current() == url("/book-classes/create"))
    <script type="text/javascript">
        var remaining;
        var order_id;
        var teacher_id;
        var teacher_timezone;
        var calendar;
        var calendar_id = "";
        var calendar_t_timezone;
        // onload update order data
        $(document).ready(function() {
            if($("#orders").length){
                var orders = $("#orders").val().split(":");
                order_id = orders[0];
                remaining = orders[1];                
            }else{
                order_id = 0;
                remaining = 0;
            }

            var rightNow = moment();

            $.datetimepicker.setLocale('en');

            $('#when').datetimepicker({
                dayOfWeekStart : 1,
                lang:'zh-cn',
                timepicker:false,
                format: "Y-m-d",
                startDate:  rightNow,
                minDate:rightNow,
            });

            $("#when").change(function(){
                $("#noteachers").hide();
                $("#profile").html("");
                $("#calendar").fullCalendar("destroy");
            });

            $('#when').datetimepicker({value:moment(), step:10});

            $('#search').click(function(){
                $("#calendar").fullCalendar("destroy");
                var dateandtime = $("#when").val();
                var clean = dateandtime.replace("/","-");
                clean = clean.replace("/","-");
                
                var url = "{{ url('api/teacher/profile/search') }}/"+clean;
                //console.log(url);

                $.ajax({
                  type: "GET",
                  url: url
                }).done(function(data) {
                    if(data.length == 0) {
                        $("#noteachers").show();
                        $("#profile").html("");
                    }else{
                        $("#noteachers").hide();
                        var thispage = "{{ url('storage') }}";
                        var profilehtml = "";
                        for(var i=0; i<data.length; i++){
                            profilehtml += "<div class='col-md-3'><div class='row'><img src='"+thispage+"/"+data[i].avatar+"' class='img-thumbnail' style='max-height:250px'></div><div class='row'>";
                            @if(auth()->user()->lang == "zh-cn")
                                profilehtml += "外教 ";
                            @else
                                profilehtml += "Teacher ";                            
                            @endif
                            profilehtml += data[i].nick+"</div><div class='row'><button onclick='viewSchedule("+data[i].id+",\""+data[i].timezone+"\")' class='btn btn-primary view-sched'>";
                            @if( auth()->user()->lang == "zh-cn" )
                                profilehtml += "查看课程";
                            @else
                                profilehtml += "View Schedule";
                            @endif
                            profilehtml += "</button></div></div>";
                        }
                        $("#profile").html(profilehtml);
                    }

                });
                
            });

            // on order change update order data
            $("#orders").change(function(){
                var orders = $("#orders").val().split(":");
                order_id = orders[0];
                remaining = orders[1];
                if(calendar_id != ""){
                    viewSchedule(calendar_id, calendar_t_timezone);
                    $("#divSubmit").hide();
                }
            });

        });


        var studentEvents = [];

        function viewSchedule(id,t_timezone) {
            var default_view = 
            calendar_id = id;
            calendar_t_timezone = t_timezone;
            $("#calendar").fullCalendar("destroy");
            studentEvents = [];
            //console.log(id);
            teacher_id = id;
            teacher_timezone = t_timezone;
            calendar = $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next,today',
                    center: 'title',
                    right: 'agendaWeek,basicDay'
                },
                //eventColor: '#378006',
                slotDuration: '00:30:00',
                slotLabelInterval: 30,
                slotLabelFormat: 'h(:mm)a',
                contentHeight: 'auto',
                eventLongPressDelay: 1000,
                timeFormat: 'H:mm',
                slotLabelFormat: 'H:mm',
                selectConstraint: "businessHours",
                defaultView: window.mobilecheck() ? "basicDay" : "agendaWeek",
                defaultTimedEventDuration: '0:25',
                defaultDate: $("#when").val(),
                eventStartEditable: false,
                allDaySlot: false,
                scrollTime: '00:00',
                lang: /^en-/.test(navigator.language) ? 'en' : 'zh-cn',
                timezone: '{{ $userTimeZone }}',
                editable: true,
                selectable: false,
                selectHelper: false,
                disableDragging: true,
                disableResizing: true,
                businessHours: {
                  dow: [ 0, 1, 2, 3, 4, 5, 6 ],
                  start: '6:00',
                  end: '24:00',
                },
                viewRender: function(){
                    if (GetIEVersion() == 0){
                        var container = $('.fc thead');
                        $(".cloned-head").remove();
                        var cloneHead = $('<tr class="fc-first fc-last cloned-head"></tr>').css({
                            zIndex: 5,
                            display: 'none'
                        }).appendTo(container);

                        $('#calendar thead th').each(function() {
                            var cloned = $(this).clone();
                            cloned.css('width', $(this).outerWidth());
                            cloned.appendTo(cloneHead);
                        });

                        $(document).on('scroll', function() {
                            var scroll = $(document).scrollTop();
                            var show = (container.offset().top) + cloneHead.outerHeight() < scroll;
                            if (show) {
                                cloneHead.show().css({
                                  position: 'absolute',
                                  backgroundColor: '#cbcdda',
                                  top: scroll - container.offset().top +65
                                });
                            } else {
                                cloneHead.hide();
                            }
                        });
                    } 
                },
                eventSources: [{
                    url: "{{ url('api/availabilities/browse') }}/"+teacher_id,
                    color: "#019875",
                    textColor: "white"
                }],
                eventOverlap: function(stillEvent, movingEvent) {
                    return false;//stillEvent.allDay && movingEvent.allDay;
                },
                //select: function(start, end) {
                //},
                eventDurationEditable: false,
                eventClick: function(calEvent, jsEvent, view) {
                        // change events to book
                    if( (calEvent.free_trial_count >= 2) && (calEvent.status == "booked") && (order_id == "free") ) {
                        bootbox.alert("This teacher has reached the limit for free trial for this day..");

                    }else{

                        var now = moment();
                        var addToTime = now.tz("{{ $userTimeZone }}").format('Z').split(":");
                        var operation = addToTime[0].toString();
                        var hr = Math.abs(parseInt(addToTime[0]));
                        var min = Math.abs(parseInt(addToTime[1]));
                        var rightNowOnYourLocal;
                        if(operation[0] == "-"){
                            rightNowOnYourLocal = now.subtract( hr, "hours");
                            rightNowOnYourLocal = now.subtract( (min+5), "minutes");
                        }
                        else {
                            rightNowOnYourLocal = now.add( hr, "hours");            
                            rightNowOnYourLocal = now.add( (min+5), "minutes");
                        }
                        if( calEvent.start.isBefore( rightNowOnYourLocal ) ) {
                            bootbox.alert('You can only book slots 5 minutes from now..');
                            calendar.fullCalendar('unselect');
                        } else {
                            // do not remove event instead update it and hide it..
                            //console.log(calEvent);
                            if(calEvent.status == "booked") {
                                calendar.fullCalendar('removeEvents', calEvent._id);
                                // return event to status open and remove in student Events
                                var title = "open";
                                var status = "open";
                                var eventData;
                                if (title && title.trim()) {
                                    eventData = {
                                      title: title,
                                      start: calEvent.start,
                                      end: calEvent.end,
                                      status: status,
                                      id: calEvent.id,
                                      timezone: "{{ $userTimeZone }}",
                                      color: "#019875",
                                    };
                                    calendar.fullCalendar('renderEvent', eventData);
                                }
                                var i;
                                for(i=0; i<studentEvents.length; i++) {
                                    if( studentEvents[i].start.toString() == calEvent.start.toString() ) {
                                        break;
                                    }
                                }
                                studentEvents.splice(i, 1);

                                calendar.fullCalendar('unselect');
                                if(studentEvents.length == 0){
                                    $("#divSubmit").hide();
                                }
                            } else {

                                if(no_package == "true") {                                    
                                    bootbox.confirm("You don't have any active package or immortal class or referral to book a class, would you like to buy a new package?", function(ans) {
                                        if(ans == true) {
                                            window.location.replace("{{ url('/orders/create') }}");
                                        }
                                    });
                                }
                                else if(studentEvents.length >= remaining) {
                                    bootbox.alert("You can only book a total of "+ remaining +"class/es");
                                }else{
                                    calendar.fullCalendar('removeEvents', calEvent._id);

                                    var title = calEvent.end.format("H:mm")+" - booked";
                                    var status = "booked";
                                    var eventData;
                                    if (title && title.trim()) {
                                        eventData = {
                                          title: title,
                                          start: calEvent.start,
                                          end: calEvent.end,
                                          id: calEvent.id,
                                          status: status,
                                        };
                                        studentEvents.push(eventData);
                                        calendar.fullCalendar('renderEvent', eventData);
                                    }
                                    calendar.fullCalendar('unselect');
                                    $("#divSubmit").show();
                                }

                            }// end else not booked

                            //console.log(studentEvents);
                        }// end else
                    }// end else if free
                }
            });// end fullcalendar


        }// view schedule

        function saveBookedClasses(){

            $("#save-booked-classes").hide();
            //convert startdate and enddates to mysql ready for insert
            for(i=0; i<studentEvents.length; i++) {
                studentEvents[i].start = studentEvents[i].start.format("YYYY-MM-DD HH:mm:ss");
                studentEvents[i].end = studentEvents[i].end.format("YYYY-MM-DD HH:mm:ss");
            }
            //console.log(order_id);
            var info = {
                classes: studentEvents,
                order_id: order_id,
                teacher_id: teacher_id,
                teacher_timezone: teacher_timezone,
                student_id: "{{ auth()->user()->id }}",
                student_timezone: "{{ $userTimeZone }}",
            }
            //console.log(info);


            var url = "{{ url('api/booking/save') }}";

            $.ajax({
              type: "POST",
              contentType: "application/json",
              data: JSON.stringify(info),
              url: url
            }).done(function(data) {
                //console.log(data);                
                $("#save-booked-classes").show();
                var m = "Some Error Occured";
                if(data == "true"){
                    @if( auth()->user()->lang=="zh-cn" )
                        m = "完成预约";
                    @else
                        m = "Booking Completed.."
                    @endif
                }
                bootbox.alert(m, function() {
                    window.location.replace("{{ url('book-classes') }}");                    
                });
                
            });

        }





function GetIEVersion() {
  var sAgent = window.navigator.userAgent;
  var Idx = sAgent.indexOf("MSIE");

  // If IE, return version number.
  if (Idx > 0) 
    return parseInt(sAgent.substring(Idx+ 5, sAgent.indexOf(".", Idx)));

  // If IE 11 then look for Updated user agent string.
  else if (!!navigator.userAgent.match(/Trident\/7\./)) 
    return 11;

  else
    return 0; //It is not IE
}


window.mobilecheck = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};


    </script>
    @elseif($isTeacher)
        <script>

            function checkTime(){
                var now = moment();
                var end = moment("{{ $dataTypeContent->end_at }}");
                if(now >= end) {
                    var form = document.getElementById('save_edit');
                    form.submit();
                }else{
                    bootbox.alert("You can't update this yet, please wait for the class end time "+end.format('YYYY-MM-DD HH:mm:ss') );
                }
            }
        </script>
    @endif
@if( Session::get("tour") !== null )
    <script src="{{ asset('vendor/enjoyhint/enjoyhint.min.js') }}"></script>
    @if( auth()->user()->hasRole('student') )
    <script src="{{ asset('js/student_tour_book_class.js') }}"></script>
    @endif
@endif
@stop


@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if($isStudent == true)
    <link rel="stylesheet" type="text/css" href="{{ asset('vendor/datetimepicker/jquery.datetimepicker.css') }}"/>
    <link rel="stylesheet" href="{{ asset('vendor/fullcalendar/fullcalendar.min.css') }}">
    @endif
    @if( Session::get("tour") !== null )
        <link rel="stylesheet" href="{{ asset('vendor/enjoyhint/enjoyhint.css') }}">        
    @endif

@stop

@php
    Session::forget("tour");
@endphp