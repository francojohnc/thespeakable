        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">

                        <!-- /.col -->
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <div class="info-box bg-green">
                            <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

                            <div class="info-box-content">
                              <span class="info-box-text">Sold </span>
                              <span class="info-box-number">{{ \App\Http\Controllers\DashboardController::soldPackages() }} </span>

                              <div class="progress">
                                <div class="progress-bar" style="width: 70%"></div>
                              </div>
                                  @php
                                    $month = new \DateTime( "now", new \DateTimeZone(auth()->user()->timezone) ) 
                                  @endphp
                                  <span class="progress-description">Month of {{ $month->format("F") }}</span>
                            </div>
                            <!-- /.info-box-content -->
                          </div>
                          <!-- /.info-box -->
                        </div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <div class="info-box" style="color:#ffffff;background-color: #499fbc">
                            <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

                            <div class="info-box-content">
                              <span class="info-box-text">Active Students</span>
                              <span class="info-box-number">{{ \App\Http\Controllers\DashboardController::activeStudents() }}</span>

                              <div class="progress">
                                <div class="progress-bar" style="width: 70%"></div>
                              </div>
                                  <span class="progress-description">
                                    Inactive: {{ \App\Http\Controllers\DashboardController::inactiveStudents() }}
                                  </span>
                            </div>
                            <!-- /.info-box-content -->
                          </div>
                          <!-- /.info-box -->
                        </div>

                        <!-- /.col -->
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <div class="info-box bg-yellow">
                            <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

                            <div class="info-box-content">
                              <span class="info-box-text">Awaiting Payment</span>
                              <span class="info-box-number"><a href="{{ url('/orders') }}" style="color:#fff">{{ \App\Http\Controllers\DashboardController::awaitingPayment() }}</a></span>

                              <div class="progress">
                                <div class="progress-bar" style="width: 70%"></div>
                              </div>
                                  <span class="progress-description">
                                    For Approval: {{ \App\Http\Controllers\DashboardController::forApproval() }}
                                  </span>
                            </div>
                            <!-- /.info-box-content -->
                          </div>
                          <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3 col-sm-6 col-xs-12">
                          <div class="info-box" style="color:#ffffff;background-color: #20987a">
                            <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>

                            <div class="info-box-content">
                              <span class="info-box-text">Earnings</span>
                              <span class="info-box-number"><a href="javascript:;" style="color:#ffffff" onclick="tallyClient()">{{ \App\Http\Controllers\DashboardController::clientEarningTotal() }}</a></span>

                              <div class="progress">
                                <div class="progress-bar" style="width: 70%"></div>
                              </div>
                                  <span class="progress-description"> Month of {{ $month->format("F") }}
                                  </span>
                            </div>
                            <!-- /.info-box-content -->
                          </div>
                          <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                       
                      <div>
                        

                      </div>

                    </div>
                </div>
            </div>
        </div>