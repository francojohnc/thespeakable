@if( auth()->user()->hasRole('teacher') )
    @include('vendor.voyager.availabilities.browse_teacher')
@endif
@if( auth()->user()->hasRole('admin') || auth()->user()->hasRole('superadmin') )
    @include('vendor.voyager.availabilities.browse_admin')
@endif