<ul class="nav navbar-nav">

@php
    if (Voyager::translatable($items)) {
        $items = $items->load('translations');
    }
@endphp

@foreach ($items as $item)

    @php
        $originalItem = $item;
        if (Voyager::translatable($item)) {
            $item = $item->translate($options->locale);
        }

        // TODO - still a bit ugly - can move some of this stuff off to a helper in the future.
        $listItemClass = [];
        $styles = null;
        $linkAttributes = null;

        if(url($item->link()) == url()->current())
        {
            array_push($listItemClass,'active');
        }

        //If have children, default false. we will check childs permissions!
        if(!$originalItem->children->isEmpty()) {
            $show_menu_item = false;
        } else {
            $show_menu_item = true;
        }

        // With Children Attributes
        if(!$originalItem->children->isEmpty())
        {
            foreach($originalItem->children as $children)
            {
                if(url($children->link()) == url()->current())
                {
                    array_push($listItemClass,'active');
                }

                // Childrens Permission Checker
                $self_prefix = str_replace('/', '\/', $options->user->prefix);
                $slug = str_replace('/', '', preg_replace('/^\/'.$self_prefix.'/', '', $children->link()));
                if ($slug != '') {
                    // Get dataType using slug
                    $dataType = $options->user->dataTypes->first(function ($value) use ($slug) {
                        return $value->slug == $slug;
                    });

                    if ($dataType) {
                        // Check if datatype permission exist
                        $exist = $options->user->permissions->first(function ($value) use ($dataType) {
                            return $value->key == 'browse_'.$dataType->name;
                        });
                    } else {
                        // Check if admin permission exists
                        $exist = $options->user->permissions->first(function ($value) use ($slug) {
                            return $value->key == 'browse_'.$slug;
                        });
                    }

                    if ($exist) {
                        if (in_array($exist->key, $options->user->user_permissions)) {
                            $show_menu_item = true;
                        }
                    }
                }


            }
            if(auth()->user()->lang=="zh-cn") {
                $linkAttributes =  'href="#' . $item->title .'-dropdown-element" data-toggle="collapse" aria-expanded="'. (in_array('active', $listItemClass) ? 'true' : 'false').'"';

            }else{
                $linkAttributes =  'href="#' . str_slug($item->title, '-') .'-dropdown-element" data-toggle="collapse" aria-expanded="'. (in_array('active', $listItemClass) ? 'true' : 'false').'"';

            }
            array_push($listItemClass, 'dropdown');
        }
        else
        {
            $linkAttributes =  'href="' . url($item->link()) .'"';
        }

        // Permission Checker
        $self_prefix = str_replace('/', '\/', $options->user->prefix);
        $slug = str_replace('/', '', preg_replace('/^\/'.$self_prefix.'/', '', $item->link()));

        if ($slug != '') {
            // Get dataType using slug
            $dataType = $options->user->dataTypes->first(function ($value) use ($slug) {
                return $value->slug == $slug;
            });

            if ($dataType) {
                // Check if datatype permission exist
                $exist = $options->user->permissions->first(function ($value) use ($dataType) {
                    return $value->key == 'browse_'.$dataType->name;
                });
            } else {
                // Check if admin permission exists
                $exist = $options->user->permissions->first(function ($value) use ($slug) {
                    return $value->key == 'browse_'.$slug;
                });
            }

            if ($exist) {
                // Check if current user has access
                if (!in_array($exist->key, $options->user->user_permissions)) {
                    continue;
                }
            }
        }

    @endphp

    @if(auth()->user()->lang=="zh-cn")
        @if($show_menu_item)
        <li class="{{ implode(' ', $listItemClass) }} " id="menu-{{ $item->title }}">
            <a {!! $linkAttributes !!} target="{{ $item->target }}">
                <span class="icon {{ $item->icon_class }}"></span>
                <span class="title">{{ $item->title }}</span>
            </a>
            @if(!$originalItem->children->isEmpty())
            <div id="{{ $originalItem->title }}-dropdown-element" class="panel-collapse collapse {{ (in_array('active', $listItemClass) ? 'in' : '') }}">
                <div class="panel-body">
                    @include('voyager::menu.admin_menu', ['items' => $originalItem->children, 'options' => $options, 'innerLoop' => true])
                </div>
            </div>
            @endif
        </li>
        @endif

    @else
        @if($show_menu_item)
        <li class="{{ implode(' ', $listItemClass) }} " id="menu-{{ str_slug($item->title, '-') }}">
            <a {!! $linkAttributes !!} target="{{ $item->target }}">
                <span class="icon {{ $item->icon_class }}"></span>
                <span class="title">{{ $item->title }}</span>
            </a>
            @if(!$originalItem->children->isEmpty())
            <div id="{{ str_slug($originalItem->title, '-') }}-dropdown-element" class="panel-collapse collapse {{ (in_array('active', $listItemClass) ? 'in' : '') }}">
                <div class="panel-body">
                    @include('voyager::menu.admin_menu', ['items' => $originalItem->children, 'options' => $options, 'innerLoop' => true])
                </div>
            </div>
            @endif
        </li>
    @endif

    @endif

@endforeach

</ul>