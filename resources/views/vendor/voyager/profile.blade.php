@extends('voyager::master')

@section('css')
    <style>
        .user-email {
            font-size: .85rem;
            margin-bottom: 1.5em;
        }
    </style>
@stop

@section('content')
    <div style="background-size:cover; background-image: url({{ Voyager::image( Voyager::setting('admin.bg_image'), config('voyager.assets_path') . '/images/bg.jpg') }}); background-position: center center;position:absolute; top:0; left:0; width:100%; height:300px;"></div>
    <div style="height:160px; display:block; width:100%"></div>
    <div style="position:relative; z-index:9; text-align:center;">
        <img src="@if( !filter_var(Auth::user()->avatar, FILTER_VALIDATE_URL)){{ Voyager::image( Auth::user()->avatar ) }}@else{{ Auth::user()->avatar }}@endif"
             class="avatar"
             style="border-radius:50%; width:150px; height:150px; border:5px solid #fff;"
             alt="{{ Auth::user()->name }} avatar">
        <h4>{{ Auth::user()->name }}</h4>
        <div class="user-email text-muted">{{ Auth::user()->email }}</div>
        <p>{{ Auth::user()->bio }}</p>

        <a href="javascript:;" onclick="editProfile()" class="btn btn-primary">{{ __('voyager::profile.edit') }}</a>
    </div>
@stop

@section('javascript')
<script src="{{ asset('vendor/bootbox/bootbox.min.js') }}"></script>
<script type="text/javascript">

    function countryChanged() {

        var country = "{{ auth()->user()->country_id }}";
        
        var url = "{{ url('/cities') }}/"+country;

        var city_id = "{{ auth()->user()->city_id }}";

        $.ajax({
          type: "GET",
          url: url
        }).done(function(data) {
            var cities = "";
            for(var i=0;i<data.length;i++) {
                if(data[i].id == city_id){
                    cities += "<option value='"+data[i].id+"' selected>"+data[i].name+"</option>";
                }else{
                    cities += "<option value='"+data[i].id+"'>"+data[i].name+"</option>";

                }
            }
            $("#city_id").html(cities);
        });

    }

    
    function editProfile(){
        var id = "{{ auth()->user()->id }}";

        var view = '<form class="form-edit-add" role="form" action="{{ url("/student/update")."/".auth()->user()->id }}" method="POST" enctype="multipart/form-data" autocomplete="off" id="save_edit" name="save_edit">{{ method_field("PUT") }}{{ csrf_field() }}';

        view += '<div class="row"><div class="col-md-12"><div class="panel panel-bordered"><div class="panel-body">';

            view += '<div class="col-md-7">';

                view += '<div class="form-group">'+
                            '<label for="first_name">First Name:</label>'+
                            '<input class="form-control" id="first_name" name="first_name" placeholder="First Name" value="{{ auth()->user()->first_name }}" type="text">'+
                        '</div>';


                view += '<div class="form-group">'+
                            '<label for="last_name">Last Name:</label>'+
                            '<input class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="{{ auth()->user()->last_name }}" type="text">'+
                        '</div>';


                view += '<div class="form-group">'+
                            '<label for="middle_name">Middle Name:</label>'+
                            '<input class="form-control" id="middle_name" name="middle_name" placeholder="Middle Name" value="{{ auth()->user()->middle_name }}" type="text">'+
                        '</div>';


                view +='<div class="form-group">'+
                            '<label for="email">E-mail</label>'+
                            '<input class="form-control" id="email" name="email" placeholder="E-mail" value="{{ auth()->user()->email }}" type="email">'+
                        '</div>';

                var password_changed = "{{ auth()->user()->password_changed }}";
                
                var has_error = "";

                if(password_changed == "false"){
                    has_error = "has-error";
                }

                view += '<div class="form-group '+has_error+'">'+
                            '<label class="control-label" for="password">Password</label>'+
                            '<br>';
                            if(password_changed == "false") {
                                view += '<small style="color:red" id="pass-error">You must change your password before you can continue..</small>';
                            }else {
                                @if(Session::has('error'))
                                    view += '<small style="color:red" id="pass-error">{{ Session::get("error") }}</small>';
                                    @php
                                        Session::forget('error');
                                    @endphp
                                @else 
                                view += '<small id="pass-error">Leave empty to keep the same</small>';
                                @endif
                            }
                            view += '<input class="form-control" id="password" name="password" value="" autocomplete="new-password" type="password" onkeyup="passwordStrengthCheck(this.value);">'+
                            '<div id="change_pass_profile_color" class="col-md-12" style="height:5px;"></div>'+
                        '</div>';

                view += '<div class="form-group">'+
                            '<label for="qq">QQ Number:</label>'+
                            '<input class="form-control" id="qq" name="qq" placeholder="QQ Number" value="{{ auth()->user()->qq }}" type="text">'+
                        '</div>';


                view += '<div class="form-group">'+
                        '<label for="wechat">WeChat:</label>'+
                        '<small id="wechat_error"></small>'+
                        '<input class="form-control" id="wechat" name="wechat" placeholder="WeChat ID" value="{{ auth()->user()->wechat }}" type="text">'+
                        '</div>';


                view += '<div class="form-group">'+
                        '<label for="gender">Gender:</label><br>'+
                        '<label><input name="gender" value="male" checked="" type="radio"> Male</label><br>';
                        if("{{ auth()->user()->gender }}" == "female"){
                            view += '<label><input name="gender" value="female" type="radio" checked=""> Female</label><br>';
                        }else{
                            view += '<label><input name="gender" value="female" type="radio"> Female</label><br>';

                        }
                        view += '</div>';

                view += '<div class="form-group">'+
                        '<label for="address">Address:</label>'+
                        '<input name="address" id="address" class="form-control" value="{{ auth()->user()->address }}" type="text">'+
                        '</div>';

                view += '<div class="form-group">'+
                        '<label>City: </label>'+
                        '<select name="city_id" id="city_id" class="form-control">'+
                        '<option value="">None</option>'+
                        '</select>'+
                        '</div>';

            view += '</div>';

            view += '<div class="col-md-5">';

                view += '<div class="form-group">'+
                            '<img src="{{ url("storage")."/".auth()->user()->avatar }}" style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">'+
                            '<input data-name="avatar" name="avatar" id="avatar" type="file">'+
                        '</div>';

            view += '</div>';

        view += '</div></div></div></div>';

        view+= '</form>';

        countryChanged();

        var dialog = bootbox.dialog({
            closeButton: false,
            title: "Edit Profile",
            message: view,
            size: "large",
            buttons: {
                cancelBooking: {
                    label: "Save",
                    className: "btn-info",
                    callback: function() {
                        var complete = 0;
                        // check we chat

                        var fname = $("#first_name").val();
                        if(fname == "") {
                            $("#first_name").parent("div").addClass("has-error");
                        }else {
                            $("#first_name").parent("div").removeClass("has-error");
                            complete++;              
                        }

                        var lname = $("#last_name").val();
                        if(lname == "") {
                            $("#last_name").parent("div").addClass("has-error");
                        }else {
                            $("#last_name").parent("div").removeClass("has-error");
                            complete++;
                        }

                        var qq = $("#qq").val();
                        var wechat = $("#wechat").val();
                        if(qq == "") {
                            // check we chat
                            if(wechat == "") {
                                $("#wechat").parent("div").addClass("has-error");
                                $("#qq").parent("div").addClass("has-error");
                                $("#wechat_error").html("Please add a QQ Number or WeChat ID");
                                $("#qq_error").html("Please add a WeChat ID or QQ Number");
                            }else{
                                $("#wechat").parent("div").removeClass("has-error");
                                $("#qq").parent("div").removeClass("has-error");
                                $("#wechat_error").html("");
                                $("#qq_error").html("");
                                complete++;
                            }
                        }else{
                            complete++;
                        }

                        var email = $("#email").val();
                        if(email == "") {
                            $("#email").parent("div").addClass("has-error");
                        }else {
                            $("#email").parent("div").removeClass("has-error");      
                            complete++;
                        }


                        if(complete == 4) {
                            $("#save_edit").submit();
                        }else{
                            bootbox.alert("Please add QQ number or WeChat ID");
                        }

                        return false;
                    }
                },
                ok: {
                    label: "Close",
                    className: "btn-danger"
                }
            }
        });

    }


function scorePassword(pass) {
    var score = 0;
    if (!pass)
        return score;

    // award every unique letter until 5 repetitions
    var letters = new Object();
    for (var i=0; i<pass.length; i++) {
        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
        score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up
    var variations = {
        digits: /\d/.test(pass),
        lower: /[a-z]/.test(pass),
        upper: /[A-Z]/.test(pass),
        nonWords: /\W/.test(pass),
    }

    variationCount = 0;
    for (var check in variations) {
        variationCount += (variations[check] == true) ? 1 : 0;
    }
    score += (variationCount - 1) * 10;

    return parseInt(score);
}

function passwordStrengthCheck(pass) {

    var errors = "";
    

    if(pass.length == 0) {
        $("#change_pass_profile_color").css("background-color","");
        $("#change_pass_color").css("background-color","");
        return "";
    }
    if(pass.length < 8) {
        errors += " minimum of 8 characters";
        $("#pass-error").html(errors);
        $("#change_pass_profile_color").css("background-color","red");
        $("#change_pass_color").css("background-color","red");
        error = "";
        return "weak";
    }

    var score = scorePassword(pass);
    if (score > 80) {
        $("#change_pass_profile_color").css("background-color","green");
        $("#change_pass_color").css("background-color","green");

        return "strong";        
    }
    if (score > 60) {
        $("#change_pass_profile_color").css("background-color","yellow");
        $("#change_pass_color").css("background-color","yellow");
        return "good";
    }
    if (score >= 30) {
        $("#change_pass_profile_color").css("background-color","red");
        $("#change_pass_color").css("background-color","red");
        return "weak";
    }

    return "";
}



    @if( auth()->user()->password_changed == "false" || auth()->user()->password_changed == "0" )
        editProfile();
    @endif

</script>




@stop
