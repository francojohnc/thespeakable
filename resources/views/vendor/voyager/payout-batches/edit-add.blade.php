@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')

    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}
                        <input type="hidden" name="status" id="status" value="confirmed">
                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $options = json_decode($row->details);
                                    $display_options = isset($options->display) ? $options->display : NULL;
                                @endphp
                                @if ($options && isset($options->legend) && isset($options->legend->text))
                                    <legend class="text-{{$options->legend->align or 'center'}}" style="background-color: {{$options->legend->bgcolor or '#f0f0f0'}};padding: 5px;">{{$options->legend->text}}</legend>
                                @endif
                                @if ($options && isset($options->formfields_custom))
                                    @include('voyager::formfields.custom.' . $options->formfields_custom)
                                @else
                                    <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                        {{ $row->slugify }}
                                        <label for="name">{{ $row->display_name }}</label>
                                        @include('voyager::multilingual.input-hidden-bread-edit-add')
                                        @if($row->type == 'relationship')
                                            @include('voyager::formfields.relationship')
                                        @else
                                            {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                        @endif

                                        @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                            {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                        @endforeach
                                    </div>
                                @endif
                            @endforeach

                            @php
                                $payouts = App\Payout::where("batch_id", $dataTypeContent->id)->get()
                            @endphp
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Cut-off Date</th>
                                        <th>Teacher Name</th>
                                        <th>Bank Account Name</th>
                                        <th>Bank Account Number</th>
                                        <th>Status</th>
                                        <th>Total Fee</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($payouts as $payout)
                                        @php
                                            $start = new \DateTime($payout->cutoff_start, new \DateTimeZone("UTC"));
                                            $end = new \DateTime($payout->cutoff_end, new \DateTimeZone("UTC"));
                                            $count = 1;
                                        @endphp
                                        <tr>
                                            <td>{{ $start->format("M d") }} - {{ $end->format("d, Y") }}</td>
                                            <td>{{ $payout->teacher_name }}</td>
                                            <td>{{ $payout->bank_name }}</td>
                                            <td>{{ $payout->bank_account_number }}</td>
                                            <td id="tdStatus{{$payout->id}}">{{ $payout->status }}</td>
                                            <td>{{ $payout->total_fee }}</td>
                                            <td>
                                                @if($payout->status == "for payment")
                                                <a href="javascript:;" class="btn btn-primary" onclick="deposit('{{ $payout->id }}')" id="btnDeposit{{ $payout->id }}">Deposited</a>
                                                <a href="javascript:;" class="btn btn-danger" onclick="unpaid('{{ $payout->id }}')" id="btnUnpaid{{ $payout->id }}">Unpaid</a>
                                                @else 
                                                    @php
                                                        $count++;
                                                    @endphp
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save" id="btnSave" style="display:none">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
<script src="{{ asset('vendor/bootbox/bootbox.min.js') }}"></script>

    <script>
        var params = {}
        var $image
        var clicked = "{{ $count ? $count : 0 }}";
        var total_payouts = "{{ $payouts->count() ? $payouts->count():0 }}";
        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });



        function deposit(id) {

            $("#btnDeposit"+id).hide();
            $("#btnUnpaid"+id).hide();
            $("#tdStatus"+id).html("deposited");
            //bootbox.alert(name+ " "+ id);
            
            var url = "{{ url('/cutoff/deposited/') }}/"+id;

            //console.log(url);

            $.ajax({
              type: "GET",
              url: url
            }).done(function(data) {
                if(data == "success"){
                    clicked++;
                }else {
                    $("#btnDeposit"+id).show();
                    $("#btnUnpaid"+id).show();
                    clicked--;
                    $("#tdStatus"+id).html("for payment");
                }
                if(clicked == total_payouts){
                    $("#btnSave").show();
                }
            });
            

        }
        function unpaid(id) {

            $("#btnDeposit"+id).hide();
            $("#btnUnpaid"+id).hide();
            $("#tdStatus"+id).html("unpaid");
            var url = "{{ url('/cutoff/unpaid/') }}/"+id;

            $.ajax({
              type: "GET",
              url: url
            }).done(function(data) {
                if(data == "success"){
                    clicked++;
                }else {
                    $("#btnDeposit"+id).show();
                    $("#btnUnpaid"+id).show();
                    clicked--;
                    $("#tdStatus"+id).html("for payment");
                }
                if(clicked == total_payouts){
                    $("#btnSave").show();
                }
            });
        }

        function check(){
            if(clicked == total_payouts){
                $("#btnSave").show();
            }
        }

        check();

    </script>
@stop
