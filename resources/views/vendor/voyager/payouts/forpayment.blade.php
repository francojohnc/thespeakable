@php
  $payouts = App\Payout::where("status","for payment")->get();
  $now =new \DateTime("now", new \DateTimeZone('Asia/Manila'));

@endphp
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Speakable Me | Payouts</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/Ionicons/css/ionicons.min.css') }}">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
    body{
      -webkit-print-color-adjust:exact;
    }
  </style>
</head>
<body onload="window.print()">

<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
    <!-- title row -->
    <div class="row">
      <div class="col-xs-12">
        <h2 class="page-header">
          <i class="fa fa-globe"></i> Speakable Me.
          <small class="pull-right">{!! $now->format("M d, Y") !!}</small>
        </h2>
      </div>
      <!-- /.col -->
    </div>
    <!-- info row -->
    <!-- /.row -->

    <!-- Table row -->
    <div class="row">
      <div class="col-xs-12 table-responsive">
        <table class="table table-striped">
          <thead>
          <tr>
            <th>Name</th>
            <th>Bank Name</th>
            <th>Bank Account Number</th>
            <th>Bank Account Name</th>
            <th>Total Fee</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            @foreach($payouts as $payout)
            <td>{{ $payout->teacher_name }}</td>
            <td>{{ $payout->bank_name }}</td>
            <td> {{ $payout->bank_account_number }}</td>
            <td>{{ $payout->bank_account_name }}</td>
            <td>PHP {{ number_format($payout->total_fee,2) }}</td>
          </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>

</body>
</html>
