<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" @if (config('voyager.multilingual.rtl')) dir="rtl" @endif>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="robots" content="none" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="admin login">
    <title>Login - {{ Voyager::setting("admin.title") }}</title>
    <link rel="stylesheet" href="{{ voyager_asset('css/app.css') }}">
    @if (config('voyager.multilingual.rtl'))
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-rtl.css">
        <link rel="stylesheet" href="{{ voyager_asset('css/rtl.css') }}">
    @endif
    <style>
        body {
            /*background-image:url("");*/
            background-color: {{ Voyager::setting("admin.bg_color", "#FFFFFF" ) }};
        }
        body.login .login-sidebar {
            border-top:5px solid {{ config('voyager.primary_color','#22A7F0') }};
        }
        @media (max-width: 767px) {
            body.login .login-sidebar {
                border-top:0px !important;
                border-left:5px solid {{ config('voyager.primary_color','#22A7F0') }};
            }
        }
        body.login .form-group-default.focused{
            border-color:{{ config('voyager.primary_color','#22A7F0') }};
        }
        .login-button, .bar:before, .bar:after{
            background:{{ config('voyager.primary_color','#22A7F0') }};
        }
    </style>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
</head>
<body class="login">
<div class="container-fluid">
    <div class="row" style="background-image:url('{{ asset('storage/speakable_logo.jpg') }}')">
 
        <div class="faded-bg animated"></div>
        <div class="hidden-xs col-sm-7 col-md-8">
            <div class="clearfix">
                <div class="col-sm-12 col-md-10 col-md-offset-2">
                    <div class="logo-title-container">
                        <?php $admin_logo_img = Voyager::setting('admin.icon_image', ''); ?>
                        @if($admin_logo_img == '')
                        <img class="img-responsive pull-left flip logo hidden-xs animated fadeIn" src="{{ voyager_asset('images/logo-icon-light.png') }}" alt="Logo Icon">
                        @else
                        <img class="img-responsive pull-left flip logo hidden-xs animated fadeIn" src="{{ Voyager::image($admin_logo_img) }}" alt="Logo Icon">
                        @endif
                        <div class="copy animated fadeIn">
                            <h1>{{ Voyager::setting('admin.title', 'Voyager') }}</h1>
                            <p>{{ Voyager::setting('admin.description', __('voyager::login.welcome')) }}</p>
                        </div>
                    </div> <!-- .logo-title-container -->
                </div>
            </div>

        </div>

        <div class="col-xs-12 col-sm-5 col-md-4 login-sidebar">
            <div class="login-container">
            <div class="form-group col-sm-offset-8">
                <select name="locale" id="locale" class="form-control" style="border: 2px solid #f2efef">
                    <option value="en" selected>English</option>
                    <option value="zh-cn">Chinese</option>
                </select>
            </div>
                <div id="login">
                <p id="signin-below">{{ __('voyager::login.signin_below') }}</p>

                <form action="{{ route('voyager.login') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group form-group-default" id="emailGroup">
                        <label id="lbl-email">{{ __('voyager::generic.email') }}</label>
                        <div class="controls">
                            <input type="text" name="email" id="email" value="{{ old('email') }}" placeholder="{{ __('voyager::generic.email') }}" class="form-control" required>
                         </div>
                    </div>

                    <div class="form-group form-group-default" id="passwordGroup">
                        <label id="lbl-password">{{ __('voyager::generic.password') }}</label>
                        <div class="controls">
                            <input type="password" name="password" placeholder="{{ __('voyager::generic.password') }}" class="form-control" required>
                        </div>
                        
                    </div>

                    <input type="hidden" id="lang-login" name="lang" value="en">
                    <button type="submit" class="btn btn-block login-button">
                        <span class="signingin hidden"><span class="voyager-refresh"></span> <span id="span-loggingin">{{ __('voyager::login.loggingin') }}...</span></span>
                        <span class="signin" id="span-login">{{ __('voyager::generic.login') }}</span>
                    </button>

                    <a href="javascript:;" id="a-forgot" onclick="forgotPassword()" class="btn btn-default pull-right">Forgot Password</a>


              </form>
              </div>

              <div id="forgot-password" style="display:none">
                 <p id="p-forgot">Forgot Password</p>
                <form action="{{ url('forgot') }}" method="POST">
                    {{ csrf_field() }}
                    <div class="form-group form-group-default" id="emailGroup">
                        <label id="lbl-email-address">Email Address:</label>
                        <div class="controls">
                            <input type="text" name="email" id="email" value="" placeholder="{{ __('voyager::generic.email') }}" class="form-control" required>
                         </div>
                    </div>

                    <button type="submit" class="btn btn-block login-button">
                        <span class="signingin hidden"><span class="voyager-refresh"></span><span id="span-sending-email">Sending Email</span></span>
                        <span class="signin" id="span-retrieve">Retrieve Password</span>
                    </button>

                    <a href="javascript:;" onclick="login()" id="a-login" class="btn btn-default pull-right">Login</a>


                </form>


              </div>

              <div style="clear:both"></div>

              @if(!$errors->isEmpty())
              <div class="alert alert-red">
                <ul class="list-unstyled">
                    @foreach($errors->all() as $err)
                    <li>{{ $err }}</li>
                    @endforeach
                </ul>
              </div>
              @endif

            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            @if (session('forgot_error'))
                <div class="alert alert-red">
                    {{ session('forgot_error') }}
                </div>
            @endif


            </div> <!-- .login-container -->

        </div> <!-- .login-sidebar -->
    </div> <!-- .row -->
</div> <!-- .container-fluid -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script>
    var btn = document.querySelector('button[type="submit"]');
    var form = document.forms[0];
    var email = document.querySelector('[name="email"]');
    var password = document.querySelector('[name="password"]');
    btn.addEventListener('click', function(ev){
        if (form.checkValidity()) {
            btn.querySelector('.signingin').className = 'signingin';
            btn.querySelector('.signin').className = 'signin hidden';
        } else {
            ev.preventDefault();
        }
    });
    email.focus();
    document.getElementById('emailGroup').classList.add("focused");

    // Focus events for email and password fields
    email.addEventListener('focusin', function(e){
        document.getElementById('emailGroup').classList.add("focused");
    });
    email.addEventListener('focusout', function(e){
       document.getElementById('emailGroup').classList.remove("focused");
    });

    password.addEventListener('focusin', function(e){
        document.getElementById('passwordGroup').classList.add("focused");
    });
    password.addEventListener('focusout', function(e){
       document.getElementById('passwordGroup').classList.remove("focused");
    });

    function forgotPassword(){
        $("#login").hide("slow");
        $("#forgot-password").show("slow");
    }

    function login(){
        $("#login").show("slow");
        $("#forgot-password").hide("slow");
    }

    $(document).ready(function(){
        
        <?php
            session_start();
            if( isset($_SESSION['lang']) && $_SESSION['lang'] == "zh-cn" ){
                echo '$("#locale").val("zh-cn");';
                echo 'zhCn();';
            }else{
                echo '$("#locale").val("en");';
                echo 'en();';                
            }

        ?>

        
        $("#locale").change(function(){
            var lang = $(this).val();
            if(lang == "zh-cn") {
                zhCn();
            }
            else if(lang == "en") {
                en();
            }
            
        });

        function zhCn(){
                $("#signin-below").html("请您登陆: ");
                $("#lbl-email").html("电子邮箱");
                $("#lbl-password").html("密码");
                $("#span-login").html("登陆");
                $("#span-loggingin").html("正在登陆");
                $("#a-forgot").html("忘记密码");

                $("#p-forgot").html("忘记密码:");
                $("#lbl-email-address").html("电子邮箱");
                $("#span-sending-email").html("正在发送邮件");
                $("#span-retrieve").html("取回密码");
                $("#a-login").html("登陆");
                $("#lang-login").val("zh-cn");
        }
        function en(){
                $("#signin-below").html("Sign In Below: ");
                $("#lbl-email").html("Email");
                $("#lbl-password").html("Password");
                $("#span-login").html("Login");
                $("#span-loggingin").html("Logging In");
                $("#a-forgot").html("Forgot Password");

                $("#p-forgot").html("Forgot Password:");
                $("#lbl-email-address").html("Email Address");
                $("#span-sending-email").html("Sending Email");
                $("#span-retrieve").html("Retrieve Password");
                $("#a-login").html("Login");
                $("#lang-login").val("en");
        }
    });

</script>
</body>
</html>
