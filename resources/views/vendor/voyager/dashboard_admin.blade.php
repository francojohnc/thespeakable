<div class="panel-body">
    @php
      $booked = \App\Http\Controllers\ChartController::chartBooked();
      $available = \App\Http\Controllers\ChartController::chartAvailable();
    @endphp

  <!-- /.col -->
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box bg-green">
      <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Gross Income</span>
        <span class="info-box-number">{{ \App\Http\Controllers\DashboardController::soldPackages() }} </span>

        <div class="progress">
          <div class="progress-bar" style="width: 70%"></div>
        </div>
            @php
              $month = new \DateTime( "now", new \DateTimeZone(auth()->user()->timezone) ) 
            @endphp
            <span class="progress-description">Month of {{ $month->format("F") }}</span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>

  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box" style="color:#ffffff;background-color: #499fbc">
      <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Total Penalty</span>
        <span class="info-box-number">{{ \App\Http\Controllers\DashboardController::activeStudents() }}</span>

        <div class="progress">
          <div class="progress-bar" style="width: 70%"></div>
        </div>
            <span class="progress-description">
              Inactive: {{ \App\Http\Controllers\DashboardController::inactiveStudents() }}
            </span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>

  <!-- /.col -->
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box bg-yellow">
      <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Total Class Fee</span>
        <span class="info-box-number"><a href="{{ url('/orders') }}" style="color:#fff">{{ \App\Http\Controllers\DashboardController::awaitingPayment() }}</a></span>

        <div class="progress">
          <div class="progress-bar" style="width: 70%"></div>
        </div>
            <span class="progress-description">
              For Approval: {{ \App\Http\Controllers\DashboardController::forApproval() }}
            </span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-md-3 col-sm-6 col-xs-12">
    <div class="info-box" style="color:#ffffff;background-color: #20987a">
      <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">Net Income</span>
        <span class="info-box-number"><a href="javascript:;" style="color:#ffffff">{{ \App\Http\Controllers\DashboardController::adminRunningTotal('current') }}</a></span>

        <div class="progress">
          <div class="progress-bar" style="width: 70%"></div>
        </div>
            <span class="progress-description"> Month of {{ $month->format("F") }}
            </span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
                       
  <div class="col-md-12">
    <div id="cut-off-recap-booked"></div>
  </div>
  <div class="col-md-12">
    <div id="cut-off-recap-available"></div>
  </div>
</div>



<script src="{{ asset('vendor/highcharts/highcharts.js') }}"></script>
<script src="{{ asset('vendor/highcharts/modules/data.js') }}"></script>
<script src="{{ asset('vendor/highcharts/modules/drilldown.js') }}"></script>
<script>

// Create the chart
Highcharts.chart('cut-off-recap-booked', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Total Booked Classes For this Cut-off'
    },
    subtitle: {
        text: 'Click the columns to view additional information.'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total Number of Booked Classes'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:f}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> booked classes<br/>'
    },

    "series": [
        {
            "name": "Current Cut-off",
            "colorByPoint": true,
            "data":<?=\App\Http\Controllers\ChartController::chartBookedSeries($booked);?>
        }
    ],
    "drilldown": {
        "series": <?=\App\Http\Controllers\ChartController::chartBookedDrillTime($booked);?>
    }
});

// Create the chart
Highcharts.chart('cut-off-recap-available', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Total Open Slots For this Cut-off'
    },
    subtitle: {
        text: 'Click the columns to view additional information.'
    },
    xAxis: {
        type: 'category'
    },
    yAxis: {
        title: {
            text: 'Total Number of Opened Slots'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:f}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> opened slot<br/>'
    },

    "series": [
        {
            "name": "Current Cut-off",
            "colorByPoint": true,
            "data":<?=\App\Http\Controllers\ChartController::chartAvailableSeries($available);?>
        }
    ],
    "drilldown": {
        "series": <?=\App\Http\Controllers\ChartController::chartAvailableDrillTime($available);?>
    }
});
</script>