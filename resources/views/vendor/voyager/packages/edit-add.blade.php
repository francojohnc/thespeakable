@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                            class="form-edit-add"
                            action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                            method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <div class="panel-body">

                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <!-- Adding / Editing -->
                            @php
                                $dataTypeRows = $dataType->{(!is_null($dataTypeContent->getKey()) ? 'editRows' : 'addRows' )};
                            @endphp

                            @foreach($dataTypeRows as $row)
                                <!-- GET THE DISPLAY OPTIONS -->
                                @php
                                    $options = json_decode($row->details);
                                    $display_options = isset($options->display) ? $options->display : NULL;
                                @endphp
                                @if ($options && isset($options->legend) && isset($options->legend->text))
                                    <legend class="text-{{$options->legend->align or 'center'}}" style="background-color: {{$options->legend->bgcolor or '#f0f0f0'}};padding: 5px;">{{$options->legend->text}}</legend>
                                @endif
                                @if ($options && isset($options->formfields_custom))
                                    @include('voyager::formfields.custom.' . $options->formfields_custom)
                                @else

                                    @if( $row->field == "currency" )
                                    <div class="form-group col-md-12">
                                        <label for="currency">Currency</label>
                                        <select name="currency" id="currency" class="form-control">
                                            <option value="PHP">PHP (Philippine Peso)</option>
                                            <option value="USD" selected>USD (US Dollar)</option>
                                        </select>
                                    </div>
                                    @elseif($row->field == "school_id")
                                        @php
                                            $schools = \App\School::get(["id","name"]);
                                        @endphp
                                    <div class="form-group col-md-12">
                                        <label for="school_id">Schools</label>
                                        <select name="school_id" id="school_id" class="form-control">
                                            @foreach($schools as $school)
                                                @if($dataTypeContent->school_id == $school->id)                                                    
                                                <option value="{{ $school->id }}" selected>{{$school->name}}</option>
                                                @else
                                                <option value="{{ $school->id }}">{{$school->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    @elseif( $row->field == "country_id" )
                                        @php
                                            $countries = \App\Country::get(["id","name"]);
                                        @endphp
                                    <div class="form-group col-md-12">
                                        <label for="country">Country</label>
                                        
                                        <select name="country_id" id="country_id" class="form-control">
                                            @foreach($countries as $country)
                                                @if($dataTypeContent->country_id == $country->id)                                                    
                                                <option value="{{ $country->id }}" selected>{{$country->name}}</option>
                                                @else
                                                <option value="{{ $country->id }}">{{$country->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    @elseif( $row->field == "unit_price" )
                                    <div class="form-group col-md-12">
                                        <label for="unit_price">Unit Price</label>
                                        <input type="text" value="{{ $dataTypeContent->unit_price }}" name="unit_price" id="unit_price" class="form-control" placeholder="15.375">
                                    </div>
                                    @elseif( $row->field == "number_of_classes" )
                                    <div class="form-group col-md-12">
                                        <label for="number_of_classes">Number Of Classes</label>
                                        <input type="text" value="{{ $dataTypeContent->number_of_classes }}" name="number_of_classes" id="number_of_classes" class="form-control">
                                    </div>
                                    @elseif( $row->field == "base_price" )
                                    <div class="form-group col-md-12">
                                        <label for="base_price" id="label_base_price">Base Price</label>
                                        <input type="text" name="base_price" id="base_price" class="form-control">
                                    </div>
                                    @else 
                                    <div class="form-group @if($row->type == 'hidden') hidden @endif col-md-{{ $display_options->width or 12 }}" @if(isset($display_options->id)){{ "id=$display_options->id" }}@endif>
                                        {{ $row->slugify }}
                                        <label for="name">{{ $row->display_name }}</label>
                                        @include('voyager::multilingual.input-hidden-bread-edit-add')
                                        @if($row->type == 'relationship')
                                            @include('voyager::formfields.relationship')
                                        @else
                                            {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                                        @endif

                                        @foreach (app('voyager')->afterFormFields($row, $dataType, $dataTypeContent) as $after)
                                            {!! $after->handle($row, $dataType, $dataTypeContent) !!}
                                        @endforeach
                                    </div>
                                    @endif()
                                @endif
                            @endforeach
                                <div>Note:Base Price is the total of unit price * number of classes, This package is for the country you select, and will be priced using the currency you selected.</div>
                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file"
                                 onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
                        {{ csrf_field() }}
                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Delete File Modal -->
@stop

@section('javascript')
    <script>
        var params = {}
        var $image

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#unit_price").blur(function(){
                var noc = $("#number_of_classes").val();
                //console.log(noc);
                var unit = $("#unit_price").val();
                //console.log(unit);
                var base_price = (parseFloat(noc) * parseFloat(unit));
                //console.log(base_price);
                $("#base_price").val( base_price );
                $("#label_base_price").html("Base Price in "+$("#currency").val());
            });
        });
    </script>
@stop
