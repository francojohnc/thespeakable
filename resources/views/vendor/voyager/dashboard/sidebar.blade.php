
<div class="side-menu sidebar-inverse">
    <nav class="navbar navbar-default" role="navigation">
        <div class="side-menu-container">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ route('voyager.dashboard') }}">
                    <div class="logo-icon-container">
                        <?php $admin_logo_img = Voyager::setting('admin.icon_image', ''); ?>
                        @if($admin_logo_img == '')
                            <img src="{{ voyager_asset('images/logo-icon-light.png') }}" alt="Logo Icon">
                        @else
                            <img src="{{ Voyager::image($admin_logo_img) }}" alt="Logo Icon">
                        @endif
                    </div>
                    <div class="title">{{Voyager::setting('admin.title', 'VOYAGER')}}</div>
                </a>
            </div><!-- .navbar-header -->

            <div class="panel widget center bgimage"
                 style="background-image:url({{ Voyager::image( Voyager::setting('admin.bg_image'), voyager_asset('images/bg.jpg') ) }}); background-size: cover; background-position: 0px;">
                <div class="dimmer"></div>
                <div class="panel-content">
                    <img src="{{ $user_avatar }}" class="avatar" alt="{{ Auth::user()->name }} avatar">
                    @if( auth()->user()->hasRole('teacher') )
                        <h4>{{ ucwords(auth()->user()->nick) }}</h4>
                    @else 
                        <h4>{{ ucwords(auth()->user()->first_name) }}</h4>
                    @endif
                    <p>{{ Auth::user()->email }}</p>

                    <a href="{{ route('voyager.profile') }}" class="btn btn-primary">{{ __('voyager::generic.profile') }}</a>
                    <div style="clear:both"></div>
                </div>
            </div>

        </div>
        @if( auth()->user()->hasRole('superadmin') )
            {!! menu('admin', 'admin_menu') !!}
        @elseif( auth()->user()->hasRole('admin') )
            {!! menu('admin', 'admin_menu') !!}
        @elseif( auth()->user()->hasRole('student') && auth()->user()->lang == "zh-cn" )
            {!! menu('student-zh-cn','admin_menu') !!}
        @elseif( auth()->user()->hasRole('student') )
            {!! menu('student','admin_menu') !!}
        @elseif( auth()->user()->hasRole('teacher') )
            {!! menu('teacher','admin_menu') !!}
        @elseif( auth()->user()->hasRole('client') )
            {!! menu('client','admin_menu') !!}
        @endif
    </nav>
</div>
