//initialize instance
var enjoyhint_instance = new EnjoyHint({});

//simple config.
//Only one step - highlighting(with description) "New" button
//hide EnjoyHint after a click on the button.
var enjoyhint_script_steps = [
    {
        "click #menu-schedule": 'To book a class, Click "Schedule" on the side bar menu.',
        shape : 'circle',
        showSkip: false
    },{
        "click #menu-book-classes": 'Then click "Book Classes" on the side bar menu.',
        shape : 'circle',
        showSkip: false        
    }
];

//set script config
enjoyhint_instance.set(enjoyhint_script_steps);

//run Enjoyhint script
enjoyhint_instance.run();