//initialize instance
var enjoyhint_instance = new EnjoyHint({});

//simple config.
//Only one step - highlighting(with description) "New" button
//hide EnjoyHint after a click on the button.
var enjoyhint_script_steps = [

    {
        "click #orders": 'Select a package to use.',
        //shape : 'circle',
        showSkip: false
    },
    {
        "click #when": 'To book a class, select your preferred date and time.',
        //shape : 'circle',
        showSkip: false
    },
    {
        "next #search": 'Click the "search" button to start searching for teachers',
        //shape : 'circle',
        showSkip: false
    },
    {
        "next .view-sched": 'Click "view schedule" button to show teacher schedule.',
        //shape : 'circle',
        showSkip: false
    },
    {
        "next .fc-content": 'To book a class click the time preferred.',
        //shape : 'circle',
        showSkip: true
    },
    {
        "next #save-booked-classes": 'Click "save" button to save your schedule.',
        //shape : 'circle',
        //showSkip: false
    }
];

//set script config
enjoyhint_instance.set(enjoyhint_script_steps);

//run Enjoyhint script
enjoyhint_instance.run();
